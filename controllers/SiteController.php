<?php

class SiteController extends CController
{
	public function actionIndex()
	{
		echo 'you\'re in the site controller <br>';
		echo 'You need to configure the modules that control the Nii framework<br/>';
		echo '<a href="'.NHtml::url('admin/modules/index').'">Module Admin</a><br>';
		echo '<a href="'.NHtml::url('user/account/logout').'">Logout</a>';
	}
}