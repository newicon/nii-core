<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InstallController
 *
 * @author robinwilliams
 */
class InstallController extends Controller {

	public $layout = '//layouts/install';

	public function actionIndex() {

		$this->redirect(array('install/step1'));
	}

	/**
	 * Action to install the database
	 */
	public function actionStep1() {

		$dbForm = new InstallDbForm();

		$this->performAjaxValidation($dbForm, 'installForm');

		// save db and install
		if (Yii::app()->request->getPost('InstallDbForm')) {

			$dbForm->attributes = $_POST['InstallDbForm'];

			if ($dbForm->validate()) {
				// install database tables and nii modules
				$dbForm->installDatabase();
				// save the config details to config file
				if ($dbForm->saveConfig()) {
                   $this->redirect(array('install/step2'));
                } else {
                     $this->render('create-config-file', array(
                        'config' => $dbForm->generateConfigPhp(),
                        'configFolder' => $dbForm->getConfigFile()));
                }
                Yii::app()->end();
			}
		}

        if (Yii::app()->isDbInstalled()) {
            if ($dbForm->configSaved()) {
                // we CAN connect to the database and a config file exists
                // should check if we have a user table?

                $this->redirect(array('install/step2'));
            } else {
                $this->render('create-config-file', array(
                        'config' => $dbForm->generateConfigPhp(),
                        'configFolder' => $dbForm->getConfigFile()));
            }
        } else {
            // we can't progress if we cant connect to db
            $this->render('step1', array('dbForm' => $dbForm));
        }
//        }catch(Exception $e){
//            echo 'oops';
//            $this->render('step1', array('dbForm' => $dbForm));
//        }
//        $this->render('step1', array('dbForm' => $dbForm));
        // database installed but the configuration could not be saved! show a DIY message
//		if ($dbInstalled && !$dbForm->configSaved()) {
//			$this->render('create-config-file', array('config' => $dbForm->generateConfigPhp(), 'configFolder' => $dbForm->getConfigFile()));
//        }
//        // render the install database form
//        else if ($dbInstalled == false && $dbForm->configSaved() == true) {
//			$this->render('step1', array('dbForm' => $dbForm));
//        }
//        else if ($dbInstalled && $dbForm->configSaved()) {
//            echo 'HELLO3'; exit;
//           // $this->redirect(array('install/step2'));
//        }

	}

	/**
	 * Action to install the first admin user.
	 */
	public function actionStep2() {

		$userForm = new InstallUserForm();
		$dbForm = new InstallForm();
		if (!$dbForm->isUserInstalled()) {
			Yii::app()->install();
		}

		$this->performAjaxValidation($userForm, 'installForm');

		// the database must be installed to do this step
		if(!$userForm->isDbInstalled($e)){
			if (YII_DEBUG){
				if($e instanceof Exception)
					Yii::app()->user->setFlash('debug', $e->getMessage());
				else
					Yii::app()->user->setFlash('debug', 'Database details have not been saved');
			}
			Yii::app()->user->setFlash('error', "Couldn't connect to database. Please check the details and try again!");
			$this->redirect(array('install/step1'));
		}

		// If a user has already been installed then you are not allowed access, unless you're a superadmin perhaps
		if($userForm->isUserInstalled()){
			// redirect you don't need to create a user, and you are no longer allowed to!
			$url = NHtml::url() ? NHtml::url() : '/';
			$this->redirect($url);
		}

		// save user details
		if (Yii::app()->request->getPost('InstallUserForm')) {
			$userForm->attributes = $_POST['InstallUserForm'];
			if ($userForm->validate()){
				$userForm->createAdminUser();
				$this->render('success');
				Yii::app()->end();
			}
		}


		$this->render('step2', array('userForm'=>$userForm));
	}

	/**
	 * Enables the user to download the config file if the config folder is not writable
	 * @param string $content the base64 encoed string of the config files content
	 */
	public function actionConfigFile($content) {

		Yii::app()->request->sendFile('local.php', base64_decode($content));
	}

}
