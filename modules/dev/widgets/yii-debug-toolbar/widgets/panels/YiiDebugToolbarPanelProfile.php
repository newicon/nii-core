<?php
/**
 * YiiDebugToolbarPanelSql class file.
 *
 * @author Sergey Malyshev <malyshev.php@gmail.com>
 */

/**
 * YiiDebugToolbarPanelSql class.
 *
 * Description of YiiDebugToolbarPanelSql
 *
 * @author Sergey Malyshev <malyshev.php@gmail.com>
 * @author Igor Golovanov <igor.golovanov@gmail.com>
 * @package YiiDebugToolbar
 * @since 1.1.7
 */
class YiiDebugToolbarPanelProfile extends YiiDebugToolbarPanelSql
{
	/**
     * Message count.
     * @var integer
     */
    private $_countMessages;

    /**
     * Logs.
     * @var array
     */
    private $_logs;

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return YiiDebug::t('Profile Logs');
    }
	
	/**
     * {@inheritdoc}
     */
    public function getMenuTitle()
    {
        return YiiDebug::t('Profile Logs');
    }

    /**
     * {@inheritdoc}
     */
    public function getMenuSubTitle()
    {
        return $this->countMessages;
    }
	
	public function getMenuImg()
	{
		return '<img width="20" height="28" alt="Database" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAcCAYAAABh2p9gAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAQRJREFUeNpi/P//PwM1ARMDlcGogZQDlpMnT7pxc3NbA9nhQKxOpL5rQLwJiPeBsI6Ozl+YBOOOHTv+AOllQNwtLS39F2owKYZ/gRq8G4i3ggxEToggWzvc3d2Pk+1lNL4fFAs6ODi8JzdS7mMRVyDVoAMHDsANdAPiOCC+jCQvQKqBQB/BDbwBxK5AHA3E/kB8nKJkA8TMQBwLxaBIKQbi70AvTADSBiSadwFXpCikpKQU8PDwkGTaly9fHFigkaKIJid4584dkiMFFI6jkTJII0WVmpHCAixZQEXWYhDeuXMnyLsVlEQKI45qFBQZ8eRECi4DBaAlDqle/8A48ip6gAADANdQY88Uc0oGAAAAAElFTkSuQmCC">';
	}
	
	public function getMenuStat()
	{
		return null;
	}
	
    /**
     * {@inheritdoc}
     */
    public function getSubTitle()
    {
		return null;
    }

    /**
     * Get count of messages.
     * @return integer
     */
    public function getCountMessages()
    {
        if (null === $this->_countMessages) {
            $this->_countMessages = count($this->logs);
        }
        return $this->_countMessages;
    }
	
	/**
     * Get logs, and store so we only do this once per call
     * @return array
     */
    public function getLogs()
    {
        if (null === $this->_logs) {
            $this->_logs = $this->filterLogs();
        }
        return $this->_logs;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->render('profile', array(
            'logs' => $this->processSummary($this->logs),
        ));
    }
	
	/**
	 * Takes each log and creates an array with appropriate columns for table display including calulating log time
	 * @param type $logs
	 * @return type
	 * @throws CException
	 */
	public function processLogs($logs)
	{
		$stack=array();
		foreach($logs as $log) {
			if($log[1]!==CLogger::LEVEL_PROFILE)
				continue;
			$message=$log[0];
			if(!strncasecmp($message,'begin:',6)) {
				$log[0]=substr($message,6);
				$stack[]=$log;
			}
			else if(!strncasecmp($message,'end:',4)) {
				$token=substr($message,4);
				if(($last=array_pop($stack))!==null && $last[0]===$token) {
					$delta=$log[3]-$last[3];
					if(!$this->groupByToken)
						$token=$log[2];
					if(isset($results[$token]))
						$results[$token]=$this->aggregateResult($results[$token],$delta);
					else
						$results[$token]=array($token,1,$delta,$delta,$delta);
				} else
					throw new CException(Yii::t('yii','CProfileLogRoute found a mismatching code block "{token}". Make sure the calls to Yii::beginProfile() and Yii::endProfile() be properly nested.',
						array('{token}'=>$token)));
			}
		}

		$now=microtime(true);
		while(($last=array_pop($stack))!==null) {
			$delta=$now-$last[3];
			$token=$this->groupByToken ? $last[0] : $last[2];
			if(isset($results[$token]))
				$results[$token]=$this->aggregateResult($results[$token],$delta);
			else
				$results[$token]=array($token,1,$delta,$delta,$delta);
		}

		$entries=array_values($results);
		$func=create_function('$a,$b','return $a[4]<$b[4]?1:0;');
		usort($entries,$func);

		$table   = array();
		$entries[] = array('count','total', 'average', 'min', 'max', 'log');
		$totalTime = 0;$noQs=0;
		foreach($entries as $index=>$entry) {
			$proc=CJavaScript::quote($entry[0]);
			$count=sprintf('%5d',$entry[1]);
			$min=sprintf('%0.5f',$entry[2]);
			$max=sprintf('%0.5f',$entry[3]);
			$total=sprintf('%0.5f',$entry[4]);
			//$average=sprintf('%0.5f',$entry[4]/$entry[1]);
			$average = 0;
			$entries[] = array($count, $total, $average, $min, $max, $proc);
		}
		
		return $entries;
	}
	
	/**
     * Format log entry
     *
     * @param array $entry
     * @return array
     */
    public function formatLogEntry(array $entry)
    {
        // extract query from the entry
        $queryString = $entry[0];
        $sqlStart = strpos($queryString, '(') + 1;
        $sqlEnd = strrpos($queryString , ')');
        $sqlLength = $sqlEnd - $sqlStart;
        
        $queryString = substr($queryString, $sqlStart, $sqlLength);

        return $entry;
    }

    /**
     * Get filter logs.
     * @return array
     */
    protected function filterLogs()
    {
        $logs = array();
        foreach ($this->owner->getLogs() as $entry) {            
            if (CLogger::LEVEL_PROFILE == $entry[1] && false === strpos($entry[2], 'system.db.CDbCommand')) {
                $logs[] = $entry;
            }
        }
        return $logs;
    }
}
