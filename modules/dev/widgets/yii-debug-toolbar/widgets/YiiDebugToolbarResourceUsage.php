<?php
/**
 * YiiDebugToolbarResourceUsage class file.
 *
 * @author Sergey Malyshev <malyshev@zfort.net>
 */

/**
 * YiiDebugToolbarResourceUsage represents an ...
 *
 * Description of YiiDebugToolbarResourceUsage
 *
 * @author Sergey Malyshev <malyshev@zfort.net>
 * @package
 * @since 1.1.7
 */
class YiiDebugToolbarResourceUsage extends CWidget
{
    public $title = 'Resource Usage';

    public $htmlOptions = array();

    private $_loadTime;

	/**
	 * get the load time in seconds
	 * @return int seconds
	 */
    public function getLoadTime()
    {
        if (null === $this->_loadTime)
        {
            $this->_loadTime = $this->owner->owner->getLoadTime();
        }
        return $this->_loadTime;
    }

    public function getRequestLoadTime()
    {
        return ($this->owner->owner->getEndTime() - $_SERVER['REQUEST_TIME']);
    }

	public function mkArr($label, $value) 
	{
		return array('label'=>$label,'value'=>$value);
	}
	
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $resources =  array(
			'pageLoadTime'=>$this->mkArr(YiiDebug::t('Page Load Time'), sprintf('%0.6F s.',$this->getLoadTime())),
			'elapsedTime'=>$this->mkArr(YiiDebug::t('Elapsed Time'), sprintf('%0.6F s.',$this->getRequestLoadTime())),
			'memoryUsage'=>$this->mkArr(YiiDebug::t('Memory Usage'), number_format(Yii::getLogger()->getMemoryUsage()/1024) . ' KB'),
			'memoryPeakUsage'=>$this->mkArr(YiiDebug::t('Memory Peak'), function_exists('memory_get_peak_usage') ? number_format(memory_get_peak_usage()/1024) . ' KB' : 'N/A')
		);

        if (function_exists('mb_strlen') && isset($_SESSION))
        {
            try {
                $resources['sessionSize'] = $this->mkArr(YiiDebug::t('Session Size'), sprintf('%0.3F KB' ,mb_strlen(serialize($_SESSION))/1024));
            } catch (Exception $e) {
                $resources['sessionSize'] = $this->mkArr(YiiDebug::t('Session Size'), 'Unknown');
            }
        }
		
		$pageLoadTimeMs = $this->getLoadTime()*1000;
		$resources['elapsedTimeMs'] =  number_format($this->getRequestLoadTime()*1000);
		$resources['pageLoadTimeMs'] =  number_format($pageLoadTimeMs);
		$resources['memoryUsageMb'] = number_format(Yii::getLogger()->getMemoryUsage()/1024/1024) . ' MB';
		
		if($pageLoadTimeMs < 300)
			$resources['pageLoadTimeStatusClass'] = 'label-success';
		elseif($pageLoadTimeMs > 300 && $pageLoadTimeMs < 500)
			$resources['pageLoadTimeStatusClass'] = 'label-warning';
		elseif($pageLoadTimeMs > 500)
			$resources['pageLoadTimeStatusClass'] = 'label-important';
		
		$this->render('resource', $resources);
    }
}