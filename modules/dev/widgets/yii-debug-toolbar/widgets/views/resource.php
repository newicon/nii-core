<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<style type="text/css">
	.nii-toolbarreset {background-color: #f7f7f7;text-align: left;background-image: -moz-linear-gradient(-90deg, #e4e4e4, #ffffff);background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#e4e4e4), to(#ffffff));border-top: 1px solid #bbb;}
	.nii-toolbarreset span, yii-debug-toolbar-link.nii-toolbarreset, .nii-toolbar-info {font-size: 11px;}
	.nii-toolbar-block {font: 11px Verdana, Arial, sans-serif;color: #2f2f2f;}
	.nii-toolbarreset img {width: auto;display: inline;}
	.nii-toolbarreset .hide-button {display: block;position: absolute;top: 12px;right: 10px;width: 15px;height: 15px;cursor: pointer;background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAMAAAAMCGV4AAAAllBMVEXDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PExMTPz8/Q0NDR0dHT09Pb29vc3Nzf39/h4eHi4uLj4+P6+vr7+/v8/Pz9/f3///+Nh2QuAAAAIXRSTlMABgwPGBswMzk8QktRV4SKjZOWmaKlq7TAxszb3urt+fy1vNEpAAAAiklEQVQIHUXBBxKCQBREwRFzDqjoGh+C2YV//8u5Sll2S0E/dof1tKdKM6GyqCto7PjZRJIS/mbSELgXOSd/BzpKIH1ZefVWpDDTHsi8mZVnwImPi5ndCLbaAZk3M58Bay0h9VbeSvMpjDUAHj4jL55AW1rxN5fU2PLjIgVRzNdxVFOlGzvnJi0Fb1XNGBHA9uQOAAAAAElFTkSuQmCC');}
	.nii-toolbar-block {white-space: nowrap;color: #2f2f2f;display: block;height:38px;border-right: 1px solid #e4e4e4;padding: 0;float: left;cursor: default;}
	.nii-toolbar-block span {display: inline-block;font-size: 11px;}
	.nii-toolbar-block .nii-toolbar-info-piece {padding-bottom: 5px;	}
	.nii-toolbar-block .nii-toolbar-info-piece:last-child {	padding-bottom: 0;}
	.nii-toolbar-block .nii-toolbar-info-piece a,.nii-toolbar-block .nii-toolbar-info-piece abbr {color: #2f2f2f;}
	.nii-toolbar-block .nii-toolbar-info-piece b {display: inline-block;	width: 110px;}
	.nii-toolbar-block .nii-toolbar-info-with-next-pointer:after {content: ' :: ';color: #999;}
	.nii-toolbar-block .nii-toolbar-info-with-delimiter {border-right: 1px solid #999;padding-right: 5px;margin-right: 5px;}
	.nii-toolbar-block .nii-toolbar-info {display: none;position: absolute;background-color: #fff;border: 1px solid #bbb;padding: 10px 8px;margin-left: -1px;bottom: 38px;border-bottom-width: 0;}
	.nii-toolbar-block .nii-toolbar-info:empty {visibility: hidden;}
	.nii-toolbar-block .nii-toolbar-status {display: inline-block;color: #fff;background-color: #666;padding: 3px 6px;border-radius: 3px;margin-bottom: 2px;vertical-align: middle;min-width: 6px;min-height: 13px;}
	.nii-toolbar-block .nii-toolbar-status abbr {color: #fff;border-bottom: 1px dotted black;}
	.nii-toolbar-block .nii-toolbar-status-green {background-color: #759e1a;}
	.nii-toolbar-block .nii-toolbar-status-red {background-color: #a33;}
	.nii-toolbar-block .nii-toolbar-status-yellow {background-color: #ffcc00;color: #000;}
	.nii-toolbar-block .nii-toolbar-status-black {background-color: #000;}
	.nii-toolbar-block .nii-toolbar-icon {display: block;}
	.nii-toolbar-block .nii-toolbar-icon > a,
	.nii-toolbar-block .nii-toolbar-icon > span {display: block;text-decoration: none;margin: 0;padding: 5px 8px;min-width: 20px;text-align: center;vertical-align: middle;	}
	.nii-toolbar-block .nii-toolbar-icon > a,
	.nii-toolbar-block .nii-toolbar-icon > a:link.nii-toolbar-block .nii-toolbar-icon > a:hover {color: black !important;}
	.nii-toolbar-block .nii-toolbar-icon img {border-width: 0;vertical-align: middle;}
	.nii-toolbar-block .nii-toolbar-icon img + span {margin-left: 5px;margin-top: 2px;}
	.nii-toolbar-block .nii-toolbar-icon .nii-toolbar-status {border-radius: 12px;border-bottom-left-radius: 0px;margin-top: 0;}
	.nii-toolbar-block .nii-toolbar-info-method {border-bottom: 1px dashed #ccc;cursor: help;}
	.nii-toolbar-block .nii-toolbar-info-method[onclick=""] {border-bottom: none;cursor: inherit;}
	.nii-toolbar-info-php {}
	.nii-toolbar-info-php-ext {}
	.nii-toolbar-info-php-ext .nii-toolbar-status {margin-left: 2px;}
	.nii-toolbar-info-php-ext .nii-toolbar-status:first-child {margin-left: 0;}
	.nii-toolbar-block a .nii-toolbar-info-piece-additional,
	.nii-toolbar-block a .nii-toolbar-info-piece-additional-detail {display: inline-block;}
	.nii-toolbar-block a .nii-toolbar-info-piece-additional:empty,
	.nii-toolbar-block a .nii-toolbar-info-piece-additional-detail:empty {display: none;	}
	.nii-toolbarreset:hover {box-shadow: rgba(0, 0, 0, 0.3) 0 0 50px;}
	.nii-toolbar-block:hover {box-shadow: rgba(0, 0, 0, 0.3) 0 0 5px;}
	.nii-toolbar-block:hover .nii-toolbar-icon {background-color: #fff;}
	.nii-toolbar-block:hover .nii-toolbar-info {display: block;}

	/***** Media query *****/
	@media screen and (max-width: 779px) {
		.nii-toolbar-block .nii-toolbar-icon > * > :first-child ~ * {
			display: none;
		}

		.nii-toolbar-block .nii-toolbar-icon > * > .nii-toolbar-info-piece-additional,
		.nii-toolbar-block .nii-toolbar-icon > * > .nii-toolbar-info-piece-additional-detail {
			display: none !important;
		}
	}

	@media screen and (min-width: 880px) {
		.nii-toolbar-block .nii-toolbar-icon a[href$="config"] .nii-toolbar-info-piece-additional {
			display: inline-block;
		}
	}

	@media screen and (min-width: 980px) {
		.nii-toolbar-block .nii-toolbar-icon a[href$="security"] .nii-toolbar-info-piece-additional {
			display: inline-block;
		}
	}

	@media screen and (max-width: 1179px) {
		.nii-toolbar-block .nii-toolbar-icon > * > .nii-toolbar-info-piece-additional {
			display: none;
		}
	}

	@media screen and (max-width: 1439px) {
		.nii-toolbar-block .nii-toolbar-icon > * > .nii-toolbar-info-piece-additional-detail {
			display: none;
		}
	}
</style>
<li class="nii-toolbar-block">
	<div class="nii-toolbar-icon">
		<a href="http://nii.com/">
			<span class="label">Nii</span>
			<span><?php echo Nii::getVersion(); ?></span>
		</a>
	</div>
	<div class="nii-toolbar-info">
		<div class="nii-toolbar-info-piece">
			Nii <b><?php echo Nii::getVersion(); ?></b>
		</div>
		<div class="nii-toolbar-info-piece">
			<a href="http://nii.com" rel="help">Documentation</a>
		</div>
		<div class="nii-toolbar-info-piece">
			<a href="http://yiiframework.com" target="_blank" rel="help">Yii <b><?php echo Yii::getVersion(); ?></b></a>
		</div>
	</div>
</li>
<li class="nii-toolbar-block">
	<div class="nii-toolbar-icon">
		<a  href="<?php echo NHtml::url('/dev/admin/phpinfo') ?>">
			<img style="width:20;height:20px;padding-top:5px;" alt="PHP" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAcCAYAAAB/E6/TAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAA/NJREFUeNq8lk1sVFUUgL958z/BIApBBhVap0x/H5eHNBpNCiRCiklrqtGEYBQXGK0LEgxiinWDMSGhYaEbEhZoKkJLF2IMjTShiVg1djI+YDEUcCGII2GAMn1vfjocF53XToIMoxTO7t5zz/nuzbnnx8XdpQp4DXgBmKt0YyVA3IyNAjeA74FDwO/lnLjK6J4HdindaKngMsTN2A/ATmC4UpAP2KN04z3+h8TN2GfANiBXDvQIMFDpK8rAhoEOIPVvoCBwQulGM7MgcTP2C7AasAHcJbrPlW60MUvy2MJFi/9KXl4AfFv6opVKN37lPkjcjD0NjGrF9e5KjCYLeXTVQCab+S+s3QAaoJRurK3EouPldg4c+AJNc1VMiVTXrAWUB3jF2SwUCojcQnPPhC6XyzGZzwPQvGoV6XSaYMiHZdm4RMPt9mDbFv5AALc2Y5fP58gX7RzGkNINUboh0WW1kkwmxbYzkkqlZGLCknR6Qt7avEmiNVFJJBIiInLzZlosy5Lx8XHp7HxblkWiYpqmWLYtqVRK0ukJse2MdHa+I5GnagQYArjogPTG5SIi8nF3l9TVNkgkUi1DxwdFRKSl5TkpFAqyd2+PNNQ1Sn19rVz+85IkEmdlxYomERHp7v5I6mobpLGhSfr6DomISDQaEeCiFqmuWTT9JcOPAnD06DH8Pj9zQg9zZOAIAG3tHWiaxsBAH16vD58nhJ3JcjZxhnB4ysXQ8UH8Pj8et5fDhw8CMG/eXIAFWmngdH0FIkImM/Or2trauXo1xfz58wEYGzs/U22rqvhxZISlS5YgIly7Zk3rNrw4lZLjN6Z8aecujF12lGvWrMG2LZZWLcEX0Nj4+qusX7+BHTu2UZicxLIsFi54HIBAaCrw58+N0di0nGw2yxNPhgmEvLzU0cqbb2xm+/b3yWVzAFc8QAJYDNDc/Axer4/e3l58Ph9XrvxNR0c7Z04n2LLlXQYHv5u+cTgcJplM8sfFSyhl4PV6+erg1/i8Xq5fv86mTRs5cyrBnDkPASRcwK5IdU1XMBjEPP0bO7s+oL/vm6niFwyVzRHbtvB43JinT9HTs4feLw/eqTp8ogH9AMFQEICRn34mGAzdFeJcJBAMoWkuTp4cLne030nxE3W19S2BgB+55ZrtWjesdGO18+u25XP5WYc4vp1aBzB67sLYvvtQufcp3RiNmzFK82hrsVkxi41vq7MuBdlAa7EN33NcgFalG3bcjN0Gotjj18XN2P57gOwH1indSDmQO45bSjeIm7EW4FOlG89WCBgBPlS6MVxcVzbXKd1wHESL/cQZIFVxP14yQPYr3UiUQG+f60SEByEaD0j+GQB9TLCYD0LMAwAAAABJRU5ErkJggg==">
		</a>
	</div>
	<div class="nii-toolbar-info">
		<div class="nii-toolbar-info-piece nii-toolbar-info-php"><b>PHP</b><span><?php echo PHP_VERSION ?></span></div>
		<div class="nii-toolbar-info-piece nii-toolbar-info-php-ext"><b>PHP Extensions</b><span class="nii-toolbar-status nii-toolbar-status-<?php echo (extension_loaded('apc') && ini_get('apc.enabled')) ? 'green' : 'red'; ?>">APC</span></div>
	</div>
</li>
<li class="nii-toolbar-block">
	<div class="nii-toolbar-icon">
		<span>
			<img width="16" height="28" alt="Time" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAcCAYAAABoMT8aAAABqUlEQVR42t2Vv0sCYRyHX9OmEhsMx/YKGlwLQ69DTEUSBJEQEy5J3FRc/BsuiFqEIIcQIRo6ysUhoaBBWhoaGoJwiMJLglRKrs8bXgienmkQdPDAwX2f57j3fhFJkkbiPwTK5bIiFoul3kmPud8MqKMewDXpwuGww+12n9hsNhFnlijYf/Z4PDmO45Yxo+10ZFGTyWRMEItU6AdCx7lczkgd6n7J2Wx2xm63P6jJMk6n80YQBBN1aUDv9XqvlAbbm2LE7/cLODRB0un0VveAeoDC8/waCQQC18MGQqHQOcEKvw8bcLlcL6TfYnVtCrGRAlartUUYhmn1jKg/E3USjUYfhw3E4/F7ks/nz4YNFIvFQ/ogbUYikdefyqlU6gnuOg2YK5XKvs/n+xhUDgaDTVEUt+HO04ABOBA5isViDTU5kUi81Wq1AzhWMEkDGmAEq2C3UCjcYXGauDvfEsuyUjKZbJRKpVvM8IABU9SVX+cxYABmwIE9cFqtVi9xtgvsC2AHbIAFoKey0gdlHEyDObAEWLACFsEsMALdIJ80+dK0bTS95v7+v/AJnis0eO906QwAAAAASUVORK5CYII=">
			<span class="label <?php echo $pageLoadTimeStatusClass; ?>"><?php echo $pageLoadTimeMs; ?> ms</span>
		</span>
	</div>
	<div class="nii-toolbar-info">
		<div class="nii-toolbar-info-piece">
			<b>Page Load Time</b>
			<span><?php echo $pageLoadTimeMs; ?> ms</span>
		</div>
		<div class="nii-toolbar-info-piece">
			<b><?php echo $elapsedTime['label']; ?></b>
			<span><?php echo $elapsedTimeMs; ?> ms</span>
		</div>
	</div>
</li>
<li class="nii-toolbar-block">
	<div class="nii-toolbar-icon">
		<span>
			<img width="13" height="28" alt="Memory Usage" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAcBAMAAABITyhxAAAAJ1BMVEXNzc3///////////////////////8/Pz////////////+NjY0/Pz9lMO+OAAAADHRSTlMAABAgMDhAWXCvv9e8JUuyAAAAQ0lEQVQI12MQBAMBBmLpMwoMDAw6BxjOOABpHyCdAKRzsNDp5eXl1KBh5oHBAYY9YHoDQ+cqIFjZwGCaBgSpBrjcCwCZgkUHKKvX+wAAAABJRU5ErkJggg==">
			<span><?php echo $memoryUsageMb; ?></span>
		</span>
	</div>
	<div class="nii-toolbar-info">
		<div class="nii-toolbar-info-piece">
			<b><?php echo $memoryUsage['label']; ?></b>
			<span><?php echo $memoryUsage['value']; ?></span>
		</div>
		<div class="nii-toolbar-info-piece">
			<b><?php echo $memoryPeakUsage['label']; ?></b>
			<span><?php echo $memoryPeakUsage['value']; ?></span>
		</div>
		<div class="nii-toolbar-info-piece">
			<b><?php echo $sessionSize['label']; ?></b>
			<span><?php echo $sessionSize['value']; ?></span>
		</div>
	</div>
</li>
<li class="nii-toolbar-block">
	<div class="nii-toolbar-icon">
		<a href="<?php echo NHtml::url('/dev/admin/index') ?>">
			<!--<img width="24" height="28" alt="Security" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAcCAYAAAB75n/uAAAC70lEQVR42u2V3UtTYRzHu+mFwCwK+gO6CEryPlg7yiYx50vDqUwjFIZDSYUk2ZTmCysHvg9ZVggOQZiRScsR4VwXTjEwdKZWk8o6gd5UOt0mbev7g/PAkLONIOkiBx+25/v89vuc85zn2Q5Fo9F95UDwnwhS5HK5TyqVRv8m1JN6k+AiC+fn54cwbgFNIrTQ/J9IqDcJJDGBHsgDgYBSq9W6ysvLPf39/SSUUU7zsQ1yc3MjmN90OBzfRkZG1umzQqGIxPSTkIBjgdDkaGNjoza2kcFgUCE/QvMsq6io2PV6vQu1tbV8Xl7etkql2qqvr/+MbDE/Pz8s9OP2Cjhwwmw29+4R3Kec1WZnZ4fn5uamc3Jyttra2qbH8ero6JgdHh5+CvFHq9X6JZHgzODgoCVW0NPTY0N+ltU2Nzdv4GqXsYSrPp+vDw80aLFYxru6uhyQ/rDb7a8TCVJDodB1jUazTVlxcXGQ5/mbyE+z2u7u7veY38BVT3Z2djopm5qa6isrK/tQWVn5qb29fSGR4DC4PDAwMEsZHuArjGnyGKutq6v7ajQaF6urq9/MzMz0QuSemJiwQDwGkR0POhhXgILjNTU1TaWlpTxlOp1uyWQyaUjMajMzM8Nut/tJQUHBOpZppbCwkM/KytrBznuL9xDVxBMo8KXHYnu6qKjIivmrbIy67x6Px4Yd58W672ApfzY0NCyNjo7OZmRkiAv8fr+O47iwmABXtoXaG3uykF6vX7bZbF6cgZWqqiqezYkKcNtmjO+CF2AyhufgjsvlMiU7vXEF+4C4ALf9CwdrlVAqlcFkTdRqdQSHLUDgBEeSCrArAsiGwENs0XfJBE6ncxm1D8Aj/B6tigkkJSUlmxSwLYhMDeRsyyUCd+lHrWxtbe2aTCbbZTn1ZD92F0Cr8GBfgnsgDZwDt8EzMBmHMXBLqD0PDMAh9Gql3iRIESQSIAXp4CRIBZeEjIvDFZAm1J4C6UK9ROiZcvCn/+8FvwHtDdJEaRY+oQAAAABJRU5ErkJggg==">-->
		<?php if (Yii::app()->user->isGuest): ?>
			<img width="24" height="28" alt="Security" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAcCAYAAAB75n/uAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RjUzNjFFNDc1RjYwMTFFMkJDMzVDMkQ0MERDODMxRDciIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RjUzNjFFNDg1RjYwMTFFMkJDMzVDMkQ0MERDODMxRDciPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpGNTM2MUU0NTVGNjAxMUUyQkMzNUMyRDQwREM4MzFENyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpGNTM2MUU0NjVGNjAxMUUyQkMzNUMyRDQwREM4MzFENyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PhzlRoIAAAO1SURBVHjaYvz//z8DLQHjqAUjwwLeZYEG53/8+adMTYM5WJjuRq2/YMgCZMvBDA9qnblUQM/yHpD5D4sepg+Xjiutq06PJsYCqJlyIAuEYIJAw68CqTN/f35nP5vnVfTvz28OEXPXPSpp9YdAvgXKf0c2RJiT9b+2i98XLhmlL3++f2X+9fYl54VdG3k+/fzDCFUixAQkmJH0nAHiQ1db0ySvPHvreO3VJ8tDm9fWXqyJ5gOKH4TKg4GOtMhvh/rpVz7dOPf57MKJ/FfXzOX6cv/ae9fGGdfEuNlgIcDMhOaz30DMzsorqIsm7gYKVqg8gwAHyz/VjKazBxozFTkkZB8ZJxYdVDSzuy5oYHP+5uQKbnVH72fwcMUSfGwcEnKMKBEmJsMFpDhhfFVdw7cPV04WEOflfCUXknnuwMwuJyY29jffnz9kef7uoyC7kPgLfBb8lg/PPaUsyPUNxFEQ4PwJNOQ0kPkH7gIB4Q8fnz4UFtAxf/Rk03wlYISyCxnZ3/9w+YS6tJjIh6+PbrPjs+Ab0DV3ZR39QRHOoBGZc5JTSuESkPkVruDJXS4hZc1XXx/eFBC18rht5eR68O/3r/8E9a2uKUQXnbp6/KAaTC0Luulvju9kfbhqitv392+kQfynWxaKvr9wVFq7asZfmJorN+9Iuhe3nDo/o9n698wGty8/fvFeOLzPTElU4P75A7tVvv3+y4bLB1xnJ9cuunjnYf2tt1+lQAKXH7/SPHn82NJ9seaToBHN8Pvffyag4Y7Guc07uaQVf3/9/VcQmHJ+s4tKMdrE51xkYUJEIYoFf75+knv+5Ucgtozz6uuvGCAlDuM//fxDYE93Rfj/v39Z9Pyi7srZej3//fEd/645E03//EMUPyhB9O7sQcWETZc6gUxjUFwixwsQn3y5f70aSmQBg+LspSsaDJeu4C2LHOb56u2nRUGXtPmSI0Ykc7Ey/9XU0noIYt+/dUP63fff7PgMkeXj+C+tb/YTxL537gQ7MChR8hCGBQqC3L/1W5YeBzK/i25bbLdjercaPgu0YwtuSnlEHQbFJ8/aGY57FkxTQikhgfiviiDXZ5jA7TefQWG/EYhXHZrTp0AoGE7N65UFqQXmja2Hl8yQhYlDzfwLigMtICMSiM2gFoIKtKUgHwBxMBDbAjEPDvM/gQpHIN4ADQ2QeisgZgXZDcTLwRUOKChBpS/Ugg9ADIoDUFqTB2JBUGLAYQGo1HwHxI+heuVAZSFU/C1IfLTSH3gLAAIMAH5vjYtQsafHAAAAAElFTkSuQmCC">
		<?php else: ?>
			<img width="24" height="28" alt="Security" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAcCAYAAAB75n/uAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6REExNUU3MkU1RjYxMTFFMkJDMzVDMkQ0MERDODMxRDciIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6REExNUU3MkY1RjYxMTFFMkJDMzVDMkQ0MERDODMxRDciPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpGNTM2MUU0RDVGNjAxMUUyQkMzNUMyRDQwREM4MzFENyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpGNTM2MUU0RTVGNjAxMUUyQkMzNUMyRDQwREM4MzFENyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pkvltd8AAAPASURBVHjaYvz//z8DLQETA43BqAUDbwHLx48fee02mZ3/9/2fMlVdzsl095DfKUMWIFsOZni/15SlprwW94DMf9j0nP58QqlwW040MRZAzZQDWSAEEwQafhVInfn+9zt7xA2/ot//fnM4CDjvKZOtPQQUZwTKf0c2hFWY5b+nuvcXeQ6lL9/+fmF+/fsV5/arW3l+f/zDCFUiBIoDZiQ9Z4D4UOG9TMm3V947frr2xXLTsY216bfi+IDiB6HyYCCiI/S7x2LSlUtfz3+edXky/5JbC7hufrv+vsdm0jVWMRZYCDCjR/JvIGYXZBHURRN3A2IOqDwDswDzvwrZ+rMlJ/IUpdlkH2XqFBy0krO9bs5nfb75YQ23u7LnM3ypiE2GXZYRWUCKTZoLSHHC+PpK+m9nP58qwCnO8SpRMv3cpBN9TmyMbG+e/HzE8vH5J0FRNvEX+Cz4nSKRdYpbmeMbiMOpwP4zUSL9NJD5B6ZAkE3ow5PPj4SNec0eLXu5UAkYoezW/Hb3z3w+qS4sLfTh3vc77Pgs+MbOxHHXU9IXFOEMKUpZJ+U4FC4BmV9hCu5/v8elwq/x6vb3mwJOgm63Hc2dDn77+/WfKZ/FtSzJglPHbh5Rg+cDdNP3fdjFOuv5NLf3P95Kg/grXi8WPfXpqHSv8rS/MDUPrt+XrLZvONVzrdW660+z2++vv3gPXTloJqDId//A5f0qf7/+ZcPlA672Kw2Lnlx6Uv/11ncpkMDry280T545tdT1pPUkaEQz/Pv1nwlouGOlTsNOoO9+//nyT5BNlOW3BKskY6ppxkVGFkbsRcXnP5/kvj3/FYgt4/x6/ScGSInD+N+f/hJoPFIb/u//X5YQrfC7rlIezz/8ecc/48BU0/9/EHUMShAd+3RY8WjomU4g0xiUmpDjBYhPbn27QQ1ZPSgorp2/rnGN4TrOHM344cMHB5uVJvtpUdAdCT/jiBHJzNzMfzXU1B+C2Lce3pL+/e4POz5DOOTY/huLm/4EsU89PMn++9UflDyEYQG3POfvmWqLjoOCeaXgUrvJ+/rV8FmQoZx3M0Qs4jAoPudzzXSc+2q2Enp98JdbheMzTODLna+gsN8IxKumnZykQCgYpl6cIAtSe//H3a3zT86VhTsUYuZfUBxoARmRQGwGtRBUoC0F+QCIg4HYFoh5cJj/CVQ4AvEGaGiA1FuBClpQiAHxcpAFvEAGyGZhqAUfgBgUB6C0Jg8qGUCJAVexD8TvgPgxVK8cEAtAxd+CxBlH20UDbgFAgAEA7xV1ThTgTUYAAAAASUVORK5CYII=">
		<?php endif; ?>
		<!--<span class="nii-toolbar-status nii-toolbar-status-<?php echo Yii::app()->user->isGuest ? 'red' :  'green'; ?>"></span>-->
		</a>
	</div>
	<div class="nii-toolbar-info">
		<?php if (Yii::app()->user->isGuest): ?>
			You are not authenticated.
		<?php else: ?>
			<div class="nii-toolbar-info-piece">
				<b>Logged in as</b>
				<span class="nii-toolbar-status nii-toolbar-status-green"><?php echo Yii::app()->user->name ?></span>
			</div>
			<div class="nii-toolbar-info-piece">
				<b>Authenticated</b>
				<span class="nii-toolbar-status nii-toolbar-status-green">Yes</span>
			</div>
		<?php endif; ?>
	</div>
</li>