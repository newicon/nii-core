<link rel="stylesheet" type="text/css" href="<?php echo $this->getAssetsUrl(); ?>/yii.debugtoolbar.css" />
<div id="yii-debug-toolbar-switcher">
    <a href="javascript:;//"></a>
</div>

<div id="yii-debug-toolbar" style="display:none;">
    <div id="yii-debug-toolbar-buttons">
        <ul>
            <li><br />&nbsp;<br /></li>
			<?php $this->widget('YiiDebugToolbarResourceUsage', array(
                'title'=>'Resource usage',
            )); ?>
			<!-- START: Debug Panels -->
            <?php foreach ($panels as $panel): ?>
            <li class="yii-debug-toolbar-button <?php echo $panel->id ?> nii-toolbar-block">
                <a class="yii-debug-toolbar-link nii-toolbar-icon" href="javascript:;//" id="yii-debug-toolbar-tab-<?php echo $panel->id ?>">
					<span>
						<?php echo $panel->menuImg ? $panel->menuImg : '<span>?</span>' ?>
						<span><?php echo CHtml::encode($panel->menuTitle); ?></span>
						<?php echo ($panel->menuStat) ? $panel->menuStat : ''; ?>
						<small class="nii-toolbar-info-piece-additional-detail"><?php echo (!empty($panel->menuSubTitle)) ? $panel->menuSubTitle : '';  ?></small>
					</span>
                </a>
            </li>
            <?php endforeach; ?>
			<!-- END: Debug Panels -->
			
        </ul>
    </div>

    <?php foreach ($panels as $panel) : ?>
    <div id="<?php echo $panel->id ?>" class="yii-debug-toolbar-panel">
        <div class="yii-debug-toolbar-panel-title">
        <a href="javascript:;//" class="yii-debug-toolbar-panel-close"><?php echo YiiDebug::t('Close')?></a>
        <h3>
            <?php echo CHtml::encode($panel->title); ?>
            <?php if ($panel->subTitle) : ?>
            <small><?php echo $panel->subTitle; ?></small>
            <?php endif; ?>
        </h3>
        </div>
        <div class="yii-debug-toolbar-panel-content">
            <div class="scroll">
                <div class="scrollcontent">
                <?php $panel->run(); ?>
                <br />
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>

<script>window.jQuery || document.write('<script src="<?php echo $this->getAssetsUrl() ?>/jquery.js"><\/script>')</script>
<script type="text/javascript" src="<?php echo $this->getAssetsUrl() ?>/yii.debugtoolbar.js"></script>
<script type="text/javascript" src="<?php echo $this->getAssetsUrl() ?>/jquery.cookie.js"></script>
<script type="text/javascript">
jQuery(function($){
	yiiDebugToolbar.init()
})
</script>
