
<a class="btn btn-warning" href="<?php echo CHtml::normalizeUrl(array('/dev/admin/flushAssets')) ?>">Flush Assets Folder</a>
<a class="btn btn-warning" href="<?php echo CHtml::normalizeUrl(array('/dev/admin/flushCache')) ?>">Flush Cache (includes schema)</a>
<a class="btn btn-warning" href="<?php echo CHtml::normalizeUrl(array('/dev/admin/flushRuntime')) ?>">Flush Runtime Folder</a>
<a class="btn btn-warning" href="<?php echo CHtml::normalizeUrl(array('/dev/admin/ApcClearCache')) ?>">Flush APC Cache</a>