<?php
/**
 * Nii view file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<?php $this->beginContent('//layouts/admin1column'); ?>

<div class="container">
	<p>
		YII_ENVIRONMENT: <span class="label label-info"><?php echo Yii::app()->params->environment; ?></span>
		YII_DEBUG: <span class="label label-info"><?php echo YII_DEBUG ? 'TRUE' : 'FALSE' ?></span>
	</p>
<hr/>

<div class="row">
	<div class="span3">
		<?php $this->renderPartial('/layout/_sidebar') ?>
	</div>
	<div class="span9">
		<?php echo $content ?>
	</div>
</div>
</div>

<?php $this->endContent(); ?>