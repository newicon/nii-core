<?php

/**
 * Nii view file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<div class="well" style="padding: 8px 0;">
	<ul class="nav nav-list">
		<li class="nav-header">Tools</li>
		<li class="<?php echo NHtml::active('/dev/admin/index') ?>"><a href="<?php echo NHtml::url('/dev/admin/index') ?>">Cache</a></li>
		<li class="<?php echo NHtml::active('/dev/admin/phpinfo') ?>"><a href="<?php echo NHtml::url('/dev/admin/phpinfo') ?>">PHP info</a></li>
		<li class="<?php echo NHtml::active('/dev/admin/mail') ?>"><a href="<?php echo NHtml::url('/dev/admin/mail') ?>">Mail</a></li>
		<li class="divider"></li>
		<li class="nav-header">Documentation</li>
		<li class="<?php echo NHtml::active('/dev/documentation/tags') ?>"><a href="<?php echo NHtml::url('/dev/documentation/tags') ?>">Tags</a></li>
		<li class="<?php echo NHtml::active('/dev/documentation/css') ?>"><a href="<?php echo NHtml::url('/dev/documentation/css') ?>">CSS Styles</a></li>
	</ul>
</div>