<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<style>

</style>
<h2>CSS Styles</h2>
<p>Nii comes with some utiltiy css styles for frequently used cross module things.</p>

<h3>Speach Bubbles</h3>

<p>Expanding on bootstraps media object. A fancy looking speech bubble media block. 
	This does not intefere with the default bootstrap styling it simply adds an inner div to perform the fancy bubble styling.
	To create a speech bubble we wrap the content of the bootstrap .media-body in a div with the <code>.speech-bubble</code> 
	or <code>.speech-bubble-right</code> class.
</p>
<div class="media">
	<div class="media-image pull-left">
		<img src="http://www.gravatar.com/avatar/84a6778f82775b54a5f7ba38c916f68f?s=48&amp;r=G&amp;d=mm">
	</div>
	<div class="media-body">
		<div class="speech-bubble">
			<strong>Freddy</strong> <small class="muted">Just this second, wait err this last second, wait...</small> <br/>
			Steve thinks he likes a world where he only has to copy and paste fancy looking blocks of pre styled code. Yum.
		</div>
	</div>
</div>

<div class="media">
	<div class="media-image pull-right">
		<img src="http://www.gravatar.com/avatar/84a6778f82775b54a5f7ba38c916f68f?s=48&amp;r=G&amp;d=mm">
	</div>
	<div class="media-body">
		<div class="speech-bubble-right">
			<strong>Freddy</strong> <small class="muted">Just this second, wait err this last second, wait...</small> <br/>
			Steve thinks he likes a world where he only has to copy and paste fancy looking blocks of pre styled code. Yum.
		</div>
	</div>
</div>

<div class="media">
	<div class="media-image pull-left">
		<img src="http://www.gravatar.com/avatar/84a6778f82775b54a5f7ba38c916f68f?s=48&amp;r=G&amp;d=mm">
	</div>
	<div class="media-body">
		<div class="speech-bubble">
			<strong>Freddy</strong> <small class="muted">Just this second, wait err this last second, wait...</small> <br/>
			Steve thinks he likes a world where he only has to copy and paste fancy looking blocks of pre styled code. Yum.
		</div>
		<div class="speech-comments">
			<div class="line">
				<div class="unit">
					<span class="badge badge-tiny" style="margin-left:10px;">1</span>
					<small class="muted">You like this</small>
				</div>
				<div class="lastUnit">
					<div class=" text-right">
						<span class="liked" title="Unlike"><i class="icon-thumbs-up" style="opacity:1;"></i><small>Liked</small></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br/>
<?php $this->beginWidget('CTextHighlighter', array('language' => 'html')); ?>
<div class="media">
	<div class="media-image pull-left">
		<img ...>
	</div>
	<div class="media-body">
		<div class="speech-bubble">
			... bubble content
		</div>
	</div>
</div>

<div class="media">
	<div class="media-image pull-right">
		<img ...>
	</div>
	<div class="media-body">
		<div class="speech-bubble-right">
			... bubble content
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<hr/>
<h3>Input Boxes</h3>
<p>Sometimes we want other elements that are <strong>not</strong> input elements to be styled like input elements. Use the <code>.input-box</code> class to grant a div or other element input styling</p>

<div class="input-box" style="padding:5px;">Hello I am a div but I look like an input box</div>

<br/>
<?php $this->beginWidget('CTextHighlighter', array('language' => 'html')); ?>
<div class="input-box" style="padding:5px;">Hello I am an input box</div>
<?php $this->endWidget(); ?>