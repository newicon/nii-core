<?php

class AdminController extends AController {

	public $layout = '/layout/dev';
	
	public function accessRules() {
		return array(
			// deny anyone who is not a super user
			array('deny',
				'expression'=>'!Yii::app()->user->getisSuper()'
			),
		);
	}
	
	public function actionIndex() {
		$this->render('index');
	}

	/**
	 * Flush the cache
	 */
	public function actionFlushCache($return='index'){
		try {
			Yii::app()->cache->flush();
			Yii::app()->user->setFlash('success','Cache succesfully flushed');
		}catch(Exception $e){
			Yii::app()->user->setFlash('error','Cache flush failed');
		}
		$this->redirect(array($return));
	}
	
	/**
	 * Flush APC cache
	 */
	public function actionApcClearCache($return='index')
	{
		try {
			if (function_exists('apc_clear_cache')) {
				apc_clear_cache();
				apc_clear_cache('user');
				apc_clear_cache('opcode');
			}
			Yii::app()->user->setFlash('success','APC cache succesfully flushed');
		}catch(Exception $e){
			Yii::app()->user->setFlash('error','APC cache flush failed');
		}
		$this->redirect(array($return));
	}
	
	/**
	 * Remove all assets from the assets folder
	 */
	public function actionFlushAssets($return='index'){
		Yii::app()->flushAssets();
		Yii::app()->user->setFlash('success','Assets folder succesfully flushed');
		$this->redirect(array($return));
	}
	
	/**
	 * Remove all files from the runtime folder
	 */
	public function actionFlushRuntime($return='index'){
		$ignore = array(
			Yii::app()->runtimePath.'/.gitignore',
		);
		NFileHelper::deleteFilesRecursive(Yii::app()->runtimePath,$ignore);
		Yii::app()->user->setFlash('success','Runtime folder succesfully flushed');
		$this->redirect(array($return));
	}
	
	/**
	 * Display PHP info
	 */
	public function actionPhpinfo()
	{
		phpinfo();
	}

	public function actionMail()
	{
		UserModule::sendMail('steve@newicon.net', 'Testing...', 'Hi there just testing mail delivery');

		Nii::app()->email->compose()
			->addTo('steve@newicon.net')
			->addFrom('theteam@newicon.net')
			->setBody('Hello - just testing sending API')
			->setSubject('Testing email sending')
			->send();

		$this->render('mail', ['adminEmail' => Yii::app()->params['adminEmail']]);
	}
	
}