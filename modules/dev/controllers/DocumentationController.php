<?php

/**
 * DocumentationController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Controller to manage documentation pages
 */
class DocumentationController extends AController
{
	public $layout = '/layout/dev';
	
	public function accessRules() {
		return array(
			// deny anyone who is not a super user
			array('deny',
				'expression'=>'!Yii::app()->user->getisSuper()'
			),
		);
	}
	
	public function actionTags()
	{
		$this->render('tags');
	}
	
	public function actionCss()
	{
		$this->render('css');
	}
}