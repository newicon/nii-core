<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 */
class ActivityBehavior extends NActiveRecordBehavior
{
	public function afterSave($event)
	{
		if ($this->getOwner()->isNewRecord) {
			NActivityLog::add(Yii::app()->controller->getModule()->getName(), 'created', $this->getOwner()->attributes, $this->getOwner());
		} else {
			NActivityLog::add(Yii::app()->controller->getModule()->getName(), 'updated', $this->getOwner()->attributes, $this->getOwner());
		}
	}
	
	public function afterDelete($event) 
	{
		NActivityLog::add(Yii::app()->controller->getModule()->getName(), 'deleted', $this->getOwner()->attributes, $this->getOwner());
	}
}