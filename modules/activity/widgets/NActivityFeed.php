<?php

/**
 * NActivityFeed class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */


Yii::import('activity.models.*');

/**
 * NActivityFeed Portlet
 */
class NActivityFeed extends NPortlet 
{
	/**
	 * @var string the CSS class for the content container tag. Defaults to 'portlet-body.
	 */
	public $contentCssClass='portlet-body nopadding pts';
	public $title = '';
	public $limit = 20;
	
	/**
	 * condition to filter the logs by
	 * @var string
	 */
	public $condition = '';
	
	/**
	 * boolean whether to enable the ability to share a message for the currently logged in user
	 * @var boolean
	 */
	public $canShare = true;
	
	/**
	 * @var string the CSS class for the portlet title tag. Defaults to 'portlet-header'.
	 */
	public $titleCssClass='';
	/**
	 * @var array the HTML attributes for the portlet container tag.
	 */
	public $htmlOptions=array('class'=>'');
	
	/**
	 * We call this function from the view
	 * this function is only called when the cache is invalid
	 * The whole view is cached using the dependancy generated 
	 * via the rendercontent function
	 */
	public function getLogs()
	{
		return NActivityLog::model()->with('user:withTrashed')->findAll(array('condition'=>$this->condition, 'order'=>'date DESC', 'limit'=>$this->limit));
	}
	
	/**
	 * Get a data provider object for the logs
	 * @return \CActiveDataProvider
	 */
	public function getDataProvider()
	{
		return new CActiveDataProvider('NActivityLog',array(
			'criteria' => array(
				'order'=>'date DESC',
				'with'=>array('user:withTrashed'),
				'condition'=>$this->condition
			),
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
	}
	
	protected function renderContent() 
	{
		// Add caching of activity logs
		// for now this is a very simple in theory we only ever need to get the latest
		// records since the last cache as activity logs are historic and never edited.
		// activity logs can get quite db intensive as each log may want to go and fetch additional data but this
		// should all be fine and dandy if we implement a god cache strategy.
		
		// One possibility is that we do a query that retrives the latest records in the log since the last time it was cached
		// we could simply create a file with the 
		// $lastCached = time_last_cached();
		// SQL: select from nii_activity where updated_at > $lastCached
		// render the additional rows html
		// merge these rows with the current feed rows in cache and re-cache
		// problem with this approach is that the cache keeps on growing. But maybe
		// you could simply pop rows off the bottom and set a limit somewhere to its size.
		// perhaps another way to do it would be to cache html based on months.
		// ...
		
		$cacheDependancy = new CDbCacheDependency('select max(updated_at) from '.NActivityLog::model()->tableName());
		$this->render('feed', array('cacheDependancy'=>$cacheDependancy));
	}
}