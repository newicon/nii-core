<?php
/**
 * This view file enables activity items to be rendered from a this view.  Which enables Yii componentns
 * such as CListView to render activity logs.
 */
?>
<?php if ($data->shouldDisplay()): ?>
	<?php $this->widget('activity.widgets.NActivityFeedItem', array('log'=>$data)); ?>
<?php endif; ?>