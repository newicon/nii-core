<div class="media activity">
	<div class="pull-left">
		<a href="<?php echo NHtml::url($log->user->getRouteProfile()) ?>"><img src="<?php echo $log->user->getProfileImageUrl(); ?>" style="border-radius:3px;" /></a>
	</div>
	<div class="media-body">
		<div class="speech-bubble ">
			<strong><a href="<?php echo NHtml::url($log->user->getRouteProfile()) ?>"><?php echo $log->user->name; ?></a></strong> 
			<small class="muted" style="margin-left:5px;"><span class="activity-date"><?php echo NTime::todayYesterdayDate($log->date) ?></span></small> <br/>
			<?php echo $log->render(); ?>
		</div>
		<div class="speech-comments">
			<?php
				// the comments widget works very well under activity logs
				// however it looks a bit in your face, as the styling is very bold,
				// we probably need a more subtle styled version of the comments widget.
				// or an option that changes the looks and feel slightly.
				// $this->widget('nii.widgets.comments.NiiComments', array(
				//	'model'=>$log,
				//	'id'=>$log->id,
				//	'childComments'=>false
				// ));
			?>
			<div class="line">
				<div class="unit">
					<?php if ($log->countLikes() > 0) : ?>
						<span class="badge badge-tiny" style="margin-left:10px;"><?php echo $log->countLikes(); ?></span>
					<small class="muted"><?php echo $this->whoHasLikedHtml($log); ?></small>
					<?php endif; ?>
				</div>
				<div class="lastUnit">
					<div class=" text-right">
						<?php if ($log->countLikes() > 0) : ?>
							<?php if ($log->youLike()) : ?>
								<a class="liked" href="javascript:NActivity.unlike(<?php echo $log->id; ?>)" title="Unlike"><i class="icon-thumbs-up" style="opacity:1;"></i><small>Liked</small></a>
							<?php else: ?>
								<a class="like" href="javascript:NActivity.like(<?php echo $log->id; ?>)" title="Unlike"><i class="icon-thumbs-up"></i><small>Like</small></a>
							<?php endif; ?>
						<?php else: ?>
							<a class="like" href="javascript:NActivity.like(<?php echo $log->id; ?>)"><i class="icon-thumbs-up"></i><small>Like</small></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>