<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<style>
	.activity .activity-share-input{border:1px solid #ccc;padding:5px 8px;border-radius: 2px;box-shadow: inset 0 1px 2px #ddd;background-color: #fafafa;background-image: -webkit-linear-gradient(top,#f7f7f7,white);background-image: linear-gradient(top,#f7f7f7,white);}
	.activity textarea{border:none;box-shadow:none;background: transparent;padding:0px;resize: none; }
	.badge-tiny{font-size: 11px;line-height: 10px;padding: 2px 5px;}
	a.like:hover{text-decoration:none;}
	.activity .likes{border-top: 1px solid #e1e1e1;margin:10px -10px -5px -10px;padding: 2px 0px 0px 0px;}
	.activity .icon-thumbs-up, .activity .icon-comment{opacity:0.5;margin:0px 3px 0px 10px;}
	a.liked{color:#666;}
	.items{margin-top: 20px;}
</style>
<?php //if($this->beginCache('activity-logs-cache', array('dependency'=>$cacheDependancy, 'duration'=>5184000))) { ?>
<div id="activity-feed">
	<div id="activity-feed-replace">
	
		<?php if ($this->canShare) : ?>
			<div class="media activity">
				<div class="pull-left">
					<img src="<?php echo Yii::app()->user->record->getProfileImageUrl(); ?>" style="border-radius:3px;" />
				</div>
				<div class="media-body">
					<div class="speech-bubble">
						<div id="activity-mini-share" class="activity-share-input muted">What's new?</div>
						<div id="activity-share-comment" style="display:none;">
							<div class="activity-share-input prl mbm">
								<a href="#"><i class="icon-remove" style="opacity:0.3;position:absolute;right:18px;"></i></a>
								<textarea class="input-block-level input-hide" placeholder="What's new?"></textarea>
							</div>
							<a href="#" class="btn share">Share</a>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php
		
			$this->widget('bootstrap.widgets.TbListView', array(
				'id'=>'activity-list',
				'dataProvider'=>$this->dataProvider,
				'itemView'=>'_feed-item',
				'template'=>"{items}\n{pager}\n<div class='clearfix'>&nbsp;</div>",
			));
		?>

		<?php //$this->endCache(); } ?>

	</div>
</div>

<script>

$(function(){
	
	var activityFeedEl = $('#activity-feed');
	
	activityFeedEl.on('click', '#activity-mini-share', function(){
		$('#activity-mini-share').hide();
		$('#activity-share-comment').show().find('textarea').focus();
	});
	activityFeedEl.on('click', '.activity .icon-remove', function(){
		$('#activity-share-comment textarea').val('');
		$('#activity-mini-share').show();$('#activity-share-comment').hide()
	});
	activityFeedEl.on('click','.activity .share', function(){
		if (!$(this).is('.disabled')) {
			$(this).addClass('disabled').html('Sharing...');
			$.post("<?php echo NHtml::url('/activity/index/activityShare') ?>", {activityShare:$('#activity-share-comment textarea').val()}, function(){
				nii.ajaxReplace('activity-feed-replace', function(){})
			});
		}
		return false;
	});
	
});

var NActivity = {
	like:function(id){
		$.post('<?php echo NHtml::url('/activity/index/like') ?>', {id:id}, function(){
			nii.ajaxReplace('activity-feed-replace')
		});
		return false;
	},
	unlike:function(id){
		$.post('<?php echo NHtml::url('/activity/index/unlike') ?>', {id:id}, function(){
			nii.ajaxReplace('activity-feed-replace')
		});
		return false;
	}
}

</script>
