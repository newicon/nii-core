<?php

/**
 * NActivityItem class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Show a single NActivityLog item
 */
class NActivityFeedItem extends NWidget
{
	/**
	 * @var NActivityLog 
	 */
	public $log;

	public function run()
	{
		$this->render('feed-item', array(
			'log'=>$this->log
		));
	}
	
	public function whoHasLikedHtml($log)
	{
		$ret = '';
		$likesCount = $log->countLikes();
		if ($likesCount == 0)
			return '';
		
		if ($log->youLike()) {
			
			$likes = array();
			
			// Rebuild the likes array without the current user
			foreach ($log->likes as $k => $l) {
				if ($l['user_id'] != Yii::app()->user->id)
					$likes[] = $l;
			}
			
			$likesCount -= 1;
			
			$ret .= 'You';
			
			// You and Steve like this
			if ($likesCount == 1) {
				$ret .= ' and ' . $likes[0]->user->name;
			}
			
			// You, Steve and Mark like this
			if ($likesCount == 2) {
				$ret .= ', ' . $likes[0]->user->name;
				$ret .= ' and ' . $likes[1]->user->name;
			}
			
			// You, Steve, Mark and 1 other like this
			if ($likesCount == 3) {
				$ret .= ', ' . $likes[0]->user->name;
				$ret .= ', ' . $likes[1]->user->name;
				$ret .= ' and 1 other';
			}
			
			// You, Steve, Mark and 3 others like this
			if ($likesCount > 3) {
				$ret .= ', ' . $likes[0]->user->name;
				$ret .= ', ' . $likes[1]->user->name;
				$others = $likesCount - 2;
				$ret .= ' and '.$others.' others';
			}
			$ret .= ' like this';
			
		} else {
			
			if ($likesCount == 1) {
				$ret .= $log->likes[0]->user->name . ' likes this';
			}
			
			if ($likesCount == 2) {
				$ret .= $log->likes[0]->user->name;
				$ret .= ' and ' . $log->likes[1]->user->name;
				$ret .= ' like this';
			}
			
			elseif ($likesCount > 2) {
				$ret .= $log->likes[0]->user->name;
				$ret .= ', ' . $log->likes[1]->user->name;
				$othersLikes = ($likesCount-2);
				$ret .= ' and ' . $othersLikes . ($othersLikes==1 ? ' other ' : ' others');
				$ret .= ' like this';
			}
			
		}
		
		return $ret;
	}
}