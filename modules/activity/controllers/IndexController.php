<?php

/**
 * IndexController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of IndexController
 *
 * @author 
 */
class IndexController extends AController
{
	public function actionActivityShare()
	{
		if (isset($_POST['activityShare'])) {
			$share = $_POST['activityShare'];
			NActivityLog::add('activity', 'share', $share, 'feed');
		}
	}
	
	public function actionLike()
	{
		if (isset($_POST['id'])) {
			$id = $_POST['id'];
			$find = NActivityLike::model()->findByPk(array('user_id'=>Yii::app()->user->id, 'activity_id'=>$id));
			if ($find === null) {
				$like = new NActivityLike;
				$like->user_id = Yii::app()->user->id;
				$like->activity_id = $id;
				$like->save();
			}
		}
	}
	
	public function actionUnlike()
	{
		if (isset($_POST['id'])) {
			$id = $_POST['id'];
			$pk = array('user_id'=>Yii::app()->user->id, 'activity_id'=>$id);
			NActivityLike::model()->deleteByPk($pk);
		}
	}
}