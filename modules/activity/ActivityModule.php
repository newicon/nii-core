<?php

/**
 * AcitivtyModule class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of AcitivtyModule
 *
 * @author 
 */
class ActivityModule extends NWebModule
{
	public function init()
	{
		Yii::import('activity.models.*');
	}
	
	public function install()
	{
		NActivityLog::install('NActivityLog');
		NActivityLog::install('NActivityLike');
	}
}