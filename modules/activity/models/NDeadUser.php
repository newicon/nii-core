<?php

/**
 * DeadUser class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Keep a consistent interface when there is no user record.
 */
class NDeadUser extends CComponent
{
	public function getName()
	{
		return 'unknown';
	}
	
	public function getProfileImage($size=40)
	{
		return "<img src=\"{$this->getProfileImageUrl($size)}\" />";
	}
	
	public function getProfileImageUrl($size=40)
	{
		return 'www.gravatar.com/avatar/?d=mm&s='.$size;
	}
	
	public function getRouteProfile()
	{
		return '';
	}
}