<?php

/**
 * NActivityLike class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of NActivityLike
 *
 * @author 
 */
class NActivityLike extends NActiveRecord
{
	/**
	 * table name
	 * @return string
	 */
	public function tableName()
	{
		return '{{nii_activity_like}}';
	}
	/**
	 * get the static instance for passed in classname string
	 * @param string $className
	 * @return NActivityLike
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public function relations()
	{
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'user_id')
		);
	}
	
	public function schema()
	{
		return array(
			'columns'=>array(
				'user_id'=>'int',
				'activity_id'=>'int',
				'updated_at'=>'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
				'Primary key (user_id, activity_id)'
			),
			'keys'=>array(
				array('user_id'),
				array('updated_at')
			)
		);
	}
}