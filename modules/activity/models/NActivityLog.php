<?php

/**
 * NActivityLog class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
Yii::import('activity.models.NDeadUser');

/**
 * Activity!! FEED!
 * 
 * A few key concepts to grasp with this component and all will make perfect sense.
 * Really there is only adding of logs and creating view files to render the them
 * It's best practise to attach logs through standard Yii events. DONT BE LAZY! USE EVENTS! This allows other things to
 * hook into these things later. This will allows other things to attach to the events such as sending emails and notifications
 * 
 * 1: Each activity log is categorised by a top level "category" field and a "type" field. The category is typically the module which is responsible
 *    for that particular activity, for example adding an email to a contact could have the category of "contact" and the type of "email-add" or possibly
 *    you could create a more generic "attribute-changed" type. This is stored in the database together with any additional data you may want
 * 
 *    To add an activity log:
 *    NActivityLog::add("category", "type", [data], $model)
 * 
 *    The category, type and model will get stored as strings, NActivity then uses these to look up a view file to
 *    render the log.
 *    This will look in the Yii alias path of
 *    category.activity.model.type.php
 *    e.g.: crm.activity.CrmContact.created.php would be the view path looked for ofa log with a category of "crm",
 *    a type of "created" and a model of "CrmContact"
 * 
 * 2: Activity logs are usually usefull events!
 *    Generally as activity logs represent key events in the system it is good practice to rasie an event where you would
 *    like to trigger the activity log then later attach the add log function NActivityLog::add(...) to the event.
 *    you can attach data and info to the event object as necessary but typically you would write a function like so:
 *    <code>
 *    $object->onMyEvent = function($event) {
 *        NActivityLog::add('mymodule', 'my-event-name', $event->params, $event->sender)
 *    }
 *    </code>
 * 
 * <code>
 * 
 * Example: 
 * ~~~
 * // Inside the afterSave function of a contact model
 * public function afterSave()
 * {
 *     $data = array_merge($this->attributes, array('attribute_name'=>$name, 'attribute_old_value'=>$value))
 *     Yii::app()->module('contact')->onAttributeChanged(new CEvent($this, $data))
 * }
 * 
 * // Defined event in the contact module 
 * // if your module has lots of events its probably wise to create a seperate singleton class to attach and raise events from
 * // just make sure it extends CComponent so that is can raise events and have events attached to it
 * public function onAttributeChanged($event)
 * {
 *     $this->raiseEvent('onAttributeChanged', $event);
 * }
 * 
 * // Finally add a new log when the onAttributeChanged event is fired
 * Yii::app()->module('contact')->onAttributeChanged = function($event)
 * {
 *     NActivityLog::add('crm', 'attribute-changed', $event->params, $event->sender);
 * }
 * ~~~
 * </code>
 */
class NActivityLog extends NActiveRecord
{
	/**
	 * Deprecated
	 */
	public $title = '';
	
	public $body = '';
	
	/**
	 * Add a log feed item
	 * @param string $category the category of the log, This should be the module name
	 * @param string $type the name of the log, e.g. task-status-change there should be a corosponding renderer with he same category and type defined
	 * @param mixed $data array or a string, this can be useful if you want to display specific info
	 * @param mixed $model
	 * @param mixed $modelId string|int
	 * @return NActivityLog
	 */
	public static function add($category, $type, $data, $model='', $modelId=0)
	{
		$al = new NActivityLog;
		$al->category = $category;
		$al->type = $type;
		$al->model = is_object($model) ? get_class($model) : $model;
		$al->model_id = ($modelId==0) ? (is_object($model) ? $model->id : 0) : $modelId;
		$al->date = NTime::unixToDatetime(time());
		$al->data = $data;
		$al->user_id = Yii::app()->user->id;
		$al->save();
		return $al;
	}
	
	/**
	 * Return a boolean indicating whether we should render this log or not.
	 * This can then be used in rendering functions:
	 * <code>
	 * foreach ($logs as $log) {
	 *		if (!$log->shouldDisplay()) continue;
	 *		// display logs code...
	 * }
	 * </code>
	 * @return boolean
	 */
	public function shouldDisplay()
	{
		return true;
	}
	
	/**
	 * Store result of calling the render function
	 * @var mixed 
	 */
	private $_render;
	
	/**
	 * Render the view file representing this log
	 * @return string html
	 */
	public function render()
	{
		if ($this->_render === null) {
			// check that the module responsible for this log is active
			$module = Yii::app()->getModule($this->category);
			if ($module !== null) {
				$viewAlias = "{$this->category}.activity.{$this->model}.{$this->type}";
				$viewfile = Yii::getPathOfAlias($viewAlias) . ".php";
				if (file_exists($viewfile)) {
					$this->_render = Yii::app()->controller->renderPartial($viewAlias, array('log'=>$this), true);
					if ($this->_render=='')
						$this->_render = "You can change this activity log by creating the following view file : $viewfile";
				} else {
					// if we are in debug mode then we want to display a message showing where the view file should be
					// for this log
					// Lets always show this message
					// if (YII_DEBUG) 
						$this->_render = "You can change this activity log by creating the following view file : $viewfile";
					//else
						// In production mode return false, disabling this log from rendering all together
					//	$this->_render = false;
				}
			} else {
				// the module responsible for raising this log is not currently available.
				// therefore do not display its activity
				$this->_render = false;
			}
		}
		return $this->_render;
	}
	
	/**
	 * Gets a user object related to this log, if the user does not exist then an NDeadUser
	 * record is returned that has the same interface as the default User object
	 * but returns name unknown etc.
	 * @return \NDeadUser || User
	 */
	public function getUser()
	{
		$user = $this->getRelated('user');
		if ($user === null) {
			// create a dead user object
			
			$user = new NDeadUser();
		}
		return $user;
	}
	
	/**
	 * Do not use user model directly as the logs may be old
	 * and the user may not exist
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
			'likes'=>array(self::HAS_MANY, 'NActivityLike', 'activity_id', 'order'=>'likes.updated_at DESC', 'with'=>'user')
		);
	}
	
	/**
	 * instance cache of number of likes
	 * @see self::countLikes
	 * @var int 
	 */
	private $_countLikes;
	
	/**
	 * get the count of total number of likes for this acitivy log
	 * @see self::countLikes 
	 * @return int
	 */
	public function countLikes()
	{
		if ($this->_countLikes === null) {
			$this->_countLikes = NActivityLike::model()->countByAttributes(array(
				'activity_id'=>$this->id,
			));
		}
		return $this->_countLikes;
	}
	
	/**
	 * instance cache of result of self::youLike function
	 * @see self::youLike
	 * @var boolean 
	 */
	private $_youLike;
	
	/**
	 * returns true if the current logged in user likes the activity log
	 * @return boolean
	 */
	public function youLike()
	{
		if ($this->_youLike === null) {
			$exists = NActivityLike::model()->findByPk(array(
				'user_id'=>Yii::app()->user->id,
				'activity_id'=>$this->id,
			));
			$this->_youLike = ($exists===null) ? false : true;
		}
		return $this->_youLike;
	}
	
	/**
	 * override default get, to encode json if getting the data attribute
	 * @param type $name
	 * @return type
	 */
	public function __get($name)
	{
		if ($name == 'data') {
			$data = parent::__get($name);
			return CJSON::decode($data);
		}
		if($name =='user')
			return $this->getUser();
		return parent::__get($name);
	}
	
	/**
	 * override __set to json encode the $value
	 * @param type $name
	 * @return type
	 */
	public function __set($name, $value)
	{
		if ($name=='data')
			$value = CJSON::encode($value);
		parent::__set($name, $value);
	}
	
	public function tableName()
	{
		return '{{nii_activity}}';
	}
	
	public function rules()
	{
		return array(
			array('user_id, date, data, type, model, model_id, category', 'safe'),
			array('user_id, date, data, type, model, model_id, category', 'safe', 'on'=>'search')
		);
	}
	
	/**
	 * @param string $className
	 * @return NActivityLog
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				'user_id'=>'int',
				'model'=>'string',
				'model_id'=>'int',
				'date'=>'datetime',
				'category'=>'string null',
				'type'=>'string null',
				'data'=>'text null',
				'updated_at'=>'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
			),
			'keys'=>array(
				array('user_id'),
				array('updated_at')
			)
		);
	}
}