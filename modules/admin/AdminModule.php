<?php

class AdminModule extends NWebModule {

	public $name = 'Administration';
	public $description = 'Admin module for configuring the admin area';
	public $version = '1.0';
	
	public $logo;
	public $fixedHeader = false;
	public $menuAppname = true;
	public $topbarColor;
	public $h2Color = '#404040';
	public $h3Color = '#404040';
	public $menuSearch = false;
  public $defaultRoute = '/admin/index/dashboard';

	public function init() {
		Yii::import('admin.components.*');
		Yii::import('admin.models.*');
		$this->setComponent('dashboard', new AdminDashboard);
	}
	
	public function setup(){
		Yii::app()->menus->addMenu('main');
		
		// Yii::app()->menus->addItem('main', 'Dashboard', array('/admin/index/dashboard'), null, array(
		// 	'linkOptions'=>array('icon'=>'icon-home'),
		// ));
		
		Yii::app()->menus->addMenu('secondary');

		Yii::app()->menus->addItem('secondary', 'Admin', '#', null, array(
			'visible' => Yii::app()->user->checkAccess('menu-admin'),
			'icon'=>'icon-cog'
		));
		Yii::app()->menus->addItem('secondary', 'Updates', array('/admin/updates/index'), 'Admin', array(
			'visible' => Yii::app()->user->checkAccess('admin/updates/index'),
		));
		Yii::app()->menus->addItem('secondary', 'Settings', array('/admin/settings/index'), 'Admin', array(
			'visible' => Yii::app()->user->checkAccess('admin/settings/index'),
		));
		Yii::app()->menus->addItem('secondary', 'Modules', array('/admin/modules/index'), 'Admin', array(
			'visible' => Yii::app()->user->checkAccess('admin/modules/index'),
		));
		
		Yii::app()->menus->addMenu('user');
		
		Yii::app()->menus->addItem('user', 'User', '#', null, ['icon' => 'icon-user']);
		Yii::app()->menus->addItem('user', 'My Account', array('/user/admin/account'), 'User', array(
			'linkOptions'=>array('data-toggle'=>'modal', 'data-target'=>'#modal-user-account', 'data-backdrop'=>'static'),
		));
		
		Yii::app()->menus->addDivider('secondary','Admin');
		
		
		Yii::app()->menus->addItem('secondary','Users',array('/user/admin/users'),'Admin',array(
			'visible' => Yii::app()->user->checkAccess('user/admin/users'),
		));
		Yii::app()->menus->addItem('secondary','Permissions',array('/user/admin/permissions'),'Admin',array(
			'visible' => Yii::app()->user->checkAccess('user/admin/permissions'),
		));
		
//		Yii::app()->menus->addDivider('secondary','Admin');
//		Yii::app()->menus->addItem('secondary','Audit Trail',array('/user/audit/index'),'Admin',array(
//			'visible' => Yii::app()->user->checkAccess('user/audit/index'),
//		));
	}
	
	public function install(){
		$this->installPermissions();
	}

	public function settings() {
		return array(
			'general' => '/admin/settings/general',
			'presentation' => '/admin/settings/presentation',
		);
	}

	public function permissions() {
		return array(
			'admin' => array('description' => 'Admin',
				'tasks' => array(
					'modules' => array('description' => 'Manage Modules',
						'roles' => array('administrator'),
						'operations' => array(
							'admin/modules/index',
							'admin/modules/enable',
							'admin/modules/disable',
							'menu-admin',
						),
					),
					'settings' => array('description' => 'Manage Settings',
						'roles' => array('administrator'),
						'operations' => array(
							'admin/settings/index',
							'admin/settings/page',
							'admin/settings/general',
							'admin/settings/presentation',
							'menu-admin',
						),
					),
					'settings' => array('description' => 'Trash Items',
						'roles' => array('administrator','editor'),
						'operations' => array(
							'nii/index/trash',
						),
					),
					'dashboard' => array('description' => 'Dashboard',
						'roles' => array('administrator','editor','viewer'),
						'operations' => array(
							'admin/index/index',
							'admin/index/dashboard',
						),
					),
				),
			),
		);
	}

}