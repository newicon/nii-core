<?php

/**
 * Header class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of Header
 *
 * @author 
 */
class Header extends CWidget
{
	
	public $actions;
	public $title = null;
	public $showBreadcrumbs = true;
	
	public function run()
	{
		echo '<div class="page-header" style="border-bottom:0px">';
		echo $this->title===false ? '' : '<h1>' . ($this->title!==null ? $this->title : Yii::app()->controller->title) . '</h1>';
		echo '<div class="action-buttons">';
		echo $this->actions;
		$this->widget('bootstrap.widgets.TbButtonGroup',array(
			'buttons'=>Yii::app()->controller->actionsMenu,
		));
		echo '</div>';
		if ($this->showBreadcrumbs == true)
			$this->widget('bootstrap.widgets.TbBreadcrumbs',array('links'=>Yii::app()->controller->breadcrumbs,'htmlOptions'=>array('style'=>'margin-bottom:0px;')));
		echo '</div>';
	}
}