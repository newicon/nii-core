<?php

class IndexController extends AController {

	public function actionIndex() {
		$this->redirect(array($this->module->defaultRoute));
	}

	public function actionDashboard() {
		$this->title = 'Dashboard';
		$this->render('dashboard', array(
			'portlets' => $this->module->dashboard->portlets,
		));
	}
}