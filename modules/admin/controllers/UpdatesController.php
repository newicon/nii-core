<?php

class UpdatesController extends AController 
{

	public function actionIndex() {
		$this->render('index');
	}
	
	/**
	 * Runs the main application installation
	 */
	public function actionInstall(){
		// lets also flush the cache incase schema chaching is on
		try {
			Yii::app()->cache->flush();
			Yii::app()->installAll();
			Yii::app()->cache->flush();
			Yii::app()->flushAssets();
			$logsRaw = Yii::getLogger()->getLogs(CLogger::LEVEL_INFO, 'NActiveRecord::install');
			$logs = array(); foreach($logsRaw as $l) $logs[] = strtok($l[0], "\n");
			$additional = (YII_DEBUG) ? '<pre>'.implode("\n", $logs).'</pre>' : '';
			
			Yii::app()->user->setFlash('success','Modules and db installed.' . $additional);
		} catch (Exception $e){
			throw $e;
		}
		$this->redirect(array('index'));
	}

}