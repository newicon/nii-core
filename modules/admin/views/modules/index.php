<?php
$dataProvider = new CArrayDataProvider($data, array(
			'pagination'=>array(
				'pageSize'=>20,
            ),
	));

$this->widget('nii.widgets.grid.NGridView', array(
	'id' => 'modules-grid',
	'dataProvider' => $dataProvider,
	'columns' => array(
		array(
			'name' => 'name',
			'header' => 'Name',
		),
		array(
			'name' => 'description',
			'header' => 'Description',
		),
		array(
			'name' => 'version',
			'header' => 'Version',
		),
		array(
			'name' => 'enabled',
			'header' => 'Status',
			'type'=>'raw',
			'value'=>'($data["enabled"]) ? "<span class=\"label label-success\">Enabled</span>" : "<span class=\"label label-important\">Disabled</span>" '
		),
		array(
			'name' => 'actions',
			'header' => 'Actions',
			'type' => 'raw',
			'htmlOptions' => array('width' => '100', 'align' => 'center'),
			'value' => '($data["enabled"]) ? CHtml::link("Disable", array("/admin/modules/disable","module"=>$data["id"]), array("class"=>"btn btn-danger disable")) : CHtml::link("Enable", array("/admin/modules/enable","module"=>$data["id"]), array("class"=>"btn btn-success enable"))',
		),
	),
));
?>
<script>
	jQuery(function($){
		$('.btn.enable,.btn.disable').live('click',function(){
			if($(this).is('.enable'))
				$(this).html('Enabling').addClass('disabled');
			else
				$(this).html('Disabling').addClass('disabled');
		});
	});
</script>