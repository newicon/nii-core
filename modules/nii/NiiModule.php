<?php
Yii::import('nii.models.*');
/**
 * NiiModule class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 * @package nii
 */
class NiiModule extends NWebModule
{

	public $name = 'Nii';

	/**
	 * to change the jquery ui theme point this to the theme folder.
	 * if blank defaults to assets folder nii theme
	 * @var string url path to the jquery ui theme folder
	 */
	public $juiThemeUrl;

	/**
	 *  Load the Nii module assets. Defaults to true.
	 */
	public $registerScripts = true;

	/**
	 * jquery ui theme name
	 * defaults to nii.
	 * @var string
	 */
	public $juiTheme = 'nii';

	public function init() {
		Yii::import('nii.models.*');
		Yii::import('nii.components.npush.*');
		Yii::import('nii.components.behaviors.*');
		Yii::import('nii.extensions.*');
		Yii::import('nii.widgets.markdown.*');
		Yii::import('nii.widgets.select2.Select2');
		// set alias to the bootstrap widgets
		Yii::setPathOfAlias('bootstrap', Yii::getPathOfAlias('nii.extensions.bootstrap'));

		if(Yii::app()->multiTenant){
			// this is important it makes the cache specific to the domain application instance.
			// The cache is shared across the application. Therefore the cache id must be specific to each users application
			// domain instance
			Yii::app()->cache->keyPrefix = Yii::app()->getSubDomain();
			// fix the cahce file path and filemanager path to be specific per domain
			Yii::app()->fileManager->location = Yii::getPathOfAlias('domain.uploads.'.Yii::app()->getSubDomain());
		}

		Yii::app()->setComponents(array(
			'activity'=>array(
				'class'=>'activity.components.NActivity'
			),
			'push' => array(
				'class'=>'nii.components.npush.NFaye',
			),
			'bootstrap'=>array(
				'class'=>'bootstrap.components.Bootstrap',
			),
		));
        $this->registerScriptPackages();
		Yii::import('nii.widgets.notes.models.NNote');
	}

	public function setup()
    {
    	Yii::app()->getModule('admin')->dashboard->addPortlet('activity-feed','activity.widgets.NActivityFeed','main');
	}

    /**
     * Note this does not publish them just register them as handy package
     * Thus a package can be published:
     * Yii::app()->getClientScript()->registerPackage('nii');
     * Yii::app()->getClientScript()->registerPackage('backabone');
     */
    public function registerScriptPackages()
    {
        $cs = Yii::app()->getClientScript();
        $cs->packages =  array(
			'nii' => array(
				'basePath'=>'nii.assets',
				'baseUrl'=>$this->getAssetsUrl(),
				'js'=>array(
					'js/jquery.metadata.js',
					'js/nii.js',
				),
				'css'=>array(
					//'oocss/all.css',
					'jqueryui/nii/jquery-ui.css',
				),
				'depends'=>array('jquery.ui')
			),
			'backbone'=>array(
				'basePath'=>'nii.assets',
				'baseUrl'=>$this->getAssetsUrl(),
				'js'=>array(
					'js/backbone/underscore-1.8.3.min.js',
					'js/backbone/backbone-0.9.10.min.js',
				),
				'depends'=>array('jquery')
			),
			'fullcalendar'=>array(
				'basePath'=>'nii.assets',
				'baseUrl'=>$this->getAssetsUrl(),
				'css'=>array(
					'js/fullcalendar/fullcalendar.css',
				),
				'js'=>array(
					'js/fullcalendar/fullcalendar.min.js',
				),
				'depends'=>array('jquery')
			)
		);
    }

	public function registerScripts()
    {
		Yii::app()->push->registerScript();
        $cs = Yii::app()->getClientScript();
		$cs->registerCssFile($this->getAssetsUrl().'/nii.css');
		$cs->registerCoreScript('yiiactiveform');
		$cs->registerPackage('nii');
		$cs->registerPackage('backbone');
        // register global JS variables
        $js = 'if(!nii){nii={};}nii.baseUrl="'.  NHtml::url() . '";';
        $js .= (Yii::app()->user->isGuest ? '' : 'nii.userId=' . Yii::app()->user->id . ';');
        $cs->registerScript('nii.globals', $js, CClientScript::POS_HEAD);
	}

	/**
	 * return array of the scripts not to include from ajax
	 * note: these will likely mirror the scripts registered on every page load by
	 * NiiModule::registerScripts
	 */
	public function ajaxScriptMap()
    {
		return array (
			'jquery.js'=>false,
			'jquery.min.js'=>false,
			'jquery-ui.min.js'=>false,
			'jquery-ui.css'=>false,
			'jquery.tipsy.js'=>false,
			'jquery.tipsy.css'=>false,
			'jquery.metadata.js'=>false,
			'tipsy.css'=>false,
			'jquery.hotkeys.js'=>false,
			'nii.js'=>false,
			'all.css'=>false,
			'sprite.css'=>false,
			'backbone-0.9.10.min.js'=>false,
			'underscore-1.4.4.min.js'=>false,
			'jquery.gritter.min.js'=>false,
			'jquery.gritter.css'=>false,
			'jquery.yiiactiveform.js'=>false,
			'bootstrap-min.css'=>false,
			'autosize.js'=>false,
			'moment.min.js'=>false,
			'bootstrap-datetimepicker.js'=>false,
			'tapiControl.js'=>false,
			'main.js'=>false,
			// Prevent gridview styles from loading on ajax
			'styles.css'=>false,
			'bootstrap.min.js'=>false,
			'functions.js'=>false,
			// if bbq is prevented to load it breaks ajax updates of
			// list views due to yii accessing querystirng parameters
			//'jquery.ba-bbq.js'=>false,
		);
	}

	public function install()
	{
		Yii::import('nii.components.behaviors.*');
		Yii::import('nii.widgets.activity.*');


		$activity = $this->getModule('activity');
		if ($activity !== null)
			$activity->install();

        NFile::install();
		NLog::install('NLog');

		Yii::import('nii.widgets.notes.models.NNote');
		NNote::install();

		NAttachment::install();

        // is this relationship component not specific to contact module?
		// NRelationship::install();
		NSettings::install();
		NTaggable::install();
		Yii::import('nii.widgets.comments.NiiComments');
		NiiComments::install();
	}

	public function uninstall()
	{
		Yii::import('nii.components.behaviors.*');
		NTaggable::uninstall();
	}

	/**
	 * shortcut method to return the Nii module
	 * @return NiiModule
	 */
	public static function get()
    {
		return yii::app()->getModule('nii');
	}
}
