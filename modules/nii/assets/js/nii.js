
// Nii custom functions. Call with nii.functionName
(function (){
	window.nii = {
            
                // baseUrl vautomatically populated by NiiModule.php on jQuery start
                baseUrl: '',

		// automatically populated by NiiModule.php on jQuery start
		userId: null,

		// Displays an alert asking the user to confirm their action and redirects to the given url on success
		confirmCancel: function(el, urlel) {
			if ($(el).val() == '1') {
				var answer = confirm("You have unsaved changes.\n\nAre you sure you wish to leave this page?")
				if (!answer) {
					return;
				} 
			}
			var url = $(urlel).attr('href');
			window.location(url);
		},

		// Simple! Capitalises the first letter...
		capitaliseFirstLetter: function(string){
			return string.charAt(0).toUpperCase() + string.slice(1);
		},

		// Displays a message, which hides itself after a period of time
		// =================================
		// Definable params are:
		//		msg:	The message you wish to display,
		//		params: {
		//			className: class to override styling of message - classes such as 'error',
		//			timeOut: timeout in milliseconds, defaults to 4000 (4 seconds)
		//		}
		showMessage: function (msg,params) {
			if (!params) params = [];
			var className = params['className'];
			if (!className) className = 'success';
			var timeOut = 4000;
			if (params['timeOut']) timeOut = params['timeOut'];
			$("#message").css('position','fixed');
			$("#message").attr("class","");
			if (className) $('#message').addClass('alert-message '+className);
			$("#message").html(msg).slideDown('fast', function() {
				setTimeout(function() {
					$( "#message" ).slideUp('fast');
				}, timeOut );
			});
		},

		//	Adds leading zeros to numbers to pad them to a given length
		leadingZeros: function(num, totalChars, padWith) {
			num = num + "";
			padWith = (padWith) ? padWith : "0";
			if (num.length < totalChars) {
				while (num.length < totalChars) {
					num = padWith + num;
				}
			} else {}

			if (num.length > totalChars) { //if padWith was a multiple character string and num was overpadded
				num = num.substring((num.length - totalChars), totalChars);
			} else {}

			return num;
		},

		// Simple password suggestion function, useful for creating secure passwords in forms
		suggestPassword: function(fieldID) {
			var pwchars = new Array("abcdefhjmnpqrstuvwxyz","ABCDEFGHJKLMNPQRSTUVWYXZ","23456789","|!@#$%&*\/=?;:\-_+~^}][");
			var passwordlength = 3;    // but x3  do we want that to be dynamic?  no, keep it simple :)
			var passwd = document.getElementById(fieldID);
			passwd.value = '';

			for (j = 0; j < 4; j++){
				for ( i = 0; i < passwordlength; i++ ) {
					passwd.value += pwchars[j].charAt( Math.floor( Math.random() * pwchars[j].length ) )
				}
			}
			return passwd.value;
		},
		// why is this underscore and the others camelCase!!?
		number_format: function (number, decimals, dec_point, thousands_sep) {
			number = (number + '').replace(/[^0-9+-Ee.]/g, '');
			var n = !isFinite(+number) ? 0 : +number,
					prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
					sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
					dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
					s = '',
					toFixedFix = function (n, prec) {
							var k = Math.pow(10, prec);
							return '' + Math.round(n * k) / k;
					};
			// Fix for IE parseFloat(0.55).toFixed(0) = 0;
			s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
			if (s[0].length > 3) {
					s[0] = s[0].replace(/B(?=(?:d{3})+(?!d))/g, sep);
			}
			if ((s[1] || '').length < prec) {
					s[1] = s[1] || '';
					s[1] += new Array(prec - s[1].length + 1).join('0');
			}
			return s.join(dec);
		},

		addCommas: function (nStr) {
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		},

		isNumber: function(val) {
			var parm = '0123456789';
			for (i=0; i<parm.length; i++) {
				if (val.indexOf(parm.charAt(i),0) == -1) return false;
			}
			return true;
		},

		// safe way to use console log without crashing IE
		log:function(log){
			if(console)
				console.log(log);
		},

		// Use this to update a section of the page
		// by passing in its id.
		// If you want to, you can specify a function to run after each page section
		// has been updated
		// elementIds split with commas (no space)
		ajaxReplace:function(elementIds, afterFunction){
			nii.ajaxLoadReplace(window.location.href, elementIds, afterFunction)
		},

		// Load the url and extract matched ids defined by replaceIds, then replace
		// these new elements
		// @url url to load by ajax
		// @replaceIds defined like 'my-id-1,my-id-2' note no space between commas 
		// and no css selector hash
		ajaxLoadReplace:function(url, replaceIds, afterFunction){
			// Changes the string to an array
			var elements = replaceIds.split(',');
			$.get(url, function(data){
				// need to ensure there is one root node. Otherwise jquery can not always parse html properly
				// so surround response html with a div
				var $data = $('<div>' + data + '</div>');
				// Loops through the elements updating them
				$.each(elements, function(index, value) {
					var updateId = '#'+value;
					$(updateId).replaceWith($(updateId, $data));

					if(_.isFunction(afterFunction)){
						afterFunction(this, $data);
					}
				});
			});
		},

		/**
		 * converts a javascript date object to a mysql date string
		 * @param date string mysql date string YYYY-MM-DD
		 */
		dateToMysql:function(date){
			if(_.isUndefined(date) || _.isNull(date))
				return false;
			return date.getFullYear() + '-' +
			(date.getMonth() < 9 ? '0' : '') + (date.getMonth()+1) + '-' +
			(date.getDate() < 10 ? '0' : '') + date.getDate();
		},
		/**
		 * converts a mysql date string to a javascript date object
		 * @param date string mysql date string YYYY-MM-DD
		 */
		mysqlToDate:function(date){
			var t = date.split(/[- :]/);
			// Apply each element to the Date function
			return new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
		}
	}
})();

/**
 * In-Field Label jQuery Plugin
 * http://fuelyourcoding.com/scripts/infield.html
 *
 * Copyright (c) 2009 Doug Neiner
 * Dual licensed under the MIT and GPL licenses.
 * Uses the same license as jQuery, see:
 * http://docs.jquery.com/License
 */
(function(d){d.InFieldLabels=function(e,b,f){var a=this;a.$label=d(e);a.label=e;a.$field=d(b);a.field=b;a.$label.data("InFieldLabels",a);a.showing=true;a.init=function(){a.options=d.extend({},d.InFieldLabels.defaultOptions,f);if(a.$field.val()!==""){a.$label.hide();a.showing=false}a.$field.focus(function(){a.fadeOnFocus()}).blur(function(){a.checkForEmpty(true)}).bind("keydown.infieldlabel",function(c){a.hideOnChange(c)}).bind("paste",function(){a.setOpacity(0)}).change(function(){a.checkForEmpty()}).bind("onPropertyChange",
function(){a.checkForEmpty()})};a.fadeOnFocus=function(){a.showing&&a.setOpacity(a.options.fadeOpacity)};a.setOpacity=function(c){a.$label.stop().animate({opacity:c},a.options.fadeDuration);a.showing=c>0};a.checkForEmpty=function(c){if(a.$field.val()===""){a.prepForShow();a.setOpacity(c?1:a.options.fadeOpacity)}else a.setOpacity(0)};a.prepForShow=function(){if(!a.showing){a.$label.css({opacity:0}).show();a.$field.bind("keydown.infieldlabel",function(c){a.hideOnChange(c)})}};a.hideOnChange=function(c){if(!(c.keyCode===
16||c.keyCode===9)){if(a.showing){a.$label.hide();a.showing=false}a.$field.unbind("keydown.infieldlabel")}};a.init()};d.InFieldLabels.defaultOptions={fadeOpacity:0.5,fadeDuration:300};d.fn.inFieldLabels=function(e){return this.each(function(){var b=d(this).attr("for");if(b){b=d("input#"+b+"[type='text'],input#"+b+"[type='search'],input#"+b+"[type='tel'],input#"+b+"[type='url'],input#"+b+"[type='email'],input#"+b+"[type='password'],textarea#"+b);b.length!==0&&new d.InFieldLabels(this,b[0],e)}})}})(jQuery);

/**
 * Nii jQuery Plugin
 * Handy setup, initialisation and other nii related goodies.
 *
 */
(function($) {
	$.fn.nii = {
		/**
		 * applies twitter tooltip plugin to any element with a rel="tooltip" tag
		 */
		tooltip:function(){
			$('*[rel="tooltip"]').tooltip();
		},
		
		/**
		 * initialise form elements
		 */
		form:function(){
			// focus 
			$('.niiform').unbind();
			$('body').delegate(':input','mouseover.niiform',function(){$(this).closest(".field").addClass("over");});
			$('body').delegate(':input','mouseout.niiform',function(){$(this).closest(".field").removeClass("over");});
			$('body').delegate(':input','focusin.niiform',function(){$(this).closest(".field").addClass("focus");});
			$('body').delegate(':input','focusout.niiform',function(){$(this).closest(".field").removeClass("focus");});
			$('body').delegate('.input','click.niiform',function(e){
				var $input = $(e.target).children().eq(0);
				if($input.is(':input')){$input.focus();}
			});
			$('body').delegate('label[for]','mouseover.niiform',function(){
				// find associated form element
				var $el = $('#'+$(this).attr('for'))
				if ($el.length){
					var $in = $el.closest('.field');
					if($in.length)
						$in.addClass('over');
				}
			});
			$('body').delegate('label[for]','mouseout.niiform',function(){
				// find associated form element
				var $el = $('#'+$(this).attr('for'))
				if ($el.length){
					var $in = $el.closest('.field');
					if($in.length)
						$in.removeClass('over');
				}
			});
			$('.inFieldLabel').inFieldLabels({fadeDuration:0});
		}
		
	};
})(jQuery);

jQuery(function($){
	// add tipsy
	$.fn.nii.tooltip();
	// form stuff
	$.fn.nii.form();
});


// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

/**
 * Overrides and adds functions to the standard yiiactiveform js 
 * 
 * 
 */
(function()
{
	/**
	 * makes drawing an individual form fields error message slightly more sexy
	 */
	if(jQuery.fn.yiiactiveform != undefined){
		jQuery.fn.yiiactiveform.updateInput = function(attribute, messages, form) 
		{
			attribute.status = 1;
			var hasError = messages!=null && $.isArray(messages[attribute.id]) && messages[attribute.id].length>0;
			var $error = $('#'+attribute.errorID, form);
			var $container = $.fn.yiiactiveform.getInputContainer(attribute, form);
			$container.removeClass(attribute.validatingCssClass)
					.removeClass(attribute.errorCssClass)
					.removeClass(attribute.successCssClass);

			if(hasError) {
					$error.html(messages[attribute.id][0]).hide().fadeIn('slow');
					$container.addClass(attribute.errorCssClass);
			}
			else if(attribute.enableAjaxValidation || attribute.clientValidation) {
					$container.addClass(attribute.successCssClass);
			}
			if(!attribute.hideErrorMessage)
					$error.toggle(hasError);

			attribute.value = $('#'+attribute.inputID, form).val();

			return hasError;

		}
			
		/**
		 * @param form the string form id or jquery form element
		 * @param options options object
		 * - success    : function to call when the form successfully validates
		 * - error      : the ajax error callback
		 * - attributes : array of element id's to validate (defaults to undefined, meaning validate all attributes)
		 */
		$.fn.yiiactiveform.doValidate = function(form, options){
	
			if(options == undefined)
				options = {};
			
			var $form = $(form);
			var settings = $form.data('settings');
			var messages = {};
			var needAjaxValidation = false;
			$.each(settings.attributes, function(){
				var msg = [];
				if (this.clientValidation != undefined) {
					var value = $('#'+this.inputID, $form).val();
					this.clientValidation(value, msg, this);
					if (msg.length) {
						if (options.attributes == undefined)
							messages[this.id] = msg;
						else
							// options.attributes have been defined so only add a message to the specific fields 
							// we want to validate
							if ($.inArray(this.id,options.attributes) != -1)
								messages[this.id] = msg;
					}
				}
				if (this.enableAjaxValidation)
					needAjaxValidation = true;
			});
			
			if(needAjaxValidation){
				$.ajax({
					url : settings.validationUrl,
					type : $form.attr('method'),
					data : $form.serialize()+'&'+settings.ajaxVar+'='+$form.attr('id'),
					dataType : 'json',
					success : function(data) {
						if (data != null && typeof data == 'object') {
							$.each(settings.attributes, function() {
								if (!this.enableAjaxValidation)
									delete data[this.id];
								// remove the attributes that we do not want to redraw
								if(options.attributes != undefined && ($.inArray(this.id, options.attributes) == -1))
									delete data[this.id];
								
							});
							$.fn.yiiactiveform.drawValidation($form, options.attributes, $.extend({}, messages, data));
						}

						// we only call the success callback if the form is valid!
						if (data.length==0 && options.success!=undefined)
							options.success();

					},
					error : function() {
						if (options.error!=undefined) {
							options.error();
						}
					}
				});
			}else{
				$.fn.yiiactiveform.drawValidation($form, options.attributes, messages);
				// we only call the success callback if the form is valid!
				if (data != undefined && data.length==0 && options.success!=undefined)
					options.success();
			}
		}
		
		/**
		 * The form to draw the validation on.
		 * Draws the validatiion for each field
		 * 
		 * @param $form the jquery form object
		 * @param attributes an array of field id's to draw the validation for, leave undefined for all.
		 * @param data the json validation result (as returned by CActiveForm::validate)
		 * 
		 */
		$.fn.yiiactiveform.drawValidation = function($form, attributes, data){
			var hasError=false;
			$.each($form.data('settings').attributes, function(){
				if(attributes != undefined){
					if ($.inArray(this.id, attributes) != -1) {
						hasError = $.fn.yiiactiveform.updateInput(this, data, $form) || hasError;
						if(this.afterValidateAttribute!=undefined) {
							afterValidateAttribute($form,this,data,hasError);
						}
					}
				} else {
					hasError = $.fn.yiiactiveform.updateInput(this, data, $form) || hasError;
					if(this.afterValidateAttribute!=undefined) {
						afterValidateAttribute($form,this,data,hasError);
					}
				}
			});
		}
	}
})();
