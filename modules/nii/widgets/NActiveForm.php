<?php

/**
 * NActiveForm class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
class NActiveForm extends CActiveForm
{

    public $enableAjaxValidation = true;
    public $enableClientValidation = true;
    public $errorMessageCssClass = 'errorMessage help-inline';
    public $clientOptions = array('validateOnSubmit'=>true);

    public function init()
    {
        // add small script to add focus class to parent .field element
        if (!isset($this->clientOptions['inputContainer']))
            $this->clientOptions['inputContainer'] = '.control-group';
        if (!isset($this->htmlOptions['class']))
            $this->htmlOptions['class'] = 'form-horizontal';
        parent::init();
    }

    public function markdown()
    {
        // todo add nii markdown input widget
    }

    public function wysiwygRedactor($model, $attribute, $options=null)
    {
        // todo
    }

    /**
     * 
     * @param CActiveRecord $model
     * @param string $attribute
     * @param array $config ckeditor config options
     * see nii.widgets.editor.CKkceditor
     * @see NActiveRecord::wysiwygToolbar
     */
    public function wysiwyg($model, $attribute, $config=array())
    {
        $widget = $this->createWidget('nii.widgets.wysiwyg.ckeditor.Editor');
        $configArray = CMap::mergeArray(array(
            'toolbar'=>$widget->wysiwygToolbar(),
            'removePlugins'=>'elementspath',
            'skin'=>'moono',
            'toolbarCanCollapse'=>false,
            'resize_enabled'=>true,
            'bodyId'=>'fullDescription',
            'pasteFromWordRemoveFontStyles'=>true,
            'pasteFromWordRemoveStyles'=>true,
        ), $config);

        $this->widget('nii.widgets.wysiwyg.ckeditor.Editor',array(
            "attribute"=>$attribute,
            'model'=>$model,
            'config'=>$configArray
        ));
    }

    /**
     * Renders an HTML label for a model attribute.
     * This method is a wrapper of {@link CHtml::activeLabelEx}.
     * Please check {@link CHtml::activeLabelEx} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated label tag
     */
    public function labelEx($model, $attribute, $htmlOptions=array())
    {
        if (empty($htmlOptions)) {
            $htmlOptions = array('class' => 'control-label');
        }
        return parent::labelEx($model, $attribute, $htmlOptions);
    }

    /**
     * Begin a field html block.  Must call $form->endField() after as this only creates the start divs etc
     * and does not close the tags
     * @param object $model
     * @param string $attribute
     * @param boolean $error show error class
     * @param string $controlClass
     * @return string html
     */
    public function beginField($model, $attribute, $error=true, $controlClass='')
    {
        $errorClass = ($error && $model->hasErrors($attribute)) ? ' error' : '';
        $return = '<div class="control-group' . $errorClass . '">';
        $return .= $this->labelEx($model, $attribute);
        $return .= "<div class=\"controls $controlClass\">";
        return $return;
    }

    /**
     * Draws the end information for the field, closes html divs etc
     * @see self::beginField
     * @param object $model  the model to get the information from
     * @param string $attribute  the attribute in the model
     * @param array $hint  a hint (help) field for the field
     */
    public function endField($model=null, $attribute=null, $hint=null)
    {
        $return = '';
        if ($hint) {
            if (isset($hint['html']))
                $return .= "<span class='help-inline'>" . $hint['html'] . "</span>";
            else
                $return .= "<a href='#' class='$hint[class]' title='$hint[title]'>$hint[label]</a>";
        }
        if ($model && $attribute)
            $return .= $this->error($model, $attribute);
        $return .= '</div></div>';
        return $return;
    }

    /**
     * Shorthand function to draw a form element
     * @param CActiveRecord $model
     * @param string $attribute
     * @param string $type the function name in NActiveForm or CActiveForm that renders the field should have the interface
     * function fieldName($model, $attribute $htmloptions=array()) existing functions:
     * - urlField
     * - emailField
     * - numberField
     * - rangeField
     * - dateField
     * - textField
     * - hiddenField
     * - passwordField
     * - textArea
     * - fileField
     * - radioButton
     * - checkBox
     * - dropDownList
     * - checkBoxList
     * - radioButtonList
     * - listBox
     * @param type $data
     * @param type $htmlOptions
     * @return type
     */
    public function field($model, $attribute, $type='textField', $data=null, $htmlOptions=array(), $showErrors=true)
    {
        return $this->editField($model, $attribute, $type, $data, $htmlOptions, $showErrors);
    }

    /**
     * Provides a similar wrapper as the field function but draws a CInputWidget instead of a standard element
     * @param object $model
     * @param string $attribute
     * @param string $widgetPath e.g. crm.widgets.form.ContactDropDown
     */
    public function fieldWidget($model, $attribute, $widgetPath, $widgetOptions=array(), $htmlOptions=array(), $showErrors=true)
    {
        return $this->editField($model, $attribute, $widgetPath, $widgetOptions, $htmlOptions, $showErrors);
    }

    /**
     * Creates a form field using a default structure for labels, field and error message
     * @param CModel $model the data model
     * @param string $attribute The attribute to display
     * @param string $type The type of field you want i.e textField, dropDownList can also be a widget path (widget must inherit fdrom CInputWidget)
     * @param array $data Required for dropDownList/radioButtonList type or if the $type is a widget path, the data is an array of options passed to the widget
     * @param array $htmlOptions  Any optional field or HTML options to be applied
     *   Field options are additional elements to be rendered with the field
     *   HTML options are passed in on to Yii and are anything except the field options listed here
     *   Field options are
     *     'control-class' - additional classes to add to the div.control div
     *     'hint' - a hint field that provides extra help information for the field. Pass in
     *        - 'class' - the css classes for the hint
     *        - 'label' - the displayed label for the hint
     *        - 'title' - the text for the hint to be shown on hover
     *       or
     *        - html - the html to be displayed for the hint
     *	   'helpInline' - text to display as a hint inline using bootstrap styles
     *     'surround' - creates a surrounding div to the input - set to the class for the div
     *
     * @return string The HTML to be rendered
     */
    public function editField($model, $attribute, $type='textField', $data=null, $htmlOptions=array(), $showErrors=true)
    {
        $controlClass = $hint = NData::arrayRemove('control-class', $htmlOptions);
        $controlGroupClass = $hint = NData::arrayRemove('control-group-class', $htmlOptions);
        $hint = NData::arrayRemove('hint', $htmlOptions);
        $helpInline = NData::arrayRemove('helpInline', $htmlOptions);
        $surround = NData::arrayRemove('surround', $htmlOptions);
        $fieldPrefix = NData::arrayRemove('fieldPrefix', $htmlOptions);
        if ($fieldPrefix) $surround .= ' input-prepend';
        $fieldSuffix = NData::arrayRemove('fieldSuffix', $htmlOptions);
        if ($fieldSuffix) $surround .= ' input-append';

        $return = $this->beginField($model, $attribute, $showErrors, $controlClass, $controlGroupClass);
        if ($surround !== null)
            $return .= "<div class='$surround'>";
        // Dropdown, radio button list, checkbox list
        if ($type == 'dropDownList' || $type == 'radioButtonList' || $type == 'checkBoxList') {
            $return .= $this->$type($model, $attribute, $data, $htmlOptions);
            // Everything else
        } else {
            if ($fieldPrefix)
                $return .= '<span class="add-on">'.$fieldPrefix.'</span>';
            if (method_exists($this, $type)) {
                $return .= $this->$type($model, $attribute, $htmlOptions);
            } else {
                $default = array('model'=>$model, 'attribute'=>$attribute);
                $return .= $this->widget($type, CMap::mergeArray($default, $data), true);
            }
            if ($fieldSuffix)
                $return .= '<span class="add-on">'.$fieldSuffix.'</span>';
        }
        if ($surround !== null)
            $return .= '</div>';

        if ($helpInline)
            $return .= '<span class="help-inline">'.$helpInline.'</span>';

        $return .= $this->endField($model, $attribute, $hint);
        return $return;
    }

    /**
     * Creates a field using a default structure for labels and text
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $data When the attribute is a represented by a array lookup i.e select boxes
     * @return string The HTML output
     */
    public function viewField($model, $attribute, $data=array())
    {
        $return = $this->beginField($model, $attribute, false);
        $value = CHtml::resolveValue($model, $attribute);
        $return .= '<span class="view-field">' . (array_key_exists($value, $data) ? $data[$value] : $value) . '</span>';
        $return .= $this->endField();
        return $return;
    }

    /**
     * Creates a checkbox field
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @return string HTML output
     */
    public function checkBoxField($model, $attribute)
    {
        return $this->labelEx($model, $attribute, array(
            'label' => $this->checkBox($model, $attribute),
            'class' => 'checkbox',
        ));
    }

    /**
     * Creates a form date field
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $options Additional options to be passed into the datePicker widget
     * @return CInputWidget
     */
    public function dateField($model, $attribute, $options=array())
    {
        $options['model'] = $model;
        $options['attribute'] = $attribute;
        return $this->widget('nii.widgets.forms.DateInput', $options, true);
    }

    public function autoComplete($model, $attribute, $source, $alt_attribute=null, $options=array())
    {
        $options['source'] = $source;
        $options['name'] = $alt_attribute ? $alt_attribute : $attribute;
        $options['value'] = $alt_attribute ? CHtml::resolveValue($model, $alt_attribute) : CHtml::resolveValue($model, $attribute);
        if(!isset($options['options'])) {
            $hidden_field_id = get_class($model).'_'.$attribute;
            $options['options'] = array(
                'showAnim' => 'fold',
                'change' => 'js:function(event, ui) {
                    if (ui.item)
                        $("#'.$hidden_field_id.'").val(ui.item.id);
                    else
                        $("#'.$hidden_field_id.'").val(null);
        }',
        );
        }
        $return = $this->widget('zii.widgets.jui.CJuiAutoComplete', $options, true);
        if($alt_attribute)
            $return .= $this->hiddenField($model, $attribute);
        return $return;
    }

}
