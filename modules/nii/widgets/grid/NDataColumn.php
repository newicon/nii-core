<?php

Yii::import('zii.widgets.grid.CDataColumn');
Yii::import('bootstrap.widgets.TbDataColumn');
/**
 * Description of NDataColumn
 *
 * @author robinwilliams
 */
class NDataColumn extends TbDataColumn {

	public $export;
	public $exportValue;

	public function renderDataCsvCell($row)
	{
		$data=$this->grid->dataProvider->data[$row];
		if($this->exportValue!==null)
			$value=$this->evaluateExpression($this->exportValue,array('data'=>$data,'row'=>$row));
		else if($this->name!==null)
			$value=CHtml::value($data,$this->name);
		return $value===null ? $this->grid->nullDisplay : $this->grid->getFormatter()->format($value,$this->type);
	}
	
	public function renderDataExcelCell($row)
	{
		$data=$this->grid->dataProvider->data[$row];
		if($this->exportValue!==null)
			$value=$this->evaluateExpression($this->exportValue,array('data'=>$data,'row'=>$row));
		else if($this->name!==null)
			$value=CHtml::value($data,$this->name);
		return $value===null ? $this->grid->nullDisplay : $value;
	}
	
		/**
	 * Renders the filter cell content.
	 * This method will render the {@link filter} as is if it is a string.
	 * If {@link filter} is an array, it is assumed to be a list of options, and a dropdown selector will be rendered.
	 * Otherwise if {@link filter} is not false, a text field is rendered.
	 * @since 1.1.1
	 */
	protected function renderFilterCellContent()
	{
		if(is_string($this->filter))
			echo $this->filter;
		else if($this->filter!==false && $this->grid->filter!==null && $this->name!==null && strpos($this->name,'.')===false)
		{
			if(is_array($this->filter))
				echo NHtml::activeChznDropDown($this->grid->filter, $this->name, $this->filter, array('prompt'=>'','style'=>'width:100%','class'=>'input-block-level'));
			else if($this->filter===null)
				echo CHtml::activeTextField($this->grid->filter, $this->name, array('id'=>false,'style'=>'width:100%','class'=>'input-block-level'));
		}
		else
			parent::renderFilterCellContent();
	}
	
	public function renderDataOdsCell($row)
	{
		$data=$this->grid->dataProvider->data[$row];
		if($this->exportValue!==null)
			$value=$this->evaluateExpression($this->exportValue,array('data'=>$data,'row'=>$row));
		else if($this->name!==null)
			$value=CHtml::value($data,$this->name);
		return $value===null ? $this->grid->nullDisplay : str_replace('&','&amp;',$this->grid->getFormatter()->format($value,$this->type));
	}
	
	
	/**
	 * Renders the data cell content.
	 * This method evaluates {@link value} or {@link name} and renders the result.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data associated with the row
	 */
	protected function renderDataCellContent($row, $data)
	{
		if($this->value!==null)
			$value=$this->evaluateExpression($this->value,array('data'=>$data,'row'=>$row));
		else if($this->name!==null)
			$value=CHtml::value($data,$this->name);
		if($value===null){
			echo $this->grid->nullDisplay;
		} else {
			// THIS BREAKS THE PERMISSION GRID, NEEDS MORE WORK FOR HIGHLIGHTING SEARCH RESULTS
			// SO: should be fixed now.
			if($this->type == 'text' && $this->grid->filter != null ) {
				// get search result
				$search = $this->grid->filter->{$this->name};
				echo NHtml::hilightText($this->grid->getFormatter()->format($value,$this->type), $search);
			} else			
				echo $this->grid->getFormatter()->format($value,$this->type);
			// TODO: implement hilighting for other column data types. e.g. raw
		}
	}
	
}