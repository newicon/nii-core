<?php

/**
 * NWebcamAction class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * NWebcamSave saves a webcam image into the filemanager
 * and echos out a json string in the format
 * url: a string url to the image
 * fileId: the file id assigned by the filemanager.  This id should be saved to retrieve the url at a later date.
 * remeber: the url is only temporary the fileManager may update the url of an image at any time. the fileId is constant and never changes.
 * @author Steven O'Brien <steven.obrien@newicon.net>
 */
class NWebcamSave extends CAction
{
	public function run()
	{
		$webcamImg = file_get_contents('php://input');
		$fileId = NFileManager::get()->addFile('webcam.jpg', $webcamImg);
		$ret = array(
			'url'=>NFileManager::get()->getUrl($fileId, 'webcam'),
			'fileId'=>$fileId
		);
		echo CJSON::encode($ret);
		Yii::app()->end();
	}
}