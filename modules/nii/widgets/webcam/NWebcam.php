<?php
/**
 * NWebcam class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * NWebcam widget wrapper for jpegcam
 *
 * @author 
 */
class NWebcam extends NWidget
{
	/**
	 * action to post the captured image to
	 * defaults to /nii/widget/webcam
	 * @var mixed route 
	 */
	public $action = array('/nii/widget/webcam');
	
	/**
	 * 1 - 100 jpeg capture quality
	 * @var int 
	 */
	public $quality = 90;
	
	/**
	 * width of the camera capture area image
	 * @var int 
	 */
	public $width = 320;
	
	/**
	 * height of the camera capture area image
	 * @var int 
	 */
	public $height = 240;
	
	/**
	 * runs the widget 
	 */
	public function run()
	{
		$assetUrl = $this->getAssetsUrl();
		$cs = Yii::app()->getClientScript();
		
        $cs->registerScriptFile($assetUrl.'/webcam.js', CClientScript::POS_BEGIN);
		// default javascript settings
		$js  = "webcam.swf_url = '$assetUrl/webcam.swf';";
        $js .= "webcam.shutter_url = '$assetUrl/shutter.mp3';";
		$js .= "webcam.set_api_url('".NHtml::url($this->action)."');";
		$js .= "webcam.set_quality({$this->quality});";
		
		$cs->registerScript('NWebcam', $js, CClientScript::POS_BEGIN);
		
		echo '
			<!-- Next, write the movie to the page at 320x240 -->
			<div class="webcam-container" style="width:'.$this->width.'px;height:'.$this->height.'px;"></div>';
		
		$js2 = '$(".webcam-container").html(webcam.get_html('.$this->width.', '.$this->height.'))';
		$cs->registerScript('NWebcam', $js2);
		
	}
}