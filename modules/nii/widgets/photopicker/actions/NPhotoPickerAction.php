<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
Class NPhotoPickerAction extends CAction
{
	public function run(){
		// we must resize and crop the image, and return the user image id
		// save the new cropped image in the user's profile image (image_id) field
		// we also need to save the original image and manipulations so it can be edited
		$file = NFileManager::get()->getFile($_POST['id']);
		
		$image = Yii::app()->image->load($file->getPath());
		
		$image->resize($_POST['width'], $_POST['height'], Image::NONE)
			->crop($_POST['cropWidth'], $_POST['cropHeight'], abs($_POST['cropTop']), abs($_POST['cropLeft']));
		
		$img = $image->generate();
		
		// create a new file manager managed image from the cropped image
		$id = NFileManager::get()->addFile('ppicker_'.$file->original_name, $img);
		
		$data = array(
			'id'=>$_POST['id'],
			'sliderValue'=>$_POST['sliderValue'],
			'url'=>NFileManager::get()->getUrl($_POST['id'], 'original-profile-image'),
			'width'=>$_POST['width'],
			'height'=>$_POST['height'],
			'top'=>$_POST['cropTop'],
			'left'=>$_POST['cropLeft'],
		);
		
		echo CJSON::encode(array(
			'cropImage'=>array(
				'url'=>NFileManager::get()->getUrl($id),
				'id'=>$id
			),
			'originalImage'=>$data
		));
	}
}