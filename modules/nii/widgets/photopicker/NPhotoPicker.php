<?php

/**
 * PhotoPicker widget class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Photopicker widget enables a user to edit and upload a profile image
 * The result is a file manager id stored in the configured attribute.
 * 
 * To use this widget you must have two attributes in the model 
 *
 * @author 
 */
class NPhotoPicker extends NInputWidget
{
	public $attributeData = 'image_data';

	public function run(){
		
		$url = $this->getAssetsUrl();
		
		if(!$this->hasModel())
			throw new CException('You must specify a model for the photopicker widget');
		
		$id = 'photopicker-' . CHtml::activeId($this->model, $this->attribute);
		
		Yii::app()->clientScript->registerScript($id, "$('#$id').photopicker();", CClientScript::POS_READY);
		
		$this->widget('nii.widgets.plupload.PluploadWidget', array('config' => array('runtimes' => 'html5,flash,silverlight')));
		
		$this->render('photopicker', array(
			'id'=>$id,
			'webcam_width'=>225,
			'webcam_height'=>168
		));
	}
}