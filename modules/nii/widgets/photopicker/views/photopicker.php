<?php
/**
 * photopicker javascript and templates
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>


<div id="<?php echo $id; ?>" class="photopicker">
	<div class="photopicker-thumb">

	</div>
	<?php echo CHtml::activeHiddenField($this->model, $this->attribute, array('class'=>'image-id')); ?>
	<?php echo CHtml::activeHiddenField($this->model, $this->attributeData, array('class'=>'image-data')); ?>
</div>

<script id="template-photo-picker-thumb" type="text/template">
	<?php if (empty($this->model->{$this->attribute})): ?>
		<img id="<?php echo $id; ?>-image" src="<?php echo $this->getAssetsUrl() . '/add-photo.png' ?>" />
	<?php else: ?>
		<img id="<?php echo $id; ?>-image" src="<?php echo NHtml::urlFile($this->model->{$this->attribute}, 'profile-image'); ?>" />
	<?php endif; ?>
</script>

<script id="template-photo-picker-pop" type="text/template">
	<style>
		.user-photo-picker{border-radius:10px;border:1px solid #858585;display:none;background-color:#fff;width:245px;position:absolute;z-index:1055;box-shadow:2px 2px 8px #333;}
		.pop-image{background:url("<?php echo $this->getAssetsUrl() . '/pop-arrow-left.png' ?>") no-repeat;width:19px;height:37px;left:-18px;top:10px;}
		.ui-slider-horizontal{height:5px;}
		.ui-slider{border:1px solid #bbb;background: rgb(187,187,187); /* Old browsers */background: -moz-linear-gradient(top,  rgba(187,187,187,1) 0%, rgba(240,240,240,1) 100%); /* FF3.6+ */background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(187,187,187,1)), color-stop(100%,rgba(240,240,240,1))); /* Chrome,Safari4+ */background: -webkit-linear-gradient(top,  rgba(187,187,187,1) 0%,rgba(240,240,240,1) 100%); /* Chrome10+,Safari5.1+ */background: -o-linear-gradient(top,  rgba(187,187,187,1) 0%,rgba(240,240,240,1) 100%); /* Opera 11.10+ */background: -ms-linear-gradient(top,  rgba(187,187,187,1) 0%,rgba(240,240,240,1) 100%); /* IE10+ */background: linear-gradient(top,  rgba(187,187,187,1) 0%,rgba(240,240,240,1) 100%); /* W3C */filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bbbbbb', endColorstr='#f0f0f0',GradientType=0 ); /* IE6-9 */}
		.ui-slider .ui-slider-handle{border-radius:8px;width:13px;height:13px;border:1px solid #aaa;background: rgb(255,255,255); /* Old browsers */background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(204,204,204,1) 100%); /* FF3.6+ */background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(100%,rgba(204,204,204,1))); /* Chrome,Safari4+ */background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(204,204,204,1) 100%); /* Chrome10+,Safari5.1+ */background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(204,204,204,1) 100%); /* Opera 11.10+ */background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(204,204,204,1) 100%); /* IE10+ */background: linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(204,204,204,1) 100%); /* W3C */filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#cccccc',GradientType=0 ); /* IE6-9 */}
		.ui-slider-horizontal .ui-slider-handle{top:-5px;}
		.user-photo-picker .commandBtns{}
		.drop-thumb img {cursor:pointer;}
	</style>
	<div class="pop-image" style="position:absolute;bottom:-11px;"></div>
	
	<div style="margin:10px;">

		<div id="form-norm">
			<div id="user-photo-picker-drop" class="thumbnail mbm" >
				<div class="drop-thumb" style="width:<%= width %>px;height:<%= height %>px;overflow:hidden;">
					<div style="background-color:#eee;width:<%= width-10 %>px;height:<%= height-10 %>px;padding:10px 0px 0px 10px;position:absolute;">
						<div class="drop-here-hint" style="border:3px dashed #ccc;width:<%= (width-20-6) %>px;height:<%= (height-20-6) %>px;border-radius:10px;text-align:center;">
							<div style="color:#aaa;text-align:center;font-size:20px;margin-top:20px;line-height:30px;">Drag<br/>Photo<br/>Here</div>
						</div>
						<div class="loading-spinner" style="display:none;width:<%= (width-20) %>px;height:<%= (height-20) %>px;"></div>
					</div>
					<img style="max-width:none;" />
				</div>
			</div>
			
			<div style="height:20px;position:relative;">
				<img class="slidericon" style="position:absolute;width:11px;left:2px;top:-1px;" src="<?php echo $this->getAssetsUrl() . '/picimage.png' ?>" />
				<div class="mbm slider" style="position:absolute;width:175px;left:21px;"></div>
				<img class="slidericon" style="position:absolute;right:2px;top:-3px;" src="<?php echo $this->getAssetsUrl() . '/picimage.png' ?>" />
			</div>	
			
			<div class="commandBtns">
				<a href="#" rel="tooltip" data-placement="bottom" title="Take a photo using your web cam" class="btn mrm webcam"><i class="icon-facetime-video"></i></a> 
				<a id="pickfiles" rel="tooltip" data-placement="bottom" title="Choose a photo from your computer" href="#" class="btn mrm" style="width:60px;">Choose...</a> 
				<a href="#" class="btn btn-primary done" style="width:55px;">Done</a>
			</div>
		</div>
		
		
		<div id="form-webcam" style="display:none;">
			<?php $this->widget('nii.widgets.webcam.NWebcam',array('width'=>$webcam_width,'height'=>$webcam_height)); ?>

			<!-- Some buttons for controlling things -->
			<div class="commandBtns">
				<form class="man mtm">
					<a href="#" rel="tooltip" data-placement="bottom" title="Choose a photo from your computer" class="btn mrm picture"><i class="icon-picture"></i></a> 
					<a class="btn" rel="tooltip" data-placement="bottom" title="Configure web cam settings" onClick="webcam.configure(); return false;" ><i class="icon-cog"></i></a>
					<a rel="tooltip" data-placement="bottom" title="capture webcam image" class="webcam-capture btn btn-inverse"><i class="icon-camera icon-white"></i> Capture</a>
					<a style="display:none;" class="webcam-save btn btn-primary">Done</a>
				</form>
			</div>
			<div id="upload-results">

			</div>
		</div>
		
	</div>
</script>

<script>
	

(function($){

	var photoPickerPop = Backbone.View.extend({
		className:'user-photo-picker',
		id:'user-photo-picker',
		template:_.template($('#template-photo-picker-pop').html()),
		uploader:null,
		initialize:function(){
			webcam.set_hook('onComplete', _.bind(this.webcamUploadComplete,this));
		},
		events:{
			'click .done':'done',
			'click .webcam-save':'webcamSave',
			'click .webcam-capture':'webcamCapture',
			'click .webcam':'setModeWebcam',
			'click .picture':'setModePicture'
		},
		open:function(){
			this.render();
			
			this.model.set({originalImage: $.parseJSON($("#<?php echo CHtml::activeId($this->model, $this->attributeData); ?>").val())})
			
			nii.log(this.model.get('originalImage'));
			
			if(!_.isNull(this.model.get('originalImage'))){
				this.setImage(this.model.get('originalImage').url, _.bind(function(){
					$(this.img).width(this.model.get('originalImage').width)
						.css('left',this.model.get('originalImage').left+'px')
						.css('top',this.model.get('originalImage').top+'px');
						
					this.model.set({id : this.model.get('originalImage').id});
					this.model.set({url : this.model.get('originalImage').url});
					
					this.$(".slider").slider("option", "value", this.model.get('originalImage').sliderValue);
				},this));
			}
		},
		render:function(){
			this.$el.html(this.template(this.model.toJSON()));
			this.$el.appendTo('body').show()
				.position({my:'left top',at:'right top', of:$('#<?php echo $id; ?>-image'),offset:'10 0'});
			$('<div id="user-photo-picker-bg" class="modal-backdrop fade in" style="opacity:0.3" ></div>').css('zIndex','1050')
				.appendTo('body').click(this.close);
			$(".webcam-container").html(webcam.get_html(<?php echo $webcam_width; ?>, <?php echo $webcam_height; ?>));
			$('#user-photo-picker-bg')
			$.fn.nii.tooltip();
			
			this.uploader = new plupload.Uploader({
				runtimes : "html5,flash",
				browse_button : "pickfiles",
				max_file_size : '10mb',
				drop_element : "user-photo-picker-drop",
				url : "<?php echo NHtml::url('nii/widget/plupload'); ?>",
				flash_swf_url:"<?php echo $this->createWidget('nii.widgets.plupload.PluploadWidget')->getAssetsUrl(); ?>/plupload.flash.swf",
				filters: [{
					title: "Image files",
					extensions: "jpg,jpeg,png,gif"
				}]
			});
			this.uploader.init();
			if(!this.uploader.features.dragdrop){
				alert('Your browser does not support drag and drop uploading.')
			}
			this.uploader.bind('FilesAdded',	_.bind(this.filesAdded, this));
			this.uploader.bind('UploadProgress',_.bind(this.uploadProgress,this));
			this.uploader.bind('FileUploaded',	_.bind(this.fileUploaded,this));
			this.uploader.bind('UploadComplete',_.bind(this.uploadComplete,this));
			this.uploader.bind('Error',			_.bind(this.error,this));
			
			this.$(".slider").slider();
			this.disable();
		},
		img:null,
		setImage:function(srcUrl, imgLoadedCallback){
			this.setModeLoaded();
			this.enable();
			this.img = $(new Image()).css('maxWidth','none');
			$('#user-photo-picker-drop img').remove()//fadeOut(function(){$(this).remove();});
			this.img.load(_.bind(function(){
				$('#user-photo-picker-drop .drop-thumb').append(this.img);
				this.img.hide().fadeIn();

				var width = this.img.width();
				var height = this.img.height();
				var minWidth = null;

				// calculate the maximum alloud width
				// calculate minimum width - (if landscape image then min width may be larger then the limit width)
				// calculate the minimum allowed width
				if(width > height){
					// lets limit the image by the height as this is the smallest dimension
					minWidth = this.model.get('height') * (width / height);

				} else if(height > width) {
					// limit by width
					minWidth = this.model.get('width');
				}

				// if the image is smaller than the container (limit width) then add some so it can be blown up
				if(this.srcMinWidth > width){
					width = minWidth + 100;
				}

				this.resize(minWidth);

				this.$('.slider').slider('option', {
					value:0,
					min:minWidth,
					max:width,
					slide: _.bind(function(event, ui) {
						this.resize(ui.value);
						this.positionFix(false);
					},this)
				})
				this.$('.slider').slider('enable');

				this.img.draggable({
					stop:_.bind(function(){
						this.positionFix();
					},this)
				})

				// further manipulations to be done by callback function
				if(_.isFunction(imgLoadedCallback))
					imgLoadedCallback.call();

			},this)).attr('src', srcUrl);

		},
		filesAdded:function(up, files){
			// remove multiple files so we only upload the last one
			if(up.files.length > 1)
				this.uploader.splice(0,up.files.length-1);
			if(up.files.length > 0) {
				this.uploader.start();
				this.setModeLoading();
			}
		},
		uploadProgress:function(up, file){
		},
		fileUploaded:function(up, file, info){
			var json = $.parseJSON(info.response);
			this.model.set({id:json.id, url:json.url})
			this.setImage(json.url);
		},
		uploadComplete:function(up, file, info){

		},
		error:function(up, err){
			alert(err.message + (err.file ? ", File: " + err.file.name : ""));
			up.refresh(); // Reposition Flash/Silverlight
		},
		resize:function(newImageWidth, focalLeft, focalTop){
			var img = this.img;
			var curWidth = img.width(),
				curHeight = img.height();
			var curTop = img.css('top').replace('px','');

			// get current left position
			var curLeft = img.css('left').replace('px','');

			// 1. get the current focal point in relation to the visible box
			focalLeft = _.isUndefined(focalLeft) ? this.model.get('width')/2 : focalLeft;
			focalTop = _.isUndefined(focalTop) ? this.model.get('height')/2 : focalTop;

			// 2. get the distance from the focal point to the left edge of the zoom image
			var focalLeftTotal = Math.abs(curLeft) + focalLeft;
			var focalTopTotal = Math.abs(curTop) + focalTop;

			// 3. divide the distance from the left to the focal point by the image width to get the zoomratio
			var zoomLeftRatio = Math.round(100 * focalLeftTotal / curWidth) / 100
			var zoomTopRatio  = Math.round(100 * focalTopTotal / curHeight) / 100

			// 4. get the zoom incrememnt size and multiply by the zoomratio
			// e.g. if an image focal ratio is a 3rd along the iomage and the increment size is 300 the left value needs to increae by 100
			img.width(newImageWidth);

			// set left value - zoom increment (new zoom width - current width)
			var incrementLeft = (newImageWidth - curWidth);
			var offsetIncrement = incrementLeft * zoomLeftRatio;
			var newLeft = parseInt(curLeft) - parseInt(offsetIncrement);
			img.css('left',newLeft+'px');

			// set top value
			var newHeight = img.height();
			var incrementTop = (newHeight - curHeight);
			var offsetIcrementTop = incrementTop * zoomTopRatio;
			var newTop = parseInt(curTop) - parseInt(offsetIcrementTop);
			img.css('top',newTop+'px');
		},
		positionFix:function(animate){
			var img = this.img;
			animate = _.isUndefined(animate) ? true : false;

			var left = img.css('left').replace('px','');
			var top = img.css('top').replace('px','');

			var leftMax = (this.model.get('width') - img.width());
			var topMax = -(img.height() - this.model.get('height'));

			var animateOptions = {queue:false, duration:'fast', easing:'swing'};
			if(left <= leftMax && leftMax<=0){
				animate ? img.animate({'left':leftMax+'px'},animateOptions) : img.css('left',leftMax+'px');
			}
			if(left > 0){
				animate ? img.animate({'left':'0px'},animateOptions) : img.css('left','0px');
			}
			if(top > 0){
				animate ? img.animate({'top':'0px'},animateOptions) : img.css('top','0px');
			}
			if(top < topMax /*&& topMax <= 0*/){
				animate ? img.animate({'top':topMax+'px'},animateOptions) : img.css('top',topMax+'px');
			}
		},
		enable:function(){
			this.$('.slider').slider('enable');
			$('.slidericon').fadeTo(0,1);
		},
		disable:function(){
			this.$('.slider').slider('disable');
			$('.slidericon').fadeTo(0,0.3);
		},
		close:function(){
			$('#user-photo-picker').fadeOut(function(){$(this).remove();});
			$('#user-photo-picker-bg').fadeOut(function(){$(this).remove();});
		},
		webcamSave:function(){
			$('#upload-results').html('<h1>Uploading...</h1>');
			webcam.upload();
		},
		webcamUploadComplete:function(msg) {
			var r = $.parseJSON(msg);
			this.setModePicture();
			this.setImage(r.url);
			this.model.set({id:r.fileId, url:r.url});
		},
		webcamCapture:function(){
			$wc = $('.webcam-container');
			$('<div></div>')
				.appendTo($('body'))
				.width($wc.width()).height($wc.height())
				.position({my:'center',at:'center',of:$wc})
				.css('backgroundColor','#fff')
				.css('zIndex','999999')
				.fadeOut();
			this.setModeLoading();
			webcam.snap();
		},
		setModeWebcam:function(){
			$('#form-webcam').show();$('#form-norm').hide();
			$('.tooltip').remove();
			return false;
		},
		setModePicture:function(){
			$('#form-webcam').hide();$('#form-norm').show();
			$('.tooltip').remove();
			return false;
		},
		setModeLoaded:function(){
			this.$('.loading-spinner').fadeOut(_.bind(function(){
				this.$('.drop-here-hint').fadeIn();
			},this));
		},
		setModeLoading:function(){
			this.$('.drop-here-hint').hide();
			this.$('.loading-spinner').fadeIn();
		},
		done:function(){
			if(!_.isNull(this.model.get('id')) && !_.isNull(this.model.get('url'))){
				// resize dimaension
				var img = this.$('.drop-thumb img');
				var data = {
					width       : img.width(),
					height      : img.height(),
					sliderValue : this.$('.slider').slider('value'),
					cropTop     : img.css('top').replace('px',''),
					cropLeft    : img.css('left').replace('px',''),
					cropWidth   : this.model.get('width'),
					cropHeight  : this.model.get('height'),
					id          : this.model.get('id')
				}
				
				$.post("<?php echo NHtml::url(array('nii/widget/photopicker')); ?>",data,function(r){
					$("#<?php echo CHtml::activeId($this->model, $this->attribute); ?>").val(r.cropImage.id);
					$("#<?php echo CHtml::activeId($this->model, $this->attributeData); ?>").val(JSON.stringify(r.originalImage));
					$('#<?php echo $id; ?>-image').attr('src',r.cropImage.url);
				},'json');
			}
			this.close();
		}
	});

	var photoModel = Backbone.Model.extend({
		defaults:{
			width:215,
			height:194,
			originalImage:null,
			id:null,
			url:null
		}
	});
	
	// view to display the user thumb nail
	var thumbView = Backbone.View.extend({
		tagName:'a',
		className:'thumbnail',
		template:_.template($('#template-photo-picker-thumb').html()),
		events:{
			'click':'thumbClick'
		},
		thumbClick:function(){
			var popup = new photoPickerPop({
				model:this.model
			});
			popup.open();
		},
		render:function(){
			this.$el.css('width','100px');
			this.$el.html($('#template-photo-picker-thumb').html());
			return this;
		}
	});
	
	var methods = {
		init : function(options) { 
			return this.each(function(){	
				var m = new photoModel();
				var v = new thumbView({model:m});
				$(this).find('.photopicker-thumb').html(v.render().el);
			});
		}
	};

	$.fn.photopicker = function(method) {
		// Method calling logic
		if (methods[method]) {
			return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || ! method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
		}    
	};

})(jQuery);
	
	
</script>