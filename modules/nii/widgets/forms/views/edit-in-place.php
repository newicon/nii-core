<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<style>.nii-edit-in-place:hover{cursor:pointer;background-color:#FEFFE8;}</style>
<span class="nii-edit-in-place" data-id="<?php echo $this->model->id ?>" data-attribute="<?php echo $this->attribute ?>" data-model="<?php echo get_class($this->model); ?>" >
	<span class="eip-view"><?php echo ($this->model->{$this->attribute}) ? $this->model->{$this->attribute} : $this->noValue; ?></span>
	<span class="eip-edit" style="display:none;">
		<input type="text" class="input-block-level input-large" />
	</span>
</span>