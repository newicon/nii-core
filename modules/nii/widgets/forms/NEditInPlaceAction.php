<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
Class NEditInPlaceAction extends CAction
{
	public function run()
	{
		$attr = $_POST['attribute'];
		$m = NData::loadModel($_POST['model'], $_POST['id'], 'No model found');
		$m->$attr = $_POST['val'];
		$m->save();
	}
}