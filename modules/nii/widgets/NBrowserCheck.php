<?php

/**
 * Browser Check Widget.
 *
 * Will check the client browser type and version against a configured JSON object.
 * If the browser is not compatilbe, an appropriate message is displayed.
 *
 * Add the following to config->params
 * 'browserCheck' => '{"chrome": {"min": 1}, "ie": {"min": 6}, "firefox": {"min": 3, "max": 160}, "safari": {"min": 4}, "opera": {"min": 4} }',
 *
 * Only browsers in the config will be considered compatible.
 *
 * @author Duncan Porter <duncan.porter@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2014 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * @author duncan
 */

class NBrowserCheck extends NWidget
{
	public $config;

	public function run()
	{
		?>
		<div class='nBrowserCheck' style='display: none;'>
			<div class='bcContent'></div>
			<div class='bcHide' onclick='hideBrowserCheck(this);'>&times;</div>
		</div>

		<script>
		(function() {
			var bConf = {
				"ugMsg": "We recommend you upgrade for the best user experience",
				"dgMsg": "We recommend you downgrade for the best user experience",
				"icBsr": "<i class='icon-exclamation-sign' style='color: red;'></i> Incompatible browser: some aspects of this website may not function correctly.<br/>  <span style='font-size: 0.9em'>We recommend that you try one of the following: ",
				"browsers": {
					"chrome": {
						"icon": "http://upload.wikimedia.org/wikipedia/commons/8/87/Google_Chrome_icon_(2011).png",
						"download": "https://www.google.com/chrome",
						"notes": "Browser from Google.  Newicon recommends you use this browser for the best speed and security."
					},
					"firefox": {
						"icon": "",
						"download": "http://www.mozilla.com/firefox",
						"notes": "Browser from Mozilla.  Newicon also recommends Firefox."
					},
					"internet explorer": {
						"icon": "",
						"download": "http://www.microsoft.com/ie",
						"notes": "Microsoft&#39;s web browser"
					},
					"safari": {
						"icon": "",
						"download": "http://www.apple.com/safari/",
						"notes": "Apple&#39;s Web Browser."
					},
					"opera": {
						"icon": "",
						"download": "http://www.opera.com/browser/",
						"notes": "Opera Web browser."
					}
				}
			};

			checkBrowser();

			function checkBrowser() {
				// check cookie and don't show the message if cookie is set
				if (readCookie('hideBrowserCheck'))
					return;

				var config = $.parseJSON('<?php echo $this->config; ?>');
				var $bcDiv  = $('.nBrowserCheck');

				var BrowserDetect = browserDetect();
				BrowserDetect.init();

				if (typeof(browserDetect.browser) == undefined)	{
					out = "Your browser cannot be detected.  It may not be compatible.";
					$bcDiv.show().find('.bcContent').html(out);
					return;
				}

				if (config[BrowserDetect.browser]) {
					if (BrowserDetect.version) {
						var brsr = BrowserDetect.browser
						var thisConf = config[brsr];

						if (thisConf.min && BrowserDetect.version < thisConf.min)
						{
							$bcDiv.show().find('.bcContent').html("The minimum version of "+brsr.charAt(0).toUpperCase() + brsr.slice(1)+" supported is "+thisConf.min+". "+bConf.ugMsg+" : <a href='"+bConf['browsers'][brsr].download+"'>"+bConf['browsers'][brsr].download+"</a>");
							return;
						}

						if (thisConf.max && BrowserDetect.version > thisConf.max)
						{
							$bcDiv.show().find('.bcContent').html("The maximum version of "+brsr.charAt(0).toUpperCase() + brsr.slice(1)+" supported is "+thisConf.max+". "+bConf.dgMsg+" : <a href='"+bConf['browsers'][brsr].download+"'>"+bConf['browsers'][brsr].download+"</a>");
							return;
						}
					}
				}
				else {
					$bcDiv.show().find('.bcContent').html(bConf.icBsr+ supportedBrowsers(bConf));
				}
			}

			/**
			 *	Return a list of supported browsers with install links (and icons?)
			 **/
			function supportedBrowsers(conf) {
				var bList = "";

				for (var i in conf.browsers) {
					var name = i.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
					bList += "<a href='' title='" +conf.browsers[i]['notes']+ "'>" +name+ "</a>, ";
				}



				bList = bList.replace(/,\s$/, '');

				return bList;
			}

			/*
			 * from quirksmode.org
			 */
			function browserDetect()
			{
				return {
					init: function () {
						this.browser = this.searchString(this.dataBrowser).toLowerCase() || "An unknown browser";
						this.version = this.searchVersion(navigator.userAgent)
							|| this.searchVersion(navigator.appVersion)
							|| "an unknown version";
						this.OS = this.searchString(this.dataOS).toLowerCase() || "an unknown OS";
					},
					searchString: function (data) {
						for (var i=0;i<data.length;i++)	{
							var dataString = data[i].string;
							var dataProp = data[i].prop;
							this.versionSearchString = data[i].versionSearch || data[i].identity;
							if (dataString) {
								if (dataString.indexOf(data[i].subString) != -1)
									return data[i].identity;
							}
							else if (dataProp)
								return data[i].identity;
						}
					},
					searchVersion: function (dataString) {
						var index = dataString.indexOf(this.versionSearchString);
						if (index == -1) return;
						return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
					},
					dataBrowser: [
						{
							string: navigator.userAgent,
							subString: "Chrome",
							identity: "Chrome"
						},
						{ 	string: navigator.userAgent,
							subString: "OmniWeb",
							versionSearch: "OmniWeb/",
							identity: "OmniWeb"
						},
						{
							string: navigator.vendor,
							subString: "Apple",
							identity: "Safari",
							versionSearch: "Version"
						},
						{
							prop: window.opera,
							identity: "Opera",
							versionSearch: "Version"
						},
						{
							string: navigator.vendor,
							subString: "iCab",
							identity: "iCab"
						},
						{
							string: navigator.vendor,
							subString: "KDE",
							identity: "Konqueror"
						},
						{
							string: navigator.userAgent,
							subString: "Firefox",
							identity: "Firefox"
						},
						{
							string: navigator.vendor,
							subString: "Camino",
							identity: "Camino"
						},
						{		// for newer Netscapes (6+)
							string: navigator.userAgent,
							subString: "Netscape",
							identity: "Netscape"
						},
						{
							string: navigator.userAgent,
							subString: "MSIE",
							identity: "IE",
							versionSearch: "MSIE"
						},
						{
                            string: navigator.userAgent,
                            subString: "Trident",
                            identity: "IE",
                            versionSearch: "rv"
                        },
						{
							string: navigator.userAgent,
							subString: "Gecko",
							identity: "Mozilla",
							versionSearch: "rv"
						},
						{ 		// for older Netscapes (4-)
							string: navigator.userAgent,
							subString: "Mozilla",
							identity: "Netscape",
							versionSearch: "Mozilla"
						}
					],
					dataOS : [
						{
							string: navigator.platform,
							subString: "Win",
							identity: "Windows"
						},
						{
							string: navigator.platform,
							subString: "Mac",
							identity: "Mac"
						},
						{
						   string: navigator.userAgent,
						   subString: "iPhone",
						   identity: "iPhone/iPod"
					    },
					    {
						   string: navigator.userAgent,
						   subString: "iPad",
						   identity: "iPad"
					    },
						{
							string: navigator.platform,
							subString: "Linux",
							identity: "Linux"
						},
						{
							string: navigator.platform,
							subString: "android",
							identity: "Android"
						}
					]

				};
			}

		})();


		function createCookie(name, value, days) {
		    var expires;

		    if (days) {
		        var date = new Date();
		        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		        expires = "; expires=" + date.toGMTString();
		    } else {
		        expires = "";
		    }
		    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
		}

		function readCookie(name) {
		    var nameEQ = escape(name) + "=";
		    var ca = document.cookie.split(';');
		    for (var i = 0; i < ca.length; i++) {
		        var c = ca[i];
		        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
		        if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
		    }
		    return null;
		}

		function hideBrowserCheck(div) {
			$(div).parents(".nBrowserCheck").hide();
			createCookie('hideBrowserCheck', '1');
		}
		</script> 

		<?php
	}
}