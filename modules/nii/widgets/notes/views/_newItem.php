<div class="newNote">
	<?php if(isset($this->displayUserPic)&&$this->displayUserPic):?>
		<div class="unit profilePic">
				<?php 
//					$this->widget('user.widgets.NUserImage',array(
//						'user' => Yii::app()->user->record,
//					));
				?>
		</div>
	<?php endif;?>
	<div class="unit note lastUnit">
		<div class="markdownInput" style="display:none;">
			<div class="field">
				<?php 
					$this->widget('modules.nii.widgets.markdown.NMarkdown',array(
						'name'=>$id.'-nnote',
						'htmlOptions'=>array("class"=>"noteInput input", "style"=>"width: 100%;height:150px;")
					));
				?>
			</div>
			<div class="pull-right">
				<input type="button" value="Cancel" class="btn nnote-cancel-button">
				<input type="button" value="Add" class="btn btn-primary add add-note-button">
			</div>
		</div>
		<div class="newNoteBox">
			<?php echo str_replace('{newNoteText}', $this->newNoteText, $this->addNoteButtonHtml); ?>
		</div>
	</div>
</div>