<?php

/**
 * NMarkdownParser class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
require_once(Yii::getPathOfAlias('system.vendors.markdown.markdown').'.php');
if(!class_exists('HTMLPurifier_Bootstrap',false))
{
	require_once(Yii::getPathOfAlias('system.vendors.htmlpurifier').DIRECTORY_SEPARATOR.'HTMLPurifier.standalone.php');
	HTMLPurifier_Bootstrap::registerAutoload();
}

/**
 * Extends Yii's CMarkdownParser
 * You can use [php] in a code block to syntax highlight code.
 * @see CMarkdownParser
 * 
 * This class adds github flavoured markdown to the markdown parser.
 * Enabling ```code``` blocks and sensible line break handling.
 * 
 *     [php]
 *     if ($isTrue) {
 *         $do->something()
 *	   }
 *
 * @property string $defaultCssFile The default CSS file that is used to highlight code blocks.
 */
class NMarkdownParser extends CMarkdownParser
{
	/**
	 * Overload to enable single-newline paragraphs
	 * https://github.com/github/github-flavored-markdown/blob/gh-pages/index.md#newlines
	 */
	function formParagraphs($text)
	{
		return parent::formParagraphs($text);
	}

	/**
	 * Overload to support ```-fenced code blocks
	 * https://github.com/github/github-flavored-markdown/blob/gh-pages/index.md#fenced-code-blocks
	 */
	function doCodeBlocks($text)
	{
		$text = preg_replace_callback(
			'#' .
			'^```' . // Fenced code block
			'[^\n]*$' . // No language-specific support yet
			'\n' . // Newline
			'(.+?)' . // Actual code here
			'\n' . // Last newline
			'^```$' . // End of block
			'#ms', // Multiline mode + dot matches newlines
			array($this, '_doCodeBlocks_callback'), $text
		);

		return parent::doCodeBlocks($text);
	}
}