<?php
/**
 * NMarkdown class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * NMarkdownFormat parses text in markdown format and outputs as html
 * NMarkdown extends CMarkdown but adds in a few extras into the markdown parser!
 *
 *
 * @author steve
 */
class NMarkdownFormat extends CMarkdown
{
		
  public function transform($output) 
	{
		return parent::transform($output);
	}
	
	/**
	 * Creates a markdown parser.
	 * By default, this method creates a {@link CMarkdownParser} instance.
	 * @return NMarkdownParser the markdown parser.
	 */
	protected function createMarkdownParser()
	{
		return new NMarkdownParser;
	}
	
}