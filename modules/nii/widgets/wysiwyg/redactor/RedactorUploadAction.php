<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Controller action method to handle uploading.
 * This can be attached to any controller, enabling different permissions and
 * access control to be applied.
 */
class RedactorUploadAction extends CAction 
{
	public function run()
	{
		$array = array();
		$file = NFile::uploadImage('redactor', $error);
		if ($file) {
			// prepare response for redactor
			$array = array(
				'filelink' => $file->url,
				'filename' => $file->original_name
			);
		} else {
			$array['error'] = $error;
		}
		
		echo stripslashes(json_encode($array));
		exit;
	}
}
