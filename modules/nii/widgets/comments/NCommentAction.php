<?php

/**
 * NCommentAction class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
Yii::import('nii.widgets.comments.models.NiiComment');

/**
 * 
 */
class NCommentAction extends CAction
{

	public function run()
	{
		$comment = new NiiComment;
		$comment->attributes = $_POST['NiiComment'];
		$comment->save();
		
		
		// get default widget properties
		if (isset($_POST['NiiCommentsWidget'])){
			$params = $_POST['NiiCommentsWidget'];
		}
		
		
		$params['comment'] = $comment;
		$params['userString'] = $comment->user_string;
		
		if ($comment->parent_id == 0) {
			echo Yii::app()->controller->renderPartial('nii.widgets.comments.views._comment', $params, true);
		} else {
			echo Yii::app()->controller->renderPartial('nii.widgets.comments.views._child', $params, true);
		}
	}

}