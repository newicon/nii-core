<?php
/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<style>
	.niicomment.media{margin-bottom:0px;}
	.niicomment .media-body{padding-bottom:2px;}
	.niicomment-child{margin-bottom:0px;margin-top:0px;}
	.niicomment-child .media-body{margin-right:12px;}
	.niicomment-child .speech-bubble{border-top: 0px;border-radius: 0px 0px 3px 3px;margin-left: 37px;}
	
	 .niicomment-child .speech-bubble{border-radius:0px;box-shadow:none;}
	 .niicomment-child:first-child .speech-bubble{border-radius:0px;box-shadow:none;}
	 .niicomment-child:first-child .media-body{padding-bottom:0px;}
	 .niicomment-child:last-child .speech-bubble{border-radius:0px 0px 3px 3px;box-shadow:0px 1px 2px rgba(0,0,0,0.3)}
	 .niicomment-child:last-child .media-body{padding-bottom:2px;}
	 
	 .niicomment-convo{margin-bottom:10px;}
	 .niicomment-controls{visibility:hidden}
	 .niicomment:hover .niicomment-controls{visibility:visible}
</style>
<div id="<?php echo $this->widgetId; ?>" class="nii-comment-container">
	<?php $comments = NiiComment::model()->findAllByAttributes(array('model' => $this->model, 'model_id' => "{$this->id}", 'parent_id'=>0)); ?>

	<div class="comments">
	<?php foreach ($comments as $comment): ?>
		<?php $this->render('_comment', array('comment'=>$comment, 
            'displayImages' => $displayImages,
            'childComments' => $childComments,
			'userString' => $userString,
		)); ?>
	<?php endforeach; ?>
	</div>
	<?php if ($userString!==null || !Yii::app()->user->isGuest) : ?>
		<div class="media nii-comment-form">
		 <?php if($displayImages) : ?>
			<a class="pull-left" href="#">
				<?php echo Yii::app()->user->getProfileImage(64) ?>
			</a>
		 <?php endif; ?>
			<div class="media-body">
				<div class="speech-bubble commentsbox">
					<form style="margin:0px;" class="form-horizontal" id="wiki-question" action="/nii/public/htdocs/wiki/question/ask" method="post">
						<div class="speech-bubble-inner">
							<div class="control-group">
								<input type="hidden" name="NiiComment[model]" value="<?php echo $this->model ?>" />
								<input type="hidden" name="NiiComment[model_id]" value="<?php echo $this->id; ?>" />
								<?php if ($userString) : ?>
									<input type="hidden" name="NiiComment[user_string]" value="<?php echo $userString ?>" />
								<?php endif; ?>
								<textarea name="NiiComment[text]" id="WikiComment_text" style="height:30px;" class="input-block-level comment-input" placeholder="Add a comment"></textarea>
							</div>
							<div class="control-group">
								<input name="NiiCommentsWidget[displayImages]" type="hidden" value="<?php echo $displayImages; ?>" />
								<input name="NiiCommentsWidget[childComments]" type="hidden" value="<?php echo $childComments; ?>" />
								<input name="NiiCommentsWidget[displayImages]" type="hidden" value="<?php echo $displayImages; ?>" />
								<button type="submit" class="btn btn-primary pull-right comment-post">Post Your Comment</button>
							</div>
						</div>
					</form>		
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>


<script type="text/template" id="template-nii-comment-form">
<div class="media niicomment-child nii-comment-form">
  <?php if($displayImages) : ?>
	<a class="pull-left" href="#">
		<?php echo Yii::app()->user->getProfileImage(54) ?>
	</a>
  <?php endif; ?>
	<div class="media-body">
		<div class="speech-bubble commentsbox">
			<form style="margin:0px;" class="form-horizontal" id="" method="post">
				<div class="speech-bubble-inner">
					<div class="control-group">
						<input type="hidden" name="NiiComment[parent_id]" id="NiiComment_parent_id" value="" />
						<input type="hidden" name="NiiComment[model]" value="<?php echo $this->model ?>" />
						<input type="hidden" name="NiiComment[model_id]" value="<?php echo $this->id; ?>" />
						<?php if ($userString) : ?>
							<input type="hidden" name="NiiComment[user_string]" value="<?php echo $userString ?>" />
						<?php endif; ?>
						<textarea name="NiiComment[text]" style="height:30px;" class="input-block-level comment-input" placeholder="Add a comment"></textarea>
					</div>
					<div class="control-group">
						<input name="NiiCommentsWidget[displayImages]" type="hidden" value="<?php echo $displayImages; ?>" />
						<input name="NiiCommentsWidget[childComments]" type="hidden" value="<?php echo $childComments; ?>" />
						<input name="NiiCommentsWidget[displayImages]" type="hidden" value="<?php echo $displayImages; ?>" />
						<button class="btn btn-primary pull-right comment-reply">Post Your Comment</button>
					</div>
				</div>
			</form>		
		</div>
	</div>
</div>
</script>



<script>
	
	$('#<?php echo $this->widgetId; ?> .comment-input').focus(function(){
		$(this).animate({height: '75'}, 250)
	}).blur(function(){
		if ($(this).val() == '')
			$(this).animate({height: '30'}, 500)
	});

	// Todo: place this and load in seperately so if you have multiple widgets on a page
	// this script will only be included once
	var NiiComment = Backbone.View.extend({
		initialize:function() {
			this.setElement($('#'+this.options.widgetId));
		}, 
		events: {
			'click .comment-post':'commentPost',
			'click .delete':'commentDelete',
			'click .omment-reply-form':'commentReplyForm',
			'click .comment-reply':'commentReplyPost'
		},
		/**
		 * save a new comment
		 */
		commentPost:function(e) {
			if ($(e.currentTarget).closest('form').find('textarea').val() == '') 
				return false;
			var data = $(e.currentTarget).closest('form').serialize();
			var t = this;
			$.post(this.options.urlComment, data , function(html){
				$('#'+t.options.widgetId+' .comments').append(html);
				t.$el.trigger({type:'add',data: data});
			});
			$(e.currentTarget).closest('form').find('textarea').val('').css({height:'30'});
      
			return false;
		},
		/**
		 * delete a comment	 
		 */
		commentDelete:function(e) {
			if (confirm('Are you sure you wish to delete this comment?')) {
				var data = {id:$(e.currentTarget).closest('.speech-bubble').attr('data-id'), userString: this.options.userString};
				$.post(this.options.commentDelete, data);
				// if we are deleting the root comment then lets delete the whole convo
				// otherwise just delete this individual comemnt
				var convo = $(e.currentTarget).closest('.niicomment-convo');
				if (convo.attr('data-id') == data.id){
					convo.remove();
				} else {
					$(e.currentTarget).closest('.niicomment').remove();
				}
				this.$el.trigger({type:'delete',data: data});
				return false;
			}
		},
		/**
		 * Show a comment reply form
		 */
		commentReplyForm:function(e) {
			var parentId = $(e.currentTarget).attr('data-id');
			var $form = $('<div></div>').html($('#template-nii-comment-form').html());
			$form.find('#NiiComment_parent_id').val(parentId);
			$('#'+this.options.widgetId).find('.niicomment-children[data-id="'+parentId+'"]').append($form.html());
			return false;
		},
		
		/**
		 * post a comment reply
		 */
		commentReplyPost:function(e) {
			var form = $(e.currentTarget).closest('form');
			if (form.find('textarea').val() == '') 
				return false;
			var data = $(e.currentTarget).closest('form').serialize();
			$.post(this.options.urlComment, data, function(html){
				form.closest('.nii-comment-form').before(html);
				form.closest('.nii-comment-form').remove();
			});
			this.$el.trigger({type:'reply',data: data});
			return false;
		}
		
	})
		
	var a = new NiiComment({
		widgetId:'<?php echo $this->widgetId; ?>',
		userString:'<?php echo $userString ?>',
		urlComment:'<?php echo NHtml::url('/nii/widget/comment') ?>',
		urlCommentDelete:'<?php echo NHtml::url('/nii/widget/comment-delete') ?>'
	});
	
</script>