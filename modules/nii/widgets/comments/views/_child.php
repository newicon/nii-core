

<div class="media niicomment niicomment-child">
	<a class="pull-left" href="#">
		<?php echo $comment->user->getProfileImage(54) ?>
	</a>
	<div class="media-body">
		<?php Yii::app()->controller->renderPartial('nii.widgets.comments.views._bubble', array('comment'=>$comment, 'userString'=>$userString)); ?>
	</div>
</div>