<?php

/**
 * Nii view file.
 * 
 * Show an individual comment
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<div class="speech-bubble commentsbox" data-id="<?php echo $comment->id; ?>">
	<div class="clearfix" style="padding-bottom:5px;">
		<div class="pull-left"><strong><?php echo $comment->userName;  ?></strong></div>
		<div class="pull-right"><small style="color:#999;"><?php echo NTime::timeAgoInWords($comment->timestamp); ?></small></div>
	</div>
	<div class="control-group">
		<?php echo $comment->text; ?>
	</div>
	<div class="control-group row niicomment-controls">
		<?php if ((!Yii::app()->user->isGuest && $comment->user_id == Yii::app()->user->id) || ($userString!==null && $userString == $comment->user_string) || Yii::app()->user->isSuper): ?>
			<a href="#" data-id="<?php echo $comment->id; ?>" class="pull-right text-small delete"><small class="text-error">Delete</small></a>
		<?php endif; ?>
	</div>
</div>