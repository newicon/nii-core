<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<div class="niicomment-convo" data-id="<?php echo $comment->id ?>">
	<div class="media niicomment">
    <?php if($displayImages) : ?>
		<a class="pull-left" href="#">
			<?php echo $comment->user->getProfileImage(64) ?>
		</a>
    <?php endif; ?>
		<div class="media-body">
			<?php Yii::app()->controller->renderPartial('nii.widgets.comments.views._bubble', array('comment'=>$comment, 'userString'=>$userString)); ?>
		</div>
	</div>

  <?php if($childComments) : ?>
	<!-- Render Child Comments -->
	<div class="niicomment-children" data-id="<?php echo $comment->id ?>">
		<?php foreach ($comment->getReplies() as $reply) : ?>
			<?php Yii::app()->controller->renderPartial('nii.widgets.comments.views._child', array('comment'=>$reply, 'userString'=>$userString)); ?>
		<?php endforeach; ?>
	</div>
	<!-- // Render Child Comments -->
	
	<div style="margin-left:100px;" data-id="<?php echo $comment->id ?>" class="omment-reply-form btn btn-link"><i class="icon-share-alt mrs" style="opacity:0.5"></i><small>Reply to this comment</small></div>
  <?php endif; ?>
</div>