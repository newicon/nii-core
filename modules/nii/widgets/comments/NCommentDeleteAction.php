<?php

/**
 * NCommentAction class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
Yii::import('nii.widgets.comments.models.NiiComment');
/**
 * 
 */
class NCommentDeleteAction extends CAction
{
	public function run()
	{
		$comment = NData::loadModel('NiiComment', $_POST['id'], 'No comment found');
		$userString = isset($_POST['userString']) ? $_POST['userString'] : null;
		if ((!Yii::app()->user->isGuest && $comment->user_id == Yii::app()->user->id) || ($userString!==null && $userString == $comment->user_string) ||  Yii::app()->user->isSuper) {
      $replies = $comment->getReplies();
      foreach( $replies as $reply)
        $reply->delete();
			$comment->delete();
		}
	}
}