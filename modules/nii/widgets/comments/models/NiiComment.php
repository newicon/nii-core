<?php

/**
 * NiiComments class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Model to store comments
 */
class NiiComment extends NActiveRecord
{
	public function tableName()
	{
		return '{{nii_comment}}';
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function rules()
	{
		return array(
			array('model, model_id, text, parent_id, user_string', 'safe')
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}
	
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				// comments can handle one level of depth i.e. one thread. people can reply to an individual comment.
				// but not to a comment of a comment
				// e.g. 
				// comment 1
				// -- comment 1.1
				// -- comment 1.2
				// comment 2
				// -- comment 2.1
				'parent_id'=>'int not null default \'0\'',
				// key to use
				'model'=>'string',
				'model_id'=>'string',
				'text'=>'text',
				'user_id'=>'int',
				'user_string' => 'varchar(150)',
				'timestamp'=>'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
			),
			'keys'=>array(
				array('model'),
				array('user_id'),
				array('parent_id'),
				array('model_id')
			),
            'dblib'=>array(
                'columns'=>array(
                    'id'=>'pk',
                    'parent_id'=>'int not null default 0',
                    // key to use
                    'model'=>'string',
                    'model_id'=>'string',
                    'text'=>'text',
                    'user_id'=>'int',
						  'user_string' => 'varchar(150)',
                    //'timestamp'=>'datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
                ),
            )
		);
	}
	
	public function beforeSave()
	{
		if (!Yii::app()->user->isGuest)
			$this->user_id = Yii::app()->user->id;
		return parent::beforeSave();
	}
	
	/**
	 * Find all replies for the current comment
	 * @return array
	 */
	public function getReplies()
	{
		return NiiComment::model()->findAllByAttributes(array('model'=>$this->model, 'model_id'=>$this->model_id, 'parent_id'=>$this->id));
	}
	
	public function getUserName()
	{
		if ($this->user_id)
			return $this->user->name;
		else
			return $this->user_string;
	}
	
}