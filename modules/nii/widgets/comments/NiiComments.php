<?php

/**
 * NiiComment class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

Yii::import('nii.widgets.comments.models.NiiComment');

/**
 * Widget that adds comments to a page.
 * To identify a comment set use the model (string) and id (string)
 * This would essentially apply to a url slug for example comments on a post might be:
 * /blog/post/3
 * where "post" is the model and "3" is the id.
 * 
 * These can also be any string of your choice. It just provides a key for the comments to attach to. Typically the model
 * attribute would be the model class name and the id the unique key of that model that comments are attached.
 * 
 * Typical useage:
 * <code>
 * <?php 
 *     $this->widget('nii.widgets.comments.NiiComments', array(
 *         'model'=>$model, 
 *         'id'=>$model->id
 *     )); 
 * ?>
 * </code>
 * 
 * Add comments
 * Depends on modules:
 * nii, user
 * 
 *  Javascript Events
 *  *****************
 * 
 * The widget emits events when comments are added or deleted. The events fire from the parent widget div 
 * (i.e. the div.nii-comment-container element). The events are
 * - add - a comment has been added. The function will be passed the data of the comment
 * - delete - a comment has been deleted. The function will be passed the id of the deleted comment (probably not of any use externally)
 * - reply - a comment has been to replied to. The event handler will be passed the data of the reply
 *
 */
class NiiComments extends NWidget
{
	/**
	 * @var string
	 */
	private $_model;
	
	public $id;
	
	public $displayImages = true;
	public $childComments = true;
	public $userString = null;
  
	public function setModel($value)
	{
		if (is_object($value))
			$value = get_class($value);
		$this->_model = $value;
	}
	
	public function getModel()
	{
		return $this->_model;
	}
	
	public function getWidgetId()
	{
		return 'niicomments-'.$this->model.$this->id;
	}
	
	public function run()
	{
		$params = array(
			'displayImages'  => $this->displayImages,
			'childComments'  => $this->childComments,
			'userString'	 => $this->userString,
		);
		$this->render('comment', $params);
	}
	
	public static function install()
	{
		NActiveRecord::install('NiiComment');
	}
}