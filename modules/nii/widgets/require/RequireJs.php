<?php
/**
 * Require class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Use require js in a view
 *
 * Usage:
 * 
 * <?php $this->widget('crm.widgets.require.RequireJs', array(
 *	'path'=>'crm.js', // will publish scripts in the crm module's "js" directory
 *	'script'=>'main'  // loads the main.js file, within this you can use requirejs
 * )); ?>
 * 
 * You will probably want to set the assetManager component's forceCopy
 * property to true for your development environment, so that changes to asset
 * js files are loaded in when you refresh.
 */
class RequireJs extends NWidget 
{
	
	/**
	 * Yii alias path to the scripts folder
	 * typically moduleName.js
	 * @property string $path
	 */
	public $path;
	
	/**
	 * The script to load in, within this script require js will work
	 * @var string 
	 */
	public $script = 'main';
	
	/**
	 * Registers Require js to clientScript and includes in html head
	 * Publishes the scripts in the specified path
	 * Registers require config script 
	 * @throws CException if the path property has not been defined
	 */
	public function run()
	{
		$requireUrl = $this->getAssetsUrl();
		Yii::app()->clientScript->registerScriptFile($requireUrl . '/require.js', CClientScript::POS_HEAD);
		
		// get the scripts directory url
		if ($this->path === null)
			throw new CException('No script path has been defined.  You must define a path alias you wish to load scripts from. e.g. moduleName.js');
		$jsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias($this->path));
		
		// register require js config
		$js = 'require.config({baseUrl: "'.$jsUrl.'",waitSeconds: 15});require(["'.$this->script.'"],function(){});';
		Yii::app()->clientScript->registerScript('requireInit', $js, CClientScript::POS_END);
	}
}