<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Create a tag input field.
 * Enables you to create new tags.
 * 
 * This is useful for fields storing comma delimited items, such as email address from and to fields
 * Example:
 * 
 * $this->widget('nii.widgets.select2.TagInput', array(
 *		'model'=>$model,
 *		'attribute'=>'to',
 *		'settings'=>array(
 *			'tags'=>array('someone@test.com','another@somewhere.com'),
 *		)
 * ))
 */
class TagInput extends Select2 
{
	/**
	 * Js settings will be encoded and passed to the select2 jquery plugin
	 * @var array 
	 */
	public $settings = array();
	
	public function run()
	{
		// set default settings, such as token separator to ,
		$defaultSettings = array('tokenSeparators'=>array(','));
		$this->settings = CMap::mergeArray($defaultSettings, $this->settings);
		
		list($name, $id) = $this->resolveNameID();
		
        if (isset($this->model)) {
			echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
        } else {
			echo CHtml::textField($name, $this->value, $this->htmlOptions);
        }
		
        $this->registerScripts($id);
		$settings = CJavaScript::encode($this->settings);
        Yii::app()->getClientScript()->registerScript("{$id}_select2", "$('#{$id}').select2({$settings});");
	}
}
