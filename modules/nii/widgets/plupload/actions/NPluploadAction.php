<?php

/**
 * PluploadAction class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Action for handling the uploading of a files from plupload widget
 * uses the NFileManager to manage the uploaded file
 */
class NPluploadAction extends CAction
{
	/**
	 * action to handle plupload file uploads.
	 * Anything echo'd here will be passed to the javascript plupload "fileUploaded:function(up, file, info)" function
	 * and will be accessible through the info.response parameter.  So the encoded json here can be accessed within this function
	 * via "var json = $.parseJSON(info.response)" which will have the "id" and "url" keys as encoded and echoed here.
	 * The Yii::app()->controller->createWidget('nii.widgets.plupload.PluploadWidget')->processUploadFileManager() line
	 * is essential as it handles plupload chunking and other aspects and talks to the plupload javascript object in order to show file upload
	 * progress and other info.
	 */
	public function run()
	{
		$id = Yii::app()->controller->createWidget('nii.widgets.plupload.PluploadWidget')->processUploadFileManager();
		echo CJSON::encode(array(
			'id'=>$id,
			'url'=>NFileManager::get()->getUrl($id)
		));
	}
}