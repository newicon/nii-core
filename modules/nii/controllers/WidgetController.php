<?php

/**
 * WidgetController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * WidgetController holds actions for nii widgets.
 * Many widgets required to call controller cations. Such as file upload widgets, comments, and notes widgets etc.
 * Widget controller actions should be defined as seperate CAction objects.
 * This controller class simply enables a default value for widget actions to be set.  
 * If you need to further specify permissions or change filters and behavior you can implement 
 * the action in your own controller
 */
class WidgetController extends AController
{
	// Temporary fix, while we figure out a more secure method, 
	// of allowing comments from non-logged-in users
	public function accessRules()
	{
		return array(
			array('allow',
				'users' => array('*'),
			),
			array('allow',
				'actions' => array('comment-delete', 'editInPlaceUpdate'),
				'users' => array('?'),
			)
		);
	}
	
	public function actions()
	{
		return array(
			'webcam'=>'nii.widgets.webcam.actions.NWebcamSave',
			'plupload'=>'nii.widgets.plupload.actions.NPluploadAction',
			'photopicker'=>'nii.widgets.photopicker.actions.NPhotoPickerAction',
			'editInPlaceUpdate'=>'nii.widgets.forms.NEditInPlaceAction',
			'comment'=>'nii.widgets.comments.NCommentAction',
			'comment-delete'=>'nii.widgets.comments.NCommentDeleteAction',
			'redactorUpload'=>'nii.widgets.wysiwyg.redactor.RedactorUploadAction'
		);
	}
	
}