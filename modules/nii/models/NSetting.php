<?php

/**
 * NSetting class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of NSetting
 *
 * @author 
 */
class NSetting extends NActiveRecord
{
	public function tableName()
	{
		return '{{nii_setting}}';
	}
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
	
	/**
	 * table schema def appropriate for NActiveRecord install
	 * @return array
	 */
	public function schema()
	{
		return array(
			'columns'=>array(
				'category'=>'varchar(64) NOT NULL default "system"',
				'key'=>'string',
				'value'=>'text',
				'PRIMARY KEY(`category`, `key`)'
			)
		);
	}
}
