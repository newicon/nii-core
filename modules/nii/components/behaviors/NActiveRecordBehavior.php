<?php

/**
 * Extends the base CActiveRecordBehavior to add additional events when used with an NActiveRecord class
 *
 * @author steve
 */
class NActiveRecordBehavior extends CActiveRecordBehavior
{
	/**
	 * Adds the additional afterInstal event that is raised after the table has been installed
	 * @return array events (array keys) and the corresponding event handler methods (array values).
	 * @see CActiveRecordBehavior::events
	 */
	public function events()
	{
		return array_merge(parent::events(), array(
			'onBeforeInstall'=>'beforeInstall',
			'onAfterInstall'=>'afterInstall',
			'onAfterDeleteAllByAttributes'=>'afterDeleteAllByAttributes',
			'onAfterDeleteAll'=>'afterDeleteAll',
			'onAfterDeleteByPk'=>'afterDeleteByPk'
		));
	}

	/**
	 * Responds to {@link NActiveRecord::onBeforeInstall} event.
	 * Override this method if you want to handle the corresponding event of the {@link NActiveRecordBehavior::owner owner}.
	 * @param CEvent $event event parameter
	 */
	public function beforeInstall($event){}
	
	/**
	 * Responds to {@link NActiveRecord::onAfterInstall} event.
	 * Override this method if you want to handle the corresponding event of the {@link NActiveRecordBehavior::owner owner}.
	 * @param CEvent $event event parameter
	 */
	public function afterInstall($event){}
	
	/**
	 * event called after CActiveRecord::deleteAllByAttributes function
	 * @param CEvent $event
	 * - event properties:
	 * - sender: CActiveRecord
	 * - params: array(
	 *		'attributes'=>array
	 *		'condition'=>string
	 *		'params'=>string condition parameters
	 *		'rowsAffected'=> the rows deleted by this function
	 */
	public function afterDeleteAllByAttributes($event){}
	
	/**
	 * event called after CActiveRecord::afterDeleteAll function
	 * @param CEvent $event
	 * - event properties:
	 * - sender: CActiveRecord
	 * - params: array(
	 *		'condition'=>string
	 *		'params'=>string condition parameters
	 *		'rowsAffected'=> the rows deleted by this function
	 */
	public function afterDeleteAll($event){}
	
	/**
	 * event called after CActiveRecord::afterDeleteByPk function
	 * @param CEvent $event
	 * - event properties:
	 * - sender: CActiveRecord
	 * - params: array(
	 *		'pk'=>the primary key of the record
	 *		'condition'=>string
	 *		'params'=>string condition parameters
	 *		'rowsAffected'=> the rows deleted by this function
	 */
	public function afterDeleteByPk($event){}
	
	/**
	 * If a behavior wishes to add addiontal columns
	 * implement the schema array
	 * 
	 * @see NActiveRecord::schema
	 * @see NActiveRecord::getSchemaComplete
	 * @return aray schema array
	 * array(
	 *     'columns'=>array(
	 *         'col_name'=>'col type'
	 *     )
	 * )
	 */
	public function schema()
	{	
		return array();
	}
}

