<?php
/**
 * NTaggable class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * The Nii tree table plugin
 * the owner ActiveRecord must have an id column which is a single primary key column
 *
 * NOTES: when moving nodes sometimes you have to call 
 * <code>$record->refresh()</code> 
 * to see the changes performed by this class
 * This is because often the moving and manipulating the tree affects many record at once.
 * 
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @version 1.3.9
 */
class NTreeTable extends NActiveRecordBehavior 
{
	/**
	 * The left column name
	 * @var string
	 */
	public $left = 'tree_left';
	/**
	 * The right column name
	 * @var string
	 */
	public $right = 'tree_right';
	/**
	 * The tree level
	 * @var string
	 */
	public $level = 'tree_level';
	
	/**
	 * This class also maintains an acurate parent id cos its great and darn handy
	 * @var string
	 */
	public $parent = 'tree_parent';

	/**
	 * define additonal columns to add to the table
	 */
	public function schema()
	{
		$this->getProperties($left, $right, $level, $parent);
		return array(
			'columns'=>array(
				"$left"=>'int',
				"$right"=>'int',
				"$level"=>'int',
				"$parent"=>'int',
			)
		);
	}
	
	/**
	 * Returns left right and level values of the columns.
	 *
	 * This function has been designed to save space in functions which rely on
	 * the included values over and over again.
	 *
	 * Please pass this function blank variables. It will automagically change them for you.
	 * For example if you called getProperties($left);
	 *
	 * From that point onwards, $left will equal the left column name in the database.
	 * @param string $left null
	 * @param string $right null
	 * @param string $level null
	 */
	public function getProperties(&$left=null, &$right=null, &$level=null, &$parent=null) 
	{
		$left = $this->left;
		$right = $this->right;
		$level = $this->level;
		$parent = $this->parent;
	}

	/**
	 * Returns all the nodes in a tree
	 * @return array of CActiveRecord's
	 */
	public function getAllNodes()
	{
		$baseNode = $this->getTreeRoot();
		return $this->getDescendentsOf($baseNode);
	}

	/**
	 * Gets the root row of the tree table
	 * @return CActiveRecord / null if none found
	 */
	public function getTreeRoot()
	{
		$search = $this->getOwner()->find($this->left . '=0');
		return $search;
	}
	
	/**
	 * whether the node is a root node
	 * 
	 * @return boolean 
	 */
	public function isRoot()
	{
		$this->getProperties($left);
		return ($this->getOwner()->$left==0);
	}

	/**
	 * Gets a criteria object prepopulated with criteria that selects descendants of a particular node through all levels
	 *
	 * Descendants are different to children as they are not limited to only the next level
	 * of the tree down.
	 * 
	 * @param CActiveRecord $node The node which you would like to find all the descendant of
	 * @return CDbCriteria
	 */
	public function getCriteriaDescendentsOf(CActiveRecord $node)
	{
		$this->getProperties($left, $right);
		$ta = $this->getOwner()->tableAlias;
		$c = new CDbCriteria(array(
			'condition' => "$ta.$left > :lft AND $ta.$right < :rgt",
			'params' => array(':lft' => $node->$left, ':rgt' => $node->$right),
			'order' => "$ta.$left ASC"
		));
		return $c;
	}
	
	/**
	 * get a criteria object to filter and get all the descendants of the current node
	 * @see self::getCriteriaDescendentsOf
	 * @return CDbCriteria object
	 */
	public function getCriteriaDescendents()
	{
		return $this->getCriteriaDescendentsOf($this->getOwner());
	}
	
	/**
	 * Gets all of the descendants of a particular node through all levels
	 *
	 * Descendants are different to children as they are not limited to only the next level
	 * of the tree down.
	 * 
	 * @param CActiveRecord $node The node which you would like to find all the descendant of
	 * @return CActiveRecord
	 */
	public function getDescendentsOf(CActiveRecord $node)
	{
		$criteria = $this->getCriteriaDescendentsOf($node);
		return $this->getOwner()->findAll($criteria);
	}

	/**
	 * shortcut to getDescendentsOf
	 * @see self::getDescendentsOf
	 * @return array of CActiveRecord's
	 */
	public function getDescendents()
	{
		return $this->getDescendentsOf($this->getOwner());
	}
	
	/**
	 * count the number of descendents the record has
	 * @return integer number of descendents
	 */
	public function countDescendentsOf(CActiveRecord $node)
	{
		$criteria = $this->getCriteriaDescendentsOf($node);
		return $this->getOwner()->count($criteria);
	}
	
	/**
	 * count the number of descendents the record has
	 * @see self::countDecendentsOf
	 * @return integer number of descendents
	 */
	public function countDescendents()
	{
		return $this->countDescendentsOf($this->getOwner());
	}
	
	/**
	 * get an array of all descendant item ids
	 */
	public function getDescendentIds()
	{
		$criteria = $this->getCriteriaDescendents();
		$criteria->select = 'id';
		return Yii::app()->db->commandBuilder->createFindCommand($this->getOwner()->tableName(), $criteria)->queryColumn();
	}
	
	/**
	 * Descendents scope returns the criteria for all descendents of the current record
	 * @return NActiveRecord
	 */
	public function descendents()
	{
		$this->getOwner()->getDbCriteria()->mergeWith($this->getCriteriaDescendents());
		return $this->getOwner();
	}
	
	/**
	 * get the criteria object of searching for ancestors
	 * @param CActiveRecord $node
	 * @param boolean $youngestFirst
	 * @return CDbCriteria 
	 */
	public function getCriteriaAncestorsOf(CActiveRecord $node, $youngestFirst=false)
	{
		$order = $youngestFirst ? 'DESC' : 'ASC';
		$this->getProperties($left, $right);
		$c = new CDbCriteria(array(
			'condition' => "$left < :lft AND $right > :rgt",
			'params' => array(':lft' => $node->$left, ':rgt' => $node->$right),
			'order' => "$left $order"
		));
		return $c;
	}
	
	/**
	 * get the criteria object to get all ancestors of the current node
	 * $node->getCriteriaAncestors() will get all ancestors of $node
	 * @see self::getCriteriaAncestorsOf
	 * @param boolean $youngestFirst
	 * @return CDbCriteria object 
	 */
	public function getCriteriaAncestors($youngestFirst=false)
	{
		return $this->getCriteriaAncestorsOf($this->getOwner(), $youngestFirst);
	}

	/**
	 * Get all ancestors of a given node
	 * 
	 * @param CActiveRecord $node The node to get all the ancestors on
	 * @param boolean $youngestFirst optional default false will all ancestors starting by oldest first, 
	 * if true ancestors will be returned from youngest to oldest. 
	 * (age references the tree level, 0 being the oldest posible root level node)
	 * @return array of CActiveRecord
	 */
	public function getAncestorsOf(CActiveRecord $node, $youngestFirst=false)
	{
		$criteria = $this->getCriteriaAncestorsOf($node, $youngestFirst);
		return $this->getOwner()->findAll($criteria);
	}

	/**
	 * shortcut function to getAncestorsOf
	 * @see self::getAncestorsOf
	 * @param $youngestFirst boolean order ancestors by youngest or oldest first.
	 * oldest refers to the root level node (the oldest posible position)
	 * @return array of CActiveRecord's 
	 */
	public function getAncestors($youngestFirst=false)
	{
		return $this->getAncestorsOf($this->getOwner(), $youngestFirst);
	}
	
	/**
	 * Ancestos scope returns the criteria for all ancestors of the current record
	 * @return NTreeTable 
	 */
	public function ancestors()
	{
		$this->getOwner()->getDbCriteria()->mergeWith($this->getCriteriaAncestorsOf($this->getOwner()));
		return $this->getOwner();
	}
	
	/**
	 * gets a criteria object prepopulated with criteria that selects children nodes form the specified $node
	 * 
	 * @param CActiveRecord $node
	 * @return CDbCriteria
	 */
	public function getCriteriaChildrenOf(CActiveRecord $node)
	{
		$this->getProperties($left, $right, $level);
		$c = new CDbCriteria(array(
			'condition' => "$left > :nodeLeft AND $right < :nodeRight AND $level = :level",
			'params' => array(':nodeLeft' => $node->$left, ':nodeRight' => $node->$right, ':level' => $node->$level + 1)
		));
		return $c;
	}
	
	/**
	 * get criteria object to select all children of current node
	 * @see self::getCriteriaChildrenOf
	 * @return CDbCriteria 
	 */
	public function getCriteriaChildren()
	{
		return $this->getCriteriaChildrenOf($this->getOwner());
	}
	
	/**
	 * returns all children nodes of supplied node
	 *
	 * @param CActiveRecord $node
	 * @return array CActiveRecords or empty array
	 */
	public function getChildrenOf(CActiveRecord $node)
	{
		$c = $this->getCriteriaChildrenOf($node);
		return $this->getOwner()->findAll($c);
	}

	/**
	 * This function is designed to be called from a row object and will discover all
	 * of the rows children.
	 *
	 * Note: When you find the children, it will only return the next level
	 * To find everything below the selected row, use getDescendants
	 *
	 * @see NTreeTable::getChildrenOf
	 * @return array CActiveRecords or empty array
	 */
	public function getChildren()
	{
		return $this->getChildrenOf($this->getOwner());
	}
	
	/**
	 * active record scope to filter the children
	 * @return NTreeTable
	 */
	public function children()
	{
		$this->getOwner()->getDbCriteria()->mergeWith($this->getCriteriaChildren());
		return $this->getOwner();
	}
	
	/**
	 * returns a CActiveRecord or boolean false
	 * 
	 * @param CActiveRecord $node
	 * @return CActiveRecord
	 */
	public function getParentOf(CActiveRecord $node)
	{
		$this->getProperties($left, $right, $level);
		$res = $this->getOwner()->find(array(
			'condition' => "`$left` < :lft AND `$right` > :rgt AND `$level` = :level",
			'params' => array(':lft' => $node->$left, ':rgt' => $node->$right, ':level' => $node->$level - 1)
		));

		if ($res === null) {
			return false;
		}
		return $res;
	}

	/**
	 * shortcut function to getParentOf
	 * 
	 * @return CActiveRecord 
	 */
	public function getParent()
	{
		return $this->getParentOf($this->getOwner());
	}
	
	/**
	 * gets the path to node from root, uses row::__toString() method to get node names, or,
	 * if specified used the $colName of the node object for the string path $node->$colName
	 *
	 * @param CActiveRecord $node the treeRow node to draw the path for
	 * @param string $seperator path seperator
	 * @param string $colName the table column name to use as the string path if null then the __tostring method will be used
	 * @param array $options an array of options
	 *  - bool includeNode default false, whether or not to include the current node at end of path
	 *  - bool includeRoot default false, whether or not to include the root node output at the beginning of the path
	 *  - int limit default -1 (less than 0 means no limit) limit the number of parents ancesters shown
	 * @return string string representation of path
	 */
	public function getPathOf($node, $seperator = ' / ', $colName=null, $options=array())
	{
		
		$defaultOptions = array(
			'includeRoot'=>false,
			'includeNode'=>false,
			'limit'=>-1,
		);
		$options = CMap::mergeArray($defaultOptions, $options);
		
		// find ancestors
		$c = $this->getCriteriaAncestorsOf($node);
		$c->limit = $options['limit'];
		$ancestors = $this->getOwner()->findAll($c);
		
		// uild a simple path array (to implode later)
		$path = array();
		if ($ancestors) {
			foreach ($ancestors as $a) {
				if ($a->isRoot() && !$options['includeRoot'])
					continue;
				$path[] = ($colName) ? $a->$colName : $a->__toString();
			}
		}
		if ($options['includeNode']) {
			$path[] = ($colName) ? $node->$colName : $node->__toString();
		}

		return implode($seperator, $path);
	}
	
	/**
	 * get the path of the current owner record
	 * @see self::getPathOf
	 * @param string $seperator path seperator
	 * @param string $colName the table column name to use as the string path if null then the __tostring method will be used
	 * @param array $options an array of options
	 *  - bool includeNode default false, whether or not to include the current node at end of path
	 *  - bool includeRoot default false, whether or not to include the root node output at the beginning of the path
	 *  - int limit default -1 (less than 0 means no limit) limit the number of parents ancesters shown
	 * @return type 
	 */
	public function getPath($seperator = ' / ', $colName=null, $options=array())
	{
		return $this->getPathOf($this->getOwner(), $seperator, $colName, $options);
	}

	/**
	 * get a node from its path eg a/c/b/d/w
	 * returns false if the first node in the path does not exist. For example if there is no such node "a"
	 *
	 * Two byRef variables exist to return the progress if some nodes exist
	 * but the node with the full path does not. For example:
	 * if our path is a/b/c/d/e and all nodes up to node c exits, but node d does not,
	 * then the $node variable will contain an array of all nodes up to "c"
	 * and the $latestPath variable will be an array containing a,b,c
	 *
	 * @param $path
	 * @param $colName
	 * @param $nodes
	 * @param $latestPath
	 * @return Newicon_Db_TreeRow or false if no node found
	 */
	public function getNodeWithPath($path, $colName, &$nodes=array(), &$latestPath=array())
	{
		$pathBits = explode('/', $path);
		$node = $this->fetchRow("$colName=?", $pathBits[0]);

		if (!$node) {
			return false;
		}

		foreach ($pathBits as $bit) {

			// don't run first loop as we have already found the first node
			if ($bit == $pathBits[0]) {
				continue;
			}

			$nodeRs = $node->getChildrenQuery()->where("$colName=?", $bit)->go();

			$nodes[] = $node;
			$latestPath[] = $node->$colName;

			if (0 == $nodeRs->count()) {
				return false;
			}

			$node = $nodeRs->current();
		}

		$nodes[] = $node;
		$latestPath[] = $node->$colName;

		return $node;
	}
	
	/**
	 * create a drop down list of all decendant nodes under $node
	 * if $node is null then the root node is assumed.
	 * Uses self::getNameIndented to display an indented representation of each node
	 * @see self::getNameIndented
	 * @param string $nameCol string representing a column to use as the nodes name defaults to 'name'
	 * @param string $valueCol string representing the name of the value column defaults to 'id'
	 * @param CActiveRecord $node 
	 * @return array value=>name (id=>name) suitable for use in dropdown list 
	 */
	public function getDropDownData($nameCol='name', $valueCol='id', $node=null)
	{
		$data = array();
		if($node===null)
			$node = $this->getTreeRoot();
		foreach($node->getDescendents() as $n)
			$data[$n->$valueCol] = $n->getNameIndented($nameCol);
		return $data;
	}
	
	/**
	 * 	Returns the node's name, indented for use in dropdowns
	 * @param string $column - the column to use as the node's name
	 * @return string - indented node name 
	 */
	public function getNameIndented($column)
	{
		$this->getProperties($left, $right, $level);
		$space='';
		$space = str_replace(' ', '&nbsp;', str_pad($space, $this->getOwner()->$level - 1, ' '));
		return ($space != '' ? $space . ($this->getOwner()->$level > 2 ? '&nbsp;&nbsp;' : '') . '&LeftFloor;&nbsp;' : '') . $this->getOwner()->$column;
	}

	/**
	 * detach a node from the heirarchy
	 * 
	 * @param CActiveRecord $node 
	 */
	public function detachNode(CActiveRecord $node)
	{
		$this->getProperties($left, $right);
		$node->$left = -1;
		$node->$right = -1;
		$node->save();
	}

	/**
	 * deletes a node and all it's descendants
	 * 
	 * @param CActiveRecord $node | optional if null will assume current owner
	 * @throws Exception if a mysql exception occurs db will roll back
	 * @return boolean true on successful deletion 
	 */
	public function deleteNode(CActiveRecord $node=null)
	{
		// set node if null to be the current owner object 
		$node = ($node===null) ? $this->getOwner() : $node;
		
		$this->getProperties($left, $right, $level);

		$connection=Yii::app()->db;
		$t=$connection->beginTransaction();
		try {
			$this->getOwner()->deleteAll(array(
				'condition' => "$left >= :left AND $right<= :right",
				'params' => array(':left'=>$node->$left, ':right' => $node->$right))
			);

			$first = $node->$left + 1;
			$delta = $node->$right - $node->$left - 1;
			$this->_shiftRLValues($first, $delta);
			$t->commit();
		} catch (Exception $e) {
			$t->rollBack();
			throw $e;
		}
		return true;
	}
	
	/**
	 * deletes a node but keeps the children moves them on to the parent node
	 * The node to delete is assumed to be the owner of the behavior unless specified by $deleteNode parameter
	 * 
	 * @param CActiveRecord $parentNode the parent tree node to place the children nodes under if null uses the $dleteNodes parent
	 * @param CActiveRecord $deleteNode defaults to null assumes to be thw behavior owner
	 */
	public function deleteNodePreserveChildren($parentNode=null, $deleteNode=null)
	{
		if($deleteNode===null)
			$deleteNode = $this->getOwner();
		
		if($parentNode===null)
			$parentNode = $deleteNode->getParent();
		
		// move all children out of the node we want to delete
		if($deleteNode->hasChildren()) {
			foreach($deleteNode->getChildren() as $child) {
				// each time we call moveAsLastChild all child nodes can be potentially updated so we
				// must refresh  each active record
				$child->refresh();
				$child->moveAsLastChildOf($parentNode, $child);
			}
		}
		$deleteNode->refresh();
		$this->deleteNode($deleteNode);
	}
	
	/**
     * determines if node is equal to subject node
     * @param CActiveRecord $node node to compare
     * @return bool            
     */    
    public function isEqualTo(CActiveRecord $subj)
    {
        return (($this->getLeftValue() == $subj->getLeftValue()) &&
                ($this->getRightValue() == $subj->getRightValue()));
    }
    
	/**
     * determines if node is child of subject node
     * @param CActiveRecord $node node to compare
     * @return bool
     */
    public function isDescendantOf(CActiveRecord $node)
    {
        return (($this->getLeftValue()  > $node->getLeftValue()) &&
                ($this->getRightValue() < $node->getRightValue()));
    }

    /**
     * determines if node is child of or sibling to subject node
     * @param CActiveRecord $node node to compare
     * @return bool            
     */
    public function isDescendantOfOrEqualTo(CActiveRecord $node)
    {
        return (($this->getLeftValue() >= $node->getLeftValue()) &&
                ($this->getRightValue() <= $node->getRightValue()));
    }

    /**
     * determines if node is ancestor of subject node
     * @param CActiveRecord $node node to compare
     * @return bool            
     */
    public function isAncestorOf(CActiveRecord $node)
    {
        return (($node->getLeftValue() > $this->getLeftValue()) &&
                ($node->getRightValue() < $this->getRightValue()));
    }
	
	/**
	 * get the left value stored in the database
	 * @return int 
	 */
	public function getLeftValue()
	{
		$left = $this->left;
		return $this->getOwner()->$left;
	}
	
	/**
	 * get the right value stored in the database
	 * @return int 
	 */
	public function getRightValue()
	{
		$right = $this->right;
		return $this->getOwner()->$right;
	}
	
	/**
	 * Determines if node is valid
	 *
	 * @return bool 
	 */
	public function isValidNode($treeNode)
	{
		if ($treeNode instanceof CActiveRecord) {
			$this->getProperties($left, $right);
			return ($treeNode->$right > $treeNode->$left);
		} else {
			return false;
		}
	}
	
	/**
	 * adds the $node to the root node
	 * 
	 * @param CActiveRecord $node 
	 */
	public function addNodeToRoot($node)
	{
		// get root node
		$root = $this->getTreeRoot();
		$this->insertAsLastChildOf($node, $root);
	}

	/**
	 * inserts node as parent of dest record
	 *
	 * @param $insertNode the node to be inserted (will become the parent of $refNode)
	 * @param $refNode the node that already exists and will become the child of $insertNode
	 * @return bool
	 * @todo Wrap in transaction
	 */
	public function insertAsParentOf(CActiveRecord $insertNode, CActiveRecord $refNode)
	{
		$this->getProperties($colLeft, $colRight, $colLevel, $colParent);

		// cannot insert a node that has already has a place within the tree
		if ($this->isValidNode($insertNode))
			return false;

		// cannot insert as parent of root
		if ($refNode->$colLeft == 0)
			return false;

		// cannot insert as parent of itself
		if ($refNode === $insertNode) {
			throw new CException("Cannot insert node as parent of itself");
			return false;
		}

		$newLeft = $refNode->$colLeft;
		$newRight = $refNode->$colRight + 2;
		$newLevel = $refNode->$colLevel;

		try {
			$t = $this->getDb()->beginTransaction();

			// Make space for new node
			$this->_shiftRlValues($refNode->$right + 1, 2);

			// Slide child nodes over one and down one to allow new parent to wrap them
			$sql = "UPDATE `$tableName` SET ";
			$sql .= " $colLeft   = $colLeft+1, $colRight  = $colRight+1, $colLevel  = $colLevel+1, ";
			$sql .= " $colParent = {$insertNode->getPrimaryKey()}";
			$sql .= " WHERE $colLeft >= $newLeft AND $colRight<= $newRight";
			$this->getDb()->createCommand($sql)->execute();
			$this->_insertNode($insertNode, $newLeft, $newRight, $refNode->$colLevel, $refNode->$colParent);
			$t->commit();
		} catch (Exception $e) {
			$t->rollback();
			throw $e;
		}

		return true;
	}

	/**
	 * inserts node as previous sibling of refNode record
	 *
	 * @return bool
	 * @todo Wrap in transaction
	 */
	public function insertBeforeNode(CActiveRecord $insertNode, CActiveRecord $refNode)
	{
		// cannot insert a node that has already has a place within the tree
		if ($this->isValidNode($insertNode))
			return false;
		// cannot insert as sibling of itself
		if ($insertNode === $refNode) {
			throw CException("Cannot insert node as previous sibling of itself");
			return false;
		}

		$this->getProperties($left, $right, $level, $parent);
		
		$newLeft = $refNode->$left;
		$newRight = $refNode->$left + 1;

		try {
			$t = $this->getDb()->beginTransaction();

			$this->_shiftRlValues($newLeft, 2);

			$this->_insertNode($insertNode, $newLeft, $newRight, $refNode->$level, $refNode->$parent);

			$t->commit();
		} catch (Exception $e) {
			$t->rollback();
			throw $e;
		}

		return true;
	}

	/**
	 * inserts node as next sibling of dest record
	 *
	 * @return bool
	 * @todo Wrap in transaction
	 */
	public function insertAfterNode(CActiveRecord $insertNode, CActiveRecord $refNode)
	{
		// cannot insert a node that already has a place within the tree
		if ($this->isValidNode($insertNode))
			throw Exception('node is not valid!');
		return false;

		// cannot insert as sibling of itself
		if ($insertNode === $refNode) {
			throw Exception("Cannot insert node as next sibling of itself");
			return false;
		}
		
		$this->getProperties($left,$right,$level);

		$newLeft = $refNode->$right + 1;
		$newRight = $refNode->$right + 2;

		try {
			$t = $this->getDb()->beginTransaction();

			
			$this->_shiftRlValues($newLeft, 2);
			$this->_insertNode($insertNode, $newLeft, $newRight, $refNode->tree_level, $refNode->tree_parent);

			$t->commit();
		} catch (Exception $e) {
			$t->rollback();
			throw $e;
		}

		return true;
	}

	/**
	 * Inserts a new node into the tree
	 * @param CActiveRecord $node
	 * @param int $left The new left value
	 * @param int $right The new right value
	 * @param int $level The tree level
	 * @param int $parentId
	 */
	private function _insertNode(CActiveRecord $node, $left, $right, $level, $parentId)
	{
		$this->getProperties($colLeft,$colRight,$colLevel, $colParent);
		$node->$colLeft = $left;
		$node->$colRight = $right;
		$node->$colLevel = $level;
		$node->$colParent = $parentId;
		$node->save();
	}

	/**
	 * Inserts node as first child of dest record
	 * @return bool Whether or not the insert was a success
	 */
	public function insertAsFirstChildOf(CActiveRecord $node, CActiveRecord $parentNode)
	{
		// cannot insert as child of itself
		if ($parentNode === $node) {
			throw new CException("Cannot insert node as first child of itself");
			return false;
		}
		
		$this->getProperties($left,$right,$level);

		$newLeft = $parentNode->$left + 1;
		$newRight = $parentNode->$left + 2;

		try {
			$t = $this->getDb()->beginTransaction();

			$this->_shiftRlValues($newLeft, 2);
			$this->_insertNode($node, $newLeft, $newRight, $parentNode->$level + 1, $parentNode->getPrimaryKey());

			$t->commit();
		} catch (Exception $e) {
			$t->rollback();
			throw $e;
		}

		return true;
	}

	/**
	 * inserts node as last child of $parentNode record
	 * @throws Exception
	 * @param CActiveRecord $node node to be inserted as last child of $parentNode
	 * @param CActiveRecord $parentNode
	 * @return bool
	 */
	public function insertAsLastChildOf(CActiveRecord $node, CActiveRecord $parentNode)
	{
		// cannot insert a node that has already has a place within the tree
		if ($this->isValidNode($node))
			return false;
		// cannot insert as child of itself
		if ($parentNode === $node) {
			throw new CException("Cannot insert node as last child of itself");
		}

		$this->getProperties($left,$right,$level);
		
		$newLeft = $parentNode->$right;
		$newRight = $parentNode->$right + 1;

		try {
			$t = Yii::app()->getDb()->beginTransaction();

			$this->_shiftRlValues($newLeft, 2);
			$this->_insertNode($node, $newLeft, $newRight, $parentNode->$level + 1, $parentNode->getPrimaryKey());

			$t->commit();
		} catch (Exception $e) {
			$t->rollback();
			throw $e;
		}

		return true;
	}

	/**
	 * Inserts the current node (specified by $this->getOwner())
	 * as the last child of node specified by $node param
	 * @see self::inserAsLastChildOf
	 * @param NActiveRecord $node
	 * @return type 
	 */
	public function insertAsLastChild($node)
	{
		return $this->insertAsLastChildOf($node, $this->getOwner());
	}
	
	/**
	 * convienience function for inserting a child as the last child of $node
	 * this function does the same as self::insertAsLastChild();
	 * @see self::insertAsLastChildOf  
	 * @param type $node
	 * @return boolean true on success 
	 */
	public function addChild($node)
	{
		return $this->insertAsLastChildOf($node, $this->getOwner());
	}
	
	/**
	 * moves the current node (specified by $this->getOwner()) into the parent node specified
	 * by $node param
	 * @see self::moveAsLastChildOf
	 * @param NActiveRecord $node the parent node this node will be moved into
	 * @return void
	 */
	public function moveInsideNode($node)
	{
		$this->moveAsLastChildOf($node, $this->getOwner());
	}

	/**
	 * moves $node as first child of $parentNode record
	 *
	 * @param $parentNode CActiveRecord
	 * @param $node CActiveRecord if null uses the owner
	 * @return boolean
	 * @throws CException
	 */
	public function moveAsFirstChildOf(CActiveRecord $parentNode, CActiveRecord $node=null)
	{
		if ($node===null)
			$node = $this->getOwner();
		$this->getProperties($colLeft, $colRight, $colLevel, $colParent);
		if ($parentNode === $node || $node->isAncestorOf($parentNode)) {
			throw new CException("Cannot move node as first child of itself or into a descendant");
			return false;
		}
		$oldLevel = $node->$colLevel;
		$node->$colLevel = $parentNode->$colLevel + 1;
		$node->$colParent = $parentNode->getPrimaryKey();
		$node->save();
		$this->_updateNode($node, $parentNode->$colLeft + 1, $node->$colLevel - $oldLevel);

		return true;
	}

	/**
	 * moves node as child of parent record position determines the position within the children
	 * to place the node. A position of 0 is equivelant to the first child. and thus the same as
	 * calling moveAsFistChildOf()
	 *
	 * @param CActiveRecord $node node to move
	 * @param CActiveRecord $parentNode the parent node of the new location
	 * @param int $position the position within the children of $parentNode in which to place the $node
	 * @return boolean
	 * @throws CException
	 */
	public function moveAsChildOf(CActiveRecord $node, CActiveRecord $parentNode, $position)
	{
		$this->getProperties($colLeft, $colRight, $colLevel, $colParent);
		
		if ($parentNode === $node || $node->isAncestorOf($parentNode)) {
			throw new CException("Cannot move node as first child of itself or into a descendant");
			return false;
		}
		// we should check the position is a sensible one
		if ($position != 0) {
			$children = $parentNode->getChildren();
			// position describes the new position for this node so look at the
			// sibling node above to calculate the new position of this node,
			// the child array is zero based
			$position = $position - 1;
			if (!$children->offsetExists($position))
				throw new CException("No child exists at position $position");

			$newLeft = $children[$position]->$colRight + 1;
		}else {
			// position is zero so insert as first child
			$newLeft = $parentNode->$colLeft + 1;
		}

		$oldLevel = $node->$colLevel;
		$node->$colLevel = $parentNode->$colLevel + 1;
		$node->$colParent = $parentNode->getPrimaryKey();
		$node->save();

		$this->_updateNode($node, $newLeft, $node->$colLevel - $oldLevel);

		return true;
	}

	/**
	 * moves node as last child of $parentNode
	 * 
	 * @param CActiveRecord $parentNode the parent node
	 * @param CActiveRecord $node the record that will become the last cild of $parentNode if null uses the owner
	 * @throws CException
	 * @return void
	 */
	public function moveAsLastChildOf(CActiveRecord $parentNode, CActiveRecord $node=null)
	{
		$this->getProperties($colLeft, $colRight, $colLevel, $colParent);
		
		if ($node===null)
			$node = $this->getOwner();
		if ($parentNode === $node || $node->isAncestorOf($parentNode))
			throw new CException("Cannot move node as last child of itself or into one of its ancestors");

		$oldLevel = $node->$colLevel;
		$node->$colLevel = $parentNode->$colLevel + 1;
		$node->$colParent = $parentNode->getPrimaryKey();
		
		$node->save();
		
		$this->_updateNode($node, $parentNode->$colRight, $node->$colLevel - $oldLevel);
	}

	/**
	 * moves $node before $refNode
	 * 
	 * @param CActiveRecord $node the node to move before $refNode
	 * @param CActiveRecord $refNode 
	 * @return void
	 */
	public function moveBeforeNode($node, $refNode)
	{
		$this->getProperties($colLeft, $colRight, $colLevel, $colParent);
		$oldLevel = $node->$colLevel;
		$node->$colLevel = $refNode->$colLevel;
		$node->$colParent = $refNode->$colParent;
		$node->save();
		$this->_updateNode($node, $refNode->$colLeft, $node->$colLevel - $oldLevel);
	}

	/**
	 * moves $node after the $refNode
	 * 
	 * @param CActiveRecord $node 
	 * @param CActiveRecord $refNode 
	 */
	public function moveAfterNode($node, $refNode)
	{
		$this->getProperties($colLeft, $colRight, $colLevel, $colParent);
		$oldLevel = $node->$colLevel;
		$node->$colLevel = $refNode->$colLevel;
		$node->$colParent = $refNode->$colParent;
		$node->save();
		$this->_updateNode($node, $refNode->$colRight + 1, $node->$colLevel - $oldLevel);
	}
	
	/**
	 * move node's and its children to location $destLeft and updates rest of tree
	 *
	 * @param int $destLeft destination left value
	 */
	private function _updateNode($node, $destLeft, $levelDiff)
	{
		$this->getProperties($colLeft, $colRight, $colLevel);
		$left = $node->$colLeft;
		$right = $node->$colRight;
		$treeSize = $right - $left + 1;

		try {
			$t = $this->getDb()->beginTransaction();

			// Make room in the new branch  
			$this->_shiftRlValues($destLeft, intval($treeSize));

			if ($left >= $destLeft) { // src was shifted too?
				$left += $treeSize;
				$right += $treeSize;
			}

			// update level for descendants
			$tableName = $this->getOwner()->tableName();
			$sql = "UPDATE `$tableName` SET $colLevel = $colLevel + $levelDiff WHERE $colLeft > $left AND $colRight < $right";
			$this->getDb()->createCommand($sql)->execute();
			
			// now there's enough room next to target to move the subtree
			$this->_shiftRlRange($left, $right, $destLeft - $left);

			// correct values after source (close gap in old tree)
			$this->_shiftRlValues($right + 1, -$treeSize);

			$t->commit();
		} catch (Exception $e) {
			$t->rollback();
			throw $e;
		}

		return true;
	}

	/**
	 * adds '$delta' to all Left and Right values that are >= '$first'. '$delta' can also be negative.
	 *
	 * Note: This method does not wrap its database queries in a transaction. This should be done
	 * by the invoking code.
	 *
	 * @param int $first First node to be shifted
	 * @param int $delta Value to be shifted by, can be negative
	 */
	public function _shiftRlValues($first, $delta)
	{
		$this->getProperties($left, $right);

		// shift left columns
		$tableName = $this->getOwner()->tableName();
		$this->getDb()->createCommand("UPDATE `$tableName` SET `$left` = $left + $delta WHERE $left >= $first")->execute();
		$this->getDb()->createCommand("UPDATE `$tableName` SET `$right` = $right + $delta WHERE $right >= $first")->execute();		
	}
	
	/**
	 * get the active db connection
	 * 
	 * @return CDbConnection 
	 */
	public function getDb()
	{
		return $this->getOwner()->getDbConnection();
	}

	/**
	 * adds '$delta' to all Left and Right values that are >= '$first' and <= '$last'.
	 * '$delta' can also be negative.
	 *
	 * Note: This method does not wrap its database queries in a transaction. This should be done
	 * by the invoking code.
	 *
	 * @param int $first First node to be shifted (L value)
	 * @param int $last	Last node to be shifted (L value)
	 * @param int $delta Value to be shifted by, can be negative
	 */
	private function _shiftRlRange($first, $last, $delta)
	{
		$this->getProperties($left,$right);
		$tableName = $this->getOwner()->tableName();
		$this->getDb()->createCommand("UPDATE `$tableName` SET `$left` = $left + $delta WHERE $left >= $first AND $left <= $last")->execute();
		$this->getDb()->createCommand("UPDATE `$tableName` SET `$right` = $right + $delta WHERE $right >= $first AND $right <= $last")->execute();	
	}

	/**
	 * Recusive *Expensive* function that rebuilds the tree based on the parentId
	 * 
	 * @param CActiveRecord $parent
	 * @param int $left
	 * @param int $level
	 * @return void
	 */
	public function rebuildTree($parent, $left, $level=0)
	{
		$this->getProperties($colLeft,$colRight,$colLevel,$colParent);
		
		// the right value of this node is the left value + 1
		$right = $left + 1;

		// get all children of this node
		// getChildrenByParentId
		$children = $this->getOwner()->findAll(array(
			'condition'=>"$colParent=:pid", 
			'params'=>array(':pid'=>$parent->getPrimaryKey()),
			'order'=>$colLeft
		));

		foreach ($children as $node) {
			// recursive execution of this function for each child of this node
			// $right is the current right value, which is
			// incremented by the rebuild_tree function
			$right = $this->rebuildTree($node, $right, $level + 1);
		}

		// we've got the left value, and now that we've processed
		// the children of this node we also know the right value
		$parent->$colLevel = $level;
		$parent->$colLeft = $left;
		$parent->$colRight = $right;
		$parent->save();

		// return the right value of this node + 1
		return $right + 1;
	}

	/**
	 * Checks and settings that should be carried out after a tree table has been created
	 * by default a tree table needs a root node in order to add new nodes.
	 * 
	 * @return void
	 */
	public function afterInstall($event)
	{
		$this->getProperties($left, $right, $level, $parent);
		// check we don't already have a root node
		
		if ($this->getOwner()->countByAttributes(array($left=>0)))
			return;
		
		// if the table exists
		if($this->getDb()->getSchema()->getTable($event->sender->tableName())){
			// ok, so create a root node
			$rowClass = get_class($this->getOwner());
			$row = new $rowClass;
			$row->name = 'root';
			$row->$left = 0;
			$row->$right = 1;
			$row->$parent = 0;
			$row->$level = 0;
			$row->save();
		}
	}

	/**
	 * if the node has children
	 * 
	 * @return boolean 
	 */
	public function hasChildren()
	{
		$this->getProperties($left, $right);
		$node = $this->getOwner();
		return ($node->$right > ($node->$left + 1)) ? true : false;
	}

	/**
	 * Get an array in jstree format representing all nodes within the $rootNode
	 * 
	 * @param string $nameCol the column name to use ass the nodes name
	 * @param string $typeCol the column name to use as the nodes type
	 * @param CActiveRecord $rootNode the node to start from (typically the root)
	 * @return array 
	 */
	public function generateJstreeArray($rootNode=null, $nameCol='name', $typeCol=null)
	{
		$tree= null;
		if($rootNode===null)
			$rootNode = $this->getTreeRoot();
		$this->_generateJstreeArrayRecursive($rootNode, $tree, $nameCol, $typeCol);
		return $tree;
	}
	
	/**
	 * recursive function to generate jstree array
	 * 
	 * @param string $nameCol the column name to use ass the nodes name
	 * @param string $typeCol the column name to use as the nodes type
	 * @param CActiveRecord $rootNode
	 * @param array $tree 
	 */
	private function _generateJstreeArrayRecursive($rootNode, &$tree=array(), $nameCol='name', $typeCol=null)
	{
		// if child
		$myArr = $this->getJstreeNodeArray($rootNode, $nameCol, $typeCol);
		$tree[] =& $myArr;
		
		if($rootNode->hasChildren()){
			$items = array();
			$myArr['children'] =& $items;
			foreach($rootNode->getChildren() as $child) {
				$this->_generateJstreeArrayRecursive($child, $items, $nameCol, $typeCol);
			}
		}
	}
	
	/**
	 * Return the array format for a single jstree node.
	 * This ignores the node's children to complete an entrie array including children
	 * use generateJstreeArray function
	 * 
	 * @param string $nameCol the column name to use ass the nodes name
	 * @param string $typeCol the column name to use as the nodes type
	 * @param CActiveRecord $node
	 * @return array 
	 */
	public function getJstreeNodeArray($node, $nameCol='name', $typeCol=null)
	{
		$ret = array(
			'data'=>$node->$nameCol,
			'attr'=>array(
				'data-id'=>$node->getPrimaryKey(),
				'data-name'=>$node->name,
				'title'=>$node->description,
				'id'=>'treenode'.$node->getPrimaryKey()
			)
		);
		if($typeCol!==null)
			$ret['attr']['rel'] = $node->$typeCol;
		if($node->hasChildren()){
			$ret['state'] = 'closed';
		}
		return $ret;
	}

}