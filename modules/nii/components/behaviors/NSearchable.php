<?php

/**
 * NVersionable class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * NVersionable records changes to a record.
 * To use this behavior you must attach it to both the versionable model class and the history class.
 * Both the Versionable and history class must extend NActiveRecord.
 * The history class only needs a tableName() function and a behaviors() function (where you must add this behavior to)
 * You must set the $historyModelClass and $versionableModelClass on both models
 * 
 * # limitations
 * Assumes the primary key column in the versionable model is called "id"
 * Does not handle composite primary keys (yet)
 */
class NVersionable extends NActiveRecordBehavior
{

	/**
	 * 
	 */
	public function beforeSave($event)
	{
		// re-index
	}
	
	/**
	 * 
	 */
	public function beforeDelete($event)
	{
		// remove index
	}

}