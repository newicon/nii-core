<?php
/**
 * NTrashBinBehavior class file.
 *
 * Based on work by:
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @link http://code.google.com/p/yiiext/
 * @license http://www.opensource.org/licenses/mit-license.php
 */

/**
 * TODO: why is this called trash behavior but then you cant call model->trash. 
 * instead its reffered to everywhere as *removed*
 * 
 * 
 * NTrashBinBehavior allows you to remove the model in the trash bin and restore them when needed.
 * Added additional schema function to add a trash column to the table when the behavior is added
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @version 0.2
 * @package yiiext.behaviors.model.trashBin
 * @since 1.1.4
 */
class NTrashBinBehavior extends NActiveRecordBehavior
{
	/**
	 * @var string The name of the table field where the data is stored.
	 * Required to set on init behavior.
	 * Defaults to "trashed"
	 */
	public $trashFlagField = 'trashed';
	
	/**
	 * @var mixed The value to set for removed model.
	 * Default is 1.
	 */
	public $removedFlag=1;
	
	/**
	 * @var mixed The value to set for restored model.
	 * Default is 0.
	 */
	public $restoredFlag=0;
	
	/**
	 * @var bool If except removed model in find results.
	 * Default is false.
	 */
	public $findRemoved=false;
	
	/**
	 * @param bool The flag to disable filter removed models in the next query.
	 */
	protected $_withRemoved=false;
	
	/**
	 * Override default attach behavior function
	 * @param type $owner
	 * @throws CException
	 */
	public function attach($owner)
	{
		// Check required var trashFlagField
		if(!is_string($this->trashFlagField) || empty($this->trashFlagField))
		{
			throw new CException(Yii::t('yiiext','Required var "{class}.{property}" not set.',
				array('{class}'=>get_class($this),'{property}'=>'trashFlagField')));
		}
		parent::attach($owner);
	}
	
	/**
	 * Set value for trash field.
	 *
	 * @param mixed Value for trash field.   
	 * @return CActiveRecord
	 */
	public function setTrashFlag($value)
	{
		$owner=$this->getOwner();
		$owner->{$this->trashFlagField}=$value;
		if ($value == $this->removedFlag)
			$owner->trashed_on = NTime::unixToDatetime();
		
		return $owner;
	}
	
	/**
	 * Event stub to call after an object is trashed
	 * @param CEvent $event
	 */
	public function onAfterTrash($event)
	{
		$this->raiseEvent('onAfterTrash', $event);
	}
	
	/**
	 * alias to self::remove
	 * also assumes you want to save
	 * @return boolean result of parent save call
	 */
	public function trash()
	{
		$this->remove();
		$this->onAfterTrash(new CEvent($this));
		// we are trashing this record, so don't bother running the validation
		return $this->getOwner()->save(false);
	}
	
	/**
	 * Remove model in trash bin.
	 *
	 * @return CActiveRecord
	 */
	public function remove()
	{
		return $this->setTrashFlag($this->removedFlag);
	}
	
	/**
	 * Restore removed model from trash bin.
	 *
	 * @return CActiveRecord
	 */
	public function restore()
	{
		return $this->setTrashFlag($this->restoredFlag);
	}
	
	/**
	 * Check if model is removed in trash bin.
	 *
	 * @return bool
	 */
	public function getIsRemoved()
	{
		return $this->getOwner()->{$this->trashFlagField}==$this->removedFlag;
	}
	
	/**
	 * Check if model is removed in trash bin.
	 *
	 * @return bool
	 */
	public function getIsTrashed()
	{
		return $this->getIsRemoved();
	}
	
	/**
	 * Disable excepting removed models for next search.
	 *
	 * @return CActiveRecord
	 */
	public function withRemoved()
	{
		$this->_withRemoved=true;
		return $this->getOwner();
	}
	
	/**
	 * Disable excepting removed models for next search.
	 *
	 * @return CActiveRecord
	 */
	public function withTrashed()
	{
		return $this->withRemoved();
	}
	
	/**
	 * Add condition to filter out removed models.
	 *
	 * @return CActiveRecord
	 * @since 1.1.4
	 */
	public function filterRemoved()
	{
		$owner=$this->getOwner();
		$criteria=$owner->getDbCriteria();
		//Yii::app()->db->quoteColumnName($this->trashFlagField);
		$criteria->addCondition($this->tableAlias().'.'.$this->trashFlagField.'!='.CDbCriteria::PARAM_PREFIX.CDbCriteria::$paramCount);
		$criteria->params[CDbCriteria::PARAM_PREFIX.CDbCriteria::$paramCount++]=$this->removedFlag;

		return $owner;
	}
	
	/**
	 * Add condition to filter out removed models.
	 *
	 * @return CActiveRecord
	 * @since 1.1.4
	 */
	public function filterTrashed()
	{
		return $this->filterRemoved();
	}
	
	/**
	 * scope to retrun all trashed models
	 * @return NActiveRecord
	 */
	public function removed()
	{
		$owner=$this->getOwner()->withRemoved();
		$criteria=$owner->getDbCriteria();
		$criteria->addCondition($this->tableAlias().'.'.$this->trashFlagField.' = '.$this->removedFlag);
		return $owner;
	}
	
	/**
	 * scope to retrun all trashed models
	 * @return NActiveRecord
	 */
	public function trashed()
	{
		return $this->removed();
	}
	
	/**
	 * Add condition before find, for except removed models.
	 *
	 * @param CEvent
	 */
	public function beforeFind($event)
	{
		if($this->getEnabled() && !$this->findRemoved && !$this->_withRemoved)
			$this->filterRemoved();

		$this->_withRemoved=false;
		parent::beforeFind($event);
	}
	
	/**
	 * returns the alias of the table
	 * @return string
	 */
	public function tableAlias()
	{
		return $this->getOwner()->getTableAlias(false, false);
	}
	
	/**
	 * implements NActiveRecordBehavior to modify parent table schema
	 * @return array
	 */
	public function schema()
	{
		$field = $this->trashFlagField;
		return array(
			'columns'=>array(
				"$field"=>'tinyint NOT NULL default 0',
				"trashed_on"=>'datetime null'
			)
		);
	}
}