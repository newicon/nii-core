<?php

class NPdfCreator extends CApplicationComponent
{

  /****************************************************************************
   * CONSTANTS
   ***************************************************************************/

  /*
   * PAGE ORIENTATION CONSTANTS
   */
  const ORIENTATION_PORTRAIT  = 'Portrait';
  const ORIENTATION_LANDSCAPE = 'Landscape';

  const PAGESIZE_A4           = 'A4';
  const PAGESIZE_LETTER       = 'letter';

  /***************************************************************************/

  // These can be configured through application configuration
  public $orientation = self::ORIENTATION_PORTRAIT;
  public $pageSize    = self::PAGESIZE_A4;
  public $binary      = '/usr/local/bin/wkhtmltopdf';

  public $coverUrl    = null;
  public $coverOptions= array();
  public $toc         = false;
  public $tocOptions  = array();

  protected $_html    = null;
  protected $_pages   = array();
  protected $_title   = null;
  protected $_genOptions = array();
  




  /***************************************************************************/

  /**
   * Implementation of CApplicationComponent::init 
   *
   * @return parent::init()
   */
  public function init()
  {
    $this->getBinaryPath();
    return parent::init();
  }

  /**
   * Set the path to the binary executable
   * @param string $path - the new binary path
   * @return null
   */
  public function setBinaryPath($path)
  {
    if(realpath($path)==false)
      throw new Exception("Path is not absolute");
    $this->binary = realpath($path);
  }

  /**
   * Return the binary executable path
   * Throws error if it does not exist
   * @return string 
   */
  public function getBinaryPath()
  {
    if(file_exists($this->binary)===false)
      throw new Exception("Unable to find wkhtmltopdf binary!");
    return $this->binary;

  }

  /**
   * Get the orientation
   * @return string
   */
  public function getOrientation()
  {
    return $this->orientation;
  }

  /**
   * Set the orientation
   * @param string $orientation - the new orientation, either 
   *        NPdfCreator::ORIENTATION_LANDSCAPE or NPdfCreator::ORIENTATION_PORTRAIT
   * @return null
   */
  public function setOrientation($orientation)
  {
    if(in_array($orientation,array(self::ORIENTATION_LANDSCAPE,self::ORIENTATION_PORTRAIT))==false)
      throw new Exception("Unknown orientation $orientation");
    $this->orientation = $orientation;
    return;
  }

  /**
   * Get the current page size setting
   * @return string
   */
  public function getPageSize()
  {
    return $this->pageSize;
  }

  /**
   * Set the page size
   * @param string $pageSize - either NPdfCreator::PAGESIZE_A4 or NPdfCreator::PAGESIZE_LETTER
   * @return null
   */
  public function setPageSize($pageSize)
  {
    $this->pageSize = (string)$pageSize;
    return;
  }
  
  /**
   * Get the document title
   */
  public function getTitle()
  {
    return $this->_title;
  }
  
  /**
   * Set the title
   */
  public function setTitle($title)
  {
    $this->_title = $title;
  }
  
  /**
   * Add a page to the output
   * @param string $pageUrl - the url of the page to include
   * @param array $pageOptions - an array of options to pass with this page. 
   *        See the wkhtmltopdf documentation for the options that can be 
   *        provided
   * @return null
   */
  public function addPage($pageUrl,$pageOptions = array())
  {
    $this->_pages[$pageUrl] = $pageOptions;
  }

  /**
   * Set the cover for the output
   * @param string $coverUrl - the url of the cover
   * @param array $pageOptions - an array of options to pass with the cover
   *        See the wkhtmltopdf documentation for details of these options
   * @return null
   */
  public function setCover($coverUrl, $coverOptions = array())
  {
    $this->coverUrl = $coverUrl;
    $this->coverOptions = $coverOptions;
  }

  /**
   * Set the TOC
   * @param array $tocOptions - an array of options to pass with the TOC
   *        See the wkhtmltopdf documentation for more details 
   */
  public function setTOC($tocOptions = array())
  {
    $this->toc = true;
    $this->tocOptions = $tocOptions;
  }
  
  /**
   * Set the general options
  **/
  public function setGenOptions($genOptions = array())
  {
    $this->_genOptions = $genOptions;
  }

  /**
   * Internal function to generate the required command-line based
   * on the object state
   * @return string
   */
  protected function _createCommand()
  {
    $command  = $this->getBinaryPath();
    $command .= " --orientation " . $this->getOrientation();
    $command .= " --page-size " . $this->getPageSize();

    // Append the title, if set
    if(isset($this->_title))
    {
      $command .= " --title '" . $this->_title ."'";
    }
    
    // Append any general options
    if(!empty($this->_genOptions))
    {
      foreach($this->_genOptions as $gOpt => $gVal)
      {
        $command .= sprintf(" --%s %s",$gOpt, is_null($gVal) ? '' : "'$gVal'");
      }
    }
    
    $command .= " ";

    // Append the TOC, if any
    if($this->toc === true)
    {
      $command .= "toc ";
      foreach($tocOptions as $tOpt => $tVal)
      {
        $command .= " --$tOpt $tVal";
      }
    }

    // Append the cover, if any
    if(isset($this->coverUrl))
    {
      $command .= "cover " . $this->coverUrl;
      foreach($coverOptions as $cOpt => $cVal)
      {
        $command .= " --$cOpt $cVal";
      }
    }

    // Append pages to the command
    foreach($this->_pages as $pageUrl => $pageOptions)
    {
      $command .= "page $pageUrl";
      foreach($pageOptions as $pOpt => $pVal)
      {
        $command .= " --$pOpt $pVal";
      }
    }
    $command .= " -"; // Output goes to stdout
    return $command;
  }

  /**
   * Internal function to create the PDF to stdout, capture it
   * and return it
   * @return string - the raw PDF data
   */
  protected function _createPdf()
  {
    $command = $this->_createCommand();
    $content = $this->_pipeExec($command);
    // Add error checking

    $data = $content['stdout'];
    return $data;
  }

  /**
   * Primary function of the class, this function outputs the PDF 
   * to a specified filename
   * @param string $filename - the output filename
   * @param boolean $download - true if download file rather than save to FS
   * @return null
   */
  public function output($filename, $download=false)
  {
    // Check we have some pages to render
    if(count($this->_pages) === 0)
      throw new Exception("At least one page must be added before outputing");

    if (!$download)
      file_put_contents($filename,$this->_createPdf());
    else
    {
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Type: application/pdf');
        header('Content-Type: application/download');
        header('Content-Transfer-Encoding: binary');
        header('Content-Disposition: attachment; filename="'.$filename.'"');

        echo($this->_createPdf());
    }

    return;
  }

  /**
   * Advanced Execution Routine
   * @param string $cmd - the command to be run
   * @param string $input - any additional input to be piped to the command
   * @return array('stdout' => $stdout, 'stderr' => $stderr, 'return' => $return)
   */
  private static function _pipeExec($cmd, $input=''){
    $pipes = array();
    $proc = proc_open($cmd, array(0 => array('pipe', 'r'), 1 => array('pipe', 'w'), 2 => array('pipe', 'w')), $pipes, null, null, array('binary_pipes'=>true));
    fwrite($pipes[0], $input);
    fclose($pipes[0]);

          // From http://php.net/manual/en/function.proc-open.php#89338
    $read_output = $read_error = false;
    $buffer_len  = $prev_buffer_len = 0;
    $ms          = 10;
    $stdout      = '';
    $read_output = true;
    $stderr      = '';
    $read_error  = true;
    stream_set_blocking($pipes[1], 0);
    stream_set_blocking($pipes[2], 0);

          // dual reading of STDOUT and STDERR stops one full pipe blocking the other, because the external script is waiting
    while ($read_error != false or $read_output != false){
      if ($read_output != false){
        if(feof($pipes[1])){
          fclose($pipes[1]);
          $read_output = false;
        } else {
          $str = fgets($pipes[1], 1024);
          $len = strlen($str);
          if ($len){
            $stdout .= $str;
            $buffer_len += $len;
          }
        }
      }

      if ($read_error != false){
        if(feof($pipes[2])){
          fclose($pipes[2]);
          $read_error = false;
        } else {
          $str = fgets($pipes[2], 1024);
          $len = strlen($str);
          if ($len){
            $stderr .= $str;
            $buffer_len += $len;
          }
        }
      }

      if ($buffer_len > $prev_buffer_len){
        $prev_buffer_len = $buffer_len;
        $ms = 10;
      } else {
                  usleep($ms * 1000); // sleep for $ms milliseconds
                  if ($ms < 160){
                    $ms = $ms * 2;
                  }
                }
              }

            $rtn = proc_close($proc);
              return array(
                'stdout' => $stdout,
                'stderr' => $stderr,
                'return' => $rtn
                );
            }
}