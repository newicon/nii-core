<?php

/**
 * POST /faye HTTP/1.1
 * Host: hub.newicon.net:8008
 * Content-Type: application/json
 * Cache-Control: no-cache
 * 
 * {"channel":"/push/messages", "data":{"event":"message", "message":"Hi there"}}
 */
class NFaye
{
	public $url = 'https://hub.newicon.net:8008/faye';

	public function __construct()
	{

	}

	public function init()
	{

	}

	public function send($channel, $data = array())
	{
		$this->postJSON($this->url, json_encode(array('channel' => $channel, 'data' => $data)));
	}

	// This is an adapter function so we can drop-in replace NPush
	public function trigger($channel,$type,$data = array())
	{
		$channel = '/hub/' . str_replace('-','/',$channel);
		$this->send($channel, array('type' => $type, 'message' => $data));
	}

	public function postJSON($url,$body)
	{
		$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_VERBOSE, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);
		// Set connection timeout to a fraction of the total time, e.g., 0.2 seconds
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0.5);
		// Set total timeout to 0.5 seconds for the entire cURL operation
        curl_setopt($curl, CURLOPT_TIMEOUT, 0.5);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($body)
            )
        );
        //curl_exec($curl);
        //curl_close($curl);
	}

	public function registerScript()
	{
		// include faye from assets instead of by the faye service as it breaks all javascript
		// if faye service is not running
		$path = Yii::app()->getAssetManager()->publish(dirname(__FILE__).'/assets');
		//Yii::app()->getClientScript()->registerScriptFile($path . '/client.js');
	}
}