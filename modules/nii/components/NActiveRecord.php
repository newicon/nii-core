<?php

/**
 * {name} class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * NActiveRecord
 *
 * @author steve
 */
class NActiveRecord extends CActiveRecord 
{
	/**
	 * Shorthand method to Yii::app()->db->createCommand()
	 *
	 * @return CDbCommand
	 */
	public function cmd() {
		return Yii::app()->db->createCommand()->from($this->tableName());
	}

	/**
	 * ability to create a command from the records current dbCriteria
	 * this enables:
	 * ProjectTask::model()->my()->tasks()->totalBudget()->commandFind()->queryScalar() 
	 * @param CDbCriteria $criteria
	 * @return CDbCommand
	 */
	public function cmdCriteria($criteria = null) {
		$criteria = ($criteria === null) ? $this->getDbCriteria() : $criteria;
		return Yii::app()->db->commandBuilder->createFindCommand($this->tableName(), $criteria);
	}

	/**
	 * Shorthand method to Yii::app()->db->createCommand()->select()->from
	 *
	 * @return CDbCommand
	 */
	public function cmdSelect($select = '*') {
		return Yii::app()->db->createCommand()->select($select)->from($this->tableName());
	}

	/**
	 * proxies to getPrimaryKey method
	 * @see CActiveRecord::getPrimaryKey
	 * @return mixed primary key value 
	 */
	public function id() {
		return $this->getPrimaryKey();
	}

	/**
	 * return an array of the table schema,
	 * you can use Yii's abstract column names as defined in 
	 * yii.db.ar.schema.mysql.CMysqlSchema $columnTypes
	 * 
	 * For mysql the columnTypes are as follows: (as of Yii 1.1.7)
	 * 'pk' => 'int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY',
	 * 'string' => 'varchar(255)',
	 * 'text' => 'text',
	 * 'integer' => 'int(11)',
	 * 'float' => 'float',
	 * 'decimal' => 'decimal',
	 * 'datetime' => 'datetime',
	 * 'timestamp' => 'timestamp',
	 * 'time' => 'time',
	 * 'date' => 'date',
	 * 'binary' => 'blob',
	 * 'boolean' => 'tinyint(1)',
	 * 
	 * You can also specify table indexs.
	 * 'keys'=>array(
	 * 		array('string: index name', 'string: index table column name', 'boolean: unique (defaults to false)')
	 * )
	 * 
	 * You can also specify foreign key relationships
	 * @see CDbCommand->aaddForeignKey() 
	 * 'foreignKeys'=>array(
	 * 		array(
	 * 			'string: name of relation',
	 * 			'string: column',
	 * 			'string: ref table name',
	 * 			'string: ref columns separate with commas for multiple columns',
	 * 			'string: the ON DELETE command string (e.g. CASCADE)',
	 * 			'string: the ON UPDATE command string (e.g. NO ACTION)'
	 * 	)
	 * 
	 * A typical example to create a contact table
	 * If a column is specified with definition only (e.g. 'PRIMARY KEY (name, type)'), it will be directly inserted into the generated SQL.
	 * return array(
	 * 		'columns'=>array(
	 * 			'id'=>'pk',
	 * 			'title'=>'string',
	 * 			'first_name'=>'string NOT NULL',
	 * 			'last_name'=>'string',
	 * 			'company'=>'string',
	 * 			'type'=>"enum('CONTACT','COMPANY','USER')",
	 * 			'company_id'=>'int',
	 * 			'user_id'=>'int',
	 * 			'PRIMARY KEY (name, type)'
	 * 		),
	 * 		'keys'=>array(
	 * 			array('index_company_id', 'company_id'),
	 * 			array('user_id')
	 * 			array('username_key', 'username', true) // create a unique index
	 * 		)
	 * 		'foreignKeys'=>array(
	 * 			array('user_to_contact','user_id','tbl_user','id','CASCADE','CASCADE')
	 * 		)
	 * 	);
	 */
	public function schema() {
		return array();
	}

	/**
	 * if true it will drop any database that currently exists and reinstall it
	 */
	public static $installForce = false;

	/**
	 * install the table from its defined schema, the child active record model
	 * must implement the schema function.
	 * The install can be called on existing tables, if the schema defined in the php
	 * class file is different from the mysql table the function will attempt to merge them.
	 * It will ONLY ADD columns, indexs or foreign key constraints
	 * 
	 * @param string $className 
	 */
	public static function install($className = __CLASS__)
	{
		Yii::log('Installing model: "'.$className.'"', CLogger::LEVEL_INFO, 'NActiveRecord::install');

		// set up a new instance
		$t = new $className(null);
		// attach behaviors (allows behaviors to be aware of the install process
		$t->attachBehaviors($t->behaviors());

		// raise before install events (hooks for behaviors)
		$t->beforeInstall();
		$t->onBeforeInstall(new CEvent($t));

		// do the install
		$db = $t->getDbConnection();
		$realTable = $t->getRealTableName();
		$exists = $db->getSchema()->getTable($t->tableName(), true);

		if (self::$installForce && $exists) {
			// the table exists we want to force install it so lets drop it
			// so that it is recreated
			Yii::app()->db->createCommand()->dropTable($t->tableName());
			Yii::log('Dropped table: "'.$t->tableName().'"', CLogger::LEVEL_INFO, 'NActiveRecord::install');
			$exists = $db->getSchema()->getTable($t->tableName());
		}

		$s = $t->getSchemaComplete();

		if (!array_key_exists('columns', $s))
			throw new CException('The schema array must contain the array key "columns" with an array of the columns');
		// fill the options key with null if not defined
		$s['options'] = array_key_exists('options', $s) ? $s['options'] : null;

		if ($db->getDriverName() == 'mysql') {
			self::installMysql($db, $t, $realTable, $s, $exists);
		} else if (preg_match('/^(dblib|sqlsrv)$/', $db->getDriverName())) {
			self::installDblib($db, $t, $realTable, $s, $exists);
		}
		else
			Yii::log("DB driver " . $db->getDriverName() . " not recognised", 'error');

		$t->afterInstall();
		$t->onAfterInstall(new CEvent($t));
	}

	public static function installMysql($db, $t, $tableName, $schema, $exists)
	{
		// add table columns
		if (!$exists) {
			// set default table options if no options defined
			if ($schema['options'] == null)
				$schema['options'] = 'ENGINE=InnoDB DEFAULT CHARSET=utf8';
			$db->createCommand()->createTable($tableName, $schema['columns'], $schema['options']);
			$exists = $db->getSchema()->getTable($t->tableName());
			Yii::log('Created table: "'.$tableName.'"', CLogger::LEVEL_INFO, 'NActiveRecord::install');
		} else {
			// adds columns that dont exist in the database
			// a column can also be a sql statement and so the key is not a column
			// lets remove these (all columns where the key is not a string must be a statement and not a column
			$cols = array();
			foreach ($schema['columns'] as $key => $c)
				if (is_string($key))
					$cols[$key] = $c;
			$missingCols = array_diff(array_keys($cols), array_keys($exists->columns));
			
			
			foreach ($missingCols as $col) {
				$db->createCommand()->addColumn($tableName, $col, $schema['columns'][$col]);
				Yii::log("Add table column: \"$col\" to table: \"$tableName\"", CLogger::LEVEL_INFO, 'NActiveRecord::install');
			}
		}

		// add table keys / indexs
		if (array_key_exists('keys', $schema)) {
			// see what keys the current table has
			$curtKeysRes = $db->createCommand("show keys from $tableName")->queryAll();
			// build a flat quick access array
			$curKeys = array();
			foreach ($curtKeysRes as $keyRow)
				$curKeys[] = $keyRow['Key_name'];

			foreach ($schema['keys'] as $k) {
				$name = $k[0];
				$unique = array_key_exists(2, $k) ? $k[2] : false;
				$column = array_key_exists(1, $k) ? $k[1] : $name;
				// check if key is already in the table
				if (!in_array($name, $curKeys)) {
					$db->createCommand()->createIndex($name, $tableName, $column, $unique);
					Yii::log("Add index: \"$column\" to table: \"$tableName\"", CLogger::LEVEL_INFO, 'NActiveRecord::install');
				}
			}
		}

		// add foriegn keys
		if (array_key_exists('foreignKeys', $schema)){
			foreach ($schema['foreignKeys'] as $f) {
				$delete = array_key_exists(4, $f) ? $f[4] : NULL;
				$update = array_key_exists(5, $f) ? $f[5] : NULL;
				// if the key is not already in on the table add it.
				if ($exists && !array_key_exists($f[1], $exists->foreignKeys))
					$db->createCommand()->addForeignKey(
						$f[0], $tableName, $f[1], $f[2], $f[3], $delete, $update);
			}
		}
	}

	public static function installDblib($db, $t, $tableName, $schema, $exists)
	{
		// add table columns
		$schema['columns'] = array_key_exists('dblib', $schema) ? $schema['dblib']['columns'] : $schema['columns'];
		if (!$exists) {
			//dp($schema['columns']);exit;
			$db->createCommand()->createTable(
				$tableName, $schema['columns'], $schema['options']);
			$exists = $db->getSchema()->getTable($t->tableName());
			Yii::log('Created table: "'.$tableName.'"', CLogger::LEVEL_INFO, 'NActiveRecord::install');
		} else {
			// adds columns that dont exist in the database
			// a column can also be a sql statement and so the key is not a column
			// lets remove these (all columns where the key is not a string must be a statement and not a column
			$cols = array();
			foreach ($schema['columns'] as $key => $c)
				if (is_string($key))
					$cols[$key] = $c;
			$missingCols = array_diff(array_keys($cols), array_keys($exists->columns));
			foreach ($missingCols as $col) {
				$db->createCommand()->addColumn($tableName, $col, $schema['columns'][$col]);
				Yii::log("Add table column: \"$col\" to table: \"$tableName\"", CLogger::LEVEL_INFO, 'NActiveRecord::install');
			}
		}
		if (array_key_exists('keys', $schema)) {
			// TODO
		}
	}

	/**
	 * removes the table from the database and destroys all data
	 * BE CAREFUL!
	 */
	public static function uninstall($className)
	{
		$tbl = NActiveRecord::model($className)->getRealTableName();
		Yii::app()->db->createCommand()->dropTable($tbl);
	}

	/**
	 * method that can be overriden in child classes
	 */
	public function beforeInstall() {}

	/**
	 * method that can be overriden in child classes
	 */
	public function afterInstall() {}

	/**
	 * An event called after installing the table, may be used to add rows as part
	 * of the install, you will want to check the table has been created successfully
	 * 
	 * <code>
	 * Yii::app()->getMyDb()->getSchema()->getTable($sender->tableName());
	 * </code>
	 * 
	 * @param CEvent $event 
	 */
	public function onAfterInstall($event) {
		$this->raiseEvent('onAfterInstall', $event);
	}

	/**
	 * an event called before installing the table
	 * 
	 * @param CEvent $event 
	 */
	public function onBeforeInstall($event) {
		$this->raiseEvent('onBeforeInstall', $event);
	}

	/**
	 * tries to determine the standard datatype of the attribute
	 * @param string $attribute 
	 */
	public function getAttributeDataType($attribute) {
		$dataType = 'string';
		$schema = $this->schema();
		if (!empty($schema) && array_key_exists('columns', $schema)) {
			$cols = $schema['columns'];
			if (array_key_exists($attribute, $cols)) {
				$dataType = $cols[$attribute];
			}
		}
		return $dataType;
	}

	/**
	 * Builds the schema array, asks behaviors if they want to add to the schema
	 * @see NActiveRecord::shema with results in attached behaviors 
	 * @see NActiveRecordBahevior::schema
	 * @return array schema
	 */
	public function getSchemaComplete() {
		$schema = $this->schema();
		foreach ($this->behaviors() as $behavior => $config) {
			if ($this->asa($behavior) instanceof NActiveRecordBehavior) {
				$schema = CMap::mergeArray($schema, $this->asa($behavior)->schema());
				if (isset($schema['dblib'])) {
					$schema['dblib'] = CMap::mergeArray($schema['dblib'], $this->asa($behavior)->schema());
				}
			}
		}
		return $schema;
	}

	/**
	 * Adds tablePrefix to tableName if necessary
	 * returns the raw string table name used by the database
	 * @return string 
	 */
	public function getRealTableName() {
		$realName = $this->tableName();
		if ($this->getDbConnection()->tablePrefix !== null && strpos($realName, '{{') !== false)
			$realName = preg_replace('/\{\{(.*?)\}\}/', $this->getDbConnection()->tablePrefix . '$1', $realName);
		return $realName;
	}

	/**
	 * Generic parent function for displaying a model's columns in a grid (or exporting)
	 * This function should be set in every model extending NActiveRecord and follow the structure that NGridView expects
	 * 
	 * @return array 
	 */
	public function columns() {
		return array();
	}

	/**
	 * 	Returns an array of available scopes to list as buttons above the grid view
	 * 	Currently only items is in use, but this could easily be extended to set the default etc.
	 * 	@return array - uses formatting like 'scopes' in NScopeList and NGridView
	 * 
	 * 	Example array to be returned:
	 * 	array(
	 * 		'items' => array(
	 * 			'scope' => array(
	 * 				'label' => 'Scope Name',
	 * 				'description' => 'Description of the scope',
	 * 			),
	 * 		)
	 * 	)
	 * 
	 */
	public function getGridScopes() {
		return array();
	}

	/**
	 * Get the model in the search scenario state. Populate with search attributes
	 * $_GET[$className];
	 * @param string $className
	 * @return NActiveRecord in scenario search
	 */
	public function searchModel() {
		$className = get_class($this);
		$model = new $className('search');
		$model->unsetAttributes();
		if (isset($_GET[$className]))
			$model->attributes = $_GET[$className];
		return $model;
	}

	/**
	 * Override default CActiveRecord count method as it *forgets* to include criteria included 
	 * by the "before find" event (often used in behaviors) such as trash bin behavior meaning counts of the database
	 * go wrong
	 * @param mixed $condition query condition or criteria.
	 * @param array $params parameters to be bound to an SQL statement.
	 * @return string the number of rows satisfying the specified query condition. Note: type is string to keep max. precision.
	 */
	public function count($condition = '', $params = array()) {
		Yii::trace(get_class($this) . '.count()', 'system.db.ar.CActiveRecord');
		$this->beforeFind();   // +  // rise "onBeforeFind"
		$builder = $this->getCommandBuilder();
		$criteria = $builder->createCriteria($condition, $params);
		$criteria->mergeWith($this->getDbCriteria());  // +  // take model's criteria into account
		$this->applyScopes($criteria);

		if (empty($criteria->with))
			return $builder->createCountCommand($this->getTableSchema(), $criteria)->queryScalar();
		else {
			$finder = new CActiveFinder($this, $criteria->with);
			return $finder->count($criteria);
		}
	}

	/**
	 * Deletes rows with the specified primary key.
	 * See {@link find()} for detailed explanation about $condition and $params.
	 * @param mixed $pk primary key value(s). Use array for multiple primary keys. For composite key, each key value must be an array (column name=>column value).
	 * @param mixed $condition query condition or criteria.
	 * @param array $params parameters to be bound to an SQL statement.
	 * @return integer the number of rows deleted
	 */
	public function deleteByPk($pk, $condition = '', $params = array()) 
	{
		$rowsAffected = parent::deleteByPk($pk, $condition, $params);
		if ($rowsAffected)
			$this->onAfterDeleteByPk(new CEvent($this, array_merge(func_get_args(), array('rowsAffected' => $rowsAffected))));
		return $rowsAffected;
	}

	/**
	 * event raised after calling the NActiveRecord::deleteByPk function
	 * @see CActiveRecord::deleteByPk
	 * @param CEvent $event
	 */
	public function onAfterDeleteByPk($event) 
	{
		$this->raiseEvent('onAfterDeleteByPk', $event);
	}

	/**
	 * Deletes rows with the specified condition.
	 * See {@link find()} for detailed explanation about $condition and $params.
	 * @param mixed $condition query condition or criteria.
	 * @param array $params parameters to be bound to an SQL statement.
	 * @return integer the number of rows deleted
	 */
	public function deleteAll($condition = '', $params = array()) 
	{
		$rowsAffected = parent::deleteAll($condition, $params);
		if ($rowsAffected)
			$this->onAfterDeleteByPk(new CEvent($this, array_merge(func_get_args(), array('rowsAffected' => $rowsAffected))));
		return $rowsAffected;
	}

	/**
	 * event raised after calling the NActiveRecord::deleteAll function
	 * @see CActiveRecord::deleteAll
	 * @param CEvent $event
	 */
	public function onAfterDeleteAll($event) 
	{
		$this->raiseEvent('onAfterDeleteAll', $event);
	}

	/**
	 * Deletes rows which match the specified attribute values.
	 * See {@link find()} for detailed explanation about $condition and $params.
	 * @param array $attributes list of attribute values (indexed by attribute names) that the active records should match.
	 * An attribute value can be an array which will be used to generate an IN condition.
	 * @param mixed $condition query condition or criteria.
	 * @param array $params parameters to be bound to an SQL statement.
	 * @return integer number of rows affected by the execution.
	 */
	public function deleteAllByAttributes($attributes, $condition = '', $params = array())
	{
		$rowsAffected = parent::deleteAllByAttributes($attributes, $condition, $params);
		if ($rowsAffected)
			$this->onAfterDeleteAllByAttributes(new CEvent($this, array_merge(func_get_args(), array('rowsAffected' => $rowsAffected))));
		return $rowsAffected;
	}

	/**
	 * event raised after calling the CActiveRecord::deleteAllByAttributes function
	 * @see CActiveRecord::deleteAllByAttributes
	 * @param CEvent $event
	 */
	public function onAfterDeleteAllByAttributes($event)
	{
		$this->raiseEvent('onAfterDeleteAllByAttributes', $event);
	}
}
