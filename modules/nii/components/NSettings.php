<?php
/**
 * NSettings 
 * 
 * Based on http://www.yiiframework.com/extension/settings/
 *  
 * @package nii 
 * @author steve obrien <steve@newicon.net> credit: twisted1919 (cristian.serban@onetwist.com)
 * @copyright OneTwist CMS (www.onetwist.com)
 * @access public
 *
 * 1.1e - Special thanks to Gustavo (http://www.yiiframework.com/forum/index.php?/user/6112-gustavo/)
 */
class NSettings extends CApplicationComponent
{
	/**
	 * store settings to reduce db calls
	 * array(
	 *	[category] => array([key] => value)
	 * )
	 * @var array
	 */
    protected $settings = array();
	
    /**
	 * store settings to reduce db calls
	 * array(
	 *	[key] => array([category] => value)
	 * )
	 * @var array
	 */
    protected $settingsByKey = array();

	/**
	 * If true the settings will load all settings across all categories and
	 * cahce internally on the $this->setting property. this is good if there are 
	 * no more than a few hundred settings.
	 * If this is false, then the settings will load a category at a time.
	 * Meaning that it will create one db call for each category setting requested.
	 * @property loadAll
	 * @var boolean 
	 */
	public $loadAll = true;

    /**
     * NSettings::set()
     * 
     * @param string $category name of the category 
     * @param mixed $key 
     * can be either a single item (string) or an array of item=>value pairs 
     * @param mixed $value value to set for the key, leave this empty if $key is an array
     * @param bool $toDatabase whether to save the items to the database
     * @return CmsSettings
     */
    public function set($key='', $value='', $category='system', $toDatabase=true)
    {
        if(is_array($key)) {
            foreach($key AS $k=>$v)
                $this->set($k, $v, $category, $toDatabase);
        } else {
			$setting = NSetting::model()->findByPk(array('key'=>$key, 'category'=>$category));
			if ($setting === null) {
				$setting = new NSetting;
				$setting->category = $category;
				$setting->key = $key;
			}
			$setting->value = $this->encode($value);
			$setting->save();
			// update object cache store
			if (!array_key_exists($category, $this->settings))
				$this->settings[$category] = array();
			$this->settings[$category][$key] = $value;
        }
        return $this;   
    }
	
    /**
     * NSettings::get()
     * 
     * @param string $category name of the category
     * @param mixed $key 
     * can be either :
     * empty, returning all items of the selected category
     * a string, meaning a single key will be returned
     * an array, returning an array of key=>value pairs  
     * @param string $default the default value to be returned
     * @return mixed
     */
    public function get($key='', $category='system', $default=null)
    {
    	if ($key) {
			// see if we have the value locally
			// checking key in category
			if ($this->_existsInternal($key, $category)) {
				return $this->settings[$category][$key];
			}
			// load the db
			$this->load($category);
			if ($this->_existsInternal($key, $category)) {
				return $this->settings[$category][$key];
			}
		} else {
			return $this->getAll($category, $default);
		}
		return $default;
    }

    public function getAll($category, $default)
    {
		if (array_key_exists($category, $this->settings)) {
			return $this->settings[$category];
		}
		$this->load($category);
		if (array_key_exists($category, $this->settings)) {
			return $this->settings[$category];
		}
		return $default;
    }

	/**
	 * See if setting is loaded in the $this->settings property
	 * @param type $key
	 * @param type $category
	 * @return type
	 */
	private function _existsInternal($key, $category)
	{
		return (array_key_exists($category, $this->settings) 
			&& array_key_exists($key, $this->settings[$category]));
	}

    /**
     * Delete an item or all items from a category
     * 
     * @param string $category the name of the category 
     * @param mixed $key 
     * can be either:
     * empty, meaning it will delete all items of the selected category 
     * a single key
     * an array of keys
     * @return CmsSettings
     */
    public function delete($key='', $category)
    {
		NSetting::model()->deleteByPk(array('category'=>$category, 'key'=>$key));
        return $this;
    }

    /**
     * load from database the items of the specified category
     * and store internally on $this->settings;
     * @param string $category
     * @return array the items of the category, or an empty array
     */
    public function load($category)
    {
		if ($this->loadAll) {
			$this->loadAll();
			if (array_key_exists($category, $this->settings))
				return $this->settings[$category];
			return array();
		}
		if (!array_key_exists($category, $this->settings)) {
			$settings = Yii::app()->db->createCommand()
				->from('nii_setting')
				->where('category=:category', array(':category'=>$category))
				->queryAll();
			$this->settings[$category] = array();
			foreach ($settings as $set) {
				$this->settings[$category] = array(
					$set['key'] => $this->decode($set['value'])
				);
			}
		}
		// the category searched for may not exist
		if (array_key_exists($category, $this->settings))
			return $this->settings[$category];
		return array();
    }

    /**
     * Load from database the items of the specified key
     * and store internally on $this->settingsByKey;
     * @param string $key
     * @return array the items of the category, or an empty array
     */
    public function loadByKey($key)
    {
		if ($this->loadAll) {
			$this->loadAll();
			$this->cacheSettingsByKey();
			if (array_key_exists($key, $this->settingsByKey))
				return $this->settingsByKey[$key];
			return array();
		}
		if (!array_key_exists($key, $this->settingsByKey)) {
			$settings = Yii::app()->db->createCommand()
				->from('nii_setting')
				->where('key=:key', array(':key'=>$key))
				->queryAll();
			$this->settingsByKey[$key] = array();
			foreach ($settings as $set) {
				$this->settingsByKey[$key] = array(
					$set['category'] => $this->decode($set['value'])
				);
			}
		}
		// the category searched for may not exist
		if (array_key_exists($key, $this->settings))
			return $this->settingsByKey[$key];
		return array();
    }
	
	private $_loadedAll = false;
	
	public function loadAll()
	{
		if ($this->_loadedAll == false) {
			$settings = Yii::app()->db->createCommand()
				->from('nii_setting')
				->queryAll();
			foreach ($settings as $set) {
				if (array_key_exists($set['category'], $this->settings)) {
					$this->settings[$set['category']][$set['key']] = $this->decode($set['value']);
				} else {
					$this->settings[$set['category']] = array(
						$set['key'] => $this->decode($set['value'])
					);
				}
			}
			$this->_loadedAll = true;
		}
		return $this->settings;
	}

	/**
	 * Populate the settingsByKey property.
	 * This allows us to find all of the settings for a particular key via array index.
	 * @return void
	 */
	public function cacheSettingsByKey() {
		foreach ($this->settings as $category => $keys) {
			//$this->settingsByKey[$key] = 
			foreach ($keys as $key => $value) {
				if (!isset($this->settingsByKey[$key])) {
					$this->settingsByKey[$key] = array($category => $value);
				} else {
					$this->settingsByKey[$key] = array_merge($this->settingsByKey[$key], array($category => $value));
				}
				
			}
		}
	}

	public static function install()
    {
         NSetting::install('NSetting');
	}
	
	public function encode($value) 
	{
		//return @serialize($value);
		return CJSON::encode($value);
	}
	
	public function decode($value)
	{
		return CJSON::decode($value);
		//return @unserialize($value);
	}
}
