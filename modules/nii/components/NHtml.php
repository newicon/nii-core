<?php

/**
 * NHtml class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 * @package nii
 */

/**
 * NHtml provides helper functions
 *
 * @author steve
 * @package nii
 */
class NHtml extends CHtml
{

	/**
	 * Format a route into a url a shorthand method of calling normalizeUrl
	 *
	 * For convienience the $route parameter can be a string or an array
	 * A yii route is defined array('/moduleId/controllerId/actionId', 'getParam1'=>'getParamVal1')
	 * This function allows route to be a string as well for example
	 * NHtml::url('/crm/contact/show', array('id'=>3))
	 * however you can also use this the same as calling Yii::app()->createUrl
	 * If you use NHtml::url() and pass no parameters the base url of the application will be returned
	 *
	 * @see CWebApplication::createUrl
	 * @param mixed (string|array) $route if route is an array the $getArray parameter is ignored
	 * @param array $getArray additional get parameters
	 * @return string
	 */
	public static function url($route='', $getArray=array()) {
		// if the route is an array then it contains the route string and params
		if (is_array($route)) {
			 $getArray = $route;
			 $route = $getArray[0];
			 unset($getArray[0]);
		}
		return Yii::app()->createUrl($route, $getArray);
	}

	/**
	 *
	 * @param mixed $route
	 * @param array $params
	 * @param string $schema http|https
	 * @return string absolute url
	 */
	public static function urlAbsolute($route='', $params = array(), $schema = '')
	{
		if (is_array($route)) {
			$params = $route;
			$route = $params[0];
			unset($params[0]);
		}
		return Yii::app()->createAbsoluteUrl($route, $params, $schema);
	}

	/**
	 * Matches a route to see if it is the current route.
	 * the specificity of the match is based on the passed in route for example:
	 * - if $route is: 'user' then this will function will return true for all controllers and actions within the user module.
	 * - if $route is: 'user/admin' this will return true for all actions under the user module's admin controler.
	 * - if $route is: 'user/admin/account' this will only return true for the account action.
	 * @param string $route ModuleId / ControllerId / ActionId
	 */
	public static function isUrl($route)
	{
		$route = trim($route, '/');
		$currentRoute = Yii::app()->controller->getRoute();
		list($m, $c, $a) = NData::getArrayItems(array(0,1,2), explode('/', $currentRoute));
		$r = explode('/', $route);
		switch(count($r)){
			case 1:
				// match just module
				return $r[0] == $m;
			case 2:
				// match the module and the controller
				return ($r[0] == $m && $r[1] == $c);
			case 3:
				// match the module / controller /action
				return ($r[0] == $m && $r[1] == $c && $r[2] == $a) ;
		}
		return false;
	}

	/**
	 * returns an active class based on the result of  self::isUrl
	 * @see self::isUrl
	 * @param string $route module/ or module/controller or module/controller/action
	 * @return string active or empty string depending on if the route is curreently active
	 */
	public static function active($route)
	{
		return self::isUrl($route) ? 'active' : '';
	}

	/**
	 * returen the theme base url
	 */
	public static function themeUrl(){
		echo Yii::app()->theme->baseUrl;
	}

	public static function baseUrl() {
		if (isset(Yii::app()->params['baseUrl'])) {
			return Yii::app()->params['baseUrl'];
		}
		return Yii::app()->request->baseUrl;
	}

	/**
	 * Lowercase the first leter of a string
	 *
	 * @param $str
	 * @return string
	 */
	public static function lcFirst($str) {
		if (function_exists('lcfirst'))
			return lcfirst($str);
		return strtolower(substr($str, 0, 1)) . substr($str, 1);
	}

	/**
	 * Takes some text and finds all occurances of $searchTerm and highlights it with
	 * <span class="$hilightClass"></span>
	 *
	 * @param string $textToHilight
	 * @param string $searchTerm
	 * @param string $hilightClass
	 * @param array $htmlOptions
	 * @return string with highlighted text
	 */
	public static function hilightText($textToHilight, $searchTerm, $hilightClass='searchHilite', $htmlOptions=array())
	{
		if (is_array($searchTerm))
			return '';
		//if search term is empty then return the string do not perform the search
		if (empty($searchTerm))
			return $textToHilight;
		if (!isset($htmlOptions['class']))
			$htmlOptions['class'] = $hilightClass;
		$search = preg_quote($searchTerm, '/');
		return preg_replace("/($search)/i", '<span ' . CHtml::renderAttributes($htmlOptions) . '>$1</span>', $textToHilight);
	}

	/**
	 * breaks a large string into a smaller preview string of a defined character length
	 * (defined by $charactersLong)
	 *
	 * It also breaks words longer than $wordBreakLength (defaults to 15 characters) with the $wordBreakChar
	 * (defaults to "-")
	 *
	 * @param string $subject The large text to form a preview of
	 * @param int $charLimit The maximum number of characters to preview
	 * @param array $options array of options with the following keys:
	 *  - 'wordBreakLength' : The maximum number of characters allowed for any one word
	 *  - 'wordBreakChar'   : The character to break words longer than $wordBreakLength into
	 *  - 'append'          : A string to append to the end of the return text if shortened, defaults to '...',
	 *                        set to false to prevent any text from being appended
	 * @return string the formatted preview text
	 */
	public static function previewText($subject, $charLimit=150, $options=array())
	{
		$options = CMap::mergeArray(array(
			'wordBreakLength'=>15,
			'wordBreakChar'=>'-',
			'append'=>false
		), $options);
		extract($options);
		$totalTxt = substr($subject, 0, $charLimit);
		// break words longer than 14 characters
		$patrn = '/([^\s]{' . $wordBreakLength . '})/';
		$txt = preg_replace($patrn, $wordBreakChar, $totalTxt);
		if ($append && strlen($subject) > $charLimit)
			$txt .= $append;
		return $txt;
	}

	/**
	 * genRandomString - Generates random string
	 * @author Macinville <http://macinville.blogspot.com>
	 * @license MIT License <http://www.opensource.org/licenses/mit-license.php>
	 * @param int $length Length of the return string.
	 * @param string $chars User-defined set of characters to be used in randoming. If this is set, $type will be ignored.
	 * @param array $type Type of the string to be randomed.Can be set by boolean values.
	 * <ul>
	 * <li><b>alphaSmall</b> - small letters, true by default</li>
	 * <li><b>alphaBig</b> - big letters, true by default</li>
	 * <li><b>num</b> - numbers, true by default</li>
	 * <li><b>othr</b> - non-alphanumeric characters found on regular keyboard, false by default</li>
	 * <li><b>duplicate</b> - allow duplicate use of characters, true by default</li>
	 * </ul>
	 * @return string The generated random string
	 */
	public static function genRandomString($length=10, $chars='', $type=array()) {
		 //initialize the characters
		 $alphaSmall = 'abcdefghijklmnopqrstuvwxyz';
		 $alphaBig = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		 $num = '0123456789';
		 $other = '`~!@#$%^&*()/*-+_=[{}]|;:",<>.\/?' . "'";

		 $characters = "";
		 $string = '';
		 //defaults the array values if not set
		 isset($type['alphaSmall'])  ? $type['alphaSmall']: $type['alphaSmall'] = true;  //alphaSmall - default true
		 isset($type['alphaBig'])    ? $type['alphaBig']: $type['alphaBig'] = true;      //alphaBig - default true
		 isset($type['num'])         ? $type['num']: $type['num'] = true;                //num - default true
		 isset($type['other'])        ? $type['other']: $type['other'] = false;             //othr - default false
		 isset($type['duplicate'])   ? $type['duplicate']: $type['duplicate'] = true;    //duplicate - default true


		 if (strlen(trim($chars)) == 0) {
			  $type['alphaSmall'] ? $characters .= $alphaSmall : $characters = $characters;
			  $type['alphaBig'] ? $characters .= $alphaBig : $characters = $characters;
			  $type['num'] ? $characters .= $num : $characters = $characters;
			  $type['other'] ? $characters .= $othr : $characters = $characters;
		 }
		 else
			  $characters = str_replace(' ', '', $chars);

		 if($type['duplicate'])
			  for (; $length > 0 && strlen($characters) > 0; $length--) {
					$ctr = mt_rand(0, (strlen($characters)) - 1);
					$string .= $characters[$ctr];
			  }
		 else
			  $string = substr (str_shuffle($characters), 0, $length);

		 return $string;
	}

	/**
	 * Gets the application wide button class
	 *
	 * @return string
	 */
	public static function btnClass()
	{
		if (isset(Yii::app()->params->buttonClass))
			return Yii::app()->params->buttonClass;
		return 'btn';
	}

	/**
	 * Draws a button using default button class
	 *
	 * @param type $label
	 * @param type $iconClass
	 * @param string $class
	 */
	public static function btn($label, $iconClass=null, $class=null)
	{
		$class = NHtml::btnClass() . ' ' . $class;
		if ($iconClass !== null)
			$label = "<span class=\"$iconClass\"></span>$label";
		echo CHtml::htmlButton($label, array('class' => $class));
	}

	/**
	 * Get the url address to show the imagh or use in the img src attribute
	 *
	 * @param int $id fileManager NFile id
	 * @param string $size thumbSize key name or string in the form xy-100-130
	 * @return string url
	 */
	public static function urlImageThumb($id, $type='small')
	{
		return NImage::url($id, $type);
	}

	/**
	 * Get the width from an image type
	 * @param string $type
	 * @return string width
	 */
	public static function nImageWidth($type)
	{
		$actions = NImage::get()->getType($type);
		if (array_key_exists('resize', $actions)) {
			if (array_key_exists('width', $actions['resize'])) {
				return $actions['resize']['width'];
			} else {
				return '';
			}
		}
	}

	/**
	 * Get the height from an image type
	 * @param string $type
	 * @return string height
	 */
	public static function nImageHeight($type)
	{
		$actions = NImage::get()->getType($type);
		if (array_key_exists('resize', $actions)) {
			if (array_key_exists('height', $actions['resize'])) {
				return $actions['resize']['height'];
			} else {
				return '';
			}
		}
	}
	// DOCUMENTATION PLEASE! ??
	public static function nImageSizeAttr($type)
	{
		return ' width="' . NHtml::nImageWidth($type) . '" height="' . NHtml::nImageHeight($type) . '" ';
	}

	/**
	 * Function to generate a link to a file controlled by the fileManager
	 * component
	 *
	 * @param int $id the filemanager id for the file
	 * @param string $name the name of the file
	 * @param boolean $downloadable wheather to create a download link.
	 * @return string url
	 * @see NFileManager::getUrl
	 */
	public static function urlFile($id, $name='', $downloadable=false)
	{
		return NFileManager::get()->getUrl($id, $name, $downloadable);
	}

	/**
	 * Generates a user friendly attribute label.
	 * This is done by replacing underscores or dashes with blanks and
	 * changing the first letter of each word to upper case.
	 * For example, 'department_name' or 'DepartmentName' becomes 'Department Name'.
	 * @param string $name the column name
	 * @return string the attribute label
	 */
	public static function generateAttributeLabel($name)
	{
		return ucwords(trim(strtolower(str_replace(array('-', '_', '.'), ' ', preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $name)))));
	}
	// DOCUMENTATION PLEASE!
	public static function generateAttributeId($label)
	{
		return strtolower(str_replace(array(' '), '_', $label));
	}

	/**
	 * Returns an array of enum values from an enum column on the given model
	 * @param model $model
	 * @param string $attribute - column/field name containing the enum info
	 * @return array
	 */
	public static function enumItem($model, $attribute)
	{
		$attr = $attribute;
		self::resolveName($model, $attr);
		$enum = $model->tableSchema->columns[$attr]->dbType;
		$off = strpos($enum, "(");
		$enum = substr($enum, $off + 1, strlen($enum) - $off - 2);
		$keys = str_replace("'", null, explode(",", $enum));
		for ($i = 0; $i < sizeof($keys); $i++)
			$values[$keys[$i]] = Yii::t('enumItem', '{k}', array('{k}'=>$keys[$i]));
		return $values;
	}

	// DOCUMENTATION PLEASE!
	public static function btnLink($label, $url, $iconClass=null, $attributes=array())
	{
		if ($iconClass !== null)
			$label = "<span class=\"icon $iconClass\"></span>$label";
		return CHtml::link($label, $url, $attributes);
	}

	/**
	 * Return the class for a particular mime type
	 * To use this, you need to populate a sprite folder named 'mime' with PNGs named similarly the following example translations...
	 * 		MIME TYPE				Class								Created file name (with .png extension)
	 * 		image/jpeg			mime-image_jpg			image-jpeg
	 * 		application/pdf		mime-application_pdf	application-pdf
	 * 		application/vnd.openxmlformats-officedocument.wordprocessingml.document... Seriously, Word?!
	 *
	 * @param string $mimeType
	 */
	public static function getMimeTypeByExtension($file) {

		static $extensions;
		if ($extensions === null) {
			$extensions = require(Yii::getPathOfAlias('system.utils.mimeTypes') . '.php');
			$extensions['docx'] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
			$extensions['xlsx'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
			$extensions['odt'] = 'application/vnd.oasis.opendocument.text';
			$extensions['ods'] = 'application/vnd.oasis.opendocument.spreadsheet';
		}
		if (($ext = pathinfo($file, PATHINFO_EXTENSION)) !== '') {
			$ext = strtolower($ext);
			if (isset($extensions[$ext]))
				return $extensions[$ext];
		}
		return null;
	}

	/**
	 * Returns an OOCSS icon class representing the mime icon.
	 * The defualt icon type if no match is found is a 'fam-page-white'.
	 * @param $mimetype The mime type you would like to get the icon for
	 * @return string
	 */
	static public function getMimeTypeIcon($mimetype) {
		$icon = '';

		switch ($mimetype) {
			case 'application/visio':
			case 'application/x-visio':
			case 'application/vnd.visio':
			case 'application/visio.drawing':
			case 'application/vsd':
			case 'application/x-vsd':
			case 'image/x-vsd':
			case 'zz-application':
			case 'zz-winassoc-vsd':
			case 'application/x-visio':
			case 'application/visio':
			case 'application/vnd.ms-office':
				$icon = 'fam-page-white-office';
				break;
			case 'application/msword':
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
			case 'application/vnd.oasis.opendocument.text':
				$icon = 'fam-page-white-word';
				break;
			case 'text/css':
				$icon = 'fam-css';
				break;
			case 'text/plain':
				$icon = 'fam-page-white-text';
				break;
			case 'text/html':
				$icon = 'fam-page-world';
				break;
			case 'application/pdf':
				$icon = 'fam-page-white-acrobat';
				break;
			case 'application/mspowerpoint':
				$icon = 'fam-page-white-powerpoint';
				break;
			case 'text/rtf':
			case 'text/richtext':
				$icon = 'fam-page-white-text';
				break;
			case 'application/x-shockwave-flash':
				$icon = 'fam-page-white-flash';
				break;
			case 'application/vnd.ms-excel':
			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
			case 'application/vnd.oasis.opendocument.spreadsheet':
				$icon = 'fam-page-white-excel';
				break;
			case 'text/xml':
				$icon = 'fam-page-white-code';
				break;
			case 'application/zip':
			case 'application/x-tar':
			case 'application/x-gtar':
			case 'application/x-gzip':
				$icon = 'fam-page-white-zip';
				break;
			case 'image/jpeg':
			case 'image/bmp':
			case 'image/png':
			case 'image/gif':
				$icon = 'fam-page-white-picture';
				break;
			default:
				//set up some defaults
				//if mimetype contains application text
				if (strpos($mimetype, 'application')) {
					//default application icon
					$icon = 'fam-application';
				} elseif (strpos($mimetype, 'audio')) {
					$icon = 'fam-sound';
				} elseif (strpos($mimetype, 'video')) {
					$icon = 'fam-film';
				} elseif (strpos($mimetype, 'image')) {
					$icon = 'fam-picture';
				} elseif (strpos($mimetype, 'text')) {
					$icon = 'fam-page-white-text';
				}
				$icon = 'fam-page-white';
				break;
		}

		return 'icon ' . $icon;
	}

	/**
	 * Returns a properly formatted date, using PHP date format
	 * @param string $date - original value from model
	 * @param string $format - PHP date format
	 * @param type $noDateText - alternative text for if there is no date set; can also be set to false to hide
	 * @return string formatted date
	 */
	public static function formatDate($date, $format=null, $noDateText=null)
	{
		if ($format == '' || !$format)
			$format = 'jS F Y';
		if ($date && $date != '0000-00-00')
			if (strstr($date, '-'))
				return date($format, strtotime($date));
			else
				return date($format, $date);
		else
			if ($noDateText !== false)
				return (isset($noDateText)) ? $noDateText : '<span class="noDate">No date set</span>';
	}

	/**
	 *	Returns a properly formatted date for use in a grid
	 * @param string $date - original value from model
	 * @param string $format - PHP date format
	 * @param type $noDateText - alternative text for if there is no date set; can also be set to false to hide
	 * @return function formatDate
	 */
	public static function formatGridDate($date, $format=null, $noDateText=null)
	{
		if ($format == '' || !$format)
			$format = 'd/m/Y';
		return self::formatDate($date, $format, $noDateText);
	}

	/**
	 *	Returns human readable text from a boolean value
	 * @param string $value - original boolean
	 * @param string $true - true value
	 * @param string $false - false value
	 * @param bool $blank - whether to return blank on false
	 * @return string
	 */
	public static function formatBool($value, $true=1, $false=0, $blank=false)
	{
		if ($value == $true)
			return 'Yes';
		else if ($value === $false || $blank === false)
			return 'No';
		else
			return;
	}

	/**
	 *	Returns human readable text from a boolean value
	 * @param string $value - original boolean
	 * @param string $true - true value
	 * @param string $false - false value
	 * @param bool $blank - whether to return blank on false
	 * @return string
	 */
	public static function boolImage($value, $true=1, $false=0, $blank=false) {
		if ($value == $true)
			return '<span class="icon fam-accept">&nbsp;</span>';
		else if ($value === $false || $blank === false)
			return '<span class="icon fam-delete">&nbsp;</span>';
		else
			return;
	}

	/**
	 * Returns a properly formatted price based on an integer or float
	 * @param int/float $value
	 * @param string $currency - the character, html-encoded, to place before the price
	 * @param int $decimals - number of decimals to display
	 * @return string
	 */
	public static function formatPrice($value, $currency='&pound;', $decimals=2, $showZeroDecimals=false) {
		$decimals = (strstr($value,'.') && $showZeroDecimals==true) ? 2 : 0;
		return '<span class="currency">'.$currency . '</span>'. number_format($value, $decimals);
	}

	/**
	 * Returns a properly formatted mailto link from an email address
	 * @param string $email - email address
	 * @return string
	 */
	public static function emailLink($email) {
		return '<a href="mailto:' . $email . '" class="emailLink" title="Send an email to ' . $email . '">' . $email . '</a>';
	}

	/**
	 * Overrides the default CHtml::activeLabel and adds an additional class .lbl if no htmlOptions are specified.
	 * @see CHtml::activeLabel
	 * @param CActiveRecord $model
	 * @param string $attribute
	 * @param array $htmlOptions
	 * @return string html
	 */
	public static function activeLabel($model, $attribute, $htmlOptions=array()) {
		if (empty($htmlOptions)) {
			$htmlOptions = array('class' => 'lbl');
		}
		return parent::activeLabel($model, $attribute, $htmlOptions);
	}

	public static function activeChznDropDown($model,$attribute,$data,$htmlOptions=array())
	{
		self::resolveNameID($model,$attribute,$htmlOptions);
		$selection=self::resolveValue($model,$attribute);
		$options="\n".self::listOptions($selection,$data,$htmlOptions);
		self::clientChange('change',$htmlOptions);

		if($model->hasErrors($attribute))
			self::addErrorCss($htmlOptions);

		$hidden='';
		if(!empty($htmlOptions['multiple']))
		{
			if(substr($htmlOptions['name'],-2)!=='[]')
				$htmlOptions['name'].='[]';

			if(isset($htmlOptions['unselectValue']))
			{
				$hiddenOptions=isset($htmlOptions['id']) ? array('id'=>self::ID_PREFIX.$htmlOptions['id']) : array('id'=>false);
				$hidden=self::hiddenField(substr($htmlOptions['name'],0,-2),$htmlOptions['unselectValue'],$hiddenOptions);
				unset($htmlOptions['unselectValue']);
			}
		}

		return $hidden . self::tag('select',$htmlOptions,$options);
	}

	/**
	 *	Trash button for generic trashing of model objects. To be used in views...
	 * @param string $model
	 * @param string $modelName
	 * @param mixed $returnUrl the return route string/array.
	 * @param string $successMsg
	 * @return button
	 */
	public static function trashButton ($model, $modelName, $returnUrl, $successMsg=null) {

		$label = Nii::t('Delete');
		$className = get_class($model);

		$params = array(
			'model'=>$className,
			'model_id'=>$model->id,
			'name'=>$modelName,
			'returnUrl'=> str_replace('/', '.', NHtml::url($returnUrl)),
		);
		if ($successMsg)
			$params['successMsg'] = $successMsg;

		$url = NHtml::url(array_merge(array('/nii/index/trash'),$params));

		return NHtml::btnLink($label,'#', null,
			array(
				'onclick'=> '
					var answer = confirm("Are you sure you wish to delete this '.$modelName.'?");
					if(!answer) { return; }
					window.location = "'.$url.'";
				',
				'class'=>'trash-link btn btn-danger'
			)
		);
	}

	public static function confirmLink($label, $url, $confirmMessage=null, $htmlOptions=array()) {
		if ($confirmMessage==null)
			$confirmMessage = 'Are you sure?';
		return NHtml::link($label,'#', array_merge(
				$htmlOptions,
				array('onclick'=> '
					var answer = confirm("'.$confirmMessage.'");
					if(!answer) { return; }
					window.location = "'.NHtml::url($url).'";
				')
			)
		);
	}

	public static function confirmAjaxLink($label, $url, $confirmMessage=null, $gridId=null, $htmlOptions=array()) {
		if ($confirmMessage==null)
			$confirmMessage = 'Are you sure?';

		$url['ajax']=true;

		return NHtml::link($label, '#', array_merge($htmlOptions, array(
			'onclick' => "js:$(function(){
				var answer = confirm('".$confirmMessage."');
				if (!answer) {
					return;
				}
				$.ajax({
					url: '".  NHtml::url($url)."',
					dataType: 'json',
					type: 'get',
					success: function(response){
						if (response.success) {
							".($gridId!==null ? "$.fn.yiiGridView.update('".$gridId."', {updateAll:true});" : "")."
							nii.showMessage(response.success);
							return false;
						}
					}
				});
			});")
		));
	}


	// DOCUMENTATION PLEASE!
	public static function hexLighter($hex, $factor = 8) {
		$new_hex = '';

		if ($hex[0] == '#')
			$hex = substr($hex, 1);

		$base['R'] = hexdec($hex[0] . $hex[1]);
		$base['G'] = hexdec($hex[2] . $hex[3]);
		$base['B'] = hexdec($hex[4] . $hex[5]);

		foreach ($base as $k => $v) {
			$amount = 255 - $v;
			$amount = $amount / 100;
			$amount = round($amount * $factor);
			$new_decimal = $v + $amount;

			$new_hex_component = dechex($new_decimal);
			if (strlen($new_hex_component) < 2) {
				$new_hex_component = "0" . $new_hex_component;
			}
			$new_hex .= $new_hex_component;
		}

		return '#'.$new_hex;
	}

	/**
	 * convert a hexidecimal color to rgb
	 * @param string $hex
	 * @return array()
	 */
	public static function hex2rgb($hex)
	{
		$hex = str_replace("#", "", $hex);

		if(strlen($hex) == 3) {
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} else {
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		$rgb = array($r, $g, $b);
		return $rgb; // returns an array with the rgb values
	}

	// DOCUMENTATION PLEASE!
	public static function hexDarker($hex, $factor = 8) {
		$new_hex = '';

		if ($hex[0] == '#')
			$hex = substr($hex, 1);

		$base['R'] = hexdec($hex[0] . $hex[1]);
		$base['G'] = hexdec($hex[2] . $hex[3]);
		$base['B'] = hexdec($hex[4] . $hex[5]);

		foreach ($base as $k => $v) {
			$amount = 255 - $v;
			$amount = $amount / 100;
			$amount = round($amount / $factor);
			$new_decimal = $v + $amount;

			$new_hex_component = dechex($new_decimal);
			if (strlen($new_hex_component) < 2) {
				$new_hex_component = "0" . $new_hex_component;
			}
			$new_hex .= $new_hex_component;
		}

		return '#'.$new_hex;
	}

	/**
     * Remove any illegal characters, accents, etc.
     *
     * @param string $string String to unaccent
     * @see self::urlize
     * @return string $string Unaccented string
     */
    public static function unaccent($string)
    {
        if ( ! preg_match('/[\x80-\xff]/', $string) ) {
            return $string;
        }

        if (self::seemsUtf8($string)) {
            $chars = array(
            // Decompositions for Latin-1 Supplement
            chr(195).chr(128) => 'A', chr(195).chr(129) => 'A', chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
            chr(195).chr(132) => 'A', chr(195).chr(133) => 'A', chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
            chr(195).chr(137) => 'E', chr(195).chr(138) => 'E', chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
            chr(195).chr(141) => 'I', chr(195).chr(142) => 'I', chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
            chr(195).chr(146) => 'O', chr(195).chr(147) => 'O', chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
            chr(195).chr(150) => 'O', chr(195).chr(153) => 'U', chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
			chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y', chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
			chr(195).chr(161) => 'a', chr(195).chr(162) => 'a', chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
			chr(195).chr(165) => 'a', chr(195).chr(167) => 'c', chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
			chr(195).chr(170) => 'e', chr(195).chr(171) => 'e', chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
			chr(195).chr(174) => 'i', chr(195).chr(175) => 'i', chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
            chr(195).chr(179) => 'o', chr(195).chr(180) => 'o', chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
            chr(195).chr(182) => 'o', chr(195).chr(185) => 'u', chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
            chr(195).chr(188) => 'u', chr(195).chr(189) => 'y', chr(195).chr(191) => 'y',
            // Decompositions for Latin Extended-A
            chr(196).chr(128) => 'A', chr(196).chr(129) => 'a', chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
            chr(196).chr(132) => 'A', chr(196).chr(133) => 'a', chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
            chr(196).chr(136) => 'C', chr(196).chr(137) => 'c', chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
            chr(196).chr(140) => 'C', chr(196).chr(141) => 'c', chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
            chr(196).chr(144) => 'D', chr(196).chr(145) => 'd', chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
            chr(196).chr(148) => 'E', chr(196).chr(149) => 'e', chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
            chr(196).chr(152) => 'E', chr(196).chr(153) => 'e', chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
            chr(196).chr(156) => 'G', chr(196).chr(157) => 'g', chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
            chr(196).chr(160) => 'G', chr(196).chr(161) => 'g', chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
            chr(196).chr(164) => 'H', chr(196).chr(165) => 'h', chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
            chr(196).chr(168) => 'I', chr(196).chr(169) => 'i', chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
            chr(196).chr(172) => 'I', chr(196).chr(173) => 'i', chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
            chr(196).chr(176) => 'I', chr(196).chr(177) => 'i', chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
            chr(196).chr(180) => 'J', chr(196).chr(181) => 'j', chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
            chr(196).chr(184) => 'k', chr(196).chr(185) => 'L', chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
			chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
			chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
			chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
			chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
			chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
			chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
			chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',chr(197).chr(148) => 'R', chr(197).chr(149) => 'r',
			chr(197).chr(150) => 'R', chr(197).chr(151) => 'r',chr(197).chr(152) => 'R', chr(197).chr(153) => 'r',
			chr(197).chr(154) => 'S', chr(197).chr(155) => 's',chr(197).chr(156) => 'S', chr(197).chr(157) => 's',
			chr(197).chr(158) => 'S', chr(197).chr(159) => 's',chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
			chr(197).chr(162) => 'T', chr(197).chr(163) => 't',chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
			chr(197).chr(166) => 'T', chr(197).chr(167) => 't',chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
			chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
			chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
			chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
			chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
			chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
			chr(197).chr(190) => 'z', chr(197).chr(191) => 's',
            // Euro Sign
            chr(226).chr(130).chr(172) => 'E',
            // GBP (Pound) Sign
            chr(194).chr(163) => '',
            'Ä' => 'Ae', 'ä' => 'ae', 'Ü' => 'Ue', 'ü' => 'ue',
            'Ö' => 'Oe', 'ö' => 'oe', 'ß' => 'ss',
            // Norwegian characters
            'Å'=>'Aa','Æ'=>'Ae','Ø'=>'O','æ'=>'a','ø'=>'o','å'=>'aa'
            );

            $string = strtr($string, $chars);
        } else {
            // Assume ISO-8859-1 if not UTF-8
            $chars['in'] = chr(128).chr(131).chr(138).chr(142).chr(154).chr(158)
                .chr(159).chr(162).chr(165).chr(181).chr(192).chr(193).chr(194)
                .chr(195).chr(196).chr(197).chr(199).chr(200).chr(201).chr(202)
                .chr(203).chr(204).chr(205).chr(206).chr(207).chr(209).chr(210)
                .chr(211).chr(212).chr(213).chr(214).chr(216).chr(217).chr(218)
                .chr(219).chr(220).chr(221).chr(224).chr(225).chr(226).chr(227)
                .chr(228).chr(229).chr(231).chr(232).chr(233).chr(234).chr(235)
                .chr(236).chr(237).chr(238).chr(239).chr(241).chr(242).chr(243)
                .chr(244).chr(245).chr(246).chr(248).chr(249).chr(250).chr(251)
                .chr(252).chr(253).chr(255);

            $chars['out'] = 'EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYa'
              . 'aaaaaceeeeiiiinoooooouuuuyy';

            $string = strtr($string, $chars['in'], $chars['out']);
            $doubleChars['in'] = array(
                chr(140), chr(156), chr(198),
                chr(208), chr(222), chr(223),
                chr(230), chr(240), chr(254));
            $doubleChars['out'] = array(
              'OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th'
            );
            $string = str_replace(
                $doubleChars['in'],
                $doubleChars['out'],
                $string
            );
        }

        return $string;
    }

	/**
	 * Taken from doctorine_inflector
     * Convert any passed string to a url friendly string.
     * Converts 'My first blog post' to 'my-first-blog-post'
     *
     * @param string $text Text to urlize
     *
     * @return string $text Urlized text
     */
    public static function urlize($text)
    {
		// Remove all non url friendly characters with the unaccent function
		$text = self::unaccent($text);

		if (function_exists('mb_strtolower')) {
			$text = mb_strtolower($text);
		} else {
			$text = strtolower($text);
		}

		// Remove all none word characters
		$text = preg_replace('/\W/', ' ', $text);

		// More stripping. Replace spaces with dashes
		$text = strtolower(
			preg_replace(
				'/[^A-Z^a-z^0-9^\/]+/', '-', preg_replace(
					'/([a-z\d])([A-Z])/', '\1_\2', preg_replace(
						'/([A-Z]+)([A-Z][a-z])/', '\1_\2', preg_replace(
							'/::/', '/', $text
						)
					)
				)
			)
		);

		return trim($text, '-');
	}


	/**
      * Check if a string has utf7 characters in it
      *
      * By bmorel at ssi dot fr
      *
      * @param  string $string
      * @return boolean $bool
      */
     public static function seemsUtf8($string)
     {
         for ($i = 0; $i < strlen($string); $i++) {
             if (ord($string[$i]) < 0x80) continue; # 0bbbbbbb
             elseif ((ord($string[$i]) & 0xE0) == 0xC0) $n=1; # 110bbbbb
             elseif ((ord($string[$i]) & 0xF0) == 0xE0) $n=2; # 1110bbbb
             elseif ((ord($string[$i]) & 0xF8) == 0xF0) $n=3; # 11110bbb
             elseif ((ord($string[$i]) & 0xFC) == 0xF8) $n=4; # 111110bb
             elseif ((ord($string[$i]) & 0xFE) == 0xFC) $n=5; # 1111110b
             else return false; # Does not match any model
             for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
                 if ((++$i == strlen($string)) || ((ord($string[$i]) & 0xC0) != 0x80))
                 return false;
             }
         }
         return true;
     }

	/**
	 * takes a markdown string and converts to html
	 * @param string $markdownString
	 * @return string Html
	 */
	public static function renderMarkdown($markdownString)
	{
		Yii::import('nii.widgets.markdown.*');
		$markdown = new NMarkdownFormat();
		return $markdown->transform($markdownString);
	}

	/**
	 * alias of self::renderMarkdown
	 * @param string $markdownString
	 * @return string html
	 */
	public static function markdown($markdownString)
	{
		return self::renderMarkdown($markdownString);
	}

	/**
	 * highlights a string based on the current search criteria in the grid
	 * To use this function you must pass in the grid object.  Used in the evaluated grid column expression
	 * typical useage example:
	 * <code>
	 *	'NHtml::gridSearchHilight($data->name, $this)'
	 * </code>
	 * @see NHtml::hilightText
	 * @param string $value
	 * @param NGridView $grid
	 * @return string html
	 */
	public static function gridSearchHilight($value, $grid)
	{
		$search = $grid->grid->getFormatter()->format($value, $grid->type);
		return NHtml::hilightText($search, $grid->grid->filter->{$grid->name});
	}

	/**
	* Generates an email field input for a model attribute.
	* If the attribute has input error, the input field's CSS class will
	* be appended with {@link errorCss}.
	* @param CModel $model the data model
	* @param string $attribute the attribute
	* @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
	* attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
	* @return string the generated input field
	* @see clientChange
	* @see activeInputField
	* @since 1.1.11
	*/
	public static function activeEmailField($model,$attribute,$htmlOptions=array())
	{
		self::resolveNameID($model,$attribute,$htmlOptions);
		self::clientChange('change',$htmlOptions);
		return self::activeInputField('email',$model,$attribute,$htmlOptions);
	}


	/**
	 * Get an Image based on its ID
	 * @param int $id
	 * @param string $type
	 * @param bool $failOnNoLogo
	 * @return string
	 */
	public static function getImageById($id=null, $type='grid-thumbnail-person', $failOnNoLogo=false)
	{
		$src = NHtml::getImageUrlById($id, $type, $failOnNoLogo);
		return '<img src="' . $src . '" />';
	}


	/**
	 * Get an Image based on its ID
	 * @param int $id
	 * @param string $type
	 * types the string key of the type see NImage::types, or a string of dimensions 50x50 [width]x[height] the image
	 * will be constrained inside this box to scale
	 * @param bool $failOnNoLogo
	 * @return string
	 */
	public static function getImageUrlById($id=null, $type='grid-thumbnail-person', $failOnNoLogo=false)
	{
		if ($id)
			$src = Yii::app()->image->url($id, $type);
		else {
			if ($failOnNoLogo)
				return null;
			$src = Yii::app()->image->url(0, $type);
		}
		return $src;
	}

	/**
	 * return 'error' (error class name) if the model attribute has errors
	 * useful to add 'error' class to control-group divs that do not contain compoentns that have client side validation
	 * @param CModel $model
	 * @param string $attribute
	 */
	public static function errorClass($model, $attribute)
	{
		return ($model->hasErrors($attribute) ? 'error' : '');
	}

	public static function ddText($text=null, $alt='&nbsp;')
	{
		return $text==null||$text==''?$alt:$text;
	}
}