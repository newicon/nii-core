<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 * Implements a default Deny behaviour
 */
class AController extends NController 
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/admin1column';
	/**
	 * @var array An array of actions for the current controller action
	 */
	public $actionsMenu = array();

	public function init() {
		if (!YII_DEBUG) {
			Yii::app()->errorHandler->errorAction = '/admin/error';
		}
		$this->menu = array(
			'admin' => array('label' => 'Admin', 'url' => array('/admin'), 'active' => ($this->parentActive('admin/index') || $this->parentActive('admin/settings') || $this->parentActive('admin/modules')),
				'items' => array(
					'modules' => array('label' => 'Modules', 'url' => array('/admin/modules/index')),
					'settings' => array('label' => 'Settings', 'url' => array('/admin/settings/index')),
				),
			),
		);

		// if (intval(Yii::app()->request->getParam('clearFilters'))==1) {
		// 	Yii::import('nii.widgets.grid.NGridView');
		// 	NGridView::clearFilters($this,$model);
		// }
	}

	/**
	 * Provides a default interface for adding actions to controllers
	 * @return array Set of controller actions supplied by the current module
	 */
	public function actions() {
		if (property_exists($this->module, 'actions'))
			return isset($this->module->actions[$this->id]) ? $this->module->actions[$this->id] : array();
	}

	public function parentActive($controller) {
		if ($controller == Yii::app()->controller->uniqueid)
			return true;
	}

	public function accessRules() {
		return array(
			array('allow',
				'expression' => '$user->checkAccessToRoute()',
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * This method is invoked right before an action is to be executed (after all possible filters.) 
	 * @return boolean Whether the action should be executed
	 */
	protected function beforeAction($e) {
		return $this->updatePasswordCheck($e);
	}

	/**
	 * Check if update_password flag is set in user record and redirect.
	 * This function will not be fired if the user is not logged in.
	 * @return boolean If update required, return false to stop action execution in beforeAction()
	 */
	public function updatePasswordCheck($e) {
		if ($e->id != 'password' && isset(Yii::app()->user->record) && Yii::app()->user->record->update_password)
		{
			Yii::app()->request->redirect('/user/admin/password/modal/1');
			return false;
		}

		return true;
	}

}