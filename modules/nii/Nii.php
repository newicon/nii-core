<?php
/**
 * Nii class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 * @package nii
 */

defined('DS') or define('DS',DIRECTORY_SEPARATOR);

// helper functions
if (!function_exists('dd')) {
	function dd($data) {
		echo '<pre>' . var_dump($data) . '</pre>';exit;
	}
}

/**
 * Nii The Nii application class.  It extends the Yii class.
 * Yii::app() refers to this class.
 *
 * @property Email email
 *
 * @author steve <steve@newicon.net>
 * @package nii
 */
class Nii extends CWebApplication
{
	
	/**
	 * The string key used by the setting component to store the module config array
	 * @var string
	 */
	public $moduleSettingsKey = 'modules';
	
	/**
	 * the string category used by the settings module to store the module config array
	 * @var string 
	 */
	public $moduleSettingsCategory = 'config';
	
	/**
	 * enable this app to support multiple subdomain instances
	 * @var type 
	 */
	public $multiTenant = false;
	
	/**
	 * The default controller for a domain multi site application
	 * @var string 
	 */
	public $multiTenantDefaultController = 'site';
	
	/**
	 * An array of subdomains that are not allowed
	 * in the format of array(
	 *    0 => 'hotspot',
	 *    1 => 'naughty_domain'
	 * )
	 * @var array
	 */
	public $bannedSubDomains=array();

	/**
	 * the website url host that the app is running on,
	 * this enables the app to determin the subdomain acurately
	 * @var string 
	 */
	public $hostname = 'local.newicon.org';
	
	/**
	 * the database options for the subdomain specific database component
	 * @see CDbConnection
	 * 
	 * @var array 
	 */
	public $domainDb = array(
		'username'=>'root',
		'password'=>'',
		'enableProfiling'=>false,
		'enableParamLogging'=>false,
	);
	
	/**
	 * whether to activate the https filter. If false the https filter will not redirect to 
	 * a https page
	 * @see NController::filterHttps
	 * @var type 
	 */
	public $https = false;
	
	/**
	 * the spcific user database hostname only applicable for Mysql,
	 * enables user dtabases to be hosted on other servers.
	 * @var string
	 */
	public $domainDbHostname = 'localhost';
	
	/**
	 * prefix of the the new subdomain databases
	 * @var string 
	 */
	public $domainDbPrefix = 'nii_';
	
	/**
	 * the subdomain the application is currently running under
	 * @var string
	 */
	private $_subDomain;
	
	/**
	 * stores the core module configuration
	 * @var array 
	 */
	private $_coreModules;

	protected $_isInstalled = false;

	/**
	 * stores the location of non-core modules
	 * @var array
	 */
	
	/**
	 * @return string the version of Nii framework
	 */
	public static function getVersion()
	{
		return '1.0.0';
	}
	
	/**
	 * setup the application
	 * @return Nii
	 */
	public function setup()
	{
		// initialise the nii module before we use the user module
		Yii::app()->getModule('nii');
		// Yii::app()->getModule('user');
		
		if (!$this->checkInstalled())
			return $this; 

		// multi tennant settings
		if($this->multiTenant)
			$this->setupDomainMultisite();
		
		// Loads database settings to override config
		$this->setupApplication();
		
		// initialise modules
		$this->setupModules();
		
		// run the application (process the request)
	 	return $this;	
	}

	public function isInstalled()
	{
		return $this->_isInstalled;
	}
	
	/**
	 * This function applies database settings to override config
	 * @return void
	 */
	public function setupApplication()
	{
		$this->configure(Yii::app()->settings->get('application','config'));
	}
	
	/**
	 * Checks if the system has an active db connection and user table 
	 * @return boolean
	 */
	public function checkInstalled()
	{
		$i = new InstallForm();
		if(!$i->isDbInstalled($e) || !$i->isUserInstalled()){
			$this->_isInstalled = false;
			$this->goToInstall();
			return false;
		}
		$this->_isInstalled = true;
		return true;
	}
	
	/**
	 * checks if the system has a database installed
	 * @param $e byref exception object
	 * @return boolean
	 */
	public function isDbInstalled(&$e = null){
		// if the db component is already configured then we already have a configuration for the database.
		// this enables the view to display the install user form, and hides the database details.
        $comps = Yii::app()->getComponents(false);
        if (array_key_exists('db', $comps)) {
            try {
                Yii::app()->db->getSchema()->getDbConnection();
                return true;
            } catch (Exception $e) {
                return false;
            }
        }
		return false; 
	}
	
	/**
	 * Setup all nii modules. Runs the setup function of each modules.
	 * Core modules have their setup function executed first
	 * @return void
	 */
	public function setupModules()
	{
		// Update the Yii::app()->modules configuration
		$this->configureModules();
		
		$this->initialiseModules();
		
		// setup each module 
		foreach($this->getModules() as $name => $config) {
			if(!$this->isNiiModule($name)) continue;
			Yii::app()->getModule($name)->setup();
		}
		
		// raise the onAfterModulesSetup event.
		$this->onAfterModulesSetup(new CEvent($this));
	}
	
	/**
	 * Merges all modules into the Yii modules configuration array.
	 * This must be called before any subsequent calls to Yii::app()->getModules();
	 * @see Nii::setupModules
	 * @return void
	 */
	public function configureModules()
	{
		// The core modules are defined in the application config
		// before merging with the database modules
		$this->_coreModules = $this->getModules();
		// exclude modules (like gii) from the core modules
		foreach($this->getExcludeModules() as $k=>$module)
			unset($this->_coreModules[$module]);
		
		// enable all core modules, by default enabled is false so we must
		// explicitly set enabled to true for all core modules
		// update the core configuration to include the enabled property.
		foreach($this->_coreModules as $name=>$config){
			$enabled = array_key_exists('enabled', $this->_coreModules[$name]) ? $this->_coreModules[$name]['enabled'] : true;
			$this->_coreModules[$name]['enabled'] = $enabled;
		}
		
		// Yii itself assumes core modules (defined in config) are enabled but does not update the modules configuration...
		// The abve ensures the enabled property is set. Alternatively, each module in the config file could just have it hard coded.
		// I could loop through and do getModule($name)->enabled = true, but this would initilaise the module and I don't want to do that as it is the reponsibility of the initialisation function
		// If you are interested check out line 260 of CModule.php and then the getNiiModules function
		// So either this code which I have modded a bit but still does the same thing. Or ensure we always define the 'enabled' property in the core config.php files
		$this->setModules($this->_coreModules);
		
		// Set database modules
		$activeModules = $this->getModulesDbConfig();

		if(!empty($activeModules)) {
			$this->setModules($activeModules);
		}
	}

	/**
	 * Override parent CModule::setModules
	 * If a module configuration has an 'alias' key then we defined a different alias
	 * @param array $modules
	 * @return void
	 */
	public function setModules($modules, $merge = true)
	{
		foreach($modules as $id=>$module)
		{
			if(!is_int($id) && isset($module['alias']))
			{
				if (isset($module['path'])) {
					Yii::setPathOfAlias($id, $module['path']);
				}
				$modules[$id]['class']=$id.'.'.ucfirst($id).'Module';
				unset($modules[$id]['alias']);
			}
		}
		parent::setModules($modules);
	}
	
	/**
	 * gets all NWebModules in the app and returns them in an array,
	 * This initialises each module, it calls the getModule on each module thus instantiating each if they
	 * are not already, thus running each modules initialisation (init) method
	 * @see Nii::setupModules
	 * @return void
	 */
	public function initialiseModules()
	{
		// load the modules. Loops through all modules core modules are first in the configuration array
		// and will therefore get initialised first
		try {
			foreach($this->getModules() as $name => $config) {
				if ($this->isNiiModule($name))
					Yii::app()->getModule($name);
			}
		} catch (Exception $e){
			Yii::app()->updateModule($name, 0);
			Yii::app()->user->setFlash('error',"Unable to load the '$name' module. ");
			if (YII_DEBUG){
				ob_start();
				Yii::app()->displayException($e);
				$errorHtml = ob_get_clean();
				$msg = '<strong>'.$e->getMessage().'</strong>';
				$msg .= ' <a class="label warning" href="#" onclick="jQuery(\'#exception-error-details\').toggle();return false;">Show Error Details</a>'."<div style=\"display:none;\" id=\"exception-error-details\">$errorHtml</div>";
				Yii::app()->user->setFlash('error-block-message', $msg);
			}
		}
	}
	
	
	/**
	 * return boolean true|false whether the module is a nii module
	 * @param string $name the module name key
	 * @return boolean
	 */
	public function isNiiModule($name)
	{
//		if (!array_key_exists($name, array_merge($this->getModulesAvailable(), $this->getCoreModules())))
//			return false;
		return (!in_array($name, $this->getExcludeModules()) && Yii::app()->getModule($name) instanceof NWebModule);
	}
	
	/**
	 * Get all nii modules that are active
	 * @return array [moduleId] => moduble object
	 */
	public function getNiiModules()
	{
		$ret = array();
		foreach($this->getModules()  as $name => $config){
			if (!$this->isNiiModule($name)) continue;
			if (isset($config['enabled']) && $config['enabled'] != true) continue;
			$ret[$name] = Yii::app()->getModule($name);
		}
		return $ret;
	}
	
	/**
	 * Determin if the module is a core module
	 * @name string $name module string id
	 * @return boolean
	 */
	public function isCoreModule($key)
	{
		return array_key_exists($key, $this->getCoreModules());
	}
	
	/**
	 * retuns the core module configuration
	 * @return array [moduleId]=>array modules config
	 */
	public function getCoreModules()
	{
		return $this->_coreModules;
	}
	
	/**
	 * returns an array of modules to exclude from initialisation and setup
	 * @return array list of module id strings
	 */
	public function getExcludeModules()
	{
		return array('gii');
	}
		
	/**
	 * returns a list of all modules configuration including modules available for activation
	 * @return array
	 */
	public function getModulesDbConfig()
	{
		return Yii::app()->settings->get($this->moduleSettingsKey, $this->moduleSettingsCategory, array());
	}
	
	/**
	 * Get all the available modules from a particular directory
	 * @return array ('modulesName => 'modules zombie object')
	 */
	public function getModulesFromAlias($alias) 
	{
		$directory = Yii::getPathOfAlias($alias);
		if (!is_dir($directory))
			return array();
		$modFiles = CFileHelper::findFiles($directory, array('fileTypes' => array('php'), 'level' => 1));
		$mods = array();

		foreach ($modFiles as $m) {
			$modName = basename($m);
			if (!strpos($modName, 'Module'))
				continue;
			
			$mod = str_replace('.php', '', $modName);
			$modId = strtolower(str_replace('Module', '', $mod));
			$parentDir = basename(dirname($m));
			Yii::import("$alias.$parentDir.$mod");

			// check if the module has a valid module class
			if (!@class_exists("$mod", true))
				throw new CException('Can not find the module class for the "' . $mod . '" module');

			$moduleObj = new $mod($modId, null, null, false);
			$moduleObj->setParams(array('alias' => "$alias.$modId"));
			$mods[$modId] = $moduleObj;
		}
		return $mods;
	}

	/**
	 * Gets all modules available for install / activation
	 * looks in the modules folder and finds all module class files.
	 * Each module is instantiated in an isolated environment. It's init and preInit function is not called
	 * and it is not attached to the application object
	 * @return array ('moduleName'=>'module zombie object')
	 */
	public function getModulesAvailable()
	{
		$localMods = $this->getModulesFromAlias( 'localmodules' ); 
		$commMods  = $this->getModulesFromAlias( 'commmodules' ); 
		return array_merge( $localMods, $commMods ); 
	}

	/**
	 * function to activate a module.
	 * - This function get the module from the module id
	 * - Runs the installation
	 * - runs the setup
	 * this function enables the system to add additional functionality during module activation
	 * @param type $moduleId
	 * @return NWebModule 
	 */
	public function activateModule($moduleId)
	{
		try {

			// Get the available modules to find the modules location
			$modules = $this->getModulesAvailable();
				
			$module = $modules[$moduleId];
			Yii::setPathOfAlias("commmodules.$moduleId", $module->getModulePath()); 

			$m = Yii::app()->getModule($moduleId);

			// check dependant modules
			foreach ($m->requires() as $required){
				if (Yii::app()->getModule($required)===null)
					throw new CException(Nii::t('This module requires the ' . $required . ' module to be installed'));
			}

			$m->install();
			$m->setup();
			$this->flushAssets();
			return $m;
			
		} catch(Exception $e) {
			// error activating the module. e.g. may not exist!
			// this can be caused by deleting the module files before disabling
			$this->updateModule($moduleId);
			Yii::app()->user->setFlash('error', "Unable to load the '$moduleId' module in activate");
			if (YII_DEBUG)
				throw new CException($e->getMessage());
			if(Yii::app()->controller)
				Yii::app()->controller->redirect(array('/admin/modules/index'));
		}
	}
	
    /**
	 * Enables or disables a module
	 * @param string $module the module id
	 * @param int $enabled 
	 * @return void
	 */
	public function updateModule($module, $enabled=0)
	{
		Yii::trace('UPDATE MODULE ' . "$module $enabled");
		// Find the module
		$modules = $this->getModulesAvailable();
		
		// get the current module settings
		$sysMods = Yii::app()->settings->get($this->moduleSettingsKey, $this->moduleSettingsCategory, array());
		
		// the module may not exist in the getModulesAvailable()
		// meaning that the database points to a module that has been deleted and no longer exists on the file system
		if (!array_key_exists($module, $modules)) {
			$sysMods = CMap::mergeArray($sysMods, array(strtolower($module) => array('enabled'=>0)));
			Yii::app()->settings->set($this->moduleSettingsKey, $sysMods, $this->moduleSettingsCategory);
		} else {
			$m = $modules[$module];
			$params = $m->getParams();
			$alias = $params->alias;
			// create the array format for the affected module
			$update = array(strtolower($module) => array('enabled'=>$enabled, 'alias' => $alias, 'path' => $m->getModulePath()));
			// merge the update with the existing settings
			$sysMods = CMap::mergeArray($sysMods, $update);
			// save the settings back
			Yii::app()->settings->set($this->moduleSettingsKey, $sysMods, $this->moduleSettingsCategory);
		}
		
		// update yii's module configuration
		Yii::app()->configure(array('modules'=>$sysMods));
		Yii::trace('UPDATE MODULE ' . "$module $enabled");
	}
	
	/**
	 * install all active modules
	 * invokes the install function on all active NWebModule modules
	 * In the event with multiple databases, the install will attempt to update all databases that exist.
	 * It runs each modules install function again each registered database
	 * @return void
	 */
	public function install()
	{
		// only install for the main database
		Yii::app()->cache->flush();
		foreach($this->getNiiModules() as $module) {
			$module->install();
		}
		Yii::app()->cache->flush();
	}
	
	/**
	 * install all loops through each registered database and runs the install of each module against it.
	 * This is useful for multi site subdomain systems
	 * @return void
	 */
	public function installAll()
	{
		NAppRecord::$db = null;
		$this->install();
		// need to loop through sub databases if on a multi site install
		if($this->multiTenant){
			$doms = AppDomain::model()->findAll();
			foreach($doms as $d){
				// get the specific database for this domain
				$db = $this->domainDbPrefix.$d->domain;
				NAppRecord::$db = $this->_getDb($db);
				foreach($this->getNiiModules() as $module){
					$module->install();
				}
			}
		}
	}
	
	/**
	 * Multi tennant
	 * Adds the necessary configurations to create a multiple site application.  
	 * Each separate sub domain will act as an isolated application. Sharing global user details.
	 * @return void
	 */
	public function setupDomainMultisite()
	{
		// set up application context using the subdomain
		$host = Yii::app()->request->getHostInfo();
		// define the hostname!
		$schema = Yii::app()->request->getIsSecureConnection() ? 'https://' : 'http://';
		$subdomain = trim(str_replace(array($schema, $this->hostname),'',$host),'.');

		// redirect if www.
		if($subdomain=='www')
			Yii::app()->request->redirect(str_replace('www.','',$host).Yii::app()->request->getUrl(), true, 301);

		if($subdomain==''){
			$this->defaultController = $this->multiTenantDefaultController;
		} else {
			//check domain
			$dom = AppDomain::model()->findByPk($subdomain);
			if($dom === null){
				throw new CHttpException(404, 'Domain not found');
			}else{
				$this->setSubDomain($dom->domain);
				$this->defaultController = $this->multiTenantDefaultController;
				$this->configSubDomain();
			}
		}
		// create a new application for the user on registratiion.
		UserModule::get()->onRegistrationComplete = array($this, 'registrationComplete');
	}
	
	/**
	 * handles user onRegistrationComplete event
	 * delegates to create App
	 * @param type $event
	 * @return void
	 */
	public function registrationComplete($event)
	{
		if ($this->multiTenant)
			$this->createApp($event->params['user']->domain);
	}
	
	/**
	 * installs a new application database for the specific subdomain
	 * It then runs each modules install against the new databse
	 * @param string $subdomain 
	 * @return void
	 */
	public function createApp($subdomain)
	{
		$this->subDomain = $subdomain;
		$dbName = $this->getDomainDbName();
		// install new database with the name of the subdomain .. like spanner_subdomain
		$sql = "CREATE DATABASE `$dbName`";
		$cmd = Yii::app()->getDb()->createCommand($sql);
		$cmd->execute();
		
		// install the tables and run each modules install
		$this->install();
	}
	
	/**
	 * Get the subdomin
	 * @return string 
	 */
	public function getSubDomain()
	{
		return $this->_subDomain;
	}
	
	/**
	 * Multi tennant
	 * set the subdomin
	 * @param string $subdomain
	 * @return void 
	 */
	public function setSubDomain($subdomain)
	{
		$this->_subDomain = $subdomain;
	}
	
	/**
	 * Multi tennant
	 * This is run when a subdomain is initialised.
	 * changes file paths etc to make specific to the subdomain environment
	 * @return void
	 */
	public function configSubDomain()
	{
		// to prevent cache affecting other subdomains lets create a specific runtime folder for each subdomain
		$runtime = $this->getRuntimePath().DS.$this->getSubDomain();
		if(!file_exists($runtime)){
			mkdir($runtime);
		}
		$this->runtimePath = $runtime;
	}
	
	/**
	 * Multi tennant
	 * returns the name of the database used on this specific sub domain
	 * used for multisite applications
	 * @return string the database name 
	 */
	public function getDomainDbName()
	{
		$subdomain = $this->getSubDomain();
		return $this->domainDbPrefix.$subdomain;
	}
	
	/**
	 * Multi tennant
	 * get the database connection object specific to this subdomain
	 * returns the main database connection if there is no subdomain
	 * @return CDbConnection 
	 */
	public function getSubDomainDb()
	{
		// if there is no subdomain return the main database
		if($this->getSubDomain() == ''){
			$db = Yii::app()->getDb();
			return $db;
		}
		// get the database name secific to the current subdomain
		$dbName = $this->getDomainDbName();
		
		return $this->_getDb($dbName);
	}
	
	/**
	 * Multi tennant
	 * Adds a new database component to Nii specific to the current domain application instance
	 * and returns the resulting CDbConnection.
	 * @param string $dbName the database name
	 * @return CDbConnection 
	 */
	protected function _getDb($dbName)
	{
		if(!$this->hasComponent("{$dbName}_db")) {
			$a = array(
				'class' => 'CDbConnection',
				'connectionString' => "mysql:host={$this->domainDbHostname};dbname={$dbName}",
				'emulatePrepare' => true,
				'charset' => 'utf8',
				'tablePrefix' =>'',
			);
			// properties in array $a will overwrite any *silly* properties in $this->dbOptions
			$this->domainDb = array_merge($this->domainDb, $a);
			$this->components = array("{$dbName}_db"=>$this->domainDb);
		}
		$component = "{$dbName}_db";
		return $this->$component;
	}
	
	/**
	 * function called when the system needs to be installed 
	 * this must be called instead of the run function
	 * @return void
	 */
	public function goToInstall() 
	{
		$route = explode('/',Yii::app()->getUrlManager()->parseUrl($this->getRequest()));
		if ($route[0] != 'install') {
			$this->catchAllRequest = array('install');
		}
	}
	
	/**
	 * Event called after nii has called the setup function of each module
	 * @return void
	 */
	public function onAfterModulesSetup($event)
	{
		$this->raiseEvent('onAfterModulesSetup', $event);
	}
	
	/**
	 * Clear all the assets from the assets folder
	 * @return void
	 */
	public static function flushAssets()
	{
		$ignore = array(Yii::app()->getAssetManager()->basePath.'/.gitignore');
		NFileHelper::deleteFilesRecursive(Yii::app()->getAssetManager()->basePath,$ignore);
	}
	
	/**
	 * Uses default Yii::t function and sets catagory to Nii
	 * @see Yii::t
	 * @param string $message
	 * @return string
	 */
	public static function t($message,$params=array(),$source=null,$language=null)
	{
		return Yii::t('Nii', $message, $params, $source, $language);
	}

	/**
	 * @return $this
	 */
	public static function app()
	{
		return Yii::app();
	}
	
}
