<?
class UserTest extends CDbTestCase
{

  public $fixtures=array(
    'users'=>'User',
  );


  function testSomething()
  {
    $this->assertEquals( User::model()->count(), 1 );
  }

  function test_getByEmail()
  {
    $user = new User;
    $userByEmail = $user->getByEmail( $this->users['demo']['email'] );
    $this->assertInstanceOf( 'User', $userByEmail );
    $this->assertEquals( $userByEmail['email'], $this->users['demo']['email'] );
  }
}
