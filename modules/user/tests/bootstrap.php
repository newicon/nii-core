<?php

// Include the master bootstrap file
require_once (dirname(__FILE__).'/../../../tests/master_bootstrap.php');
$env = new Environment('TEST');

// Add test-specific configuration to the environment
$testconfig = require(dirname(__FILE__).'/../config/test.php');
$env->add( $testconfig );

// Create the Nii application for test
Yii::createApplication('Nii',$env->config)->setup();

// Install the user module
Yii::app()->getModule('user')->install();

