<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	
	/**
	 * store the user record
	 * @var NActiveRecord 
	 */
	private $_user;
	
	const ERROR_EMAIL_INVALID=3;
	const ERROR_STATUS_NOTACTIV=4;
	const ERROR_STATUS_BAN=5;
	const ERROR_DOMAIN=6;
	const ERROR_SUSPENDED=7;
	
	
	/**
	 * Gets the ActiveRecord User model
	 * 
	 * @return User 
	 */
	public function getUser()
	{
		if($this->_user === null){
			if (strpos($this->username,"@")) {
				$this->_user=UserModule::userModel()->notsafe()->findByAttributes(array('email'=>$this->username));
			} else {
				$this->_user=UserModule::userModel()->notsafe()->findByAttributes(array('username'=>$this->username));
			}
		}
		return $this->_user;
	}
	
	/**
	 * Authenticates a user.
	 * @param int skipPassword - allows login to skip password. Obviously this is insecure -
	 * 		  only use if coming from a secure place, e.g. from an activation hash.	
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate($skipPassword = false)
	{
		$this->getUser();

		if($this->_user===null)
		{
			if (strpos($this->username,"@")) {
				$this->errorCode=self::ERROR_EMAIL_INVALID;
			} else {
				$this->errorCode=self::ERROR_USERNAME_INVALID;
			}
		}
		else if($this->_user->suspended == 1)
		{
			$this->errorCode=self::ERROR_SUSPENDED;
		}
		else if(!$skipPassword && !$this->_user->checkPassword($this->password))
		{
			$this->errorCode=self::ERROR_PASSWORD_INVALID;

			// increment failed login count
			$this->_user->failed_logins++;
			$this->_user->save();
		}
		else if($this->_user->status==0&&Yii::app()->getModule('user')->loginNotActive==false)
			$this->errorCode=self::ERROR_STATUS_NOTACTIV;
		else if($this->_user->status==-1)
			$this->errorCode=self::ERROR_STATUS_BAN;
		else {
			// if user module set up for subdomain apps then we need to add 
			// an extra check
			if(UserModule::get()->domain){
				if($this->_user->domain != Yii::app()->getSubDomain()){
					if(!$this->_user->superuser){
						$this->errorCode=self::ERROR_DOMAIN;
						return !$this->errorCode;
					}
				}
			}

			// set failed login count to 0
			$this->_user->failed_logins = 0;
			$this->_user->save();

			$this->_loginUser($this->_user);
		}

		if ($this->_user!==null)
			$this->_user->checkMaxFailedLogins();

		return !$this->errorCode;
	}
    
    /**
     * Determins if the identity has a validation error when attempting authentication
     * @return boolean
     */
    public function hasError()
    {
        // error code 0 means success
        $success = 0;
        return ($this->errorCode !== $success);
    }

	/**
	 * This function does not perform the user login. That is done by Yii::app()->user->login($UserIdentity (this class), $duration)
	 * The command that performs the login is called in the UserLogin model form object
	 * This function adds additional data to the user identity class before logging in.
	 * 
	 * @param NActiveRecord $user 
	 */
	protected function _loginUser($user)
	{
		if($user){
			// successful login has occured so we store some additional data
			$this->_user = $user;
			$this->_id = $this->_user->id;
			$this->username = ($user->username==null)?$user->email:$user->username;
            $this->errorCode = self::ERROR_NONE;
			// we also want to generate a random auth key which we store in the session state.
			// this will then also get stored in the cookie.
			// Then when restoring from a cookie we can compare this key with the one in the database 
			// this will implement the additional security as recomended by yii
			// here: http://www.yiiframework.com/doc/guide/1.1/en/topics.auth section 3. Cookie-based Login 
		}
	}
	
	/**
	 * Allows administrator to impersonate another user
	 * @param type $userId
	 * @return UserIdentity 
	 */
	public static function impersonate($userId)
	{
		$superUser = UserModule::userModel()->notsafe()->findByPk(Yii::app()->user->id);
		$user = UserModule::userModel()->notsafe()->findByPk($userId);
		if($user)
		{   
			//TODO: add another check here to ensure currently logged in user has permission to do this.
			Yii::app()->session['impersonate_restore'] = Yii::app()->user->id;
			Yii::app()->session['impersonate_restore_validation'] = md5($superUser->password); 
			// create some key from the password and infomation of this superuser to validate when performing a restore
			// this would ensure the correct user is being restored (so the session can not just be hacked adding in this id)
			$ui = new UserIdentity($user->email, "");
			$ui->_logInUser($user);
			Yii::app()->user->login($ui);
		}
	}
	
	/**
	 * restore the session of an impersonated user to the original user
	 * i.e. reverse the effects of impersonate
	 */
	public static function impersonateRestore()
	{
		$user = UserModule::userModel()->notsafe()->findByPk(Yii::app()->session['impersonate_restore']);
		if($user)
		{
			// check the validation session to ensure the validation key matches this user.
			// Protects against cases where the session could be hacked.  
			// You must know the users details (password and the id) to restore the user.
			if(md5($user->password) == Yii::app()->session['impersonate_restore_validation']){
				$ui = new UserIdentity($user->email, "");
				$ui->_logInUser($user);
				Yii::app()->user->login($ui);
				// remove restore session
				unset(Yii::app()->session['impersonate_restore']);
				unset(Yii::app()->session['impersonate_restore_validation']);
			}
		}
	}

   /**
    * @return integer the ID of the user record
    */
	public function getId()
	{
		return $this->_id;
	}

	public function getSubDomain()
	{
		if($this->_user !==null){
			return $this->_user->domain;
		}
		return null;
	}
    
    /**
     * Return an array of errorCode => error message
     */
    public function errorMessages()
    {
        return array(
            self::ERROR_EMAIL_INVALID => UserModule::t("Email is incorrect."),
            self::ERROR_USERNAME_INVALID => UserModule::t("Username is incorrect."),
            self::ERROR_STATUS_NOTACTIV => UserModule::t("Your account is not activated."),
            self::ERROR_STATUS_BAN => UserModule::t("Your account is blocked."),
            self::ERROR_PASSWORD_INVALID => UserModule::t("Password is incorrect."),
            self::ERROR_DOMAIN => UserModule::t("You do not have access to the current domain."),
            self::ERROR_SUSPENDED => UserModule::t("This account is suspended."),
        );
    }
    
    /**
     * A function to interpret error codes and give a human readable message
     * @see $this->errorCode
     * @param int $errorCode if not specified will use the errorCode stored in $this->errorCode
     */
    public function getErrorMessage($errorCode=null)
    {
        $errorCode = ($errorCode===null) ? $this->errorCode : $errorCode;
        $errors = $this->errorMessages();
        return isset($errors[$errorCode]) ? $errors[$errorCode] : 'unknown error code';
    }
}