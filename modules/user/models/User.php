<?php

/**
 * 
 * The following are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $password 
 * @property string $email
 * @property string $activekey
 * @property integer $createtime
 * @property integer $lastvisit
 * @property integer $superuser
 * @property integer $status
 */
class User extends NActiveRecord 
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANED=-1;

	public $fullname;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__) 
	{
		return parent::model($className);
	}
	
	/**
	 * calls parent install function 
	 * @param string $className 
	 */
	public static function install($className=__CLASS__)
	{
		parent::install($className);
	}

	/**
	 * table schema used for auto install
	 * 
	 * @see NActiveRecord::schema
	 * @return array 
	 */
	public function schema() 
	{
		return array(
			'columns' => array(
				'id' => 'pk',
				'title' => 'string',
				'first_name' => 'string',
				'last_name' => 'string',
				'company' => 'string',
				'username' => 'string',
				'password' => 'string NOT NULL',
				'update_password' => 'boolean NOT NULL DEFAULT 0',
				'email' => 'string',
				'email_verified' => 'boolean NOT NULL DEFAULT 0',
				'activekey' => 'string NOT NULL',
				'createtime' => 'datetime',
				'lastvisit' => 'datetime',
				'superuser' => 'boolean NOT NULL DEFAULT 0',
				'status' => 'boolean NOT NULL DEFAULT 0',
				'domain' => 'string',
				'plan_level' => 'string',
				'trial_ends_at' => 'datetime',
				'trial' => 'boolean NOT NULL DEFAULT 1',
				'logins' => 'int',
				'failed_logins' => 'tinyint NOT NULL DEFAULT 0',
				'suspended' => 'boolean NOT NULL DEFAULT 0',
				// could add to a behavior
				// image_id and image_data is required for adding and editing
				// the profile image 
				'image_id'=>'int NOT NULL DEFAULT 0',
				'image_data'=>'text',
			),
			'keys' => array(
				//array('username', 'username', true),
				array('email', 'email'),
				array('status'),
				array('superuser'),
				array('domain'),
				array('image_id'),
			),
            'dblib'=>array(
                'columns' => array(
                    'id' => 'pk',
                    'title' => 'string',
                    'first_name' => 'string null',
                    'last_name' => 'string null',
                    'company' => 'string null',
                    'username' => 'string null',
                    'password' => 'string NOT NULL',
                    'update_password' => 'tinyint NOT NULL DEFAULT 0',
                    'email' => 'string NOT NULL',
                    'email_verified' => 'tinyint NOT NULL DEFAULT 0',
                    'activekey' => 'string NOT NULL',
                    'createtime' => 'datetime2 null',
                    'lastvisit' => 'datetime2 null',
                    'superuser' => 'tinyint NOT NULL DEFAULT 0',
                    'status' => 'tinyint NOT NULL DEFAULT 0',
                    'domain' => 'string null',
                    'plan_level' => 'string null',
                    'trial_ends_at' => 'datetime null',
                    'trial' => 'tinyint NOT NULL DEFAULT 1',
                    'logins' => 'int NOT NULL DEFAULT 0',
                    'failed_logins' => 'tinyint NOT NULL DEFAULT 0',
                    // could add to a behavior
                    // image_id and image_data is required for adding and editing
                    // the profile image 
                    'image_id'=>'int NOT NULL DEFAULT 0',
                    'image_data'=>'text null',
                ),
            )
		);
	}

	/**
	 * return the associated database table name
	 * @return string 
	 */
	public function tableName() 
	{
		return Yii::app()->getModule('user')->tableUsers;
	}

	/**
	 * validation rules for model attributes.
	 * @return array 
	 */
	public function rules() 
	{
		// if you are not an admin, and you are not editing your own details, you get no rules!
		if (!Yii::app()->getModule('user')->isAdmin() && (Yii::app()->user->id != $this->id))
			return array();

		// things we can always do
		$rules = array(
			$rules[] = array('email', 'email'),
			array('username, domain, name, email, roleName', 'safe', 'on' => 'search'),
			array('name, title, first_name, last_name, company, plan_level, trial, 
				   trial_ends_at, logins, update_password, lastvisit, createtime, 
				   contact_id, image_id, image_data, suspended, failed_logins', 'safe'),
		);

		if (Yii::app()->getModule('user')->emailRequired) {
			$rules[] = array('email', 'required');
		}

		if (Yii::app()->getModule('user')->emailUnique) {
			$rules[] = array('email', 'unique', 'message' => UserModule::t("This email address already exists."));
		}

		if (Yii::app()->getModule('user')->usernameRequired) {
			$rules[] = array('username', 'length', 'max' => 20, 'min' => 3, 'message' => UserModule::t("Incorrect username (length between 3 and 20 characters)."));
			$rules[] = array('username', 'match', 'pattern' => "/^[A-Za-z0-9_'-]+$/u", 'message' => UserModule::t("Incorrect symbols (A-z0-9_'-)."));
			$rules[] = array('username', 'unique', 'message' => UserModule::t("This username already exists."));
		} else {
			$rules[] = array('username', 'safe');
		}
		

		// rules for mass assignment when a user is editing their own details
		if (Yii::app()->user->id == $this->id) {
			$rules = array_merge($rules, array(
				array('status', 'in', 'range' => array(self::STATUS_NOACTIVE, self::STATUS_ACTIVE, self::STATUS_BANED)),
				array('password', 'length', 'max' => 128, 'min' => 4, 'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
				));
		}

		// if the user is an admin user they are able make users super users and change passwords.
		if (Yii::app()->getModule('user')->isAdmin()) {
			$rules = array_merge($rules, array(
				array('status', 'in', 'range' => array(self::STATUS_NOACTIVE, self::STATUS_ACTIVE, self::STATUS_BANED)),
				array('password', 'length', 'max' => 128, 'min' => 4, 'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
				array('superuser', 'in', 'range' => array(0, 1)),
				array('superuser, status', 'numerical', 'integerOnly' => true),
				array('roleName', 'safe'),
			));
		}

		return $rules;
	}

	/**
	 * relational rules.
	 * @return array 
	 */
	public function relations() 
	{
		$relations['role'] = array(self::HAS_ONE, 'AuthAssignment', 'userid');
		$relations['roles'] = array(self::HAS_MANY, 'AuthAssignment', 'userid');
		return $relations;
	}

	/**
	 * customized attribute labels (name=>label)
	 * @return array 
	 */
	public function attributeLabels() 
	{
		return array(
			'title' => 'Title',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'company' => 'Company',
			'username' => UserModule::t("Username"),
			'password' => UserModule::t("Password"),
			'verifyPassword' => UserModule::t("Verify Password"),
			'update_password' => UserModule::t("Force password update on next login"),
			'email' => UserModule::t("E-mail Address"),
			'email_verified' => UserModule::t("Email Verified"),
			'verifyCode' => UserModule::t("Verification Code"),
			'id' => UserModule::t("Id"),
			'activekey' => UserModule::t("activation key"),
			'createtime' => UserModule::t("Registration date"),
			'lastvisit' => UserModule::t("Last visit"),
			'superuser' => UserModule::t("Superuser"),
			'status' => UserModule::t("Status"),
			'roleName' => 'Role',
		);
	}

	/**
	 * return custom scopes used by user.
	 * Note the notsafe scope to return all user columns
	 * @return array 
	 */
	public function scopes() 
	{
        return array(
			'active' => array(
				'condition' => 'status=' . self::STATUS_ACTIVE,
			),
			'notactive' => array(
				'condition' => 'status=' . self::STATUS_NOACTIVE,
			),
			'banned' => array(
				'condition' => 'status=' . self::STATUS_BANED,
			),
			'superuser' => array(
				'condition' => 'superuser=1',
			),
			'notsafe' => array(
				'select' => 'id, username, first_name, last_name, company, password, email, email_verified, activekey, createtime, lastvisit, superuser, status, domain, plan_level, logins'
			),
		);
	}
	
	/**
	 * Define user behaviors
	 * @return array
	 */
	public function behaviors()
	{
		return array(
			'timestampable'=>array(
				'class'=>'nii.components.behaviors.NTimestampable'
			),
			'trashable'=>array(
				'class'=>'nii.components.behaviors.NTrashBinBehavior',
			),
		);
	}

	/**
	 * Prevent the password field from ever returning under normal circumstances.
	 * You must explicitly access this information using the notsafe scope.
	 * This is annoying but very important we do not want the password field to be returned by the object.
	 * even though the password field is encryted it is still a security risk to return it!
	 * @return array 
	 */
	public function defaultScope()
    {
        return array(
            'select' => 'id, username, title, first_name, last_name, company, email, email_verified, activekey, createtime, lastvisit, superuser, status, domain, plan_level, logins, failed_logins, update_password, suspended',
        );
    }

	public static function itemAlias($type, $code=NULL) 
	{
		$items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => UserModule::t('Not active'),
				self::STATUS_ACTIVE => UserModule::t('Active'),
				self::STATUS_BANED => UserModule::t('Banned'),
			),
			'AdminStatus' => array(
				'0' => UserModule::t('No'),
				'1' => UserModule::t('Yes'),
			),
		);
		if (isset($code))
			return isset($items[$type][$code]) ? $items[$type][$code] : false;
		else
			return isset($items[$type]) ? $items[$type] : false;
	}

	/**
	 * check if the password matches
	 * @param string $password
	 * @return boolean true if the correct password
	 */
	public function checkPassword($checkPass) 
	{
		// uses a salt so that two people with the same password will have
		// different encrypted password values
		// creates a unique salt from each password
		return (UserModule::checkPassword($this->password, $checkPass));
	}

	/**
	 * encrypt the user password
	 * @param string $password
	 * @return string encrypted password 
	 */
	public function passwordCrypt($password) 
	{
		return UserModule::passwordCrypt($password, $this);
	}

	/**
	 * override the default active record set method to detect when the password attribute has been set
	 * @param type $name
	 * @param type $value 
	 */
	public function __set($name, $value)
	{
		if($name == 'password')
			$this->_passwordCrypt = true;
		parent::__set($name, $value);
	}
	
	/**
	 * a private flag indicating whther the model should crypt the password
	 * before saving it to the database, we only want to do this when the password has been set
	 * @var boolean 
	 */
	private $_passwordCrypt = false;
	
	/**
	 * Functinoality to run before saving the active record
	 * Mainly detect if we need to encrypt the password
	 * @return boolean 
	 */
	public function beforeSave() 
	{
		// check if we need to encrypt the password
		if($this->_passwordCrypt) {
			$this->password = $this->passwordCrypt($this->password);
			$this->activekey = $this->passwordCrypt(microtime() . $this->password);
			$this->_passwordCrypt = false;
		}
		
        // clear the failed login count if admin desuspends the user
		if ($this->suspended == 0 && $this->failed_logins > 0) {
            $oldRecord = self::findByPk($this->id);

            if ($oldRecord->suspended == 1) {
			    $this->_resetFailedLogins();
            }
		}

		return parent::beforeSave();
	}

	/**
	 * Retrieves the list of Users based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed users.
	 */
	public function search() 
	{
		$criteria = new CDbCriteria;

		if ($this->fullname){
			$criteria->addCondition("first_name LIKE '%$this->fullname%' OR last_name LIKE '%$this->fullname%'");
		}

		$criteria->compare('username', $this->username, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('superuser', $this->superuser, true);
		$criteria->compare('role.itemname', $this->roleName, true);

		$criteria->with = array('role');
		$criteria->together = true;

		$sort = new CSort;
		$sort->attributes = array(
			'name' => array('asc' => 'first_name', 'desc' => 'first_name DESC'),
			'roleName' => array('asc' => 'role.itemname', 'desc' => 'role.itemname DESC'),
			'*',
		);

		// get count using cheaper query
		$where = count($criteria->params) ? ' WHERE '. $criteria->condition : '';

		foreach ($criteria->params as $key => $val) {
			$where = preg_replace('/'.$key.'/', "'".$val."'", $where);
		}

		$user  = UserModule::get()->userClass;

		$dataProvider = new NActiveDataProvider($user, array(
			'criteria' => $criteria,
			'sort' => $sort,
		));
		$dataProvider->pagination->pageSize = 50;
		return $dataProvider;
	}

	/**
	 * display a user name, if first names and last name do not exist resorts 
	 * to displaying either the username or email address.
	 * @return string 
	 */
	public function getName()
	{
		if ($this->first_name != '')
			return $this->first_name . ($this->last_name ? ' ' . $this->last_name : '');
		else
			return $this->username ? $this->username : $this->email;
	}

	public function getRoleDescription() 
	{
		if ($this->role)
			return $this->role->authitem->description;
		return '';
	}

	/*
	 * Returns roles in key => value e.g. role-administrator => 'role-administrator'
	 */
	public function getRolesList()
	{
		return CHtml::listData($this->roles,'itemname','itemname');
	}

	private $_roleName;

	public function getRoleName()
	{
		if (empty($this->_roleName) && $this->role)
			$this->_roleName = $this->role->itemname;
		return $this->_roleName;
	}

	public function setRoleName($roleName)
	{
		$this->_roleName = $roleName;
	}

	public function saveRole() 
	{
		$role = $this->role ? $this->role : new AuthAssignment;
		
		$role->itemname = $this->roleName;
		$role->userid = $this->primaryKey;
		
		return $role->save();
	}
	
	public function editLink($text)
	{
		return CHtml::link($text, CHtml::normalizeUrl(array('/user/admin/editUser','id'=>$this->id())));
	}
	
	public function getImpersonateLink() {
		return CHtml::link('Impersonate', CHtml::normalizeUrl(array('/user/admin/impersonate','id'=>$this->id())));
	}
	
	private $_name;
	
	/**
	 * why?? 
	 */
	public function setName($value)
	{
		$this->_name = $value;
	}
	
	/**
	 * get a users profile image url
	 * default functionality gets the gravatar for this user.
	 * @param int $size 
	 */
	public function getProfileImageUrl($size=40)
	{
		// Display guest photo		
		return Yii::app()->controller->createWidget('user.widgets.NUserImage',array(
			'user'=>$this,
			'size'=>$size))->getImageUrl();
	}
	
	/**
	 * get a users profile image
	 * default functionality gets the gravatar for this user.
	 * @param int $size 
	 */
	public function getProfileImage($size=40, $includeName=false)
	{
		// Display guest photo		
		return Yii::app()->controller->widget('user.widgets.NUser',array(
			'user'=>$this,
			'size'=>$size,
			'includeName'=>$includeName
			), 
			true);
	}
	
	/**
	 * get a users profile image
	 * default functionality gets the gravatar for this user.
	 * @param int $size 
	 */
	public function getProfileBadge($size=40, $includeName=true)
	{
		// Display guest photo		
		return Yii::app()->controller->widget('user.widgets.NUser',array(
			'user'=>$this,
			'size'=>$size,
			'includeName'=>$includeName
			), 
			true);
	}

	/**
	 * Return the Yii route to the user profile page
	 * @return array Yii route
	 */
	public function getRouteProfile()
	{
		return array('/user/profile/index', 'id'=>$this->id);
	}
	
	/**
	 * Gets a user based on their e-mail address
	 * 
	 * This will find a user based on the supplied e-mail address
	 * ###Example
	 *		$user = new User;
	 *		$user->getByEmail('joeBloggs@newicon.net');
	 * This will return the user row for Joe Bloggs.
	 * 
	 * @param string $email the e-mail address of the user
	 * @return the user which matches the e-mail address or null if none found
	 */
	public function getByEmail($email)
	{
		$condition = array(
		    'condition'=>'email = :email',
		    'params'=>array(':email'=>$email)
		);
		return $this->find($condition);
	}


	/**
	 * Checks if user has exceeded max failed login attempt count
	 * If exceeded, user record is set to suspended
	 *
	 * @return boolean true if current attempts count < max failed attempts
	 */
	public function checkMaxFailedLogins()
	{
		if ($this->failed_logins >= Yii::app()->user->maxFailedLogins)
		{
			if ($this->suspended == 0)
			{
				$this->suspended = 1;
				$this->save();
			}
			return true;
		}

		return false;
	}

	/**
	 * resets failed logins to 0 for user
	 */
	private function _resetFailedLogins()
	{
		$this->failed_logins = 0;
		$this->save();
	}
	
}
