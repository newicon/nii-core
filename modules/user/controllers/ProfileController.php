<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of ProfileController
 *
 * @author steve
 */
class ProfileController extends AController
{
	public function actionIndex($id)
	{
		$user = NData::loadModel('User', $id, 'No user found');
		$this->render('index', array('user'=>$user));
	}
	
	public function actionImage($for)
	{
		$user = User::model()->findByPk($for);
		$url = $user->getProfileImageUrl(120);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		$raw=curl_exec($ch);
		curl_close ($ch);
		header('Content-Type: image');
		header('Content-Length: ' . strlen($raw));
		echo $raw;
		exit;
	}
}
