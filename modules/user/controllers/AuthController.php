<?php

/**
 * AuthController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Auth controller should be responsible for logging in logging out 
 * and general authentication
 *
 * @author steve
 */
class AuthController extends NController
{
	// TODO: move auth related actions currently in account controller here.
}