<?
$fixturePath = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'fixtures');
return array(
  'components' => array(
    'fixture' => array(
      'basePath' => $fixturePath, 
     ),
  ),
  /** 
     For user, these are actually already in the main configuration. However 
     for illustration purposes, the test.php configuration should be 
     responsible for importing the models/components/etc to be tested
  **/
  'import' => array(
    'application.modules.user.models.*',
    'application.modules.user.components.*',
  ),
);
