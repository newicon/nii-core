<?php

/**
 * NUser class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
Yii::import('user.widgets.*');
/**
 * NUser widget displays a nicely formated representation of a user supplied by property user
 *
 * @property mixed $user int representing the id of a user | User record
 * @property int $size size of profile image
 * @author steve
 */
class NUser extends NUserWidget
{
	/**
	 * size of profile image
	 */
	public $size = 25;
	
	/**
	 * text to display for the user if the user is not found
	 * @var type 
	 */
	public $unknown = 'unknown';
	
	/**
	 * whether to show an image if the user is not known
	 * @var type 
	 */
	public $showUnknownImage = true;
	
	/**
	 * whether to include the user name next to the image
	 * @var boolean 
	 */
	public $includeName = true;
	
	/**
	 * Append additional html output to the users name
	 * e.g. <span class="label label-info">Is a naughty boy</span>
	 * @var string (html)
	 */
	public $htmlNameAppend = '';
	
	/**
	 * run the widget
	 */
	public function run()
	{
		$user = $this->getUserRecord();
		
		$this->render('user', array(
			'user'=>$user, 
			'size'=>$this->size,
		));
	}
}