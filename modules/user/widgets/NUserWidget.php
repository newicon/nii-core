<?php

/**
 * NUserWidget class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Parent NUserWidget class for use on user widgets
 * convience user based widget functions
 *
 * @property mixed $user int representing the id of a user | User record
 * @author steve
 */
class NUserWidget extends NWidget
{
	/**
	 * id of user or User record
	 * @var mixed 
	 */
	public $user;
	
	/**
	 * stores the user model
	 * @var User | null
	 */
	private $_user;
	
	/**
	 * gets the user record
	 * @return User | null if user record is not found 
	 */
	public function getUserRecord()
	{
		if ($this->_user === null) {
			if (is_numeric($this->user))
				$this->_user = User::model()->findByPk($this->user);
			elseif ($this->user instanceOf User)
				$this->_user = $this->user;
		}
		return $this->_user;
	}
}