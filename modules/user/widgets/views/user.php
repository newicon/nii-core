<?php

/**
 * Nii view file.
 * 
 * Renders a user.
 * 
 * To be expanded to show a popup card.
 * Gets passed $user as a User row
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<?php  if ($this->includeName): ?>
	<?php $attr = isset($user) ? ' data-user="' . $user->id . '"' : ''; ?>
	<div class="media user-badge" <?php echo $attr; ?>>
		<a class="pull-left mrn" style="padding:2px;">
			<?php if (isset($user->name) || $this->showUnknownImage): ?>
				<?php $this->widget('user.widgets.NUserImage',array('user'=>$user, 'size'=>$size)); ?>
			<?php endif; ?>
		</a>
		<div class="media-body pts">
			<?php echo isset($user->name) ? $user->name : $this->unknown; ?>
			<?php echo $this->htmlNameAppend ?>
		</div>
	</div>
<?php else: ?>
	<?php if (isset($user->name) || $this->showUnknownImage): ?>
	<?php $this->widget('user.widgets.NUserImage',array('user'=>$user, 'size'=>$size)); ?>
	<?php endif; ?>
<?php endif; ?>
