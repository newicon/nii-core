<?php 
class UserAutocomplete extends CWidget
{

  public $model;
  public $attribute;
  public $inputName;

  public function init()
  {
  }

  public function run()
  {
    $input_id 	= sprintf('%s_%s',get_class($this->model), $this->attribute);  
    $input_name = sprintf('%s[%s]',get_class($this->model), $this->attribute);  
    $widget = $this->controller->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                        'name' => 'User', 
                                        'value' => $this->model->getAttribute($this->attribute),
                                        'source' => $this->controller->createUrl('/user/autocomplete/userList/'),
                                            'options' => array(
                                              'showAnim' => 'fold',
                                              'change' => "js:function(event,ui){
                                                var newid = ui.item ? ui.item.id : null;
                                                $('#$input_id').val(newid);
                                                }"
                                          )),true);
    $widget .= "<input id='$input_id' type='hidden' name='$input_name'>";
    echo $widget;
  }

}
