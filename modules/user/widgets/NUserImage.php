<?php

/**
 * NUserImage class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

Yii::import('user.widgets.*');
/**
 * NUserImage draws the profile image for this user
 * Of no image has been added then it defaults to using gravatar
 * 
 * <code>
 *  $this->widget('user.widgets.NUserImage',array(
 *		'user' => Yii::app()->user->record,
 *  ));
 * </code>
 *
 * @property mixed $user int representing the id of a user | User record
 * @property int $size size of profile image
 * @author steve
 */
class NUserImage extends NUserWidget
{
	/**
	 * size of image
	 * @var int
	 */
	public $size = 40;
	
	/**
	 * html attributes for the image tag
	 * @var array
	 */
	public $htmlOptions = array();
	
	public $defaultImgClass = 'user-thumbnail';
	
	public $tooltipText;
	
	/**
	 * draws the profile image tag
	 * We have intentionally seperated out getting the image url from drawing the image tag
	 * as some javascript models and implementations only require the image url.
	 * you can get the image url using $this->controller->createWidget(array('user'=>1))->getImageUrl();
	 */
	public function run(){
		$user = $this->getUserRecord();
		$name = $user ? $user->name : 'unknown';
		if (!array_key_exists('class', $this->htmlOptions))
			$this->htmlOptions['class'] = $this->defaultImgClass;
		
		if (!array_key_exists('style', $this->htmlOptions))
			$this->htmlOptions['style'] = 'padding:2px;';
		
		$this->htmlOptions['src'] = $this->getImageUrl();
		$this->htmlOptions['width'] = $this->size;
		$this->htmlOptions['rel'] = 'tooltip';
		$this->htmlOptions['title'] = $this->tooltipText ? $this->tooltipText : $name;

		echo CHtml::tag('img', $this->htmlOptions);
	}
	
	/**
	 * gets the users image url
	 *  
	 * @return url
	 */
	public function getImageUrl()
	{
		// could raise an event on the user module
		// onGetUserImage
		// additional functionality could then handle this
		// if the event property handled returns true then we use the image as returned by the event
		// rather than the default gravatar implementation.
		// this way image profiles can gracefully degrade back to a default gravatar implementation if 
		// no images have been added in the additional functionality
		// $event = new CEvent(array('user'=>$this->user));
		// UserModule::get()->onGetUserImage($event)
		// if($event->handled)
		//     $event->imageUr; // do something with the returned image url
		$user = $this->getUserRecord();
		// the user may not exist
		$email = $user ? $user->email : '';
		$imageId = $user ? $user->image_id : null;
		
		if ($imageId) {
			return NImage::url($imageId, "{$this->size}x{$this->size}.max");
		}
		$url = $this->createWidget('nii.widgets.Gravatar',array(
			'email'=>$email,
			'size'=>$this->size
		))->getUrl();
		return $url;
	}
	
}