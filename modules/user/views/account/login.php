<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Login"); ?>
<?php
$form = $this->beginWidget('NActiveForm', array(
    'id' => 'login-user-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'focus' => array($model, 'username'),
));

// check if user has exceeded the max number of failed retries
$suspended    = false;
$userId       = $model->getUserIdentity();
$attemptsLeft = null;

if (isset($userId))
{
    $user = $userId->getUser();

    if (isset($user))
    {
    if ($user !== null && $user->suspended == 1 && $user->checkMaxFailedLogins())
        $suspended = true;
    else
        $attemptsLeft = Yii::app()->user->maxFailedLogins - $user->failed_logins;
    }
}

?>
<div class="modal" id="modal-login-user">
    <div class="modal-header">
        <h3>Welcome to the <?php echo Yii::app()->name ?></h3>
    </div>
    <div class="modal-body">

        <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="alert alert-block alert-success">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>

        <?php if($model->hasErrors()) : ?>
            <div class="alert alert-block alert-error">
                <p>Sorry the login information is incorrect. Please try again.</p>
            </div>
        <?php else : ?>
            <div class="alert alert-block alert-info">
                <p>Please enter your details below to login to the system.</p>
            </div>
        <?php endif; ?>

        <fieldset>
            <?php echo $form->field($model, 'username', 'textField'); ?>
            <?php echo $form->field($model, 'password', 'passwordField'); ?>
            <div class="control-group">
                <div class="controls">
					<label>
						<div style="float:left">
							<?php echo $form->checkBoxField($model, 'rememberMe'); ?>
						</div>
						Keep me logged in for 30 days
					</label>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="modal-footer">
        <?php echo CHtml::submitButton(UserModule::t("Login"),array('class'=>'btn btn-primary', 'disabled'=>$suspended?'disabled':false)); ?>
        <?php if(Yii::app()->getModule('user')->enableGoogleAuth): ?>
            <a class="btn" href="<?php echo NHtml::url('/user/account/loginGoogle'); ?>" >Google</a>
        <?php endif; ?>
        <a id="password-recovery" class="btn pull-left" href="<?php echo CHtml::normalizeUrl(Yii::app()->getModule('user')->recoveryUrl) ?>">Password Recovery</a>
    </div>
</div>
<?php $this->endWidget(); ?>
<script>
jQuery(function($){
    <?php if($model->hasErrors()) : ?>
		$("#modal-login-user").effect( "shake", {times:3, distance:25}, 500);
    <?php endif; ?>
});
</script>
