<?php
$form = $this->beginWidget('NActiveForm', array(
	'id' => 'user-password-form',
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
	),
));
?>

<div class="modal hide" id="modal-change-password">
	<div class="modal-header">
		<h3><?php echo UserModule::t("Change Password"); ?></h3>
	</div>
	
	<div class="modal-body">
		<div class="alert alert-block alert-info">
			<p>Enter your new password below.</p>
		</div>
		<fieldset>
			<?php echo $form->field($model, 'password', 'passwordField'); ?>
			<?php echo $form->field($model, 'verifyPassword', 'passwordField'); ?>
		</fieldset>
	</div>
	<div class="modal-footer">
		<?php echo CHtml::submitButton(UserModule::t("Update Password"),array('class'=>'btn btn-primary')); ?>
	</div>
</div>

<?php $this->endWidget(); ?>

<script>
	jQuery(function($){
		$('#modal-change-password').modal({backdrop:'static', show:true});

		// override submit functionality to redirect on success
		$('#user-password-form').on('submit', function() {
			$.post(
				$(this).attr('action'),
				$(this).serialize(),
				function(response) {
					if (typeof(response.success) != undefined)
						window.location.href='/';
				}
			);

			return false;
		})
	});
</script>
	