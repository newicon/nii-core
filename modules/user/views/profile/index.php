<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<div class="line">
	<div class="unit">
		<div style="width:225px;">
			<a onclick="$('#modal-user-account').modal('show');return false;" href="gravatar.com/emails" rel="tooltip" title="Change your avatar at gravatar.com"><img class="thumbnail" src="<?php echo $user->getProfileImageUrl(210); ?>" /></a>
			<h1 class="profile-name" style="font-size:24px;"><?php echo $user->name; ?></h1>
			<br/><span class="badge badge-info"><?php echo WikiStat::statTotalServed($user->id); ?></span> Served<br/>
			<?php echo WikiStat::displayUserStat($user->id); ?>
		</div>
	</div>
	<div class="lastUnit" style="padding-left:20px;">
		<?php $this->widget('activity.widgets.NActivityFeed', array('condition'=>'user_id = '.$user->id, 'canShare'=>Yii::app()->user->id == $user->id)); ?>
	</div>
</div>