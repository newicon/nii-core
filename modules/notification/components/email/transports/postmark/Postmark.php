<?php

/**
 * PostmarkTransport class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
use \Zend\Mail\Transport\TransportInterface;

/**
 * Class for sending email via Postmark service
 */
class Postmark implements TransportInterface
{
	/**
	 * postmark apikey
	 * @var string 
	 */
	public $apiKey;

	/**
     * Constructor function accepts options class to configure
     * @param PostmarkOptions $options Optional
     */
    public function __construct(PostmarkOptions $options = null)
    {
        if ($options instanceof PostmarkOptions) {
           $this->apiKey = $options->getApiKey();
        }
    }
	
	/**
	 * Send the email message
	 * Sends the json encoded email message data via curl to postmark api
	 * @param \Zend\Mail\Message $message
	 * @return boolean
	 * @throws Exception
	 */
	public function send(\Zend\Mail\Message $message)
	{
		$data = $this->_prepareData($message);
		$headers = array(
			'Accept: application/json',
			'Content-Type: application/json',
			'X-Postmark-Server-Token: ' . $this->apiKey
		);
		
		$dataEncoded = json_encode($data);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.postmarkapp.com/email');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $dataEncoded);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
		
		$return = curl_exec($ch);
		$curlError = curl_error($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		if ($curlError !== '')
			throw new Exception($curlError);
		
		if (intval($httpCode) != 200) {
			if ($httpCode == 422) {
				$return = json_decode($return);
				throw new Exception($return->Message, $return->ErrorCode);
			} else {
				throw new Exception("Error while mailing. Postmark returned HTTP code {$httpCode} with message \"{$return}\"", $httpCode);
			}
		}
		
		return true;
	}
	
	/**
	 * Prepare the data stored in the message object into a postmark acceptable array format
	 * @param \Zend\Mail\Message $message
	 * @return array
	 * @throws Exception
	 */
	private function _prepareData(\Zend\Mail\Message $message)
	{
		// Note self::remove is used currently.
		// this can be changed with Yii2 to \yii\helpers\ArrayHelper::remove
		
		$headers = $message->getHeaders()->toArray();
		$data = array();
		// subject *required
		if ($message->getSubject() == null)
			throw new Exception ('Postmark requires a subject field');
		$data['Subject'] = self::remove($headers, 'Subject');
		
		// from address *required
		$from = $message->getFrom();
        if (count($from) !== 1)
            throw new Exception('Postmark requires a registered and confirmed from address');
        $data['From'] = self::remove($headers, 'From');
		
		// To, Cc, Bcc, Reply-To
		if ($message->getHeaders()->get('To') == null)
			throw new Exception('Postmark expects a To address field');
		$data['To'] = self::remove($headers, 'To');
		if ($message->getHeaders()->get('Cc') != null) 
			$data['Cc'] = self::remove($headers, 'Cc');
		if ($message->getHeaders()->get('Bcc') != null)
			$data['Bcc'] = self::remove($headers, 'Bcc');
		if ($message->getHeaders()->get('Reply-To') != false)
			$data['ReplyTo'] = self::remove($headers, 'Reply-To');
		
		// Email body content
		if ($message->getText() != '')
			$data['TextBody'] = $message->getText();
		
		if ($message->getHtml() != '')
			$data['HtmlBody'] = $message->getHtml();
		
		// Headers
		if (count($headers) >= 1) {
			$data['Headers'] = array();
			// Remove date header if it exists as postmark does not allow it
			self::remove($headers, 'Date');
			// Zend adds this header, however postmark constructs the email and so w can not say what 
			// Mime version they are using
			self::remove($headers, 'MIME-Version');
			foreach ($headers as $headerName => $headerValue) 
				$data['Headers'][] = array('name'=>$headerName, 'value'=>$headerValue);
		}
		
		// Attachments
		$body = $message->getBody();
		if ($body instanceof \Zend\Mime\Message) {
			foreach($body->getParts() as $part){
				if ($part->type != "text/plain" && $part->type != "text/html") {
					// assume we have an attachment part
					$data['Attachments'][] = array(
						'Name' => $part->filename,
						'Content' => $part->getContent(),
						'ContentType' => $part->type
					);
				}
			}
		}
		
		return $data;
	}
	
	/**
	 * Note: this function can be superceeded by Yii2's ArrayHelper class remove function
	 * Removes an item from an array and returns the value. If the key does not exist in the array, the default value
	 * will be returned instead.
	 *
	 * Usage examples,
	 *
	 * ~~~
	 * // $array = array('type' => 'A', 'options' => array(1, 2));
	 * // working with array
	 * $type = \yii\helpers\ArrayHelper::remove($array, 'type');
	 * // $array content
	 * // $array = array('options' => array(1, 2));
	 * ~~~
	 *
	 * @param array $array the array to extract value from
	 * @param string $key key name of the array element
	 * @param mixed $default the default value to be returned if the specified key does not exist
	 * @return mixed|null the value of the element if found, default value otherwise
	 */
	public static function remove(&$array, $key, $default = null)
	{
		if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
			$value = $array[$key];
			unset($array[$key]);
			return $value;
		}
		return $default;
	}
}