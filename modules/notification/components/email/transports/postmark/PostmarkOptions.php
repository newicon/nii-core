<?php

/**
 * PostmarkOptions class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Postmark Options class
 * @property apiKey
 */
class PostmarkOptions extends Zend\Stdlib\AbstractOptions
{
	protected $apiKey = '';
	
	public function setApiKey($value)
	{
		$this->apiKey = $value;
	}
	
	public function getApiKey()
	{
		return $this->apiKey;
	}
}