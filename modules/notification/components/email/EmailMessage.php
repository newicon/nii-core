<?php

/**
 * Message class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Default class providing a generic interface for specifying email messages
 */
class EmailMessage extends \Zend\Mail\Message
{
	/**
	 * Send the email message
	 */
	public function send() 
	{
		Yii::app()->email->send($this);
	}
	
	 /**
      * Set the message body
      *
      * @param  null|string|\Zend\Mime\Message|object $body
      * @throws Exception\InvalidArgumentException
      * @return EmailMessage
      */
    public function setBody($body)
    {
		if (is_string($body)) {
			$this->setText($body);
		} else if ($body instanceof \Zend\Mime\Message) {
			parent::setBody($body);
		} else {
			throw new Exception('Unsported type passed for the message body.  Must be either a string or a Zend\Mime\Message object');
		}
		return $this;
	}
	
	/**
	 * Get the Mime email body object
	 * @return \Zend\Mime\Message
	 */
	public function getBody()
	{
		$body = parent::getBody();
		if ($body === null) 
			$this->setBody(new Zend\Mime\Message);
		return parent::getBody();
	}
	
	/**
	 * Get the text portion of the email
	 * @return string returns either empty string '' or the string of the text body
	 */
	public function getText()
	{
		$text = '';
		foreach($this->getBody()->getParts() as $part) {
			if ($part->type == 'text/plain') {
				$text = $part->getContent();
				break;
			}
		}
		return $text;
	}
	
	/**
	 * Get the Html portion of the email
	 * @return string empty string '' or html string body
	 */
	public function getHtml()
	{
		$html = '';
		foreach($this->getBody()->getParts() as $part) {
			if ($part->type == 'text/html') {
				$html = $part->getContent();
				break;
			}
		}
		return $html;
	}
	
	/**
	 * Set the HTML portion of the email message
	 * @param string $html
	 * @return \EmailMessage
	 * @throws Exception
	 */
	public function setHtml($html)
	{
		return $this->setMessage($html, 'text/html');
	}
	
	/**
	 * Set the plain text portion of the email message
	 * @param string $text
	 * @return \EmailMessage
	 */
	public function setText($text)
	{
		return $this->setMessage($text, 'text/plain');
	}
	
	/**
	 * Set a message portion of the email
	 * @param string $content
	 * @param string $type mime type
	 * @return \EmailMessage
	 * @throws Exception
	 */
	public function setMessage($content, $type='text/plain')
	{
		$body = $this->getBody();
		if (!$body instanceof Zend\Mime\Message)
			throw new Exception('The message body needs to be an instance of Zend\Mime\Message');
		$part = new Zend\Mime\Part($content);
		$part->type = $type;
		$body->addPart($part);
		return $this;
	}
	
	/**
	 * Add a header to the email
	 * For a more complete interface use $this->getHeaders()
	 * @param string $name
	 * @param string $value
	 * @return \EmailMessage
	 */
	public function addHeader($name, $value)
	{
		$this->getHeaders()->addHeaderLine($name, $value);
		return $this;
	}
	
}