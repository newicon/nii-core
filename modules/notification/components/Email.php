<?php

/**
 * Email class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */


// Include the composer autoloader.  We should be doing this somewhere more central to the core
// like in the Environment class.
require_once Yii::getPathOfAlias('base').DS.'vendor'.DS.'autoload.php';

require __DIR__ . '/email/EmailMessage.php';
require __DIR__ . '/email/transports/postmark/Postmark.php';
require __DIR__ . '/email/transports/postmark/PostmarkOptions.php';

/**
 * Email component class
 * Allows you to specify default options for sending emails via the Yii::app()->email->compose() command
 *
 * A typical configuration might be:
 * ~~~
 *  // inside Yii components
 *  'components' => array(
 *		'email'=>array(
 *			'class'=>'nii.modules.notification.components.Email',
 *			'bccAllEmailsTo'=>'steve@newicon.net',
 *			'fromEmail'=>'theteam@newicon.net',
 *			'fromName'=>'Newicon',
 *			'defaultTransport'=>'postmark', // (sendmail, postmark, smtp)
 *			'transports'=>array(
 *				'postmark'=>array(
 *					'options'=>array(
 *						'apiKey'=>'15fa02db-fa13-47cc-853b-e4489f4fb18b'
 *					)
 *				),
 *				// example gmail smtp settings
 *				'smtp'=>array(
 *					'name' => 'yourdomain.com',
 *					'host' => 'smtp.gmail.com',
 *					'connection_class' => 'login',
 *					'port' => '465',
 *					'connection_config' => array(
 *						'ssl' => 'ssl', // Necessary for gmail
 *						'username' => 'user@yourdomain.com',
 *						'password' => 'password',
 *					),
 *				)
 *			)
 *		),
 *  )
 * 
 * 
 */
class Email extends CApplicationComponent
{
	/**
	 * provide an email address to bcc all emails to
	 * @var mixed string | \Zend\Mail\Address | \Zend\Mail\AddressList
	 */
	public $bccAllEmailsTo;
	/**
	 * Default email address to send emails from
	 * @var string
	 */
	public $fromEmail = 'theteam@newicon.net';
	/**
	 * Default email from name
	 * @var string 
	 */
	public $fromName = 'Newicon';
	
	/**
	 * The name of the email transport method to use by default to send emails
	 * Built in transports:
	 * - sendmail
	 * - postmark
	 * - smtp - requires configuration via \Zend\Mail\Transport\smtpOptions
	 * - file - config via \Zend\Mail\Transport\FileOptions
	 * @var string 
	 */
	public $defaultTransport = 'sendmail';
	
	/**
	 * private array store of transports
	 * @var array => transport conifg | object
	 */
	private $_transports = array(
		'postmark'=>array(
			'class' => 'Postmark',
		),
		'sendmail'=>array(
			'class' => '\Zend\Mail\Transport\Sendmail',
		),
		'smtp'=>array(
			'class' => '\Zend\Mail\Transport\Smtp',
		),
		'file'=>array(
			'class' => '\Zend\Mail\Transport\File',
		)
	);
	
	/**
	 * Compose a new email returns the EmailMessage object 
	 * as the default email message interface
	 * @return \EmailMessage 
	 */
	public function compose() 
	{
		return new EmailMessage();
	}
	
	/**
	 * Send the email message
	 * @param EmailMessage $message
	 * @param string $transport the string key of a named transport class
	 * if null then the transport defined in self::defaultTransport will be used
	 */
	public function send(EmailMessage $message, $transportName=null) 
	{
		if (!empty(Yii::app()->email->bccAllEmailsTo) && Yii::app()->email->bccAllEmailsTo != false)
			$message->addBcc(Yii::app()->email->bccAllEmailsTo);
		
		if (count($message->getFrom()) == 0) {
			$message->setFrom($this->fromEmail, $this->fromName);
		}
		
		if ($transportName===null)
			$transportName = $this->defaultTransport;
		
		$transport = $this->getTransport($transportName);
				
		$transport->send($message);
	}
	
	/**
	 * Get the transport object responsible for sending the email message
	 * This function will instanciate the transport object defined in the class property of its configuration array.
	 * If an options key is defined then it will find and populate $transportName.'Options' class with those options
	 * and then pass this class to the constructor function of the transport object.
	 * This is the standard Zend uses to configure its transport methods.
	 * @param string $transportName
	 * @return Object implementing TransportInterface
	 * @throws CException
	 */
	public function getTransport($transportName)
	{
		if (!array_key_exists($transportName, $this->_transports))
			throw new CException('No transport object or configuration exists for this transport name "'.$transportName.'"');
		// we don't want to instanciate transports multiple times
		if (is_array($this->_transports[$transportName])) {
			// if the transport is an array then it is configured but not yet instanciated
			$transportConfig = $this->transports[$transportName];
			if (!array_key_exists('class', $transportConfig))
				throw new CException('A transport configuration must define a class property');
			$transportClass = $transportConfig['class'];
			unset($transportConfig['class']);

			$options=null;
			// get transport options
			if (array_key_exists('options', $transportConfig)) {
				// Zend uses an Abstract options class to define options for transport objects
				// As a rule they are named according to a convention $className . 'Options'
				$optionsClass = $transportClass.'Options';
				$options = new $optionsClass($transportConfig['options']);
			}
			$t = new $transportClass($options);
			$this->_transports[$transportName] = $t;
		}
		return $this->_transports[$transportName];
	}
	
	/**
	 * Set an individual transport
	 * @param type $name
	 * @param mixed $transport either an object implementing TransportInterface 
	 * or an array consisting of a class key (the class of the transport) and an options key.
	 * Note: if the options key is specified you will have to have a class defined as transportClassName.'Options' in the
	 * same location
	 * @param boolean $merge whether to merge the transport config array with the existing one
	 * This will only be done if both the passed in transport and the existing one are arrays.  Otherwise the current
	 * transport for this "name" (if any) will be overridden
	 */
	public function setTransport($name, $transport, $merge=true)
	{
		if(isset($this->_transports[$name]) && $merge && is_array($this->_transports[$name]) && is_array($transport))
			$this->_transports[$name]=CMap::mergeArray($this->_transports[$name],$transport);
		else
			$this->_transports[$name]=$transport;
	}
	
	/**
	 * Set the transports array
	 * @param array $transports
	 * @param boolean $merge whether to merge the array configuration
	 */
	public function setTransports($transports, $merge=true)
	{
		foreach($transports as $name=>$transport)
			$this->setTransport($name,$transport, $merge);
	}
	
	/**
	 * @return array
	 * @see self::$_transports8
	 */
	public function getTransports()
	{
		return $this->_transports;
	}
}