<?php

/**
 * AcitivtyModule class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

Yii::import('nii.modules.notification.components.*');
/**
 * Module to manage notifications and communications methods
 * Email,
 * SMS
 * etc
 */
class NotificationModule extends NWebModule
{
	public function init(){}
	
	public function install(){}
}