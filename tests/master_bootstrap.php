<?php

// Create a 'TEST' environment
require_once (dirname(__FILE__).'/../config/Environment.php');
$env = new Environment('TEST');

// Require required files
require_once($env->yiitPath);
require_once (dirname(__FILE__).'/../modules/nii/Nii.php');
