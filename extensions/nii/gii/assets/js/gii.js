$(document).ready(function(){

  /*
   * Submit handler for 'Preview' button
   * When the preview button is clicked, we need to intercept the form that is being
   * posted and include all the resource data
   */
  var completeForm = function(){

    // Get all the resource data
    var resource_data = getAllResourceData();

    for( var i = 0; i < resource_data.length; i++ )
    {
      resourceToInputs( i, resource_data[i] );
    }
  };

  $('#preview_submit').on('click',completeForm);
  $('#generate_submit').on('click',completeForm);


  var resourceToInputs = function(index,resource)
  {
    var form = $('form');
    var nameBase = 'NiiModuleCode[resourceData]['+index+']';
    var name = $('<input>').attr('type','hidden').attr('name',nameBase + '[name]').attr('value',resource.name).appendTo(form); 
    var cname= $('<input>').attr('type','hidden').attr('name',nameBase + '[className]').attr('value',resource.className).appendTo(form); 
    var tname= $('<input>').attr('type','hidden').attr('name',nameBase + '[tableName]').attr('value',resource.tableName).appendTo(form); 

    for(var attrIdx = 0 ; attrIdx < resource.attributes.length; attrIdx++)
    {
      $('<input>').attr('type','hidden').attr('name',nameBase + '[attributes]['+attrIdx+'][name]').attr('value',resource.attributes[attrIdx].name).appendTo(form);
      $('<input>').attr('type','hidden').attr('name',nameBase + '[attributes]['+attrIdx+'][type]').attr('value',resource.attributes[attrIdx].type).appendTo(form);
      $('<input>').attr('type','hidden').attr('name',nameBase + '[attributes]['+attrIdx+'][ctype]').attr('value',resource.attributes[attrIdx].ctype).appendTo(form);
      $('<input>').attr('type','hidden').attr('name',nameBase + '[attributes]['+attrIdx+'][pk]').attr('value',resource.attributes[attrIdx].pk).appendTo(form);
      $('<input>').attr('type','hidden').attr('name',nameBase + '[attributes]['+attrIdx+'][label]').attr('value',resource.attributes[attrIdx].label).appendTo(form);
    }
  }

  $('.preview-code').on('click',function(){
      var title = $(this).attr('rel');
      completeForm();
      var preview_data =$('form').serializeArray();
      preview_data['template'] = 'default';

      $.ajax({
        type: 'POST',
	cache: false,
	url: $(this).attr('href'), 
	data: preview_data, 
	success: function(data)
	{
	  $('.code-preview-modal .modal-body').html(data); 
	  $('.code-preview-modal h3').text(title);
	  $('.code-preview-modal').modal();
	},
	error: function(XMLHttpRequest,textStatus,errorThrown){
	  $('.code-preview-modal .modal-body').html(XMLHttpRequest.responseText);
	  $('.code-preview-modal h3').text('Error Previewing Code');
	  $('.code-preview-modal').modal();
	}
      });
      return false;
  });

  // Function that adds a blank attribute to the resource modal attribute table
  $('#new_resource_attribute_btn').on('click',function()
  {
    $.get('niiModule/resourceattribute',function(data){
      $('table.resource-attribute-table tr:last').before(data);
    });
    return false;
  });

  $('#add_resource_submit').on('click',function(){
    addResource();
    clearResourceModal();
    return false;
  });

  $('#add_resource_cancel').on('click',function(){
    clearResourceModal();
    return false;
  });

  // Function for deleting a resource from the table
  $('#resource_table').on('click','button.delete-resource-btn',function(){
    var id = $(this).data('name');
    $('tr.resource-table-row[data-name='+id+']').remove();
    $('#resource_data div[data-name='+id+']').remove();
    return false;
  });

  // Function for editing a resource from the table
  $('#resource_table').on('click','button.edit-resource-btn', function(event){

    var resource_data = getResourceDataByName(this.id);
    
    $('#resource_name').val( resource_data['name'] );
    $('#resource_table_name').val( resource_data['tableName'] );
    $('#resource_class_name').val( resource_data['className'] );

    // Clear out existing attributes and fill in our new ones
    clearResourceModalAttributes();
    for( var i = resource_data.attributes.length - 1; i >= 0; i-- )
    {
      $.get('niiModule/resourceattribute',resource_data.attributes[i], function(data){
        $('table.resource-attribute-table tr:last').before(data);
      });
    }

    $('#modal_add_resource').modal('show');  
    return false;
  });

  var getAllResourceData = function()
  {
    var data = new Array();
    $('#resource_data div.nii-resource').each(function(index,element){
      data.push( resourceDataToObj( element ) );
    });
    return data;
  }

  var getResourceDataByName = function(resource_name)
  {
    var resource = $('#resource_data div.nii-resource[data-name='+resource_name+']');
    if (!resource)
    {
      return null;
    }
    return resourceDataToObj( resource );
  }

  var resourceDataToObj = function(element)
  {
    var data = $(element).data();
    data['attributes'] = new Array();
    $(element).find('div.nii-resource-attr').each( function(index,attr){
      data['attributes'][index] = $(attr).data();
    });
    return data;
  }

  var deleteResourceData = function(resource_id)
  {
    return $('#resource_data div.nii-resource[id='+resource_id+']').remove();
  }

  var deleteResourceAttributeData = function(resource_id, attribute_id)
  {
    return $('#resource_data div.nii-resource[data-resource-id='+resource_id+'] div.nii-resource-attribute[data-attribute-id='+attribute_id+']').remove();
  }

  var clearResourceModal = function()
  {
    $('#resource_name').val('');
    $('#resource_table_name').val('');
    $('#resource_class_name').val('');

    // Reset the attribute table - delete everything that is existing and load fresh one
    clearResourceModalAttributes(); 
    $.get('niiModule/resourceattribute', { name: 'id', pk: 'true', type: 'integer' }, function(data){
      $('table.resource-attribute-table tbody').prepend(data);
    }); 
    $('#modal_add_resource').modal('hide');
  }

  var clearResourceModalAttributes = function()
  {
    $('table.resource-attribute-table tr.attribute').remove();
  }

  var addResource = function()
  {
    var serverData		= {};
    serverData['name']		= $('#resource_name').val();
    serverData['table_name']	= $('#resource_table_name').val();
    serverData['class_name']	= $('#resource_class_name').val();


    // Count the current number of resources
    var resourceCount = $('#resource_data').children('input.nii-resource').size();
    var resourceId    = 'NiiModuleCode[resourceData]['+resourceCount+']';

    serverData['id']		= resourceCount;
    serverData['attributes']	= {};

    // Populate with all the attribute data
    $('table.resource-attribute-table tr.attribute').each( function(index,element){
      serverData['attributes'][index] = {};
      serverData['attributes'][index]['name'] 	= $('td .attribute-name',element).val();
      serverData['attributes'][index]['label'] 	= $('td .attribute-label',element).val();
      serverData['attributes'][index]['pk'] 	= $('td .attribute-pk',element).is(':checked');
      // For the type, it might be a custom type
      var type = $('td .attribute-type',element);
      if( type.data('attrCustom') == true )
      {
        serverData['attributes'][index]['type'] 	= 'custom';
	serverData['attributes'][index]['ctype']	= type.val();
      }
      else
      {
        serverData['attributes'][index]['type'] 	= $('td .attribute-type',element).val();
      }
    });


    // Query the server to get the HTML to append to the resource table
    $.get('niiModule/resourcerow', serverData,  function(data){
      var e = $('#resource_table tbody tr.resource-table-row[data-name='+serverData['name']+']')[0];
      if( e == undefined ) 
      {
        $('#resource_table tbody').append(data);
      }
      else
      {
        $(e).replaceWith(data);
      }
    });

    // Query the server to get the data to enter to the (hidden) resource data div 
    $.get('niiModule/resourcedata', serverData, function(data){
      var e = $('#resource_data div.nii-resource[data-name='+serverData['name']+']')[0];
      if( e == undefined )
      {
        $('#resource_data').append(data); 
      }
      else
      {
        $(e).replaceWith(data);
      }
    }); 
  };

  $('table.resource-attribute-table').on('click','button.delete-attribute-btn',function(data){
    var resourceId = $(this).data('resourceId');
    $('table.resource-attribute-table tr.attribute[data-resource-id=' + resourceId + ']').remove();
    deleteResourceAttributeData( resourceId ); 
    return false;
  });

  $('table.resource-attribute-table').on('change','select.attribute-type', function(data){
    if( $(this).find('option:selected').val() == 'custom' )
    {
      $(this).replaceWith('<input type="text" data-attr-custom="true" class="attribute-type"/>');
    }
  });

  $('#modal_add_resource').on('show',function(data){
    var module_name = $('#NiiModuleCode_moduleID').val();
    if( module_name.length > 0 )
    {
      $('#resource_table_name').data('base', module_name + "_");   
      $('#resource_class_name').data('base', ucfirst(module_name) );
    }
  });

  $('#resource_name').on( 'change', function(e){
    $('#resource_class_name').val( $('#resource_class_name').data('base') + ucfirst($(this).val()) ); 
    $('#resource_table_name').val( $('#resource_table_name').data('base') + lcfirst($(this).val()) ); 
  });

  var ucfirst = function(string)
  {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  var lcfirst = function(string)
  {
    return string.charAt(0).toLowerCase() + string.slice(1);
  }
});
