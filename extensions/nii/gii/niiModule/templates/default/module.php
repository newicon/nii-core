<?php echo "<?php\n"; ?>
/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link https://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
class <?php echo $this->moduleClass; ?> extends NWebModule
{
	
	public $name = '<?php echo $this->moduleID; ?>';
	public $description = '<?php echo $this->description; ?>';
	public $version = '0.0.1';
	
	public function init(){
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'<?php echo $this->moduleID; ?>.models.*',
			'<?php echo $this->moduleID; ?>.components.*',
		));
	}
	
	public function setup(){
		Yii::app()->menus->addItem('main', '<?php echo ucfirst($this->moduleID); ?>', '#', null, array('linkOptions'=>array('icon'=>'icon-th-list'),));
		<?php foreach ( $this->resources as $r ): ?>
		  Yii::app()->menus->addItem('main', '<?php echo $r->className; ?>',array('<?php echo $r->path('index');?>'),'<?php echo ucfirst($this->moduleID);?>');
		<?php endforeach; ?>
	}
	
	public function install(){
	<?php
	  foreach ( $this->resources as $r )
	  {
            echo "NActiveRecord::install('".$r->className."');\n";
	  }
          ?>
	}
	
	public function uninstall(){
	  <?php
	  foreach ( $this->resources as $r )
	  {
	    echo "NActiveRecord::uninstall('".$r->className."');\n";
	  }
	  ?>
		
	}
	
	public function settings(){

	}
	
	public function permissions() {

	}
}
