<?php echo '<?php'; ?>

/**
 * This is the model class for table <?php $this->tableName ?>
 *
 * The following are the available columns in table <?php echo $this->tableName ?>

 <?php 
   foreach( $this->attributes as $attribute )
   {
     echo "@attribute ".$attribute->type. " $".$attribute->name."\n ";
   }
 ?>
 */
class <?php echo $this->className ?> extends NActiveRecord 
{

  /**
   * Returns the static model of the specified AR class.
   * @return <?php echo $this->className ; ?> the static model clas
   */
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return '{{<?php echo $this->tableName; ?>}}';
  }

  /**
   * @return array validation rules for model attributes 
   */
  public function rules()
  {
    return array(
<?php foreach( $this->attributes as $attribute)
{
  $id = $attribute->name;
  if ( $attribute->ref )
    $id = $attribute->refId;
  echo "      array('$id','safe'),\n";
}?>
    );
  }
     
  /*
   * @return array relational rules
   */
  public function relations()
  {
    return array(
<?php foreach ( $this->attributes as $attribute ):?>
<?php if( $attribute->ref ):?>
'<?php echo $attribute->name; ?>' => array(self::BELONGS_TO, '<?php echo $attribute->ref['model'];?>','<?php echo $attribute->refId;?>','condition'=>''),
<?php endif;?>
<?php endforeach;?>
    );
  }

  /**
   * @return array customized attribute labels (name=>$label)
   */
  public function attributeLabels()
  {
    return array(
<?php foreach ( $this->attributes as $attribute):?>
  <?php echo "      '" . $attribute->name . "' => '" . $attribute->label . "',\n"; ?>
<?php endforeach;?>
    );
  }

  /**
   * Retrives a list of modesl based on the current search/filter conditions.
   * @return NActiveDataProvider the data provider that can return the models base on the search/filter conditions
   */
  public function search()
  {
    $criteria = $this->getDbCriteria();

<?php
foreach ( $this->attributes as $attribute )
{
  echo '    $criteria->compare(\''.$attribute->name.'\',$this->'.$attribute->name.',true);' . "\n";
}
?>

    return new NActiveDataProvider($this,array(
                                          'criteria' => $criteria,
                                          'pagination' => array(
                                            'pageSize' => 20,
                                          )
                                         )
                                  );
  }

  public function scopes()
  {
    return array();
  }

  public function columns()
  {
    return array(
<?php foreach( $this->attributes as $attribute): ?>
<?php if( !$attribute->isPrimaryKey() ): ?>
      array(<?php echo "'name' => '".$attribute->name."', 'type' => 'raw', 'value' => '\$data->display" . ucfirst($attribute->name) ."()'";?>),
<?php endif; ?>
<?php endforeach; ?>
  );
  }

<?php foreach( $this->attributes as $attribute): ?>
<?php if( !$attribute->isPrimaryKey() ): ?>
  public function display<?php echo ucfirst($attribute->name);?>()
  {
<?php if( $attribute->ref ) : ?>
    $<?php echo $attribute->name; ?> = $this-><?php echo $attribute->name; ?>; 
    return $<?php echo $attribute->name; ?> ? (string)$<?php echo $attribute->name;?> : '';
<?php else: ?>
    return $this-><?php echo $attribute->name; ?>;
    return CHtml::link($this-><?php echo $attribute->name;?>,array('<?php echo $resource->path('view');?>','id' => $this->id));
<?php endif; ?>
  }
<?php endif; ?>
<?php endforeach; ?>

  public function schema()
  {
    return array(
      'columns' => array(
<?php foreach( $this->attributes as $attribute)
{
  if( $attribute->ref )
  {
    echo "                        '".$attribute->refId."'=>'integer',\n";
  }
  else
  {
    echo "                        '".$attribute->name. "'=>'". ($attribute->ctype ? $attribute->ctype : $attribute->type) ."',\n";
  }
}?>
                        'created_date' => 'datetime',
                        'updated_date' => 'datetime',
                        'trashed' => "int(1) unsigned NOT NULL",
      ),
      'keys' => array(),
    );
  }

  function behaviours()
  {

  }

  function __toString()
  {
    return $this-><?php echo $this->displayAttribute()->name; ?>;
  }
}
