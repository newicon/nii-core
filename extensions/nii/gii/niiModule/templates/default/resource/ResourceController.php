<?php echo '<?php'; ?>

class <?php echo $this->controllerName; ?> extends AController
{

  public function actionIndex()
  {
    $modelName = '<?php echo $this->className; ?>';
    $model = new $modelName('search');
    $model->unsetAttributes();

    $this->title = '<?php echo $this->pluralize( $this->className) ; ?>';
    $this->pageTitle = Yii::app()->name . ' All <?php echo $this->pluralize( $this->className); ?>';
    $this->breadcrumbs = array(
      'All <?php echo $this->pluralize( $this->className); ?>' => array('<?php echo $this->path('index')?>'),
    );

    $this->actionsMenu = array(
      array(
        'icon' 	=> 'icon-plus',
	'label' => 'Add a <?php echo $this->className;?>',
	'url'   => '<?php echo $this->path('create'); ?>'
      ),
    );

    if(isset($_GET[$modelName]))
      $model->attributes = $_GET[$modelName];

    $this->render('index',array(
            'dataProvider' => $model->search(),
	    'model'        => $model,
    ));
  }

  public function actionView($id)
  {
    $modelName = '<?php echo $this->className; ?>';
    $model = NActiveRecord::model($modelName)->findByPk($id);

    $this->breadcrumbs = array(
      'All <?php echo $this->pluralize( $this->className ); ?>' => array('<?php echo $this->path('index')?>'),
      (string)$model => array('<?php echo $this->path('view'); ?>', 'id' => $model->id)
    );

    $this->title = (string)$model;
    $this->pageTitle = (string)$model . ' - ' . Yii::app()->name;

    $this->actionsMenu = array(
      array(
        'icon'  => 'icon-pencil',
	'label' => 'Edit',
	'url'   => array('<?php echo $this->path('edit');?>', 'id' => $model->id)
      ),
    );


    $this->render('view',array(
                   'model' => $model,
    ));
  }

  public function actionCreate()
  {
    $modelName = '<?php echo $this->className; ?>';
    $model = new $modelName; 


    $this->breadcrumbs = array(
      'All <?php echo $this->pluralize( $this->className ); ?>' => array('<?php echo $this->path('index')?>'),
      'Create a <?php echo $this->className; ?>' => array('<?php echo $this->path('create'); ?>')
    );

    if (isset($_POST[$modelName]))
    {
      $model->attributes=$_POST[$modelName];
      if($model->save())
      {
        $this->redirect(array('view','id'=>$model->id));
      }
    }

    $viewData = array(
      'model'  => $model,
      'action' => 'create'
    );

    $this->render('create',$viewData);
  }

  public function actionEdit($id)
  {
    $modelName = '<?php echo $this->className; ?>';
    $model = <?php echo $this->className; ?>::model()->findByPk($id);

    $this->title = (string)$model;
    $this->pageTitle = 'Edit ' . (string)$model . ' - ' . Yii::app()->name;

    $this->breadcrumbs = array(
      'All <?php echo $this->pluralize( $this->className ); ?>' => array('<?php echo $this->path('index')?>'),
      (string)$model => array('<?php echo $this->path('view'); ?>', 'id' => $model->id),
      'Edit <?php echo $this->className; ?>' => array('<?php echo $this->path('edit'); ?>', 'id' => $this->id)
    );

    if (isset($_POST[$modelName]))
    {
      $model->attributes = $_POST[$modelName];
      if ($model->save())
      {
        $this->redirect(array('view','id'=>$model->id));
      }
    }

    $viewData = array(
      'model'  => $model,
      'action' => 'edit'
    );
    $this->render('edit',$viewData);
  }

}
