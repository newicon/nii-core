<div class='widget-box'>
  <div class='widget-title'>
    <h5>New <?php echo ucfirst($resource->name); ?></h5> 
  </div>

  <div class='widget-content nopadding'>  
<?php echo "<?php\n"; ?>

$action = $model->isNewRecord ? NHtml::url('<?php echo $resource->path('create');?>') : NHtml::url(array('<?php echo $resource->path('edit');?>', 'id' => $model->id, 'clientOptions' => array( 'validateOnSubmit' => true ) ) );

$form = $this->beginWidget('nii.widgets.NActiveForm',array(
                            'id' 	=> '<?php echo $resource->name; ?>',
			    'enableAjaxValidation' => false, 
			    'action' 	=> $action));
?>

<?php echo '<?php echo $form->errorSummary($model); ?>'; ?>

<?php foreach ($resource->attributes as $id=>$attribute) :?>
<?php if( $attribute->name == 'id' ) continue ;?> 
<?php 
      $enum = '';
      if( $attribute->ref )
      {
        echo "  <div class='control-group'>\n";
        echo '    <?php echo $form->labelEx($model, \'' . $attribute->name . "'); ?>\n";
	echo "    <div class='controls'>\n";
	echo '      <?php ' . $attribute->ref['edit']($attribute->refId) . '?>';
	echo "    </div>\n";
	echo "  </div>\n";
      }
      else
      {
        if( $attribute->type === 'datetime' )
	{
          echo "  <div class='control-group'>\n";
	  echo '    <?php echo $form->labelEx($model,\''.$attribute->name."';?>\n";
	  echo "    <div class='controls'>\n";
	  echo '      <?php echo $form->dateField($model,\''.$attribute->name.'\'); ?>'. "\n";
	  echo "    </div>\n";
	  echo "  </div>\n";
	}
        else if( $attribute->type === 'custom' && $attribute->ctype && (strncmp('enum',$attribute->ctype,4)==0) ) 
	{
          $enum = ",'dropDownList',NHtml::enumItem(\$model,'$attribute->name')";
          echo '  <?php echo $form->'.$attributeMethod.'($model,\'' . $attribute->name .'\''.$enum.'); ?> ' . "\n";
	}
	else
	{
          echo '  <?php echo $form->field($model,\'' . $attribute->name .'\''.$enum.'); ?> ' . "\n";
	}
      }
      ?>
<?php endforeach; ?>

	  <div class="form-actions">
            <?php echo '<?php echo CHtml::submitButton($model->isNewRecord ? "Create" : "Save", array("class"=>"btn btn-primary")); ?>';?>
	    <a class='btn' href='<?php echo $resource->path('index'); ?>'>Cancel</a>
	    <?php echo "<?php echo \$model->id ? NHtml::trashButton(\$model, '".$resource->className."', '".$resource->path('index')."', 'Successfully deleted ' . (string)\$model) : ''; ?>";?>
	  </div>
<?php echo '<?php $this->endWidget(); ?>'; ?>
  </div>
</div>

