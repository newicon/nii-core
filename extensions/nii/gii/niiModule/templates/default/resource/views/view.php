<div class='widget-box'>
  <div class='widget-title'><h5><?php echo $resource->className; ?> Details</h5></div>
  <div class='widget-content nopadding form-horizontal form-view'>
    
  <?php foreach ( $resource->attributes as $attribute ) : ?>
    <?php if( $attribute->isPrimaryKey() ) continue; ?>
    <div class='control-group'>
      <label class='control-label'><?php echo $attribute->label; ?></label>
      <div class='controls'><?php echo "<?php echo \$model->".$attribute->name."; ?>"; ?></div>
    </div>
  <?php endforeach; ?>

  </div>
</div>
