<?php echo "<?php\n";?>
$this->widget('nii.widgets.grid.NGridView', array(
        'dataProvider' => $dataProvider,
        'filter' => $model,
        'id' => '<?php echo $this->className;?>Grid', 
        'enableButtons'=>true,
        'enableCustomScopes'=>false,
        'scopes'=>array('enableCustomScopes'=>false),
)); ?>

