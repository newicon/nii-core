<?php

class NiiModuleGenerator extends CCodeGenerator
{
	public $codeModel = 'ext.nii.gii.niiModule.NiiModuleCode';

	public $layout = '//layouts/admin1column';
	
	public $title = 'Create a Module';
	
	public $actionsMenu = array();
	public $breadcrumbs = array(
		'Modules' => array('/admin/modules'),
		'Create a Module' => array('/gii/niiModule'),
	);

	private $_assetsBase;

        public $availableReferences;

        public function getAssetsBase()
        {
          if ($this->_assetsBase === null) 
	  {
            $this->_assetsBase = Yii::app()->assetManager->publish(
                                Yii::getPathOfAlias('ext.nii.gii.assets'),
				false,
				-1,
				true);
                        
          }
          return $this->_assetsBase;
        }

	public function init()
	{
	  $this->getAssetsBase();
          Yii::app()->clientScript->registerScriptFile($this->assetsBase.'/js/gii.js');
	  Yii::import('ext.nii.gii.niiModule.NiiModuleResource');
          $this->gatherAvailableReferences();
	}

	/*
	 * Function to gather the available references from all other installed modules
	 */
	public function gatherAvailableReferences()
	{
	  $this->availableReferences = array();
          foreach( Yii::app()->getModules() as $moduleId=>$module )
          {
            $module = Yii::app()->getModule($moduleId);
            if( method_exists( $module, 'references' ) )
            {
              $this->availableReferences[$moduleId] = $module->references();
            }
          }
	}

        /*
	 * These functions are for rendering parts of the form so jQuery AJAX calls 
	 * can insert them to the correct place
	 */
	public function actionResourceattribute()
	{
	  $name  	= isset($_GET['name'])  ? $_GET['name']  : NULL;
	  $type  	= isset($_GET['type'])  ? $_GET['type']  : NULL;
	  $ctype 	= isset($_GET['ctype'])  ? $_GET['ctype']  : NULL;
	  $label 	= isset($_GET['label']) ? $_GET['label']  : NULL;
	  $pk    	= isset($_GET['pk'])    ? $_GET['pk']  : false;
	  $refModule	= isset($_GET['refModule']) ? $_GET['refModule'] : NULL;
	  $refName	= isset($_GET['refName']) ? $_GET['refName'] : NULL;

	  if( isset($_GET['refModule']) && isset($_GET['refName']) )
	  {
            $type = Yii::app()->controller->availableReferences[$refModule][$refName];
	  }

          $this->renderPartial('_resource_attribute_row', array('name' 		=> $name, 
	                                                        'type' 		=> $type, 
								'ctype' 	=> $ctype, 
								'label' 	=> $label, 
								'pk' 		=> $pk) );
	}

	public function actionResourcerow()
	{
	  $rowParams			= array();
	  $rowParams['name']  		= $_GET['name'];
	  $rowParams['className'] 	= $_GET['class_name'];
	  $rowParams['tableName'] 	= $_GET['table_name'];

	  $this->renderPartial('_resource_table_row', $rowParams);
	}

	public function actionResourcedata()
	{
	  $id           = $_GET['id'];
	  $name 	= $_GET['name'];
	  $className	= $_GET['class_name'];
	  $tableName	= $_GET['table_name'];
	  $attributes   = isset($_GET['attributes']) ? $_GET['attributes'] : array();
	  $resource = new NiiModuleResource( $name, $tableName, $className,$attributes, null, null, null);
	  $this->renderPartial('_resource_data',array('resource' => $resource,'id' => $id)); 
	}
}
