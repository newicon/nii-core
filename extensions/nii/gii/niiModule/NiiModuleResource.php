<?php

class NiiModuleResource extends CCodeModel
{
  
  /* Template variables */
  private $templatePath;
  private $modulePath;
  private $templateViewsPath;
  public  $files; 
  public  $module;

  /* Active Record Properties */
  public $name;
  public $tableName;
  public $className;
  public $attributes;
  public $controllerName;


  function __construct( $name, $tableName, $className, $attributes, $module, $modulePath, $templatePath )
  {
    $this->module               = $module;
    $this->templatePath 	= $templatePath; 
    $this->modulePath           = $modulePath;
    $this->templateViewsPath  	= $templatePath . DIRECTORY_SEPARATOR . 'views';
    $this->name                 = lcfirst($name);
    $this->className            = $className; 
    $this->controllerName       = ucfirst($name) . 'Controller'; 
    $this->tableName            = $tableName;
    $this->attributes		= array();

    foreach( $attributes as $id=>$p)
    {
      $ctype = isset($p['ctype']) ? $p['ctype'] : null;
      $this->attributes[] = new NiiResourceAttribute($p['name'],$p['type'],$p['pk'],$p['label'],$ctype);
    }
    $this->files                = array();
  }

  public function prepare()
  {
    $this->generateFiles();
  }

  // Generates all the required files
  public function generateFiles()
  {
    $this->generateModelFile();
    $this->generateViewFiles();
    $this->generateControllerFile();
  }

  // Generates the model file for the resource:
  private function generateModelFile()
  {
    $modelTemplateFile = $this->templatePath.DIRECTORY_SEPARATOR.'resource.php';
    $content           = $this->renderFile( $modelTemplateFile );
    $this->files[] = new CCodeFile(
        $this->modulePath.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.$this->className.'.php',
        $content ); 
  }

  // Generates the view files for the resource
  private function generateViewFiles()
  {
    $viewFiles=CFileHelper::findFiles($this->templateViewsPath);
    //$destinationPath= $this->modulePath.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.$this->name;
    $destinationPath= $this->modulePath.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.strtolower($this->name);
    foreach($viewFiles as $viewFile)
    {
      $content = $this->renderFile($viewFile);
      $this->files[] = new CCodeFile(
        $destinationPath.DIRECTORY_SEPARATOR.basename($viewFile), 
        $content); 
    }
  }

  // Generates the controller file for the resource
  private function generateControllerFile()
  {
    $controllerFile = $this->templatePath.DIRECTORY_SEPARATOR.'ResourceController.php';
    $content        = $this->renderFile( $controllerFile );

    $this->files[] = new CCodeFile(
      $this->modulePath.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$this->controllerName.'.php',
      $content ); 
  }

  function path($method)
  {
    return sprintf('/%s/%s/%s', $this->module->moduleID, strtolower($this->name),$method);
  }

  private function renderFile( $templateFile )
  {
    return $this->render( $templateFile, array( 'resource' => $this ) ); 
  }

  public function displayAttribute()
  {
    foreach( $this->attributes as $attribute )
    {
      if( !$attribute->isPrimaryKey() )
        return $attribute;
    }
  }

}

class NiiResourceAttribute
{
  public $type;
  public $ctype;
  public $name;
  public $pk;
  public $label;
  public $ref;
  public $refId;

  public function __construct( $name, $type, $pk, $label, $ctype )
  {
    $this->name	= $name; 
    $this->pk   	= $pk;
    $this->label 	= $label;
    $this->ctype 	= $ctype;
    $this->isRef	= NULL;

    // Detect whether the type is a reference or not
    if( preg_match('/ref\s+(\w+)\[([^\]]*)\]/', $type, $matches) )
    {
      $refModule 	= $matches[1];
      $refName 	 	= $matches[2];

      $this->ref	= Yii::app()->controller->availableReferences[$refModule][$refName];
      $this->type	= $type; 
      $this->refId	= $this->name . '_id';
    }
    else
    {
      $this->type 	= $type;
    }
  }

  public function isPrimaryKey()
  {
    if($this->pk == 'true')
      return true;
    return false;
  }

  public function __toString()
  {
    if(isset($this->label))
      return $this->label;
    return $this->name;
  }
}
