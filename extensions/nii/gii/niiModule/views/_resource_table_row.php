<tr data-name='<?php echo $name;  ?>' class='resource-table-row'>
  <td><?php echo $name; ?></td>
  <td><?php echo $className; ?></td>
  <td><?php echo $tableName; ?></td>
  <td>
    <div class=button-group'>
      <button class='btn btn-warning btn-mini edit-resource-btn' id='<?php echo $name ?>'>Edit</button>
      <button class='btn btn-danger btn-mini delete-resource-btn' data-name='<?php echo $name ?>'>Delete</button>
    </div>
  </td>

</tr>
