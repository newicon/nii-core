<div class='nii-resource' data-name='<?php echo $resource->name;?>' data-class-name='<?php echo $resource->className;?>' data-table-name='<?php echo $resource->tableName;?>'>
  <?php foreach ( $resource->attributes as $aid => $attribute ): ?>
    <div class='nii-resource-attr' data-name='<?php echo $attribute->name;?>' data-type='<?php echo $attribute->type;?>' data-pk='<?php echo $attribute->pk;?>' data-label='<?php echo $attribute->label;?>' data-ctype='<?php echo $attribute->ctype; ?>'></div>
  <?php endforeach; ?>
</div>
