<div class='modal hide' id='modal_add_resource' style='display: none;' aria-hidden='true'>
  <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
    <h3>Add Resource</h3>
  </div>
  <div class='modal-body'>
    <!-- Basic resource details box -->
    <div class='widget-box'>
      <div class='widget-title'>
        <h5>Resource Details</h5>
      </div>
      <div class='widget-content nopadding gii'>
        <div class='control-group'>
	  <label class='control-label'>Name</label>
	  <div class='controls'>
            <input id='resource_name' type='text'></p>
	  </div>
	</div>
        <div class='control-group'>
	  <label class='control-label'>Table Name</label>
	  <div class='controls'>
            <input id='resource_table_name' type='text'></p>
	  </div>
	</div>
        <div class='control-group'>
	  <label class='control-label'>Class Name</label>
	  <div class='controls'>
            <input id='resource_class_name' type='text'></p>
	  </div>
	</div>
        

      </div>
    </div>

    <div class='widget-box'>
      <div class='widget-title'>
        <h5>Attributes</h5>
      </div>
      <div class='widget-content nopadding gii'>
        <div class='resource-attributes'>
          <table class='table resource-attribute-table'>
            <thead>
	      <tr><th>Name</th><th>Label</th><th>Type</th><th>Prim. Key</th><th>Actions</th></tr>
	    </thead>
	    <tbody>
	      <?php $this->renderPartial('_resource_attribute_row', array('pk' => 'true','name' => 'id','type'=>'pk', 'id' => 'attr0')); ?>
	      <tr><td colspan='3'><button class='btn' id='new_resource_attribute_btn'>Add New Attribute</button></td></tr>
	    </tbody>
          </table>
	</div>
      </div>
    </div>
  </div> <!-- /eof modal-body -->

  <div class='modal-footer'>
    <a href='#' id='add_resource_cancel' class='btn' data-dismiss='modal'>Cancel</a>
    <a href='#' id='add_resource_submit' type='button' class='btn btn-primary'>Add Resource</a>
  </div>

</div>
