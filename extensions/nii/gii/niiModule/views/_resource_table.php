<table class='table table-bordered' id='resource_table'>
  <thead>
    <tr>
      <th>Name</th><th>Class Name</th><th>Table Name</th><th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php if($model->hasResourceData()): ?>
      <?php foreach( $model->resources as $id=>$resource )
      {
        $this->renderPartial('_resource_table_row', array('name' => $resource->name, 'className' => $resource->className, 'tableName' => $resource->tableName));
      }
    ?>
    <?php endif; ?>
  </tbody>
  <div id='resource_data'>
    <?php if($model->hasResourceData()): ?>
      <?php foreach( $model->resources as $id=>$resource )
      {
        $this->renderPartial('_resource_data', array('resource' => $resource, 'id' => $id));
      }
    ?>
  <?php endif; ?>
  </div>
</table>
