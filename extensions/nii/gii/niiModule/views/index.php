<style>
  td {
    text-align: center;
  }

  .modal{
    max-height: 800px;
    width: 1000px;
    margin: 0 0 0 -400px; 
    overflow-y: auto;
  }

  .modal-body{
    max-height: 800px;
  }

  #add_resource_btn{
    padding: 3px 5px 2px;
    float: right;
    margin-top: 5px;
    margin-right: 5px;
  }

  .icon-plus{
    margin-bottom: 2px;
  }
</style>

<div class="row-fluid">
  <?php $form=$this->beginWidget('NActiveForm',array('enableAjaxValidation'=>false,
                                                     'enableClientValidation'=>true)); ?>

  <!-- Module Information Box -->
  <div class="widget-box">
    <div class="widget-title">
      <span class='icon'>
        <i class='icon-home'></i>
      </span>
      <h5>Module Information</h5>
    </div>
    <div class="widget-content nopadding form gii">
        <?php echo $form->field($model,'moduleID','textField'); ?>
        <?php echo $form->field($model,'description','textArea'); ?>
    </div>
  </div>

  <!-- Resources -->
  <div class='widget-box'>
    <div class='widget-title'>
      <span class='icon'>
        <i class='icon-th'></i>
      </span>
      <h5>Resources</h5>
      <span> 
        <a id='add_resource_btn' data-toggle='modal' href='#modal_add_resource' class='btn btn-success btn-mini'><i class='icon-plus icon-white'></i> Add a new Resource</a>
      </span>
    </div>
    <div class='widget-content nopadding form gii'>

      <!-- Adding a new resource button and modal -->
      <?php $this->renderPartial('_new_resource_modal',array('model' => $model)); ?>
      <?php $this->renderPartial('_resource_table',array('model' => $model) ); ?>

    </div> <!-- /eo resources widget-content -->
  </div> <!-- /eo resources widget-box -->

  <!-- Hidden field to set the template to default -->
  <?php echo $form->hiddenField($model,'template',array('value'=>'default')); ?>

  <?php if(!$model->hasErrors()): ?>
    <div class="feedback">
      <?php if($model->status===CCodeModel::STATUS_SUCCESS): ?>
        <div class="success">
          <?php echo $model->successMessage(); ?>
        </div>
      <?php elseif($model->status===CCodeModel::STATUS_ERROR): ?>
        <div class="error">
          <?php echo $model->errorMessage(); ?>
        </div>
      <?php endif; ?>

      <?php if(isset($_POST['generate'])): ?>
        <pre class="results"><?php echo $model->renderResults(); ?></pre>
      <?php elseif(isset($_POST['preview'])): ?>
        <div class='widget-box'>
	  <div class='widget-title'>
	    <span class='icon'>
	      <i class='icon-list-alt'></i>
	    </span>
	  <h5>File Preview</h5>
	  </div>
  	  <div class='widget-content nopadding gii'>
            <?php echo CHtml::hiddenField("answers"); ?>
            <table class="preview table table-bordered">
              <tr>
                <th class="file">Code File</th>
                <th class="confirm">
                  <label for="check-all">Generate</label>
                  <?php
                    $count=0;
                    foreach($model->files as $file)
                    {
                      if($file->operation!==CCodeFile::OP_SKIP)
                        $count++;
                    }
                    if($count>1)
                      echo '<input type="checkbox" name="checkAll" id="check-all" />';
                  ?>
                </th>
              </tr>
              <?php foreach($model->files as $i=>$file): ?>
              <tr class="<?php echo $file->operation; ?>">
                <td class="file">
                  <?php echo CHtml::link(CHtml::encode($file->relativePath), array('code','id'=>$i), array('class'=>'preview-code','rel'=>$file->path)); ?>
                  <?php if($file->operation===CCodeFile::OP_OVERWRITE): ?>
                    (<?php echo CHtml::link('diff', array('diff','id'=>$i), array('class'=>'preview-code','rel'=>$file->path)); ?>)
                  <?php endif; ?>
                </td>
                <td class="confirm">
                  <?php
                  if($file->operation===CCodeFile::OP_SKIP)
                    echo 'unchanged';
                  else
                  {
                    $key=md5($file->path);
                    echo CHtml::label($file->operation, "answers_{$key}")
                      . ' ' . CHtml::checkBox("answers[$key]", $model->confirmed($file));
                  }
                  ?>
               </td>
              </ tr>
              <?php endforeach; ?>
            </table>
	  </div>
	</div>
      <?php endif; ?>
      </div>
    <?php endif; ?>

    <div class="form-actions">
      <?php echo CHtml::submitButton('Preview',array('name'=>'preview','class'=>'btn btn-primary','id'=>'preview_submit')); ?>
      <?php if($model->status===CCodeModel::STATUS_PREVIEW && !$model->hasErrors()): ?>
        <?php echo CHtml::submitButton('Generate',array('name'=>'generate','class'=>'btn btn-primary','id'=>'generate_submit')); ?>
      <?php endif; ?>
    </div>
  <?php $this->endWidget(); ?>
  </div> <!-- /widget-content form -->
  </div> <!-- /widget-box -->
</div> <!-- /row-fluid -->

<!-- Modal (initially hidden) for displaying code previews.
     Content is completed by javascript -->
<div class='modal hide fade code-preview-modal'>
  <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
    <h3></h3>
  </div>
  <div class='modal-body'>
  </div>
</div>
