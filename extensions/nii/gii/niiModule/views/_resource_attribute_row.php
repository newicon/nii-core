<tr class='attribute' data-resource-id='<?php echo $name ?>'> 
  <td><input type='text' class='attribute-name' value='<?php echo isset($name) ? $name : ''; ?>'></td>
  <td><input type='text' class='attribute-label' value='<?php echo isset($label) ? $label: ''; ?>'></td>
  <td>
      <?php $types = array('pk','integer','string','BLOB','datetime','varchar(255)','custom');
      if($type === 'custom')
      {
        echo '<input type="text" data-attr-custom="true" class="attribute-type" value=\''.$ctype.'\'/>';
      }
      else
      {
        echo "<select class='attribute-type'>\n";
        foreach ( $types as $id=>$t)
        {
          $selected = '';
          if( isset($type) )
	  {
	    if( $type == $t)
	      $selected = 'selected';
	  }
	  else if($id == 1)
	  {
	    $selected = 'selected';
          }
          echo "<option value='$t' $selected>$t</option>\n"; 
        }

	foreach ( $this->availableReferences as $module => $references )
	{
	  foreach( $references as $refName => $ref )
	  {
	    echo "<option value='ref $module"."[$refName]"."'>$refName</option>"; 
	  }
	}
	echo "</select>";
      }
      ?>
    </select>
  </td>
  <td><input class='attribute-pk' type='checkbox' <?php echo (isset($pk) && $pk === 'true' ? 'checked' : '');?>></td>
  <td><button data-resource-id='<?php echo $name ?>' class='btn btn-danger btn-mini delete-attribute-btn'>Delete</button></td>
</tr>
