<?php

class NiiModuleCode extends CCodeModel
{
  public $moduleID;
  public $description;
  public $testID;
  public $resourceData;
  public $resources;

  public function rules()
  {
    return array_merge(parent::rules(), array(
      array('moduleID', 'filter', 'filter'=>'trim'),
      array('moduleID', 'required'),
      array('moduleID', 'match', 'pattern'=>'/^\w+$/', 'message'=>'{attribute} should only contain word characters.'),
      array('moduleID', 'match', 'pattern'=>'/^[a-z0-9]+$/', 'message'=>'{attribute} should only contain lowercase characters.'),
      array('description','required'),
      array('resourceNames', 'safe'),
      array('resourceData','safe'),
    ));

  }

  public function init()
  {
    $this->resourceData  = array();
    $this->resources     = array();
  }

  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(), array(
      'moduleID' =>'Module ID',
      'resourceNames[]' => 'Resource Name'
    ));
  }

  public function successMessage()
  {
    if(Yii::app()->hasModule($this->moduleID))
      return 'The module has been generated successfully. You may '.CHtml::link('try it now', Yii::app()->createUrl($this->moduleID), array('target'=>'_blank')).'.';

    $output=<<<EOD
<p>The module has been generated successfully.</p>
<p>To access the module, you need to modify the application configuration as follows:</p>
EOD;
    $code=<<<EOD
<?php
return array(
    'modules'=>array(
        '{$this->moduleID}',
    ),
    ......
);
EOD;

    return $output.highlight_string($code,true);
  }

  public function prepare()
  {
    $this->files=array();
    $templatePath=$this->templatePath;
    $modulePath=$this->modulePath;
    $moduleTemplateFile=$templatePath.DIRECTORY_SEPARATOR.'module.php';

    // Before we render anything, create all our resources and properties
    if( $this->hasResourceData() )
    {
      foreach ( $this->resourceData as $r )
      {
        $className = $r['className'];
	$tableName = $r['tableName'];
        $name      = $r['name'];

        $resource = new NiiModuleResource( $name, $tableName, $className, $r['attributes'], $this, $modulePath, $templatePath.DIRECTORY_SEPARATOR.'resource' );
	$resource->prepare();
	$this->resources[] = $resource;
      }
    }

    $this->files[]=new CCodeFile(
      $modulePath.'/'.$this->moduleClass.'.php',
      $this->render($moduleTemplateFile)
    );

    $files=CFileHelper::findFiles($templatePath,array(
      'exclude'=>array(
        '.svn',
        '.gitignore',
	'resource'
      ),
    ));

    // Append any resource files to our files list
    foreach ( $this->resources as $resource )
    {
      $this->files = array_merge( $this->files, $resource->files );
    }

    foreach($files as $file)
    {
      if($file!==$moduleTemplateFile)
      {
        if(CFileHelper::getExtension($file)==='php')
          $content=$this->render($file);
        else if(basename($file)==='.yii')  // an empty directory
        {
          $file=dirname($file);
          $content=null;
        }
        else
          $content=file_get_contents($file);
        $this->files[]=new CCodeFile(
          $modulePath.substr($file,strlen($templatePath)),
          $content
        );
      }
    }
    
  }

  public function hasResourceData()
  {
    if( count($this->resourceData) )
    {
      return true;
    }
    return false;
  }

  public function getModuleClass()
  {
    return ucfirst($this->moduleID).'Module';
  }

  public function getModulePath()
  {
    return Yii::getPathOfAlias("localmodules").DIRECTORY_SEPARATOR.$this->moduleID;
  }
}
