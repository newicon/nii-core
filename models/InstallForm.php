<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Base class for install forms of Install
 *
 * @author steve
 */
class InstallForm extends CFormModel
{

	public function getConfigFile(){
		return Yii::getPathOfAlias('base.config.local') . '.php';
	}
	
	/**
	 * Check if the database connection details are already installed
	 * NOTE: The database details are added to Yii component db setting before the config file is created.
	 * NOTE: The database might be installed but the config file might not exist yet
	 * @param Exception $e the exception is stroed in this variable byRef if one is thrown
	 * @return boolean
	 */
	public function isDbInstalled(&$e = null){
		return Yii::app()->isDbInstalled($e);
	}
	
	/**
	 * Checks if there is a user table and if
	 * there is an admin user. If the table is not empty then it assumes 
	 * that there IS an administrator account
	 * The Nii system currently checks this on every page request.
	 * After completing the install Nii could create a lock file preventing this check in the future
	 * this would also secure the system from another install process running.
	 * @return boolean
	 */
	public function isUserInstalled(){
		$userTableExists = false;
                $userTable = Yii::app()->getModule('user')->tableUsers;
		switch (Yii::app()->db->driverName) {
			case 'dblib':
                        case 'sqlsrv':
				//TODO add more efficient db call to check if user_user table exists
				$userTableExists = Yii::app()->db->schema->getTable($userTable);
				break;
			default:
				$userTableExists = Yii::app()->db->createCommand('show tables like "' . $userTable . '"')->queryRow();
		}
		
		if ($userTableExists)
            return Yii::app()->db->createCommand()
				->select('id')
				->from(User::model()->tableName())
				->limit(1)
				->queryRow();
        else
            return false;
	}
	
	/**
	 * Checks if the local config file with database details has been saved
	 * @return boolean
	 */
	public function configSaved(){
		return file_exists($this->getConfigFile());
	}
	
}

